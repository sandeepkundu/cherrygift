<?php

namespace app;

class Deploy
{

    /**
    * A callback function to call after the deploy has finished.
    *
    * @var callback
    */
    public $postDeploy;

    /**
    * The name of the file that will be used for logging deployments. Set to
    * FALSE to disable logging.
    *
    * @var string
    */
    private $log = null;

    /**
    * The timestamp format used for logging.
    *
    * @link    http://www.php.net/manual/en/function.date.php
    * @var     string
    */
    private $dateFormat = 'Y-m-d H:i:sP';

    /**
    * The name of the branch to pull from.
    *
    * @var string
    */
    private $branch = 'master';

    /**
    * The name of the remote to pull from.
    *
    * @var string
    */
    private $remote = 'origin';

    /**
    *
    *
    * @var string
    */
    private $gitDir;

    /**
    * The directory where your website and git repository are located, can be
    * a relative or absolute path
    *
    * @var string
    */
    private $rootDir;

    /**
    * Sets up defaults.
    *
    * @param  string  $root   Account root directory
    * @param  string  $git    Git directory
    * @param  array   $data   Information about the deployment
    */
    public function __construct($root, $git, $options = array())
    {
        $this->rootDir    = realpath($root) . DIRECTORY_SEPARATOR;
        $this->gitDir     = realpath($git) . DIRECTORY_SEPARATOR;
        $availableOptions = array('log', 'dateFormat', 'branch', 'remote');

        foreach ($options as $option => $value) {
            if (in_array($option, $availableOptions)) {
                $this->{$option} = $value;
            }
        }

        $this->log("\n\nAttempting deployment...");
    }

    /**
    * Writes a message to the log file.
    *
    * @param  string  $message  The message to write
    * @param  string  $type     The type of log message (e.g. INFO, DEBUG,
    *                           ERROR, etc.)
    */
    public function log($message, $type = 'INFO')
    {
        if ($this->log) {
            // Set the name of the log file
            $filename = $this->log;

            if (!file_exists($filename)) {
                // Create the log file
                file_put_contents($filename, '');

                // Allow anyone to write to log files
                chmod($filename, 0666);
            }

            // Write the message into the log file
            // Format: time --- type: message
            $date = date($this->dateFormat);
            $log  = "{$date} --- {$type}: {$message}" . PHP_EOL;

            file_put_contents($filename, $log, FILE_APPEND);
        }
    }

    /**
    * Executes the necessary commands to deploy the website.
    */
    public function execute()
    {
        try {
            $this->log("\n\nBeginning deployment execution...");

            // Get changes data
            $changes = $this->getChanges();

            // Check if an update is required.
            if (!$this->updateRequired($changes)) {
                // Exit deployment process if we do not need to update.
                $this->log("\nNo updates to master branch");

                return;
            }

            // Make sure we're in the right directory
            // exec('cd '.$this->gitDir, $output);
            chdir($this->gitDir);
            $this->log('Changing working directory... ');

            // Discard any changes to tracked files since our last deploy
            exec('git reset --hard HEAD', $output);
            $this->log('Reseting repository... ' . implode(' ', $output));

            // Store start commit hash
            $hashes = [];
            exec('git rev-parse --short HEAD', $hashes);
            $this->log("Starting hash: {$hashes[0]}...");

            // Update the local repository
            exec('git pull ' . $this->remote . ' ' . $this->branch, $output);
            $this->log('Pulling in changes...' . implode(' ', $output));

            // Get new HEAD commit hash
            exec('git rev-parse --short HEAD', $hashes);
            $this->log("Current hash: {$hashes[1]}...");

            // Determine file changes.
            $fileChanges = [];
            $changesCmd = "git diff --name-status {$hashes[0]} {$hashes[1]}";
            exec($changesCmd, $fileChanges);
            $this->log(count($fileChanges) . ' changes found...');

            // Process file changes.
            $this->log('Begin processing individual changes.');

            $cpTpl = "cp -af {$this->gitDir}%s {$this->rootDir}%s";
            $rmTpl = "rm -rf {$this->rootDir}%s";

            $errors = [];

            foreach ($fileChanges as $change) {
                $this->log('Processing ' . $change);
                $change = trim($change);
                $parts = preg_split('/\s+/', $change);

                switch ($parts[0]) {
                    case 'M':
                    case 'C':
                    case 'R':
                    case 'A':
                        $dirName = dirname($this->rootDir . $parts[1]);

                        if (!file_exists($dirName)) {
                            mkdir($dirName, 0777, true);
                        }

                        $cmd = sprintf($cpTpl, $parts[1], $parts[1]);
                        $this->log("Running: {$cmd}");
                        exec($cmd, $errors);
                        break;

                    case 'D':
                        $cmd = sprintf($rmTpl, $parts[1]);
                        $this->log("Running: {$cmd}");
                        exec($cmd, $errors);
                        break;

                    default:
                        $this->log('Unknown file status: ' . $parts[0]);
                        break;
                }
            }

            if (!empty($errors)) {
                $numErrors = count($errors);
                $this->log("There were {$numErrors} while processing changes.");
            }

            $this->log('File change processing complete...');

            // Secure the .git directory
            exec('chmod -R og-rx .git');
            $this->log('Securing .git directory... ');

            if (is_callable($this->postDeploy)) {
                call_user_func($this->postDeploy, $this->data);
            }

            $this->log('Deployment successful.');
        } catch (Exception $e) {
            $this->log($e, 'ERROR');
        }

    }

    /**
     * Determines whether there have been changes to the master branch and
     * whether an update is required.
     *
     * @return boolean
     */
    public function updateRequired($changes)
    {
        // Check we have changes and that the changes are stored in an array.
        if (empty($changes) || !is_array($changes)) {
            return false;
        }

        // Check each change object
        foreach ($changes as $change) {
            if (is_object($change) && isset($change->new)) {
                $new = $change->new;

                if (isset($new->type) && isset($new->name)) {
                    if ($new->type == 'branch' && $new->name == $this->branch) {
                        $this->log("Found change for {$this->branch} branch.");
                        return true;
                    }
                }
            }
        }

        $this->log("No changes for {$this->branch} branch found.");

        return false;
    }

    /**
     * Decodes the posted payload data and extracts the changes object from
     * this if available.
     *
     * @return array
     */
    public function getChanges()
    {
        $payload = json_decode(file_get_contents('php://input'));

        if (!$payload) {
            $this->log("\nMissing Payload Data!");
            return null;
        }

        if (!$payload->push) {
            $this->log("\nPayload missing push data!");
            return null;
        }

        if (!$payload->push->changes) {
            $this->log("\nPayload missing changes data!");
            return null;
        }

        return $payload->push->changes;
    }

    /**
     * Checks if the supplied IP is in the supplied range/s.
     *
     * @param string $ip
     * @param array  $ranges
     *
     * @return boolean
     */
    public static function ipInRanges($ip, $ranges)
    {
        foreach ($ranges as $range) {
            if (strpos($range, '/') === false) {
                $range .= '/32';
            }

            list($range, $netMask) = explode('/', $range, 2);
            $rangeDec    = ip2long($range);
            $ipDec       = ip2long($ip);
            $wildcardDec = pow(2, (32 - $netMask)) - 1;
            $netMaskDec  = ~ $wildcardDec;

            $result = (($ipDec & $netMaskDec) == ($rangeDec & $netMaskDec));

            if ($result) {
                return true;
            }
        }

        return false;
    }
}
