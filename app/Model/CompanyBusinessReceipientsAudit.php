<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyBusinessReceipientsAudit extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'company_business_recipient_audit';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['company_business_profile_id','recipient_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at'];
        public $timestamps = false;
        public function auditDetails($id)
        {
           return self::where('company_business_profile_id', '=', $id)->get();
        }

}
