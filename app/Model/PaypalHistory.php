<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaypalHistory extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'paypal_histories';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','item_name','custom','protection_eligibility', 'txn_id', 'payer_email','payment_status','txn_type','mc_gross','mc_currency','tax','payment_type','payer_id','payment_date','address'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at', 'updated_at'];

	public function saveFailTransaction($data,$id=null) {
        
        try {
            $history = new PaypalHistory();
            
            if ($id != null) {
                $history = self::find($id);
                if (!empty($history))
                    $history->id = $id;
            }
            
            if(isset($data['user_id']) && !empty($data['user_id'])) {
                $history->user_id = $data['user_id'];
            }

        	$history->item_name = $data['item_name'];
			$history->custom = $data['custom'];
            $history->protection_eligibility = $data["protection_eligibility"];
            $history->txn_id = $data['txn_id'];
            $history->payer_email = urldecode($data['payer_email']);
            $history->payment_status = $data['payment_status'];
            $history->txn_type = $data['txn_type'];
            $history->mc_gross = $data["mc_gross"];
            $history->mc_currency = $data["mc_currency"];
            $history->tax = $data['tax'];
            $history->payment_type = $data['payment_type'];
            $history->payer_id = $data["payer_id"];
            $history->payment_date = urldecode(date('Y-m-d H:i:s',strtotime($data["payment_date"])));
            $history->address = $data["address_street"]." ".$data["address_city"]." ".$data["address_zip"]." ".$data["address_country"];
            $history->paypal_object=json_encode($data);
            $history->paypal_status=1;
            $history->save();
            	
            return true;
            
        } catch (Services_Twilio_RestException $e) {
            echo $e->getmessage();
            die;
            throw new CustomException($e->getmessage());
        }
    }
    
    public function updatePaypalHistory($data,$id=null) {
        
        try {
            $history = new PaypalHistory();
            
            if ($id != null) {
                $history = self::find($id);
                if (!empty($history))
                    $history->id = $id;
            }
            
            if(isset($data['user_id']) && !empty($data['user_id'])) {
                $history->user_id = $data['user_id'];
            }

            $history->item_name = $data['item_name'];
            $history->custom = $data['custom'];
            $history->protection_eligibility = $data["protection_eligibility"];
            $history->txn_id = $data['txn_id'];
            $history->payer_email = urldecode($data['payer_email']);
            $history->payment_status = $data['payment_status'];
            $history->txn_type = $data['txn_type'];
            $history->mc_gross = $data["mc_gross"];
            $history->mc_currency = $data["mc_currency"];
            $history->tax = $data['tax'];
            $history->payment_type = $data['payment_type'];
            $history->payer_id = $data["payer_id"];
            $history->payment_date = urldecode(date('Y-m-d H:i:s',strtotime($data["payment_date"])));
            $history->address = $data["address_street"]." ".$data["address_city"]." ".$data["address_zip"]." ".$data["address_country"];
            $history->paypal_object=json_encode($data);
            $history->paypal_status=1;
            $history->save();
                
            return true;
            
        } catch (Services_Twilio_RestException $e) {
            echo $e->getmessage();
            die;
            throw new CustomException($e->getmessage());
        }
    }


    
    
    public function getpaypalList(){
         return self::select('paypal_histories.user_id', 'paypal_histories.item_name', 'paypal_histories.custom_message', 'paypal_histories.txn_id', 'paypal_histories.payer_email','paypal_histories.payment_status','paypal_histories.payer_id','paypal_histories.payment_date','payments.paypal_history_id','payments.status','paypal_histories.paypal_object')->
                        join('payments', 'paypal_histories.id', '=', 'payments.paypal_history_id')
                        
                        ->orderby('paypal_histories.id', 'desc')
                        ->get();

    }
}
