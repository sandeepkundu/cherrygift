<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
class AdminVendorPayment extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'admin_vendor_payment';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
        
        public function getVendorList($month){
            
            
            return DB::select("select cbp.id,cbp.company_id,vp.company_business_profile_id,vp.month,vp.payment_status,company.company_name,cbp.short_desc from company_business_profile as cbp left join admin_vendor_payment as vp on vp.company_business_profile_id = cbp.id and vp.month=".$month." join company on company.id = cbp.company_id");
            
        }
        
        public function updateVendorPayment($cbp_id,$month,$year){
            $data = self::where("company_business_profile_id",$cbp_id)->where("month",$month)->count();
            if(empty($data))
            {
                $this->company_business_profile_id = $cbp_id;
                $this->month = $month;
                $this->year = $year;
                $this->payment_status = 1;
                $this->save();
            }else{

            	self::where('company_business_profile_id', $cbp_id)->where("month",$month)->update(['payment_status' => 1]);
            }
            return true;
        }

        public function updateVendorMail($cbp_id,$month,$year){
            $data = self::where("company_business_profile_id",$cbp_id)->where("month",$month)->count();
            
            if(empty($data))
            {
                $this->company_business_profile_id = $cbp_id;
                $this->month = $month;
                $this->year = $year;
                $this->mail_sent = 1;
                $this->save();
            }else{

            	self::where('company_business_profile_id', $cbp_id)->where("month",$month)->update(['mail_sent' => 1]);
            }
            return true;
        }

}
