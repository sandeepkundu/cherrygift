<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
class HomepageRecipientImages extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'homepage_recipient_images';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
       public function saveImages($image,$type)
        {
           $this->deleteImages($type);
            $this->image = $image;
            $this->type = $type;
            $this->save();
        }
        
        public function getImages(){
            return self::all();
        }
        
        public function deleteImages($type) {
            $img = self::where("type",$type);
            $img->delete();
        }
}
