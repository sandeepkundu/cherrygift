<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use View;
use mPDF;
use Mail;

class Payments extends Model
{

    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'payments';
    protected $primaryKey = 'id';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['item_name','item_number','payment_status','payment_method','payment_amount','payment_currency','txn_id','txn_type','receiver_email','payer_email','custom','first_name','last_name','company_business_id','subscr_id','payer_id'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = [];

    public function save_vendor_payment($data)
    {

        try {
            $this->user_type = $data["user_type"];
            $this->user_id = $data["user_id"];
            $this->transaction_type = $data["transaction_type"];
            $this->susbscriber_id = $data["susbscriber_id"];
            $this->transaction_id = $data["transaction_id"];
            
            $this->paypal_history_id = $data["paypal_history_id"]?$data["paypal_history_id"]:'';
            $this->status = $data["status"];
            $this->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function get_vendor_payment_profile($id,$subs_id,$txn_id) {
        return self::where("user_type",2)->where("user_id",$id)->where("susbscriber_id",$subs_id)->where("transaction_id",$txn_id)->get();
    }

    public function update_vendor_payment_status($id,$status) {
        $this->id = $id;
        $this->status = $status;
        $this->save();
    }

    public function getPaymentProfile($idsArr) {
        return self::where("status",1)->where("user_type",2)->whereIn('user_id',$idsArr)->get();
    }

    public function getPaymentProfileBySubsId($paypal_subs_id){
        return self::where("status",1)->where("user_type",2)->where("transaction_type",1)->where("susbscriber_id",$paypal_subs_id)->first();
    }

    public function updateSubscriptionStatus($id,$status) {
        self::where("id",$id)->update(array("status"=>$status));
    }

    /**
    * Email an invoice generated based on the payment record
    * @returns string The email recipient
    */
    public function sendInvoiceMail() {

        $profile = CompanyBusinessProfile::where('id', $this->user_id)->first();
        $company = Company::where('id', $profile->company_id)->first();

        $suburbObj = Suburbs::where('id', $company->suburs_id)->first();
        $suburb = $suburbObj ? $suburbObj->locality : '';

        $stateObj = States::where('id', $company->state_id)->first();
        $state = $stateObj ? $stateObj->state_name : '';

        $html = (string) View::make(
            'subscription-invoice',[
                'payment' => $this,
                'company' => $company,
                'profile' => $profile,
                'suburb'   => $suburb,
                'state'    => $state  
            ] 
        );
        $mpdf = new mPDF("en", "A4", "10"," ",0,0,0,0,0,0);
        $mpdf->WriteHTML($html);
        $name = $this->invoiceNo() . "_invoice.pdf";
        $pathToFile = public_path() . "/" . $name;
        $mpdf->Output($pathToFile, 'F');
        $email_content = "Dear " . $company->first_name . ",<br/><br/><br/>"
        . "Thank you for subscribing with cherrygift."
        . "<br/><br/>"
        . "Please find attached your invoice for your subscription."
        . "<br/><br/>"
        . "Don't forget to follow us on Facebook, Twitter, Instagram and YouTube!"
        . "<br/>"
        . "<br/>"
        . "<br/>"
        . "Cheers,"
        . "<br/>From the cherrygift team!";

        if (env('APP_DEV',false)) {
            $recipient = env('contact_email');
        }
        else {
            $recipient = $company->contact_email;
        }
        $subject = 'cherrygift Subscription Invoice #'.$this->invoiceNo();
        Mail::send(
            'emails.email', 
            array("data"=>$email_content), 
            function($message) use($subject,$recipient,$pathToFile){
                $message->subject($subject);
                $message->to($recipient);
                if (!env('APP_DEV',false)) {
                    $message->bcc(env('invoice_email'));
                }
                $message->attach($pathToFile);
        });
        unlink($pathToFile);
        return $recipient;
    }  

    public function invoiceNo()
    {
        if ($this->transaction_id == 'INVOICE') {
            return 'S-'.sprintf('%05d',$this->id);
        }
        else {
            return $this->transaction_id;
        }
    }      
}
