<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class UnregisterGiftVoucher extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'unregister_gift_voucher';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['mobile_no', 'order_voucher_id', 'voucher_send_by'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created'];
        
        
        // public function getUsers(){
        //     return self::select('first_name','last_name', 'email')->where("role","User")->orderBy('id', 'asc');
        // }
        
        // public function getUserById($id){
        //     return self::find($id);
        // }
        
        
       
}
