<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\UserCart;
use App\Model\CartVouchers;
use App\Model\OrderVoucher;
use App\Model\ResendSms;
use App\Model\UserGiftedVoucher;
use Services_Twilio;
use Services_Twilio_RestException;
use Crypt;
use Twilio\Rest\Client;
//use PDF;
use View;
use mPDF;
use Mail;

class Order extends Model {

    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'user_order';
    protected $primaryKey = 'id';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['cart_id', 'txn_id', 'payer_email', 'user_id'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['created_at', 'updated_at'];

    public function saveOrder($data) {
        
        // final check on our order we aren't about to double up on...
        if (!$this->id) {
            if ($this->checkOrderStatus($data['txn_id'])) {
                // this transaction is already there but we are going to do 
                // another insert...
                return;
            }
        }
        try {
            $cart_voucher = new CartVouchers();
            $cart = new UserCart();
            $order_code = $data["custom"];
            
            $this->txn_id = $data['txn_id'];
            
            if (!empty($data['payer_email'])) {
                $this->payer_email = $data['payer_email'];
            }

            $this->total_amount = $data["mc_gross"];
            $order = $this->save();

            if ($order) {
                $order_id = $this->id;
                
                $cart_vouchers_obj = new CartVouchers();
                $cart_vouchers = $cart_vouchers_obj->getVoucherInfoByCode($order_code);
                if($cart_vouchers==""){
                    return array("order_id" =>$order_id, "sms_error" =>'Cart voucher is null'); 
                }
                $orderVoucherObj = new OrderVoucher();
                $sms_arr = array();
                $invoice_data = array('order_id' => $order_id);
                $user_giffted_voucher = array();
                $cart_id=0;
                foreach ($cart_vouchers as $cv) {
                    $cart_id = $cv->cart_id;
                    // echo $cv->sms_schedule; exit;
                    $order_vouchers = array('user_order_id' => $order_id, "company_business_profile_id" => $cv->company_business_profile_id, "voucher_unique_id" => $orderVoucherObj->getUniqueRandomNumber(), "voucher_price" => $cv->price, "phone_number" => $cv->phone_number, "sms_date" => $cv->sms_date, "sms_hour" => $cv->sms_hour, "sms_minutes" => $cv->sms_minutes,"utc_sms_date" => $cv->utc_sms_date, "utc_sms_hour" => $cv->utc_sms_hour, "utc_sms_minutes" => $cv->utc_sms_minutes,"sms_schedule"=> $cv->sms_schedule);

                    $res_order_voucher = $orderVoucherObj->saveOrderVoucher($order_vouchers);

                    if(!$res_order_voucher){
                        return array("order_id" =>$order_id, "sms_error" =>'error in order voucher'); 
                    }

                    $order_vouchers['id'] = $res_order_voucher->id;
                    $invoice_data['voucher_list'][$cv->company_business_profile_id][] = array(
                        'price'=>$cv->price,
                        'mobile'=>$cv->phone_number,
                        'order_voucher_id' => $order_vouchers['id'],
                        'voucher_unique_id' => $order_vouchers['voucher_unique_id']
                    );
                    
                    $user_giffted_voucher[str_replace(' ', '', $cv->phone_number)][] = $res_order_voucher->id;
                    $readem_url = url() . "/redeem/" . Crypt::encrypt($res_order_voucher->id);
                    $order_vouchers['sms_message'] = $cv->sms_message . " " . $this->shortUrl($readem_url);
                    $smsRec = $orderVoucherObj->saveSmsText($order_vouchers['sms_message'], $res_order_voucher->id);
                     
                     if(!$smsRec){
                        return array("order_id" =>$order_id, "sms_error" =>'error in saving sms'); 
                    }                       

                    if (empty($cv->sms_schedule)) {
                        $sms_arr[] = $order_vouchers;
                    }
                }

                $cart_info = $cart->getCartInfo($cart_id);

                if(!$cart_info){
                    return array("order_id" =>$order_id, "sms_error" =>'error in user cart');
                }

                if(!empty($cart_info)){
                $this->cart_id = $cart_id;
                $this->user_id = $cart_info[0]["user_id"];
                $this->save();
                }

                if(!empty($cart_info[0]["id"]))
                    $giftRec = $this->userGiftedVoucher($user_giffted_voucher,$cart_info[0]["user_id"]);
                
                if(!$giftRec){
                    return array("order_id" =>$order_id, "sms_error" =>'error in user Gifted Voucher');
                }
                
               // $emaiRec = $this->sendInvoiceMail($invoice_data);
                
                // if(!$emaiRec){
                //     return array("order_id" =>$order_id, "sms_error" =>'error in sending mail');
                // }

                $sms_status = array();
                if (!empty($sms_arr)) {
                    //$sms_status[] = $this->sendBulkTwillioSms($sms_arr);
                }
                
                if(isset($cart_info[0]) && !empty($cart_info[0]["id"])) {
                    $cart = new UserCart();    
                   $cartempt = $cart->clearCart($cart_info[0]["id"]);
                }

               if (!$cartempt) {
                    return array("order_id" =>$order_id, "sms_error" =>'error in empty cart');
                }

               return array("order_id" => $order_id, "sms_error" => $sms_status);
            } else {
               return array("order_id" =>null, "sms_error" =>'transaction pending'); 
            }
        } catch (Services_Twilio_RestException $e) {
            echo $e->getmessage();
            die;
            throw new CustomException($e->getmessage());
        }
    }

    public function sendTwillioSms123($number, $text, $id) {
        $orderVoucher = new OrderVoucher();
        try {

            $result = Twilio::message('+91' . $number, $text);
        } catch (Services_Twilio_RestException $e) {
            echo $e->getmessage();
            die;
            $orderVoucher->updateSmsStatus($id, 3);
        }
    }

     public function shortUrl($url) {
        try {

            $urldata = json_decode(file_get_contents("http://api.bit.ly/v3/shorten?login=sandeepappster&apiKey=R_90a1aa5791ae4585a69a843b14b37518&longUrl=" . urlencode($url) . "&format=json"));
            //return "http://google.com";
            $test='localhost:8000//bit.ly/2g1LC6u';
            return $test;
            //return $urldata->data->url;
        } catch (Exception $e) {
            $orderVoucher->updateSmsStatus($id, 3);
        }
    }

    public function getTwillioObj() {
        $sid = env('TWILIO_SID');
        // Your Account SID from www.twilio.com/user/account
        $token = env('TWILIO_TOKEN');
        // Your Auth Token from www.twilio.com/user/account
        require base_path() . '/vendor/twilio/sdk/Services/Twilio.php';
        $client = new Services_Twilio($sid, $token);
        return $client;
    }

    public function sendBulkTwillioSms($data_arr) {
        $orderVoucher = new OrderVoucher();
        $client = $this->getTwillioObj();
        $error = array();
        foreach ($data_arr as $d) {
            try {
                if (env('APP_ENV') == 'local') {
                    $sms = $client->account->messages->sendMessage(
                        env('TWILIO_NUMBER'), 
                        "+61" . "999999999", 
                        "Testing Purpose"
                        );                                       
                } else {
                    $sms = $client->account->messages->sendMessage(
                    env('TWILIO_NUMBER'), env('MOBILE_PREFIX') . $d['phone_number'], $d['sms_message']
                    );

                }

                $orderVoucher->updateSmsStatus($d['id'], 1, $sms->sid);
            } catch (Services_Twilio_RestException $e) {
                $error[] = $e->getMessage();
                $orderVoucher->updateSmsStatus($d['id'], 2, '', $e->getMessage());
            }
        }
        return $error;
    }

    public function sendBulkReminderSms($data_arr, $msg) {
        $orderVoucher = new OrderVoucher();
        $client = $this->getTwillioObj();
        $error = array();
        foreach ($data_arr as $d) {
            try {
                if (env('APP_ENV') == 'local') {
                    $sms = $client->account->messages->sendMessage(
                        env('TWILIO_NUMBER'), 
                        "+61" . "999999999", 
                        "Testing Purpose"
                        );
                                       
                } else {
                $sms = $client->account->messages->sendMessage(
                    env('TWILIO_NUMBER'), env('MOBILE_PREFIX') . $d['phone_number'], $msg
                );
                }
            } catch (Services_Twilio_RestException $e) {
                $error[] = $e->getMessage();
            }
        }
        return $error;
    }

    public function resendBulkSms($data_arr) {
        $resendSms = new ResendSms();
        $client = $this->getTwillioObj();
        $error = array();
        foreach ($data_arr as $d) {
            
            try {
                if (env('APP_ENV') == 'local') {
                    $sms = $client->account->messages->sendMessage(
                        env('TWILIO_NUMBER'), 
                        "+61" . "476857041", 
                        "Testing Purpose"
                        ); 
                    $resendSms->addNewResendSms($d['id'], 1, $sms->sid); 
                   
                } else {
                    // dd('die');
                    $sms = $client->account->messages->sendMessage(
                    env('TWILIO_NUMBER'), env('MOBILE_PREFIX') . $d['phone_number'], $d['sms_message']
                    //env('TWILIO_NUMBER'), "+61" . "476857041", "Testing Purpose"
                    ); 
                    $resendSms->addNewResendSms($d['id'], 1, $sms->sid); 
                }             

                
            } catch (Services_Twilio_RestException $e) {
                
                $error[] = $e->getMessage();
                $resendSms->addNewResendSms($d['id'], 2, '', $e->getMessage());
            }
        }
        return $error;
    }

        public function canceledSms($data_arr) {
        $resendSms = new ResendSms();
        $client = $this->getTwillioObj();
        $error = array();
        foreach ($data_arr as $d) {
            
            try {
                                  
                    $sms = $client->account->messages->sendMessage(
                    env('TWILIO_NUMBER'), env('MOBILE_PREFIX') . $d['phone_number'], 'Cancelled Voucher'
                    //env('TWILIO_NUMBER'), "+61" . "476857041", "Testing Purpose"
                    ); 
                     
                            

                
            } catch (Services_Twilio_RestException $e) {
                
                $error[] = $e->getMessage();
            }
        }
        return $error;
    }


    public function UserLastOrder($user_id) {

        if (!empty($user_id)) {
            $result = self::where('user_id', $user_id)->orderBy('id', 'desc')->first();
            return $result->id;
        }
    }

    public function getUserOrderById($id) {
        return self::find($id);
    }
    public function getOrderInfo($order_id) {
        return self::select("users.first_name", "users.last_name", "users.email")->join("users", "users.id", "=", "user_order.user_id")->where("user_order.id", $order_id)->get()->toArray();
    }

    public function sendInvoiceMail($data) {
        //dd($data);
        @extract($data);
        $voucher_ids = array_keys($voucher_list);
        //print_r($voucher_list);die;
        $cbp = new CompanyBusinessProfile();

        $vouchers = $cbp->getMultipleRecords($voucher_ids);
        $order_user_info = $this->getOrderInfo($order_id);

        $result = array();
        foreach ($vouchers as $v) {
            if(!empty($voucher_list[$v->id])) {
                foreach($voucher_list[$v->id] as $voucher_price) 
                    $result[] = array("location" => $v->store_location, "price" => $voucher_price['price'],
                    "mobile" => $voucher_price['mobile'],
                    'voucher_unique_id' => $voucher_price['voucher_unique_id']);
            }
        }

        $html = (string) View::make('invoice', compact("result", "order_user_info", "order_id"));
        //print_r($html);die;
        $mpdf = new mPDF("en", "A4", "10"," ",0,0,0,0,0,0);
        $mpdf->WriteHTML($html);
        $name = $order_id . "invoice.pdf";
        $pathToFile = public_path() . "/" . $name;
        $mpdf->Output($pathToFile, 'F');
        //die;
        $email_content = "Dear " . $order_user_info[0]['first_name'] . "<br/><br/><br/>"
        . "Thank you for your purchase with cherrygift."
        . "<br/><br/>"
        . "Please find attached your invoice for your voucher/s."
        . "<br/><br/>"
        . "Don't forget to follow us on Facebook, Twitter, Instagram and YouTube!"
        . "<br/>"
        . "<br/>"
        . "<br/>"
        . "Cheers,"
        . "<br/>From the cherrygift team!";

        Mail::send('emails.email', array("data"=>$email_content), function($message) use($order_user_info,$pathToFile){
            //$message->from('us@example.com', 'Laravel');
            $message->subject('cherrygift Order Invoice');
            // if (env('APP_DEV',false)) {
            //     $message->to(env('contact_email'));
            // }
            // else {
            //     $message->to($order_user_info[0]["email"]);
            // }
            $message->to('mohd.nadeem@webnyxa.com');

            $message->attach($pathToFile);
        });
        unlink($pathToFile);
        return true;
    }

    public function UserGiftedVoucheroucher($data,$user_id)
    {
        //dd($data);
        $mobile_arr = array_keys($data);
        $mobile_no=array();
        $user_model = new Users();
         foreach ($mobile_arr as $key => $mob) {
        //$user_ids = $user_model->getUsersByMobile(implode(",",$mobile_arr),$user_id);
        $user_ids = $user_model->getUsersByMobile($mob,$user_id);

        $res = array();
        $rests= array();
        if($user_ids){
            foreach($user_ids as $uid) {
                if(!empty($data[$uid['mobile_number']]))
                {
                    $vouchers_ids = $data[$uid['mobile_number']];
                    //dd($vouchers_ids);
                    foreach($vouchers_ids as $vids)
                    {
                        $res[] = array("user_id"=>$uid['id'],"order_voucher_id"=>$vids,"voucher_send_by"=>$user_id);
                    }
                }
            }
            UserGiftedVoucher::insert($res);
        } else {//else part coded by sandeep for unregister voucher date-13-10-2016 also create model(UNregisterGiftVoucher) and table in database(unregister_gift_voucher)
            $mobile_no[]=$mob;
            foreach ($data as $key => $value) {
                    
                if(in_array($key, $mobile_no, TRUE)) {   
                    
                    $rests = array("mobile_no"=>$key,"order_voucher_id"=>$value[0],"voucher_send_by"=>$user_id);
                        
                    UnregisterGiftVoucher::insert($rests);
                }

            }
        }

        }
        return true;
    }

    public function checkOrderStatus($tx_id) {
        return self::where('txn_id', $tx_id)->count();
    }
}
