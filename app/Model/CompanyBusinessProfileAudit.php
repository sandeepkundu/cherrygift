<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\CompanyBusinessProfile;
use App\Model\CompanyBusinessImages;
use App\Model\CompanyBusinessOccations;
use App\Model\CompanyTimings;
use App\Model\Company;
use DB;
use App\Model\CompanyBusinessReceipients;

class CompanyBusinessProfileAudit extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_business_profile_audit';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['short_desc', 'long_desc', 'special_desc', 'logo', 'store_pin', 'category_id', 'company_id','nation_wide', 'email', 'country_code', 'phone', 'state_id', 'address', 'city_id', 'post_code', 'subscription_amount', 'website', 'is_payment', 'last_payment_date', 'merchant_pin', 'min_vouchar_amount', 'max_vouchar_amount', 'incremental_vouchar_amount', 'store_location', 'is_subscribed', 'is_approved'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function company() {
        return $this->belongsTo('App\Model\Company');
    }

    public function getStoreReport($id) {
        $company = new Company();
        $data = $company->getCompanyById($id);
        if ($data->is_subscribed == "1") {
            $dc = 'active';
        } else {
            $dc = 'deactive';
        }
        return self::where('company_id', $id)->select('store_location', 'email', 'address', 'phone', 'store_pin', 'merchant_pin', 'is_approved', 'id', 'is_payment', 'state_id as ' . $dc)->orderBy('id', 'asc');
    }

    public function getStoreById($id) {
        return self::where('id', $id)->first();
    }

    public function activateStore($id, $value) {
        self::where('id', $id)->update(array('is_approved' => $value));
        if($value != 3)
        {
        CompanyBusinessProfile::where('id', $id)->update(array('is_approved' => $value));
        }
        return 1;
    }

    public function updateStoreData($id, $data) {
        $companybusinessupdate = $this->getStoreById($id);
        $companybusinessupdate->short_desc = $data['short_desc'];
        $companybusinessupdate->long_desc = $data['long_desc'];
        $companybusinessupdate->special_desc = $data['special_desc'];
        $companybusinessupdate->category_id = $data['category_id'];
        $companybusinessupdate->website = $data['website'];
        $companybusinessupdate->company_id = $data['company_id'];
        $companybusinessupdate->email = $data['email'];
        $companybusinessupdate->country_code = $data['country_code'];
        $companybusinessupdate->phone = $data['phone_number'];
        $companybusinessupdate->state_id = $data['state'];
        $companybusinessupdate->address = $data['address'];
        $companybusinessupdate->city_id = $data['suburb'];
        $companybusinessupdate->post_code = $data['post_code'];
        $companybusinessupdate->max_vouchar_amount = $data['maximum_amount'];
        $companybusinessupdate->min_vouchar_amount = $data['minimum_amount'];
        //$companybusinessupdate->incremental_vouchar_amount = Input::get('incremental_amount');
        $companybusinessupdate->store_location = $data['store_location'];
        $companybusinessupdate->save();
        return $companybusinessupdate;
    }

    public function updateApprovedDetails($id) {
        try {
            $objCompany = CompanyBusinessProfile::findOrFail($id);
            $objCompany->delete(); // delete existing
            DB::statement("INSERT INTO company_business_profile select  * from company_business_profile_audit where id=" . $id); // Insert from company_bussiness_profile_audit table
            CompanyBusinessReceipients::where('company_business_profile_id', $id)->delete();
            DB::statement("INSERT INTO company_business_recipient (company_business_profile_id,recipient_id) select company_business_profile_id,recipient_id  from company_business_recipient_audit where company_business_profile_id=" . $id);
            CompanyTimings::where('company_business_profile_id', $id)->delete();
            DB::statement("INSERT INTO company_timings (company_business_profile_id,day,timings) select company_business_profile_id,day,timings from company_timings_audit where company_business_profile_id=" . $id);
            CompanyBusinessOccations::where('company_business_profile_id', $id)->delete();
            DB::statement("INSERT INTO company_business_occasion (company_business_profile_id,occasion_id) select company_business_profile_id,occasion_id from company_business_occasion_audit where company_business_profile_id=" . $id);
            CompanyBusinessImages::where('company_business_profile_id', $id)->delete();
            DB::statement("INSERT INTO company_business_images (company_business_profile_id,image,voucher_image) select  company_business_profile_id,image,voucher_image from company_business_images_audit where company_business_profile_id=" . $id);
        } catch (\Exception $e) {

            echo $e->getMessage();
        }
    }

    public function updateDetails($id, $data) {
        self::where('id', $id)->update($data);
        CompanyBusinessProfile::where('id', $id)->update($data);
        return 1;
    }

    public function updateBussinessProfileAudit($id, $data) {
        self::where('id', $id)->update($data);
        return 1;
    }

    public function getCompanyStore($id) {
        return self::where('company_id', $id)->where('is_payment','1')->select('id')->get();
    }
    
    public function getUpdateAmountStore() {
        return self::where('is_payment','1')->select('id')->get();
    }
    
    public function getStoreList() {
        return self::select("company_business_profile_audit.store_location","company.company_name","company_business_profile_audit.is_subscribed","company_business_profile_audit.is_approved")->join("company","company.id","=","company_business_profile_audit.company_id")->orderByRaw("company.id,company_business_profile_audit.id asc");
    }
    
    public function updateLastPaymentDate($user_id,$date,$amount){
        self::where("id",$user_id)->update(array("last_payment_date"=>$date,"subscription_amount"=>$amount));
    }
    
    public function updateSubscriptionStatus($user_id,$status) {
        self::where("id",$user_id)->update(array("is_subscribed"=>$status));
    }
    
    public function updateVoucherByMerchantPIN($merchant_pin) {
        $checkifexists = self::where('merchant_pin', $merchant_pin)->count();
        if ($checkifexists != 0) {
            self::where('cherrygift_voucher', 1)->update(['cherrygift_voucher' => 0]);
            self::where('merchant_pin', $merchant_pin)->update(['cherrygift_voucher' => 1]);
            return "updated";
        } else {
            return "failure";
        }
    }
    
}
