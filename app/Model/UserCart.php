<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Model\CartVouchers;
use Redirect;
class UserCart extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_cart';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['company_business_profile_id', 'user_id', 'user_voucher_price', 'sms_message', 'phone_number', 'sms_date', 'sms_hour', 'sms_minutes','utc_sms_date', 'utc_sms_hour', 'utc_sms_minutes', 'sent_status', 'sms_hour'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function save_cart($cart) {
        try {
            $CartVouchers = new CartVouchers();
            $getUserCart = self::where('user_id', Auth::user()->id)->count();
        } catch (Exception $e) {
            throw new CustomException($e->getmessage());
        }
        if ($getUserCart == 0) {
            // insert into cart_vouchers table 
            try {
                $this->user_id = Auth::user()->id;
                $this->total_price = $cart["voucher_price"];
                $this->save();

                $CartVouchers->cart_id = $this->id;
                $CartVouchers->company_business_profile_id = $cart["company_business_profile_id"];
                $CartVouchers->phone_number = $cart["phone_number"];
                $CartVouchers->sms_message = $cart["sms_txt"];
                $CartVouchers->sms_date = $cart["sms_date"];
                $CartVouchers->sms_hour = $cart["sms_hour"];
                $CartVouchers->sms_minutes = $cart["sms_minutes"];
                $CartVouchers->utc_sms_date = $cart["utc_sms_date"];
                $CartVouchers->utc_sms_hour = $cart["utc_sms_hour"];
                $CartVouchers->utc_sms_minutes = $cart["utc_sms_minutes"];
                $CartVouchers->price = $cart["voucher_price"];
                $CartVouchers->sms_schedule = $cart["sms_schedule"];                
                $CartVouchers->save();
                return $CartVouchers;
            } catch (Exception $e) {
                throw new CustomException($e->getmessage());
            }
        } else {
            try {
            // insert into cart_vouchers table 
                $getUserCart = self::where('user_id', Auth::user()->id)->first();
                $getUserCart->total_price = $getUserCart->total_price + $cart["voucher_price"];
                $getUserCart->save();

                $CartVouchers = new CartVouchers();
                $CartVouchers->cart_id = $getUserCart->id;
                $CartVouchers->company_business_profile_id = $cart["company_business_profile_id"];
                $CartVouchers->phone_number = $cart["phone_number"];
                $CartVouchers->sms_message = $cart["sms_txt"];
                $CartVouchers->sms_date = $cart["sms_date"];
                $CartVouchers->sms_hour = $cart["sms_hour"];
                $CartVouchers->sms_minutes = $cart["sms_minutes"];
                $CartVouchers->utc_sms_date = $cart["utc_sms_date"];
                $CartVouchers->utc_sms_hour = $cart["utc_sms_hour"];
                $CartVouchers->utc_sms_minutes = $cart["utc_sms_minutes"];
                $CartVouchers->price = $cart["voucher_price"];
                $CartVouchers->sms_schedule = $cart["sms_schedule"];
                $CartVouchers->save();
                return $CartVouchers;
            } catch (Exception $e) {
                throw new CustomException($e->getmessage());
            }
        }
    }

    public function updateUserCart($data) {
        // Update user_cart and cart_vouchers table
        try {
            $existing_cart_voucher = CartVouchers::find($data['voucher_cart_id']);
           // $cart = self::find($existing_cart_voucher->cart_id);
            //$cart->total_price = ($this->total_price - $existing_cart_voucher->price + $data['voucher_price']);
            //$cart->save();
//        $cart_voucher = new CartVouchers();
//        $cart_voucher->id = $data['voucher_cart_id'];
            $existing_cart_voucher->phone_number = $data['phone_number'];
            $existing_cart_voucher->sms_message = $data['sms_txt'];
            $existing_cart_voucher->sms_date = $data['sms_date'];
            $existing_cart_voucher->sms_hour = $data['sms_hour'];
            $existing_cart_voucher->sms_minutes = $data['sms_minutes'];
            $existing_cart_voucher->utc_sms_date = $data["utc_sms_date"];
            $existing_cart_voucher->utc_sms_hour = $data["utc_sms_hour"];
            $existing_cart_voucher->utc_sms_minutes = $data["utc_sms_minutes"];
            $existing_cart_voucher->price = $data['voucher_price'];
            $existing_cart_voucher->sms_schedule = $data["sms_schedule"];
           $existing_cart_voucher->save();
           $total_price = CartVouchers::where('cart_id',$existing_cart_voucher->cart_id)->sum('price');
        // let's update user_cart 
           DB::table('user_cart')->where('id',$existing_cart_voucher->cart_id)->update(['total_price'=>$total_price]); 
          return $existing_cart_voucher;
            
        } catch (Exception $e) {
            throw new CustomException($e->getmessage());
        }
    }

    public function getUserCartInfo() {
        try {
            return DB::table('cart_vouchers')
                            ->join('company_business_profile', 'cart_vouchers.company_business_profile_id', '=', 'company_business_profile.id')
                            ->join('company_business_images', 'cart_vouchers.company_business_profile_id', '=', 'company_business_images.company_business_profile_id')
                            ->join('user_cart', 'user_cart.id', '=', 'cart_vouchers.cart_id')
                            ->select('cart_vouchers.id', 'cart_vouchers.cart_id', 'cart_vouchers.phone_number', 'cart_vouchers.sms_message', 'cart_vouchers.sms_date', 'cart_vouchers.sms_hour', 'cart_vouchers.sms_minutes', 'cart_vouchers.price', 'company_business_profile.store_location', 'company_business_profile.logo', 'company_business_images.image','cart_vouchers.sms_schedule')
                            ->where('company_business_images.voucher_image', '=', 1)->where('user_cart.user_id', '=', Auth::user()->id)
                            ->get();
        } catch (\Illuminate\Database\QueryException $e) {
            return Redirect::back()->withErrors(['message' => trans('lang.en.message.addingvouchererror')]);
        }
        //return self::where("id", $user_id)->where("is_subscribed", $is_subscribed)->where("is_approved", $is_approved)->first();
    }

    public function get_cart_voucher($id, $user_id) {
        return DB::table("cart_vouchers")->join("user_cart", "user_cart.id", "=", "cart_vouchers.cart_id")->select("cart_vouchers.id", "cart_vouchers.company_business_profile_id", "cart_vouchers.phone_number", "cart_vouchers.sms_message", "cart_vouchers.sms_date", "cart_vouchers.sms_hour", "cart_vouchers.sms_minutes", "cart_vouchers.price")->where("cart_vouchers.id", $id)->where("user_cart.user_id", $user_id)->first();
    }

    public function getCartVoucherCount($user_id) {
        return DB::table("user_cart")->join("cart_vouchers", "cart_vouchers.cart_id", "=", "user_cart.id")->where("user_cart.user_id", $user_id)->count();
    }

    public static function getCartCountView($user_id) {
        $user_cart = new UserCart();
        return $user_cart->getCartVoucherCount($user_id);
    }

    public function getCartRow($user_id) {
        return self::where("user_id", $user_id)->first();
    }

    public function clearCart($cart_id) {
        if(!empty($cart_id)){
        $cart = self::find($cart_id);
        if(!empty($cart))
        {
        $cart->delete();
        }
        $cart_vouchers = new CartVouchers();
        
        $cart_vouchers->deleteCartVouchers($cart_id);
        return true;
    }
    }
    
    public function getCartInfo($cart_id) {
        return self::where("id",$cart_id)->get()->toArray();
    }
}
