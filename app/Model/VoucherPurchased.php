<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VoucherPurchased extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'voucher_purchased';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'sms_date', 'phone_number', 'message', 'company_business_profile_id', 'voucher_link', 'sms_minutes', 'twillio_response', 'sent_status', 'sms_hour'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created', 'updated'];

    

}
