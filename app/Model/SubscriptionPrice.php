<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
class SubscriptionPrice extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'subscription_price';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['sub_price','launch_date','prelaunch_sub_price'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at','updated_at'];
        
        public function getSubscriptionDetails($id=1) {
          return self::where('id', $id)->first();
        }
        
        public function updateSubscriptionDetails($id,$data) {            
          return self::where('id', $id)->update($data);
        }
}
