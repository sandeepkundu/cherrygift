<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Model\CompanyBusinessProfileAudit;
use DB;

class CompanyBusinessProfile extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_business_profile';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['short_desc', 'long_desc', 'special_desc', 'logo', 'store_pin', 'category_id', 'company_id', 'nation_wide', 'email', 'country_code', 'phone', 'state_id', 'address', 'city_id', 'post_code', 'subscription_amount', 'website', 'is_payment', 'last_payment_date', 'merchant_pin', 'min_vouchar_amount', 'max_vouchar_amount', 'incremental_vouchar_amount', 'store_location', 'is_subscribed', 'is_approved'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function company() {
        return $this->belongsTo('App\Model\Company');
    }

    public function getInfo($id, $is_subscribed = 1, $is_approved = 1) {

        if ($is_approved == 1) {
            $is_approved = array(1, 4);
        } else {
            $is_approved = 1;
        }
        return self::where("id", $id)->where("is_subscribed", $is_subscribed)->whereIn("is_approved", $is_approved)->first();
    }

    public function getCompanyids($company_business_profile_id) {
        return CompanyBusinessProfile::where('id', $company_business_profile_id)->select('company_id')->get();
    }

    public function getOutletSummery($id) {

        $outlet_details = self::find($id);
        $nextdate = explode(" ", Carbon::createFromFormat("Y-m-d", $outlet_details->last_payment_date)->addYear(1)->format('dS M Y'));
        return array('nextdate' => $nextdate, 'outlet_details' => $outlet_details);
    }

    public function getStoreById($id) {
        
        return self::where('id', $id)->first();
    }

    public function activateStore($id, $value) {
        $rows = $this->getStoreById($id);
        $rows->is_approved = $value;
        $rows->save();
        return 1;
    }

    public function getVenderPaymentList($month, $year) {
        return self::select("admin_vendor_payment.month", "admin_vendor_payment.payment_status", "admin_vendor_payment.mail_sent", "company.company_name", "company_business_profile.store_location", "company_business_profile.id","vc.amount","company.account_name","company.bsb","company.account_number","vc.updated_at","order_voucher.voucher_unique_id")
                        ->join("company", "company.id", "=", "company_business_profile.company_id")
                        ->join("order_voucher","company_business_profile.id","=","order_voucher.company_business_profile_id")
                        ->leftjoin(DB::raw("( select sum(order_voucher.voucher_price) as amount,order_voucher.company_business_profile_id,order_voucher.redeemed,order_voucher.redeemed_at,order_voucher.updated_at, order_voucher.voucher_unique_id from order_voucher where YEAR(order_voucher.updated_at) = $year AND MONTH(order_voucher.updated_at) = $month and order_voucher.redeemed=1 group by order_voucher.redeemed_at) as vc"),"vc.redeemed_at","=","company_business_profile.merchant_pin")
                        ->leftjoin("admin_vendor_payment", function($join) use($month, $year) {
                            $join->on("admin_vendor_payment.company_business_profile_id", "=", "company_business_profile.id")
                            ->where("admin_vendor_payment.month", '=', $month)
                            ->where("admin_vendor_payment.year", '=', $year);
                        })//->whereRaw("month(company_business_profile.created_at)<=" . $month)
                        //->whereRaw("year(company_business_profile.created_at)<=" . $year)
                        ->where('vc.redeemed', '1')
                        ->groupBy('company.id')
                        ->orderBy('vc.updated_at', 'desc')->orderBy('company_business_profile.id', 'asc');

    }

    public function store_pin($store_pin) {
        return self::where('store_pin', $store_pin)->count();
    }

    public function getMultipleRecords($arr) {
        return DB::table("company_business_profile")->whereIn("id", $arr)->get();
    }

    public function getStoresByCompanyId($id) {
        return self::select("id")->where("company_id", $id)->get();
    }

    public function updateVoucherByMerchantPIN($merchant_pin) {
        $checkifexists = self::where('merchant_pin', $merchant_pin)->count();
        if ($checkifexists != 0) {
            self::where('cherrygift_voucher', 1)->update(['cherrygift_voucher' => 0]);
            self::where('merchant_pin', $merchant_pin)->update(['cherrygift_voucher' => 1]);
            return "updated";
        } else {
            return "failure";
        }
    }

    public function updateLastPaymentDate($user_id, $date, $amount) {
        self::where("id", $user_id)->update(array("last_payment_date" => $date, "subscription_amount" => $amount));
    }

    public function updateSubscriptionStatus($user_id, $status) {
        self::where("id", $user_id)->update(array("is_subscribed" => $status));
    }

    public function beforeSubscriptionRenew() {
        return DB::table('company_business_profile')
                        ->join('company', 'company_business_profile.company_id', '=', 'company.id')
                        ->where('company_business_profile.is_subscribed', 1)
                        ->where(DB::raw("TIMESTAMPDIFF(DAY,Now(),last_payment_date)"), 30)
                        ->select('company_business_profile.email', 'company_business_profile.store_location', 'company.first_name', 'company.last_name')
                        ->get();
    }

    public function afterSubscriptionRenew($cbp_id) {
        return DB::table('company_business_profile')
                        ->join('company', 'company_business_profile.company_id', '=', 'company.id')
                        //->where('company_business_profile.is_subscribed', 1)
                        //->where('company_business_profile.last_payment_date', $date)
                        ->where('company_business_profile.id', $cbp_id)
                        ->select('company_business_profile.email', 'company_business_profile.store_location', 'company.first_name', 'company.last_name')
                        ->get();
    }

    public function checkStoreName($storename, $company_id = 0) {
        if (!empty($storename)) {
            $storecount = self::where('store_location', $storename)->where('id', $company_id)->count();
            if ($storecount != 0) {
                return;
            } else {
                return self::where('store_location', $storename)->count();
            }
        }
    }

    
    public function getVenderPaymentListPdf($id ,$month, $year) {
        

        return self::select("admin_vendor_payment.month", "admin_vendor_payment.payment_status", "company.company_name", "company.first_name","company.contact_email", "company_business_profile.store_location", "company_business_profile.id", "company_business_profile.phone", "company_business_profile.post_code", "company_business_profile.state_id", "company_business_profile.city_id", "company_business_profile.address","vc.amount","vc.voucher_unique_id", 'vc.updated_at','vc.redeemed_at',"company.account_name","company.bsb","company.account_number","company.accounts_email")
                        ->join("company", "company.id", "=", "company_business_profile.company_id")
                        ->leftjoin(DB::raw("( select (order_voucher.voucher_price) as amount,order_voucher.company_business_profile_id,order_voucher.voucher_unique_id,order_voucher.redeemed_at,order_voucher.updated_at,order_voucher.redeemed from order_voucher where YEAR(order_voucher.updated_at) = $year AND MONTH(order_voucher.updated_at) = $month ) as vc"),"vc.redeemed_at","=","company_business_profile.merchant_pin")
                        ->leftjoin("admin_vendor_payment", function($join) use($month, $year) {
                            $join->on("admin_vendor_payment.company_business_profile_id", "=", "company_business_profile.id")
                            ->where("admin_vendor_payment.month", '=', $month)
                            ->where("admin_vendor_payment.year", '=', $year);
                        })->whereRaw("month(company_business_profile.created_at)<=" . $month)
                        ->whereRaw("year(company_business_profile.created_at)<=" . $year)
                        ->where("company_business_profile.id", '=',$id)
                        ->where('vc.redeemed', '1')->orderBy('company.id', 'asc')->orderBy('company_business_profile.id', 'asc');

    }

    public static function getloc($merchant_pin) {
                     
                $cityname= self::select("store_location")
                            ->where("merchant_pin", '=',$merchant_pin)->first();
                   if(!empty($cityname))         
                        return $cityname->store_location;
                    else
                        return null;    
     }

}
