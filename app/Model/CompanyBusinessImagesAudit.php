<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class CompanyBusinessImagesAudit extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'company_business_images_audit';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['company_business_profile_id','image','voucher_image'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at','updated_at'];
        public $timestamps = false;
        
        public function getVoucherImage($id)
        {
            return self::where("company_business_profile_id", $id)->where("voucher_image", 1)->first();
        }
        public function auditDetails($id)
        {
           return self::where('company_business_profile_id', '=', $id)->get();
        }
}
