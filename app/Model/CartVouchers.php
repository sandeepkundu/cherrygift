<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\UserCart;
use DB;

class CartVouchers extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cart_vouchers';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cart_id', 'company_business_profile_id', 'phone_number', 'sms_message', 'sms_date', 'sms_hour', 'sms_minutes', 'price'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function deleteVoucharDetails($voucharid) {
        try {
            $cart = self::findOrFail($voucharid);
            $amt_price = $cart->price;
            $cart_id = $cart->cart_id;
// Delete user object from database. 
            $cart->delete();
            //echo $cart->count(); exit;
            if ($cart->count() != 1) {
                $update_amount = UserCart::find($cart_id)->decrement('total_price', $amt_price);
            } else {
                $update_amount = UserCart::find($cart_id)->delete();
            }
            //$del= UserCart::find($voucharid)->delete();
            if ($update_amount) {
                return "sucess";
            } else {
                return "failure";
            }
        } catch (Exception $e) {
            throw new CustomException($e->getmessage());
        }
    }

    public function getAllCartVouchers($cart_id) {
        return self::where("cart_id", $cart_id)->get();
    }

    public function deleteCartVouchers($cart_id) {
        if(!empty($cart_id))
        {
            $if_exists = self::where("cart_id", $cart_id)->count();
            if($if_exists !=0)
            {
        self::where("cart_id", $cart_id)->delete();
            }
    }
    return true;
    }
    
    public function addUniqeCode($cart_id){
        $code = uniqid();
        DB::table("cart_vouchers")->where("cart_id",$cart_id)->update(array("order_code"=>$code));
        return $code;
    }
    
    public function getVoucherByCode($code)
    {
        return self::where("order_code",$code)->count();
    }
    
    public function getVoucherInfoByCode($code)
    {
        return self::where("order_code",$code)->get();
    }
}