<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use Response;
class Suburbs extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'suburbs';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['state_id','locality'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
        function statecitylist()
        {
            $city_state_arr = array();
            //$statecitylist = DB::table('city')->where('status', 'Active')->select('state_id','id','city_name')->orderBy('city_name', 'asc')->get();
            $statecitylist = DB::table('suburbs')->select('state_id','id',"locality")->orderBy("locality", 'asc')->get();

            foreach($statecitylist as $allcitylist)
            {
                $allcitylist->locality = str_replace("'"," ",$allcitylist->locality);
                $city_state_arr[$allcitylist->state_id][] = $allcitylist;
            }
           
            return $city_state_arr;
        }
        
        public function getSuburbById($id) {
           return self::where('id', $id)->first();
        }
        public function getSuburbsList($id,$term)
        {
        
        $search = DB::table('suburbs')->where('state_id', $id)->where('locality', 'LIKE', $term . '%')->select('locality', 'id')->orderBy('locality', 'asc')->get();
        $books = array();
        foreach ($search as $results => $book) {
            $books[] = array('id' => $book->id, 'value' => $book->locality);
        }
        return Response::json($books);   
        }


        public static function getCity($city_id) {
                     
                $cityname= self::select("locality")
                            ->where("id", '=', $city_id)->first();

                if(!empty($cityname))               
                    return $cityname->locality;
                 else
                    return null;
            }

}
