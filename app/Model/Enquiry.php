<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Enquiry extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'enquiries';
    protected $primaryKey = 'id';

    public $timestamp = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'message',
        'contact_number'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
        
        
    public static function getEnquiryList() {
       return self::select('*')->orderBy('created_at', 'desc');

    }
        
}
