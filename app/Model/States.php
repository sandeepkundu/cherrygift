<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class States extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'states';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['state_code','state_name'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
    public function stateimages() {
        return $this->hasMany('App\Model\StateImages','state_id')->orderByRaw("RAND()");
    }

    public static function getState($state_id) {
                     
                $cityname= self::select("state_name")
                            ->where("id", '=', $state_id)->first();
            
            
               if(!empty($cityname))               
                    return $cityname->state_name;
                 else
                    return null;
            
            }

}
