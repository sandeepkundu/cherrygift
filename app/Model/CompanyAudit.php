<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\CompanyBusinessImages;
use App\Model\CompanyBusinessProfile;
use App\Model\CompanyTimings;
use App\Model\States;
use App\Model\Suburbs;
use App\Model\OrderVoucher;
use DB;

class CompanyAudit extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_audit';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'company_name', 'acn_abn_number', 'account_name', 'bsb', 'account_number', 'company_address', 'country_code', 'phone', 'company_website', 'pin', 'category_id', 'state_id', 'suburs_id', 'postal_code', 'is_subscribed'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function getCompanyReport() {
       return self::select('first_name','last_name', 'company_name', 'contact_email', 'company_address', 'phone','id','is_subscribed');

    }
    
    public function activateCompany($id, $value) {
        $rows = self::where('id', $id)->first();
        $rows->is_subscribed = $value;
        $rows->save();
        return 1;
    }

}
