<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;

class Search extends Model {

    public function searchIndex($param_array) {
        @extract($param_array);
        $raw = $this->commonQuery($param_array);
//        print_r($_POST);
        //       echo $raw->toSql();die;
        return $raw->get();
    }

    public function searchAjax($param_array) {
        @extract($param_array);
       
        $raw = $this->commonQuery($param_array);

        if (!empty($filter_cat_arr)) {
            $raw->whereIn("cbp.category_id", $filter_cat_arr);
        }

        if (!empty($filter_state_arr)) {
            $raw->whereIn("cbp.state_id", $filter_state_arr);
        }

        if (!empty($filter_city_arr)) {
            $raw->whereIn("cbp.city_id", $filter_city_arr);
        }

        if (!empty($filter_rec_arr)) {
            $raw->join("company_business_recipient as cbr2", "cbr2.company_business_profile_id", "=", "cbp.id");
            $raw->whereIn("cbr2.recipient_id", $filter_rec_arr);
        }

        if (!empty($filter_oca_arr)) {
            $raw->join("company_business_occasion as cbo2", "cbo2.company_business_profile_id", "=", "cbp.id");
            $raw->whereIn("cbo2.occasion_id", $filter_oca_arr);
        }

        //echo $raw->toSql();die;
        return $raw->get();
    }

    public function commonQuery($param_array) {
        @extract($param_array);
         // dd($param_array);
        $raw = DB::table("company_business_profile as cbp")->distinct()
                        ->select("cbp.id", "cbp.company_id", "cbp.logo", "cbp.short_desc", "cbp.store_location", "cbi.image", "company.company_name")
                        ->join("company", "company.id", "=", "cbp.company_id")
                        ->leftjoin("company_business_images as cbi", function ($join) {
                            $join->on("cbi.company_business_profile_id", "=", "cbp.id")
                            ->where("cbi.voucher_image", "=", 1);
                        })
                        ->leftjoin("categories as c1", "c1.id", "=", "cbp.category_id")
                        ->where('cbp.is_subscribed', 1)->whereIn("cbp.is_approved", array(1, 4));


        if (!empty($txt) || !empty($search_state) || !empty($search_city)) {
            if (!empty($txt)) {
                $like_category = DB::table("categories")->where("name", "like", "%" . $txt . "%")->lists("id");
                $raw->where(function($query) use ($txt, $like_category) {
                    $query->orWhere("cbp.short_desc", "like", "%" . $txt . "%");
                    $query->orWhere("cbp.store_location", "like", "%" . $txt . "%");
                    //$query->orWhere("company.company_name", "like", "%" . $txt . "%");
                    $query->orWhere("c1.name", "like", "%" . $txt . "%");
//                    if (!empty($like_category))
//                        $query->whereIn("company.category_id", $like_category);
                });
            }
            
            if (!empty($search_state)) {
                $raw->where("cbp.state_id", $search_state);
            }

            if (empty($filter_city_arr)) {
                if (!empty($search_city)) {
                    $raw->where("cbp.city_id", $search_city);
                } 
            } 


            
        } else {
            

            if (empty($url_param_id))
                $url_param_id = 1;
            switch ($url_param_type) {
                case "state":
                    $raw->where("cbp.state_id", $url_param_id);
                    break;
                case "cat":
                    $raw->where("cbp.category_id", $url_param_id);
                    break;
                case "oca":
                    $raw->join("company_business_occasion as cbo", "cbo.company_business_profile_id", "=", "cbp.id");
                    $raw->where("cbo.occasion_id", $url_param_id);
                    break;
                case "rec":
                    $raw->join("company_business_recipient as cbr", "cbr.company_business_profile_id", "=", "cbp.id");
                    $raw->where("cbr.recipient_id", $url_param_id);
                    break;
            }

            // print_r($url_param_id);
            // print_r($url_param_type);
        }

        return $raw;
    }

}
