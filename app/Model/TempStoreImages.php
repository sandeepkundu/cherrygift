<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
class TempStoreImages extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'temp_store_images';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','store_pin','image','voucher_image'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
        function insertData($data)
        {
                    //$this->user_id = $data['user_id'];
                    $this->store_pin = $data['store_pin'];
                    $this->image = $data['image'];
                    $this->voucher_image = $data['voucher_image'];
                    $this->save();
                    return $this->id;
        }
        
        public function getData($id) {
           return self::where('id', $id)->first();
        }
        
        public function deleteData($id) {
            return self::where('id', $id)->delete();
        }
}
