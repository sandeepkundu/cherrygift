<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserGiftedVoucher extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_gifted_voucher';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'order_voucher_id', 'voucher_send_by'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created'];
    
    
    public function giftHistory($user_id) {
        return self::join('order_voucher', 'user_gifted_voucher.order_voucher_id', '=', 'order_voucher.id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        ->where('user_gifted_voucher.user_id', '=', $user_id)
                        ->select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image','order_voucher.voucher_status','order_voucher.created_at','order_voucher.voucher_unique_id','order_voucher.redeemed')
                        ->orderby('order_voucher.id', 'desc')
                        ->get();
    }
    

}
