<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StateImages extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'state_images';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    

}
