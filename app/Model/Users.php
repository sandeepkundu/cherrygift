<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class Users extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
        
        public function getUsers(){
            return self::select('first_name','last_name', 'email','status','id')->where("role","User")->orderBy('id', 'asc');
        }
        
        public function getUserById($id){
            return self::find($id);
        }
        
        public function getUsersByMobile($ids,$user_id){
           return self::select("id",DB::raw("REPLACE(mobile_number,' ','') as mobile_number"))->whereRaw("FIND_IN_SET(REPLACE(mobile_number,' ',''),'".$ids."')")->where("id","!=",$user_id)->get()->toArray();
        }
        public function getUserByEmail($email)
        { 
           return self::where('email', $email)->first();
        }
        public function getUserId($id) {
        return self::where('id', $id)->first();
    }
        public function updateUserData($id,$data) {
       	//dd($data);
        $user_array = $this->getUserId($id);
        $user_array->first_name = $data['first_name'];
        $user_array->last_name = $data['last_name'];
        $user_array->email = $data['email'];
        $user_array->mobile_number = $data['mobile_number'];
       // $user_array->password = $data['password'];

        $user_array->save();

    }
}
