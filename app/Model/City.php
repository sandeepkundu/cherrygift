<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
class City extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'city';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
        function statecitylist()
        {
            $city_state_arr = array();
            $statecitylist = DB::table('city')->where('status', 'Active')->select('state_id','id','city_name')->orderBy('city_name', 'asc')->get();
            foreach($statecitylist as $allcitylist)
            {
                $city_state_arr[$allcitylist->state_id][] = $allcitylist;
            }
            
            return $city_state_arr;
        }
}
