<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
class HomepageSliderImages extends Model
{
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'hompage_slider_images';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
       public function saveImages($image)
        {
            $this->image = $image;
            $this->save();
        }
        
        public function getImages(){
            return self::all();
        }
}
