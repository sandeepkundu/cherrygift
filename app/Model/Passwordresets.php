<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Passwordresets extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'suplier_password_reset';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email','token'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
        
     public function reset($token)
        {
          return self::where('token', '=', $token)->count();  
        } 
        public function checkExpiry($token)
        {
           return self::where('token', '=', $token)
                ->where('created_at', '<', Carbon::now()->subHours(42))
                ->first();
        }
        public function deleteToken($token)
        {
            self::where('token', '=', $token)->delete();
        }
}
