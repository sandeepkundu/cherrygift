<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Order;
use DB;
use App\Model\Number;

class OrderVoucher extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_voucher';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_order_id', 'company_business_profile_id', 'voucher_unique_id', 'voucher_price', 'phone_number', 'sms_message', 'sms_date', 'sms_hour', 'sms_minutes', 'utc_sms_date', 'utc_sms_hour', 'utc_sms_minutes', 'redeemed','sms_schedule'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created', 'updated'];

    public function saveOrderVoucher($data) {

        try {
//            $this->user_order_id = $data['user_order_id'];
//            $this->company_business_profile_id = $data['company_business_profile_id'];
//            $this->voucher_unique_id = $data['voucher_unique_id'];
//            $this->voucher_price = $data['voucher_price'];
//            $this->phone_number = $data['phone_number'];
//            $this->sms_date = $data['sms_date'];
//            $this->sms_hour = $data['sms_hour'];
//            $this->sms_minutes = $data['sms_minutes'];
//            $this->save();
//            echo "<pre>";
//            print_r($data);die;
            return self::create($data);
        } catch (Exception $e) {
            throw new CustomException($e->getmessage());
        }
    }

    public function getUniqueRandomNumber() {
        $rand_num = $this->getRandomNumber();
        $check_unique = self::where("voucher_unique_id", $rand_num)->first();
        if (!empty($check_unique))
            $this->getUniqueRandomNumber();
        else
            return $rand_num;
    }

    public function getRandomNumber() {
        //$x = 10; // Amount of digits
        $min = 1000000000; //pow(10, $x); echo "<br>";
        $max = 9999999999; //pow(10, $x + 1) - 1; exit;
        return mt_rand($min, $max);
    }

    public function updateSmsStatus($id, $status, $sms_id, $msg = "") {
        $ov = self::find($id);
        $ov->sms_status = $status;
        if (!empty($sms_id))
            $ov->sms_twillio_id = $sms_id;
        if (!empty($msg))
            $ov->sms_error = $msg;
        $ov->save();
    }

    public function getPendingRedeemedVouchers($date) {
        return $getQueryResult = self::select('phone_number')->whereBetween('created_at', array($date . ' 00:00:00', $date . ' 23:59:59'))->where('redeemed', 0)->get();
    }

    public function getVouchersData($mobile_number, $user_id) {

        return self::select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.id', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image', 'order_voucher.sms_schedule')->
                        join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        ->where('user_order.user_id', '=', $user_id)
                        ->where(DB::raw('REPLACE(order_voucher.phone_number," ","")'), "like", '%'.str_replace(' ', '', $mobile_number).'%')
                        ->where('order_voucher.redeemed', '=', 0)
                        ->where('order_voucher.sms_status', '=', '1')
                        ->orderby('order_voucher.id', 'desc')
                        ->get();
    }

        public function getVouchers($mobile_number, $user_id) {
                       // $mobile_number = ltrim($mobile_number, '0');
                        $mobile_number = (int)$mobile_number;
        return self::select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.id', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image', 'order_voucher.sms_schedule','order_voucher.redeemed','order_voucher.voucher_status','order_voucher.created_at','order_voucher.voucher_unique_id')->
                        join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        //->where('user_order.user_id', '=', $user_id)
                        ->where(DB::raw('REPLACE(order_voucher.phone_number," ","")'), "like", '%'.str_replace(' ', '', $mobile_number).'%')
                        
                        ->where('order_voucher.sms_status', '=', '1')
                        ->orderby('order_voucher.id', 'desc')
                        ->get();
    }

    public function getVouchersBysuperadminwithoutpaypal($mobile_number, $user_id) {
        
                         $mobile_number = ltrim($mobile_number, '0');
        return self::select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.id', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image', 'order_voucher.sms_schedule','order_voucher.redeemed','order_voucher.voucher_status','order_voucher.created_at','order_voucher.voucher_unique_id')->
                        join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        // ->where('user_order.user_id', '=', $user_id)
                        ->where(function($q) use($mobile_number){
                        $q->where(DB::raw('REPLACE(order_voucher.phone_number," ","")'), "like", '%'.str_replace(' ', '', $mobile_number).'%')
                        ->orWhere('order_voucher.voucher_unique_id', '=', $mobile_number);
                        })
                        ->where('order_voucher.sms_status', '=', '1')
                        ->orderby('order_voucher.id', 'desc')
                        ->get();
    }


        public function getVouchersBysuperadmin($mobile_number, $user_id) {
         return self::select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.id', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image', 'order_voucher.sms_schedule','order_voucher.redeemed','order_voucher.voucher_status','order_voucher.created_at','order_voucher.voucher_unique_id')->
                        join('user_order', 'user_order.id', '=', 'order_voucher.user_order_i')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        ->where('company_business_profile.id', '=', $user_id)
                        //->where(DB::raw('REPLACE(order_voucher.phone_number," ","")'), "like", '%'.str_replace(' ', '', $mobile_number).'%')
                        ->where('order_voucher.phone_number', '=', $mobile_number)
                        ->where('order_voucher.sms_status', '=', '1')
                        ->orderby('order_voucher.id', 'desc')
                        ->get();
         }



    public function getVouchersDataArray($mobile_number, $user_id) {

        return self::select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.id', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image')->
                        join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        ->where('user_order.user_id', '=', $user_id)
                        ->where(DB::raw('REPLACE(order_voucher.phone_number," ","")'), "like", '%'.str_replace(' ', '', $mobile_number).'%')
                        ->where('order_voucher.redeemed', '=', 0)
                        ->where('order_voucher.sms_status', '=', '1')
                        ->get()->toArray();
    }

    public function getVouchersDataArrayByid($user_id) {
    
        $user_id= explode(',',$user_id);

        return self::select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.id', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image')->
                        join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        ->whereIn('order_voucher.id',$user_id)
                       //whereIn('id', array(1, 2, 3))
                       // ->where(DB::raw('REPLACE(order_voucher.phone_number," ","")'), "like", '%'.str_replace(' ', '', $mobile_number).'%')
                        ->where('order_voucher.redeemed', '=', 0)
                        ->where('order_voucher.sms_status', '=', '1')
                        ->get()->toArray();
    }

    public function getVouchersCanceledByid($user_id) {
       // dd($user_id);
        //$user_id= explode(',',$user_id);

        return self::select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.id', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image')->
                        join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        ->where('order_voucher.id',$user_id)
                       //whereIn('id', array(1, 2, 3))
                       // ->where(DB::raw('REPLACE(order_voucher.phone_number," ","")'), "like", '%'.str_replace(' ', '', $mobile_number).'%')
                        ->where('order_voucher.redeemed', '=', 0)
                        ->where('order_voucher.sms_status', '=', '1')
                        ->get()->toArray();
    }


    public function getOrderById($voucher_id) {
        return self::find($voucher_id);
    }

    public function purchageHistory($user_id) {
        return self::join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        ->where('user_order.user_id', '=', $user_id)
                        ->select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'order_voucher.sms_status', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image','order_voucher.sms_schedule','order_voucher.voucher_status','order_voucher.created_at','order_voucher.voucher_unique_id','order_voucher.redeemed')
                        ->orderby('order_voucher.id', 'desc')
                        ->get();
    }

    public function orderHistory($order_id, $user_id) {
        return self::join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')
                        ->join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
                        ->join('company_business_images', 'company_business_images.company_business_profile_id', '=', 'company_business_profile.id')
                        ->where('company_business_images.voucher_image', '=', 1)
                        ->where('order_voucher.user_order_id', '=', $order_id)
                        ->where('user_order.user_id', '=', $user_id)
                        ->select('order_voucher.company_business_profile_id', 'order_voucher.voucher_price', 'order_voucher.phone_number', 'order_voucher.sms_message', 'order_voucher.sms_date', 'order_voucher.sms_hour', 'order_voucher.sms_minutes', 'company_business_profile.logo', 'company_business_profile.store_location', 'company_business_images.image','order_voucher.sms_schedule')->get();
    }

    public function saveSmsText($smstext, $id) {
        self::find($id)->update(['sms_message' => $smstext]);
        return true;
    }

    public function totalRedeem($id, $month, $year) {
        
        $vid= $this->getRedeemedAt($id);
        if(isset($vid->merchant_pin))
        return self::where(DB::raw('MONTH(updated_at)'), '=', $month)->where('redeemed_at',$vid->merchant_pin)
        ->where('redeemed', '1')
        ->where(DB::raw('MONTH(order_voucher.updated_at)'), '=', $month)
        ->where(DB::raw('Year(order_voucher.updated_at)'), '=', $year)
        ->sum('voucher_price');
    }

    public function redeemedHistory($id, $month, $year) {

        $vid= $this->getRedeemedAt($id);
         
         if(isset($vid->merchant_pin)){                    
        return self::select('order_voucher.voucher_price', 'order_voucher.updated_at','order_voucher.voucher_unique_id', 'users.first_name','users.last_name')
                        ->join('user_order', 'order_voucher.user_order_id', '=', 'user_order.id')
                        ->join('users', 'users.id', '=', 'user_order.user_id')
                        ->where(DB::raw('MONTH(order_voucher.updated_at)'), '=', $month)
                        ->where(DB::raw('Year(order_voucher.updated_at)'), '=', $year)
                        ->where('order_voucher.redeemed_at',$vid->merchant_pin)->where('order_voucher.redeemed', '1')->get();
        }
    }

    public function sendScheduledSms() {
        //echo date("Y-m-d")."//".date("H")."//".date("i");
        $sms_info = self::select("id", "phone_number", "sms_message")->where("utc_sms_date", date("Y-m-d"))->where("utc_sms_hour", date("H"))->where("utc_sms_minutes", date("i"))->where("sms_status", 0)->get()->toArray();
        //print_r($sms_info);
        $order = new Order();
        $order->sendBulkTwillioSms($sms_info);
    }

    public function resendVoucherTransaction() {
        
    }

    public function rememberSmsUnused() {
        $sms_info = self::select("phone_number")->where(DB::raw("TIMESTAMPDIFF(DAY,sms_date,Now())"), 180)->where("redeemed", 0)->where("sms_status", 1)->get()->toArray();
        $msg = "Hi there! Forgotten something? You have 6 months left to use your cherrygift voucher. Get spending!";
        $order = new Order();
        $order->sendBulkReminderSms($sms_info, $msg);
    }

    public function getVoucherReport($id) {
        return self::select('voucher_unique_id', 'voucher_price', 'redeemed', 'redeemed_at', 'voucher_status', 'id','created_at','voucher_actioned_date','sms_date')
                        ->where('company_business_profile_id', $id)
                        ->orderby('id', 'desc');
    }

    public function updateOrderDetails($id, $data) {
       // dd($data);
        return self::where('id', $id)->update($data);
    }

    public function getRedeemedHistory($month = 0, $year = 0) {
        if (empty($month)) {
            $month = date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }
        return self::select('order_voucher.id', DB::raw('CONCAT(users.first_name, " ", users.last_name) AS name'), 'users.first_name', 'order_voucher.voucher_price', 
                'order_voucher.phone_number', 'order_voucher.created_at',
                'company_business_profile.store_location','order_voucher.voucher_unique_id','company.company_name','order_voucher.redeemed','order_voucher.voucher_status')->
                        join('user_order', 'user_order.id', '=', 'order_voucher.user_order_id')->
                        join('users', 'users.id', '=', 'user_order.user_id')->
                        join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')->
                        join('company','company.id','=','company_business_profile.company_id')->
                        whereMonth('order_voucher.created_at', '=', $month)->
                        whereYear('order_voucher.created_at', '=', $year)->groupBy('order_voucher.id')
                        ->orderby('order_voucher.id', 'asc');
    } 

    public function getAllRedeemedBycompany($id, $month, $year) {
        if (empty($month)) {
            $month = 8;//date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }
        $storeList = new CompanyBusinessProfile();
        $companyids = $storeList->getStoresByCompanyId($id);
        return self::select('order_voucher.voucher_unique_id', 'order_voucher.voucher_price', 'cbp1.store_location as purshased_at', 'order_voucher.updated_at', 'cbp2.store_location as redeemed_at')->
                        join('company_business_profile AS cbp1', 'order_voucher.company_business_profile_id', '=', 'cbp1.id')->
                        join('company_business_profile AS cbp2', 'order_voucher.redeemed_at', '=', 'cbp2.merchant_pin')->
                        where('order_voucher.redeemed', '1')->
                        whereMonth('order_voucher.updated_at', '=', $month)->
                        whereYear('order_voucher.updated_at', '=', $year)
                        ->whereIn('order_voucher.company_business_profile_id', $companyids)->groupBy('order_voucher.voucher_unique_id')
                        ->orderby('order_voucher.id', 'asc');
    }

    public function getAllCompanyForVoucher() {
        
        return self::select('company.company_name', 'company.email', 'company.company_address', 'company.phone','company.is_subscribed','company.id')->
                        join('company_business_profile', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')->
                        join('company','company.id','=','company_business_profile.company_id')->
                        groupBy('company.id')->
                        orderby('company.id', 'asc');
    }


    public function getRedeemedAt($id) {

        return CompanyBusinessProfile::select('merchant_pin')
                        ->where('id', $id)->first();

    }


}
