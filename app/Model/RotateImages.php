<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\aws\S3;

class RotateImages extends Model
{
    
    static $s3;
    static $s3_bucket;
    
    public static function rotate($src, $ext) {
        $deg = 0;
        if (function_exists('exif_read_data') && exif_imagetype($src) && (strtolower($ext) != 'png')) {
            $exif = @exif_read_data($src);
            if ($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                if ($orientation != 1) {
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                            break;
                    }
                }
            }
        }

        return $deg;
    }

    public static function upload_rotate_image($src, $ext) {
        //print_r($ext);die;
        $deg = self::rotate($src, $ext);
        //$deg = 0;
        if (strtolower($ext ) == 'jpg' || strtolower($ext) == 'jpeg')
            $source_image = imagecreatefromjpeg($src);
        else
            $source_image = imagecreatefrompng($src);

        if ($deg) {
            $source_image = imagerotate($source_image, $deg, 0);
        }

        $image_saved = imagejpeg($source_image, $src);
    }
    
    
    public static function uploadImageS3($source,$destination)
    {
        try {
            $s3_access_key = env('s3_access_key');
            $s3_secret_key = env('s3_secret_key');
            $aws_bucket = env('aws_bucket');
            self::$s3_bucket = $aws_bucket;
            self::$s3 = new \App\aws\S3($s3_access_key, $s3_secret_key);
            self::$s3->putObjectFile($source, self::$s3_bucket, $destination, S3::ACL_PUBLIC_READ,array("CacheControl"=>"max-age=2950000"));
	    @unlink($source);
            return true;
        } catch (Exception $e) {
            throw new CustomException($e->getmessage());
        }
    }
    
}
