<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\CompanyBusinessImagesAudit;
class CompanyBusinessImages extends Model
{
    public static $MAX_IMAGE_SIZEMB=3;
    public static $IMAGE_WIDTH=600;
    public static $IMAGE_HEIGHT=400;


    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'company_business_images';
    protected $primaryKey = 'id';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['company_business_profile_id','image','voucher_image'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['created_at','updated_at'];
    public $timestamps = false;

    public function getVoucherImage($id)
    {
        return self::where("company_business_profile_id", $id)->where("voucher_image", 1)->first();
    }
    public function deleteById($company_id,$voucher_img_id)
    {
        //self::where('company_business_profile_id',$company_id)->where('voucher_image',$voucher_img_id)->delete();   
        CompanyBusinessImagesAudit::where('company_business_profile_id',$company_id)->where('voucher_image',$voucher_img_id)->delete();  
    }

    /**
    * Check an image meets our criteria for upload
    * 
    * @param mixed $filename
    * @returns true if valid or string error message
    */
    static public function validateImage($filename)
    {
        // due to not using s3 now but fudging it, we should check for our local url
        $filename = convertLocalS3ToAbsolute($filename);
        if (strpos($filename,'http')===false) {
            if (@filesize($filename) > self::$MAX_IMAGE_SIZEMB*1024*1024) {
                return 'Image file size is too big.  Maximum size of '.self::$MAX_IMAGE_SIZEMB.'Mb allowed';
            }
        }
        @$info=getimagesize($filename);
        // for now we are going to force an exact size
        if ($info[0] != self::$IMAGE_WIDTH || $info[1] != self::$IMAGE_HEIGHT) {
            return 'Image must be '.self::$IMAGE_WIDTH.' x '.self::$IMAGE_HEIGHT.' pixels.  Supplied image is '.$info[0].'  x '.$info[1];
        }
        return true;
    }

     public function auditDetails($id)
        {
           return self::where('company_business_profile_id', '=', $id)->get();
        }
     
}
