<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\CompanyBusinessImages;
use App\Model\CompanyBusinessProfile;
use App\Model\CompanyBusinessProfileAudit;
use App\Model\CompanyBusinessImagesAudit;
use App\Model\CompanyTimingsAudit;
use App\Model\CompanyTimings;
use App\Model\States;
use App\Model\Suburbs;
use App\Model\OrderVoucher;
use DB;
use Crypt;
class Company extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'company_name', 'acn_abn_number', 'account_name', 'bsb', 'account_number', 'company_address', 'country_code', 'phone', 'company_website', 'pin', 'category_id', 'state_id', 'suburs_id', 'postal_code', 'is_subscribed'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function getCompanyById($id) {
        return self::where('id', $id)->first();
    }
    
    public function getCompanyDetails($param) {
        $companybusiness = CompanyBusinessProfile::find($param);
        //dd($companybusiness );
        $companybusinessimages = CompanyBusinessImages::where('company_business_profile_id', $param)->get();
        $companytiming = CompanyTimings::where('company_business_profile_id', $param)->get();
        foreach ($companytiming as $rec) {
            $companytiming[$rec->day] = $rec->timings;
        }

        $companybusinesslocation = DB::table('company_business_profile')
                        ->select('company_business_profile.id', 'company_business_profile.address as address', 'states.state_name', 'suburbs.locality')
                        ->join('states', 'states.id', '=', 'company_business_profile.state_id')
                        ->join('suburbs', 'suburbs.id', '=', 'company_business_profile.city_id')
                        ->where('company_business_profile.company_id', '=', $companybusiness->company_id)
                        ->where('company_business_profile.is_subscribed', '1')->get();
        $statename = States::where('id', $companybusiness->state_id)->pluck('state_name');
        $cityname = Suburbs::where('id', $companybusiness->city_id)->pluck('locality');
        $companyname = self::where('id', $companybusiness->company_id)->pluck('company_name');

        return array('companybusiness' => $companybusiness, 'companybusinessimages' => $companybusinessimages, 'companytiming' => $companytiming, 'companybusinesslocation' => $companybusinesslocation, 'statename' => $statename, 'cityname' => $cityname, 'companyname' => $companyname);
    }

    
    public function getCompanyDetailsStoreLocation($store_location) {
     
        $store_location=str_replace('-',' ',$store_location);
        $companybusiness = CompanyBusinessProfile::where('store_location',$store_location)->firstOrFail();
        $param=$companybusiness->id;
      if($companybusiness==null)
          return ;
        $companybusinessimages = CompanyBusinessImages::where('company_business_profile_id', $param)->get();
        $companytiming = CompanyTimings::where('company_business_profile_id', $param)->get();
        foreach ($companytiming as $rec) {
            $companytiming[$rec->day] = $rec->timings;
        }

        $companybusinesslocation = DB::table('company_business_profile')
                        ->select('company_business_profile.id', 'company_business_profile.address as address', 'states.state_name', 'suburbs.locality')
                        ->join('states', 'states.id', '=', 'company_business_profile.state_id')
                        ->join('suburbs', 'suburbs.id', '=', 'company_business_profile.city_id')
                        ->where('company_business_profile.company_id', '=', $companybusiness->company_id)
                        ->where('company_business_profile.is_subscribed', '1')->get();
        $statename = States::where('id', $companybusiness->state_id)->pluck('state_name');
        $cityname = Suburbs::where('id', $companybusiness->city_id)->pluck('locality');
        $companyname = self::where('id', $companybusiness->company_id)->pluck('company_name');

        return array('companybusiness' => $companybusiness, 'companybusinessimages' => $companybusinessimages, 'companytiming' => $companytiming, 'companybusinesslocation' => $companybusinesslocation, 'statename' => $statename, 'cityname' => $cityname, 'companyname' => $companyname);
    }

    public function voucherExists($merchant_pin, $outlates, $voucher_id) {
        $voucher_exits = self::join('company_business_profile', 'company_business_profile.company_id', '=', 'company.id')
//              ->where('company_business_profile.id','=',$voucherdetails->company_business_profile_id)
                ->where('company_business_profile.merchant_pin', '=', $merchant_pin)
                ->where('company_business_profile.company_id', '=', $outlates)
                ->count();
        $marchant_validation = self::join('company_business_profile', 'company_business_profile.company_id', '=', 'company.id')
//              ->where('company_business_profile.id','=',$voucherdetails->company_business_profile_id)
                ->where('company_business_profile.merchant_pin', '=', $merchant_pin)
                //->where('company_business_profile.company_id', '=', $outlates)
                ->count();
        //dd($marchant_validation);
        $OrderVoucher = new OrderVoucher();
        $voucherdetails = $OrderVoucher->getOrderById($voucher_id);
        $objcompanybussinessprofile = new CompanyBusinessProfile();
        $ischerrygiftvoucher = $objcompanybussinessprofile->getStoreById($voucherdetails->company_business_profile_id);
        if ($voucher_exits == 1 || ($ischerrygiftvoucher->cherrygift_voucher==1 && $marchant_validation==1)) {
            try {
                    $voucherdetails->redeemed = 1;
                    $voucherdetails->voucher_actioned_date = date('Y-m-d h:i:s');
                    $voucherdetails->redeemed_at = $merchant_pin;
                    $voucherdetails->save();
                    
                return 1;
            } catch (Exception $ex) {
                
            }
        } else {
            return 0;
        }
    }
    

    //     public function getCompanyReport() {
    //    return self::select('company_name', 'email', 'company_address', 'phone','is_subscribed','id')->orderBy('id', 'asc');

    // }


        public function getCompanyReport() {

       return self::select('order_voucher.voucher_unique_id','company.company_name', 'company.email', 'company.company_address', 
        'company.phone','company.is_subscribed','company.id','company_business_profile.store_location','company_business_profile.merchant_pin')
        ->leftjoin('company_business_profile', 'company_business_profile.company_id','=','company.id')
        ->leftjoin('order_voucher', 'company_business_profile.id', '=', 'order_voucher.company_business_profile_id')
        ->groupBy('company.id')
        ->orderBy('company.id', 'asc');
       // ->whereIn('order_voucher.voucher_unique_id'); 


    }

    // public function getCompanyReport() {
    
    //     $allvouchers=DB::table('order_voucher')->select('voucher_unique_id')->get();
    //     //dd($allvouchers[$i]->voucher_unique_id);
      
    //    // dd($allvouchers[0]->voucher_unique_id);
    //     return DB::table('order_voucher as ov')
    //        ->select('ov.voucher_unique_id','c.company_name','c.email','c.company_address','c.phone','c.is_subscribed','c.id')
    //        ->leftjoin('company_business_profile as cbp','cbp.id','=','ov.company_business_profile_id')
    //        ->leftjoin('company as c','c.id','=','cbp.company_id');
    //       // ->whereIn('ov.voucher_unique_id',explode(',',$allvouchers[0]->voucher_unique_id));
    //         //->where('ov.voucher_unique_id','=',3933159059);
    //         //->groupBy('c.id');
    // }

    // public function getCompanyReport() {

    //    return self::select('order_voucher.voucher_unique_id','company.company_name', 'company.email', 'company.company_address', 
    //     'company.phone','company.is_subscribed','company.id')
    //     ->rightjoin('company_business_profile', 'company.id','=','company_business_profile.company_id')
    //     ->rightjoin('order_voucher','order_voucher.company_business_profile_id','=', 'company_business_profile.id')
    //     ->groupBy('company.id','order_voucher.id')
    //    // ->groupBy('order_voucher.id')
    //     ->distinct('company.company_name')
    //     ->orderBy('company.id', 'asc');
    //    // ->whereIn('order_voucher.voucher_unique_id');


    // }
    
    public function getCompanyList() {
       return self::select('first_name','last_name', 'email','id','is_subscribed')->orderBy('id', 'asc');

    }
    public function activateCompany($id, $value) {
        self::where('id', $id)->update(array('is_subscribed' => $value));        
        if($value == 2){
           $updata=array('is_approved' => $value);
           CompanyBusinessProfile::where('company_id', '=', $id)->where('is_approved', '<>',0)
                   ->update($updata);
           CompanyBusinessProfileAudit::where('company_id', '=', $id)->where('is_approved', '<>',0)
                   ->update($updata);

        }   
        return 1;
    }
    
    public function updateCompanyData($id,$data) {
       
        $company_array = $this->getCompanyById($id);
        $company_array->first_name = $data['first_name'];
        $company_array->last_name = $data['last_name'];
        $company_array->company_name = $data['trading_name'];
        $company_array->contact_email = $data['contact_email'];
        $company_array->country_code = $data['country_code'];
        $company_array->acn_abn_number = $data['abn_acn_number'];
        $company_array->account_name = $data['account_name'];
        $company_array->bsb = $data['bsb'];
        $company_array->account_number = Crypt::encrypt($data['account_number']);
        $company_array->company_address = $data['company_address'];
        $company_array->country_code = $data['country_code'];
        $company_array->contact_email = $data['contact_email'];
        $company_array->phone = $data['contact_number'];
        $company_array->category_id = $data['category'];
        $company_array->state_id = $data['state'];
        $company_array->suburs_id = $data['suburb'];
        $company_array->postal_code = $data['post_code'];
        $company_array->accounts_email = $data['accounts_email'];
        $company_array->save();

    }
public function getCompanyAuditDetails($param) {
        $companybusiness = CompanyBusinessProfileAudit::find($param);
        $companybusinessimages = CompanyBusinessImagesAudit::where('company_business_profile_id', $param)->get();
        $companytiming = CompanyTimingsAudit::where('company_business_profile_id', $param)->get();
        foreach ($companytiming as $rec) {
            $companytiming[$rec->day] = $rec->timings;
        }

        $companybusinesslocation = DB::table('company_business_profile_audit')
                        ->select('company_business_profile_audit.id', 'company_business_profile_audit.address as address', 'states.state_name', 'suburbs.locality')
                        ->join('states', 'states.id', '=', 'company_business_profile_audit.state_id')
                        ->join('suburbs', 'suburbs.id', '=', 'company_business_profile_audit.city_id')
                        ->where('company_business_profile_audit.company_id', '=', $companybusiness->company_id)
                        ->where('company_business_profile_audit.is_subscribed', '1')->get();
        $statename = States::where('id', $companybusiness->state_id)->pluck('state_name');
        $cityname = Suburbs::where('id', $companybusiness->city_id)->pluck('locality');
        $companyname = self::where('id', $companybusiness->company_id)->pluck('company_name');

        return array('companybusiness' => $companybusiness, 'companybusinessimages' => $companybusinessimages, 'companytiming' => $companytiming, 'companybusinesslocation' => $companybusinesslocation, 'statename' => $statename, 'cityname' => $cityname, 'companyname' => $companyname);
    }
    public function checSupplierEmail($email,$company_id)
    {
        if(!empty($email)) {
      $count_company =  self::where('email', $email)->where('id',$company_id)->count();
      if($count_company!=0)
      {
       return;    
      }
      else
      {
        return  self::where('email', $email)->first(); 
      }
      
        }
        
    }
    public function checkCompanyName($companyname,$company_id=0)
    {
        if(!empty($companyname))
        {
        $countcomname =  self::where('company_name',$companyname)->where('id',$company_id)->count(); 
        
    if($countcomname!=0)
      {
       return;    
      }
      else
      {
        return  self::where('company_name',$companyname)->count();
      }
    
      } 

  }

        public function updateVendorData($id,$data) {//code by sandeep date-17-10-2016 for update vendor
        //dd($data);
        $vendor_array = $this->getCompanyById($id);

        $vendor_array->first_name = $data['first_name'];
        $vendor_array->last_name = $data['last_name'];
        $vendor_array->company_name = $data['trading_name'];
        $vendor_array->contact_email = $data['contact_email'];
        $vendor_array->country_code = $data['country_code'];
        $vendor_array->acn_abn_number = $data['abn_acn_number'];
        $vendor_array->account_name = $data['account_name'];
        $vendor_array->bsb = $data['bsb'];
        $vendor_array->account_number = Crypt::encrypt($data['account_number']);
        $vendor_array->company_address = $data['company_address'];
        $vendor_array->country_code = $data['country_code'];
        $vendor_array->contact_email = $data['contact_email'];
        $vendor_array->phone = $data['contact_number'];
        $vendor_array->category_id = $data['category'];
        $vendor_array->state_id = $data['state'];
        $vendor_array->suburs_id = $data['suburb'];
        $vendor_array->postal_code = $data['post_code'];
        $vendor_array->save();

    }






}
