<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ResendSms extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'resend_sms';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_voucher_id', 'status', 'twillio_id', 'error'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function addNewResendSms($id,$status,$twillio_id,$error="")
    {
        $this->order_voucher_id = $id;
        $this->status = $status;
        $this->twillio_id = $twillio_id;
        $this->error = $error;
        $this->save();
    }

}
