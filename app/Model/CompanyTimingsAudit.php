<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyTimingsAudit extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'company_timings_audit';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['company_business_profile_id','day','timings'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['created_at','updated_at'];
        //public $timestamps = false;
        public function auditDetails($id)
        {
           return self::where('company_business_profile_id', '=', $id)->get();
        }

}
