<?php
function getS3LocalPath()
{
    return public_path().'/'.env('s3_local_folder').'/'.env('s3_env');
}  

function convertLocalS3ToAbsolute($filename) {
    if (strpos($filename,$_SERVER['HTTP_HOST']) !== false) {
        $host = 'http'.(isset($_SERVER['HTTPS']) ? 's' : null).'://'.$_SERVER['HTTP_HOST'];
        return str_replace($host,public_path(),$filename);
    }
    return $filename;
}
?>
