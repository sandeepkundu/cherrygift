<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the Routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
/* User Authentication */
//$log_file=app_path('paypal.log');
//@file_put_contents($log_file,"\r\n".date('Y-m-d h:i:s')." Request - ".print_r($_SERVER,true),FILE_APPEND);

Route::get('users/login', 'Auth\AuthController@getLogin');
Route::post('users/login', 'Auth\AuthController@postLogin');
Route::get('users/logout', 'Auth\AuthController@getLogout');
Route::get('users/register', 'Auth\AuthController@getRegister');
Route::post('users/register', 'Auth\AuthController@postRegister');
Route::get('home', 'HomeController@index');
Route::get('/', 'HomeController@index');

Route::get('/sendfootermail', 'HomeController@sendfootermail');

Route::post('search', 'SearchController@index');
Route::get('search/{type?}/{name?}/{id?}', 'SearchController@index');
Route::post('search_ajax', 'SearchController@search_ajax');
Route::get('rememberSmsUnused', 'OrderController@rememberSmsUnused');
//Route::post('search', 'SearchController@index');
/* Authenticated users */
Route::group(['middleware' => 'auth'], function()
    {
        Route::get('users/dashboard', array('before' => 'auth','as'=>'dashboard', function()
            {
                return View('welcome');
        }));
});
Route::get('users/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('users/auth/facebook/callback/', 'Auth\AuthController@handleProviderCallback');
Route::get('facebookusers', array('as' => 'facebookusers', 'uses' => function(){
    return view('facebookuser');
}));
// Password reset link request Routes...
Route::controllers([
    'password' => 'Auth\PasswordController',
]);
//
///* Edit profile */
Route::get('edit-profile/{id}', array('as' => 'edit', 'uses' => function(){
    return view('edit-profile');
}));

Route::resource('users', 'AccountController');
Route::get('/destroy/{id}', 'AccountController@destroy');
Route::get('getcity','AccountController@cityList');
Route::get('contact-us','EnquiryController@getEnquiry');
Route::post('contact-us','EnquiryController@postEnquiry');
Route::get('contact-thanks',function() {
    return View::make('enquiry/thanks');
});
Route::get('admin/contact-us', function()
    {
        return View::make('admin-pages/contect-us');
});

Route::get('faq', function()
    {
        return View::make('pages/faq');
});
Route::get('admin/faq', function()
    {
        return View::make('admin-pages/faq');
});
Route::get('user/privacy', function()
    {
        return View::make('pages/privacy');
});
Route::get('admin/privacy', function()
    {
        return View::make('admin-pages/privacy');
});
Route::get('user/term-condition', function() {
        return View::make('pages/termcondition');
});
Route::get('admin/term-condition', function()
    {
        return View::make('admin-pages/termcondition-admin');
});
//

//sprint 2 end
// sprint 3
Route::get('admin-signup', function()
    {
        return View::make('admin-pages/signup');
});
Route::get('business', function()
    {
        return View::make('admin-pages/signupterms');
});
Route::post('admin/create', 'AdminController@create'); 
Route::post('admin/login', 'AdminController@postlogin'); //contact-info

Route::get('admin/contact-info', 'AdminController@showContactForm');

Route::post('admin/business-details', 'AdminController@update');
Route::get('admin/business-detail', 'AdminController@showBusinessDetail');
Route::get('admin/business-detail/{a}', 'AdminController@showBusinessDetail');
Route::get('suggesionExample', 'AccountController@suburbsList');
Route::post('admin/update-business-details','AdminController@updateBusinessDetail');
Route::post('admin/update-business-details/{a}','AdminController@updateBusinessDetail');
Route::post('admin/upload-business-images/','AdminController@uploadStoreImages');
Route::get('getemail','AccountController@checkEmail');
Route::get('getadminemail','AccountController@checkAdminEmail');
Route::post('suplier/sendresetmail','AdminController@sendresetmail');
Route::get('supplier/password/reset/{token}','AdminController@supplierReset');
Route::post('supplier/password/reset','AdminController@supplieremailReset');
Route::match(array('GET', 'POST'),'expressCheckoutIPN','PaymentController@expressCheckoutIPN');
Route::get('admin-logout', function() {
    Session::flush();
    return Redirect::to('admin-signup');
});
Route::post('admin/change-password', 'AdminController@changePassword');
Route::get('admin/change-password', 'AdminController@updatePassword'); 
Route::get('payment/notify/{id}','PaymentController@notify');
Route::get('payment/cancel/{id}','PaymentController@cancel');
Route::get('payment/thanks/{id}','PaymentController@thanks');

Route::post('payment/notify/{id}','PaymentController@notify');
Route::post('payment/cancel/{id}','PaymentController@cancel');
Route::post('payment/thanks/{id}','PaymentController@thanks');
Route::post('payment/paypal_ipn','PaymentController@paypal_ipn');
Route::get('payment/cancel/{profile_id}/{id}','PaymentController@cancelSubscription');

// Schedular
Route::get('schedular/remindredeem','VoucherController@remindRedeemVoucher');
Route::get('beforeSubscriptionRenew/{id}','PaymentController@afterSubscriptionRenew');
Route::get('payment/subscribeagain/{id}', function()
    {
        return View::make('admin-pages/payment');
});
Route::get('suplier/company','AdminController@companyExists');
Route::get('voucher-details/{id}','CartController@voucharDetail');
Route::get('edit-voucher-details/{id}','CartController@editVoucharDetail');
Route::get('buy_voucher/{voucher_id}/{price}/{voucher_cart_id?}',"CartController@buyVoucher");
Route::post('post_buy_voucher',"CartController@postBuyVoucher");
Route::get('edit_cart_voucher/{id}/{price}',"CartController@editCartVoucher");
Route::get('delete_voucher/{id}',"CartController@deleteVoucher");
Route::get('cart',"CartController@index");

Route::get('confirmationmail/{mobile_number}',"OrderController@testlink"); //for testing confirmation mail for purchase voucher
Route::get('Tintmce',"CartController@tinymceeditor");//for tinymceeditor testing

Route::post('userordercomplete/{id}',"OrderController@userOrderComplete");
Route::post('userOrderNotify/{id}',"OrderController@userOrderNotify");
Route::get('userordercomplete/{id}',"OrderController@userOrderComplete");
Route::get('pdftest',"OrderController@test");
Route::get('userOrderCancel',"OrderController@userOrderCancel");
Route::get('prePaypal/{cart_id?}',"OrderController@prePaypal");
Route::get('checkSubscriptionStatus',"PaymentController@checkSubscriptionStatus");
Route::get('getUserTimezone/{offset}',"OrderController@getUserTimezone");
Route::get('exception/read', function()
    {
        return View::make('custom_exception');
});
Route::get('order-successful/{id}',"OrderController@orderDetails");
//redeem view
Route::get('redeem/{voucher_id}','VoucherController@show');
Route::get('edit/{id}','VoucherController@edit');
Route::get('testing','VoucherController@testing');
//Route::post('validate/merchant_pin','VoucherController@update');
Route::match(array('GET', 'POST'),'validate/merchant_pin','VoucherController@update');// code added by sundeep
Route::get('user/history','OrderController@userPurchaseHistory');

Route::get('resend', function()
    {
        return View::make('resend');
});

Route::post('resend','VoucherController@reSendVouchers');
Route::get('send_all','VoucherController@reSendAll');
Route::get('smsScheduler','OrderController@schedulerForSendSMS');

// Super admin CMS


Route::group(['prefix' => 'super-admin'], function () { 

    Route::group(['middleware' => ['cmsauth']], function () {  
        Route::get('dashboard','CmsController@dashboard');
        Route::get('profile','CmsController@profile');
        Route::get('company','CmsController@customer');
        Route::get('enquiry','CmsController@enquiry');
        Route::get('voucher_history','CmsController@companylist');
        Route::get('customerdata','CmsController@customerList');
        Route::get('enquirydata','CmsController@enquiryList');
        Route::get('enquirydata/{id}','CmsController@enquiryDetails');
        Route::get('viewvoucherhistory','CmsController@getRedeemedHistoryByCompany');
        Route::get('test-invoice-email','CmsController@getTestInvoiceEmail');
        Route::post('view-customer','CmsController@viewCustomer');
        Route::post('save-customer','CmsController@saveCustomer');
        // code added by sundeep
        Route::post('view-user','CmsController@viewUser');
        Route::post('view-vendor','CmsController@viewVendor');
        Route::post('save-user','CmsController@saveUser');
        Route::post('delete-user','CmsController@deleteUser');
        Route::post('delete-company','CmsController@deleteCompany');
        Route::post('save-vendor','CmsController@saveVendor');
        Route::post('delete-vendor','CmsController@deleteVendor');
        //code end by sandeep
        Route::post('reject-customer','CmsController@rejectCustomer');
        Route::post('reject-store','CmsController@rejectStore');
        Route::match(array('GET', 'POST'),'customer-action','CmsController@customerAction');
        Route::match(array('GET', 'POST'),'store-action','CmsController@storeAction');
        // code added by sundeep
        Route::match(array('GET', 'POST'),'user-action','CmsController@userAction');
        Route::match(array('GET', 'POST'),'vendor-action','CmsController@vendorAction');
        //code end by sandeep
        Route::get('store/{id}','CmsController@store');
        Route::get('voucher_id/{id}/{month?}/{year?}','CmsController@voucherList');
        Route::get('storedata/{id}','CmsController@storeList');
        Route::get('voucherhistorydata/{id}/{month?}/{year?}','CmsController@voucherHistoryList');
        Route::get('store-detail/{id}','CmsController@storeDetail');
        Route::get('slider-images','CmsController@homepageSliderImages');
        Route::get('recipient-images','CmsController@homepageRecipientImages');
        Route::post('upload-slider-images','CmsController@uploadSliderImages');
        Route::post('upload-recipient-images','CmsController@uploadRecipientImages');
        Route::get('delete-slider-images/{id}','CmsController@deleteSliderImages');
        Route::get('store-voucher/{id}','CmsController@storeVoucher');
        Route::get('voucherdata/{id}','CmsController@storeVoucherList');
        Route::match(array('GET', 'POST'),'voucher-action','CmsController@voucherAction');
        Route::match(array('GET', 'POST'),'redeemed-action','CmsController@redeemedAction');// code added by sundeep
        Route::match(array('GET', 'POST'),'redeemed-submit','CmsController@redeemedSubmit');// code added by sundeep
        // code added by sundeep
        Route::get('users','CmsController@users');
        Route::get('usersdata','CmsController@usersdata');
        Route::get('vendors','CmsController@vendors');
        Route::get('vendorsdata','CmsController@vendorsdata');

        Route::post('resetPassword','CmsController@resetPassword');
        //code end by sandeep
        //Route::post('/resetPassword','CmsController@resetPassword');
        Route::get('users-csv-download','CmsController@downloadUsersCSV');
        Route::get('vendor-csv-download','CmsController@downloadVendorCSV');
        Route::get('admin-vendor-payment/{id?}/{year?}','CmsController@adminVendorPayment');
        Route::get('admin-vendor-payment-list/{id?}/{year?}','CmsController@adminVendorPaymentList');
        Route::get('admin-cms/pdf_form','CmsController@pdf_form');
        //Route::get('admin-vendor-generatePdf','CmsController@createPDF');
        Route::get('pdf_voucher_id/{id}/{month?}/{year?}','CmsController@voucherListPdf');
        Route::get('postmail/{id}/{month?}/{year?}','CmsController@postmail');
        Route::post('change-payment-status','CmsController@changeVendorPaymentStatus');

        Route::get('subscription-fee','SubscriptionCmsController@index');
        Route::get('make-cherrygift-voucher','SubscriptionCmsController@makeCherryVoucher');
        Route::post('subscription-update','SubscriptionCmsController@subscriptionUpdate');
        Route::post('cherrygift-voucher-update','SubscriptionCmsController@cherrygiftVoucherUpdate');
        Route::get('redeemed_history/{id?}/{year?}','CmsController@getRedeemed');
        Route::get('redeemeddata/{id?}/{year?}','CmsController@getRedeemedData');
        Route::get('storeStatusList','CmsController@storeStatusList');
        Route::get('storeStatusListData','CmsController@storeStatusListData');
        Route::post('saveImgUrl','CmsController@saveImgUrl');

        Route::get('recover-voucher','SubscriptionCmsController@makeCherryRecoverVoucher');//coded by sandeep
        Route::match(array('GET', 'POST'),'cherrygift-recover-voucher-update','SubscriptionCmsController@cherrygiftRecoverVoucherUpdate');
        //Route::post('cherrygift-recover-voucher-update','SubscriptionCmsController@cherrygiftRecoverVoucherUpdate');//coded by sandeep
        Route::post('postresendvouchers','SubscriptionCmsController@postresendvouchers');//coded by sandeep
        Route::get('payment-details','CmsController@paymentdetails');//coded by sandeep
        Route::get('paymentDetailsData','CmsController@paymentDetailsData');//coded by sandeep
    });
    Route::get('renewal','SubscriptionCmsController@subscriptionRenewal'); 
    Route::match(array('GET', 'POST'),'','CmsController@index'); 
    Route::match(array('GET', 'POST'),'login','CmsController@index');
});

Route::get('/admin/reports/{a}/{month}/{year}','ReportController@show');
Route::post('/super-admin/update-business-details/{a}','CmsController@updateBusinessDetail');
Route::post('resendNotify','VoucherController@resendNotify');
//Route::post('resendPaymentComplete','VoucherController@resendPaymentComplete');// code comment by sundeep
Route::match(array('GET', 'POST'),'resendPaymentComplete', 'VoucherController@resendPaymentComplete');// code added by sundeep
//Route::post('resendsavebeforepaypal','VoucherController@resendsavebeforepaypal');
Route::match(array('GET', 'POST'),'resendSaveBeforePaypal','VoucherController@resendSaveBeforePaypal');

Route::get('super-admin/logout','CmsController@getLogout');
Route::get('/payment/subscribe/{id}', 'PaymentController@subscribe');
Route::get('/payment/request-invoice/{id}', 'PaymentController@requestInvoice');
Route::match(array('GET', 'POST'),'payment/success/{id}', 'PaymentController@paymentSuccess');
Route::get('super-admin/getRedeemedDataInCSV/{id?}/{year?}','CmsController@getRedeemedDataInCSV');
Route::get('deleteimagesbyid','AdminController@deleteImageById');
Route::get('getesuppliermail','AdminController@checkSupplierEmail'); 
Route::get('getstorename','AdminController@checkStoreName'); 
Route::get('gettradename','AdminController@getTradeName');