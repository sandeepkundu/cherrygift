<?php

namespace App\Http\Controllers;

use App\Model\SubscriptionPrice;
use View;
use Session;
use Lang;
use Redirect;
use Illuminate\Support\Facades\Input;
use Config;
use App\Http\Controllers\PaymentController;
use App\Model\CompanyBusinessProfileAudit;
use App\Model\Payments;
use App\Model\CompanyBusinessProfile;
use App\Model\OrderVoucher;
use Auth;
use Request;
use App\Model\Order;

class SubscriptionCmsController extends Controller {

    public $id = 1;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        $subscription = new SubscriptionPrice();
        $subs = $subscription->getSubscriptionDetails($this->id);
        return View::make('admin-cms/subscription', compact('subs'));
    }

    public function subscriptionUpdate() {
        $data = Input::all();
        unset($data['_token']);
        $subscription = new SubscriptionPrice();
        $subscription->updateSubscriptionDetails($this->id, $data);
        Session::put('success-msg', Lang::get('cms.subscriptionupdate'));
        return Redirect('super-admin/subscription-fee');
    }

    public function subscriptionRenewal() {
        $subscription = new SubscriptionPrice();
        $subs = $subscription->getSubscriptionDetails();
        $amount = $subs->sub_price;
        $today=date('Y-m-d');
        $effectiveDate = date('Y-m-d', strtotime("+6 months", strtotime($subs->updated_at)));
        if(date('Y-m-d',  strtotime($subs->updated_at))== $today || $effectiveDate==$today)
        {
            $profile_audit = new CompanyBusinessProfileAudit();
            $storeids = $profile_audit->getUpdateAmountStore();
            $storeArray = array();
            foreach ($storeids as $store) {
                $storeArray[] = $store->id;
            }

            if (!empty($storeArray)) {
                $payment = new Payments();
                $paysubsids = $payment->getPaymentProfile($storeArray);
                if (!empty($paysubsids)) {
                    foreach ($paysubsids as $paysub) {
                        $profile_id = $paysub->susbscriber_id;
                        $this->subscriptionRenewalUpdate($profile_id, $amount);
                    }
                }
            }
        }
        else{
            echo 'no updates';
        }
    }

    public function subscriptionRenewalUpdate($profile_id, $amount) {
        $paypal_username = Config::get('constants.paypal_username');
        $paypal_password = Config::get('constants.paypal_password');
        $paypal_signature = Config::get('constants.paypal_signature');

        $data = array(
            'USER' => $paypal_username,
            'PWD' => $paypal_password,
            'SIGNATURE' => $paypal_signature,
            'METHOD' => 'UpdateRecurringPaymentsProfile',
            'VERSION' => '108',
            'PROFILEID' => $profile_id,
            'NOTE' => 'An optional note explaining the reason for the change',
            'AMT' => $amount,
            'CURRENCYCODE' => 'AUD'
        );
        $paycont = new PaymentController();
        $response = $paycont->postDataCurl($data);
        parse_str($response, $nvp);

        //print_r($nvp);
    }

    public function cancelStoreSubscription($profile_id, $id) {
        $paypal_username = Config::get('constants.paypal_username');
        $paypal_password = Config::get('constants.paypal_password');
        $paypal_signature = Config::get('constants.paypal_signature');
        $action = "Cancel";
        $data = array(
            'USER' => $paypal_username,
            'PWD' => $paypal_password,
            'SIGNATURE' => $paypal_signature,
            'VERSION' => '108',
            'METHOD' => 'ManageRecurringPaymentsProfileStatus',
            'PROFILEID' => $profile_id,
            'ACTION' => $action,
            'NOTE' => 'Profile cancelled at cherrygift'
        );
        $paycont = new PaymentController();
        $response = $paycont->postDataCurl($data);
        parse_str($response, $nvp);
        if (isset($nvp["ACK"]) && $nvp["ACK"] == "Success") {
            $payment_profile = Payments::where("susbscriber_id", $profile_id)->first();
            $payment_profile->status = 0;
            $payment_profile->save();
            $data = array('is_subscribed' => 2, 'is_payment' => 0);
            $profile_audit = new CompanyBusinessProfileAudit();
            $profile_audit->updateDetails($payment_profile->user_id, $data);
        } else {
            //print_r($nvp);
        }
    }

    public function cancelStore($id, $acttype) {
        if ($acttype == 'company') {
            $profile_audit = new CompanyBusinessProfileAudit();
            $storeids = $profile_audit->getCompanyStore($id);
            $storeArray = array();
            foreach ($storeids as $store) {
                $storeArray[] = $store->id;
            }
        } else {
            $storeArray[] = $id;
        }
        if (!empty($storeArray)) {
            $payment = new Payments();
            $paysubsids = $payment->getPaymentProfile($storeArray);
            if (!empty($paysubsids)) {
                foreach ($paysubsids as $paysub) {
                    $profile_id = $paysub->susbscriber_id;
                    $this->cancelStoreSubscription($profile_id, $this->id);
                }
            }
        }
    }
public function makeCherryVoucher() {
        
        return View::make('admin-cms/makecherryvoucher');
    }

    public function makeCherryRecoverVoucher() {
        
        return View::make('admin-cms/makecherryrecovervoucher');
    }


    public function cherrygiftVoucherUpdate()
    {
         $cherrygift_voucher = new CompanyBusinessProfile();
         $cherrygift_voucher_audit = new CompanyBusinessProfileAudit();
         $update_voucher = $cherrygift_voucher->updateVoucherByMerchantPIN(Input::get('cherrygift_voucher')); 
                           $cherrygift_voucher_audit->updateVoucherByMerchantPIN(Input::get('cherrygift_voucher')); 
         if($update_voucher=="updated")
         {
             return Redirect('super-admin/make-cherrygift-voucher')->withMessage(Lang::get('message.success'));
             
         }
         else
         {
          return Redirect('super-admin/make-cherrygift-voucher')->withMessage(Lang::get('message.invalideMerchantPIN'));  
         }
    }

    public function cherrygiftRecoverVoucherUpdate()
    {

          // dd(Auth::user()->id);
             $mobile_number =  Input::get('cherrygift_voucher');
             //dd($mobile_number);
             $order = new OrderVoucher();
             $purshase_history = $order->getVouchersBysuperadminwithoutpaypal($mobile_number, Auth::user()->id);
              //  dd($purshase_history);
           // return $purshase_history;//or get return json_encoded type
           // dd($purshase_history[0]['phone_number']);
             // print_r(Auth::user()->id);
              @extract($purshase_history);
            return View::make('admin-cms/makecherryrecovervoucher', compact("purshase_history","mobile_number"))->withInput(Input::all());


        
    }

     public function resendAllVouchersbyid($user_info){
           // dd($user_info);
             $order = new OrderVoucher();
             $purshase_history = $order->getVouchersDataArrayByid($user_info);
            //  dd($purshase_history );
             $orderObj = new Order();

             $error = $orderObj->resendBulkSms($purshase_history);
            // dd($error);
             // return $error;
    }
    public function postresendvouchers() {
        //dd($data = Request::all());

       if(!empty($data = Request::all())){
         @extract($data);
         // dd($data);
         $errors = $this->resendAllVouchersbyid($voucher_ids);
         if(!empty($errors)) {
           //dd('errors');
                 return Redirect('super-admin/cherrygift-recover-voucher-update')->withErrors(['errors' => $errors]);   
                }else{
                   // dd('send successfully');
                    return Redirect('super-admin/cherrygift-recover-voucher-update')->with('message', Lang::get('message.voucherSent'));
                }

       }else{

        return Redirect('super-admin/cherrygift-recover-voucher-update')->withErrors(['errors' => Lang::get('message.somethingwrong')]); 
       }
    }


}
