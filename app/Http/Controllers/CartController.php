<?php

namespace App\Http\Controllers;

//use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use App\Model\CompanyBusinessImages;
use App\Model\CompanyBusinessProfile;
use App\Model\Company;
use App\Model\UserCart;
use Session;
use View;
use Redirect;
use App\Model\CartVouchers;
use Illuminate\Support\Facades\Lang;
use DateTime;
use DateTimeZone;

class CartController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        $url = Request::url();
        if (!Auth::check()) {
            Session::put('befor_login_url', $url);
            return redirect('users/login')->withErrors([
                        'email' => Lang::get('message.login'),
            ]);
        }

        $loggedin_user = Auth::user();

        $usercartinfo = new UserCart();
         $total_cart_info = $usercartinfo->getCartRow($loggedin_user->id); 
        $cart_tax = env('CART_TAX');
        if (empty($total_cart_info)) {
            $total_cart_price = 0;
            $cart_id = 0;
        } else {
             $total_cart_price = $total_cart_info->total_price + ($total_cart_info->total_price * $cart_tax);
             $cart_id = $total_cart_info->id;
            $getusercartinfo = $usercartinfo->getUserCartInfo(); 
            if(empty($getusercartinfo))
                {$getusercartinfo=0;} 
            else {
                @extract($getusercartinfo); 
            }
        }
        return View::make('cart', compact('getusercartinfo', 'total_cart_price', 'cart_id'));
    }

    public function buyVoucher($voucher_id, $price, $voucher_cart_id = "") {
        $voucher_id=  intval($voucher_id);
        $price = intval($price);
     
        if(!empty($voucher_cart_id))
        {
        // edit cart voucher 
          $voucher_cart_id= intval($voucher_cart_id);  
        }

        $url = Request::url();
        if (!Auth::check()) {
            Session::put('befor_login_url', $url);
            return redirect('users/login')->withErrors([
                        'email' => Lang::get('message.login'),
            ]);
        }

        $data['image'] = "";
        $data['company_name'] = "";
        $data['user_price'] = "";
        $data['company_logo'] = "";
        $data['voucher_price'] = "";
        $data['company_business_profile_id'] = "";
        $valid_cart_voucher = true;

        if (!empty($voucher_cart_id)) {
            $user_cart = new UserCart();
            $loggedin_user = Auth::user();
            $cart_voucher = $user_cart->get_cart_voucher($voucher_cart_id, $loggedin_user->id);
            if (!empty($cart_voucher)) {
                // data for populate form
                $data['voucher_cart_id'] = $voucher_cart_id;
                $data['phone_number'] = $cart_voucher->phone_number;
                $data['sms_message'] = $cart_voucher->sms_message;
                $data['sms_date'] = $cart_voucher->sms_date;
                $data['sms_hour'] = $cart_voucher->sms_hour;
                $data['sms_minutes'] = $cart_voucher->sms_minutes;
            } else {
                $valid_cart_voucher = false;
            }
        }

        $user_price = $price;
        $model_company_business_profile = new CompanyBusinessProfile();
        $company_business_profile = $model_company_business_profile->getInfo($voucher_id);
       // dd($company_business_profile);
        if (!empty($company_business_profile) && ($company_business_profile->min_vouchar_amount <= $user_price && $company_business_profile->max_vouchar_amount >= $user_price) && $valid_cart_voucher) {

            $company_business_image = new CompanyBusinessImages();
            $voucher_image = $company_business_image->getVoucherImage($voucher_id);
            $company_id = $company_business_profile->company_id;
            $obj_company = new Company();
            $company = $obj_company->getCompanyById($company_id);


            if (!empty($voucher_image->image))
            $data['image'] = $voucher_image->image;
            $data['company_name'] = $company->company_name;
            $data['user_price'] = $user_price;
            $data['company_logo'] = $company_business_profile->logo;
            $data['voucher_price'] = $user_price;
            $data['company_business_profile_id'] = $voucher_id;
            $data['store_location'] = $company_business_profile->store_location;
        }
        else {
            $data['invalid_voucher'] = 1;
        }
        $data['url'] = $url;
        $data['no_search_bar'] = 1;
        return view('buy_voucher', $data);
    }

    public function postBuyVoucher() {
        //print_r(Input::all()); exit;
        if (Request::isMethod('post')) {

            $user_cart = new UserCart();
            $loggedin_user = Auth::user();
            $cart = array();
            $voucher_cart_id = Request::input("cart_voucher_id");
            $voucher_id = Request::input("company_business_profile_id");
            $voucher_price = Request::input("voucher_price");
            $voucher_timezone = Request::input("sms_timezone");
            $voucher_timezone_offset = Request::input("sms_timezone_offset");
            
            $cart["voucher_cart_id"] = $voucher_cart_id;
            $cart["voucher_price"] = $voucher_price;
            $cart["id"] = $loggedin_user->id;
            $cart["sms_schedule"] = (!empty(Request::input("sms_schedule"))) ? 1 : 0;
            $cart["sms_date"] = (!empty(Request::input("sms_schedule"))) ? Request::input("sms_date") : date("Y-m-d");
            $cart["phone_number"] = Request::input("phone_number");
            $cart["sms_txt"] = Request::input("sms_txt");
            $cart["company_business_profile_id"] = $voucher_id;
//        $user_cart->voucher_link = "test link";
            $cart["sms_hour"] = Request::input("sms_hour");
            $cart["sms_minutes"] = Request::input("sms_minutes");
            $cart["utc_sms_date"] = "";
            $cart["utc_sms_hour"] = "";
            $cart["utc_sms_minutes"] = "";
            if(!empty($cart["sms_schedule"])) {
                //echo $voucher_timezone."//".$voucher_timezone_offset;die;
                if(!empty($voucher_timezone) && !empty($voucher_timezone_offset)) {
                    
            date_default_timezone_set($voucher_timezone);
            $utc_datetime = $cart["sms_date"]." ".$cart["sms_hour"].":".$cart["sms_minutes"].":00";
            $dt = new DateTime($utc_datetime);
            $tz = new DateTimeZone('UTC');
            $dt->setTimezone($tz);
            $cart["utc_sms_date"] = $dt->format('Y-m-d');
            $cart["utc_sms_hour"] = $dt->format('H');
            $cart["utc_sms_minutes"] = $dt->format('i');
                } else {
            $cart["utc_sms_date"] = $cart["sms_date"];
            $cart["utc_sms_hour"] = $cart["sms_hour"];
            $cart["utc_sms_minutes"] = $cart["sms_minutes"];
                }
                
            } 
//            echo "<pre>";
//            print_r($cart);die;
            if (empty($voucher_cart_id))
                $res = $user_cart->save_cart($cart);
            else
                $res = $user_cart->updateUserCart($cart);
            if ($res) { 
                return redirect('cart');
            } else 
                return Redirect::back()->withErrors(['message' =>  Lang::get('message.addingvouchererror')]);
        }
    }

    public function voucharDetail($store_location) {
        //dd($store_location);
        $company = new Company();
        $companydata = $company->getCompanyDetailsStoreLocation($store_location);
        if($companydata==null)
           return  Redirect::back()->withErrors(['message' =>'Not a valid voucher']);
        @extract($companydata);
        $preview = 0;
        return View::make('admin-pages.companypreview', compact('companybusinessimages', 'companybusiness', 'statename', 'cityname', 'companyname', 'companybusinesslocation', 'companytiming', 'preview'));
    }

    public function editVoucharDetail($param) {
        $user_cart = new UserCart();
        $loggedin_user = Auth::user();
        $cart_voucher = $user_cart->get_cart_voucher($param, $loggedin_user->id);
        $preview = 0;
        $edit_cart_voucher = 1;
        if (!empty($cart_voucher)) {
            $cart_voucher_price = $cart_voucher->price;
            $company = new Company();
            $companydata = $company->getCompanyDetails($cart_voucher->company_business_profile_id);
            @extract($companydata);

            $cart_voucher_id = $param;
            return View::make('admin-pages.companypreview', compact('companybusinessimages', 'companybusiness', 'statename', 'cityname', 'companybusinesslocation', 'companyname', 'companytiming', 'preview', 'cart_voucher_price', 'edit_cart_voucher', 'cart_voucher_id'));
        } else {
            $invalid = 1;
            return View::make('admin-pages.companypreview', compact("invalid", 'preview'));
        }
    }

    public function deleteVoucher($voucharid) {
        $vouchar = new CartVouchers();
        $deletevouchar = $vouchar->deleteVoucharDetails($voucharid);
        if ($deletevouchar) {
            return "sucess";
            //$company = new Company();
            //$companydata=$company->getCompanyDetails(Auth::user()->id);
        }
    }


    public function tinymceeditor(){
        //dd('asasas');
         return View::make('Tinymceeditor');
    }
    

}
