<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Model\City;
use App\Model\Search;
use App\Model\Suburbs;
use View;

class SearchController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index($type = null,$name=null, $id = null) {
        $res = array();
        $city_model = new City();
        $Suburbs_model = new Suburbs();
        $search_model = new Search();
        $state_city_map = $Suburbs_model->statecitylist();
        $param_array = array();
        $param_array['txt'] = trim(Request::Input('search_txt'));
        $param_array['search_state'] = Request::Input('search_state');
        $param_array['search_city'] = Request::Input('search_city');
        $param_array['url_param_type'] =  $type;
        $param_array['url_param_id'] =  $id;
        // dd($param_array);
        $res = $search_model->searchIndex($param_array);        
        $data['voucher_arr'] = $res;
        $data['state_city_map'] = $state_city_map;
        $data['url_param_type'] = $type;
        $data['url_param_id'] = $id;
        $data['url_param_name'] = $name;
        //print_r($state_city_map);die;
        //print_r($data);die;
        //dd($param_array);
        return View('search', $data);
    }

    public function search_ajax() {

        $search_model = new Search();

        $filter = Request::Input('filter_json');
        $param_array = array();
        $param_array['txt'] = !empty($filter['search']['search_txt']) ? trim($filter['search']['search_txt']) : '';
        $param_array['search_state'] = !empty($filter['search']['search_state']) ? $filter['search']['search_state'] : '';
        $param_array['search_city'] = !empty($filter['search']['search_city']) ? $filter['search']['search_city'] : '';
        $param_array['url_param_type'] = !empty($filter['search']['url_param_type']) ? $filter['search']['url_param_type'] : '';
        $param_array['url_param_id'] = !empty($filter['search']['url_param_id']) ? $filter['search']['url_param_id'] : '';

        $filter_cat = !empty($filter['cat']) ? $filter['cat'] : array();
        $filter_state = !empty($filter['state']) ? $filter['state'] : array();
        $filter_city = !empty($filter['city']) ? $filter['city'] : array();
        $filter_oca = !empty($filter['oca']) ? $filter['oca'] : array();
        $filter_rec = !empty($filter['rec']) ? $filter['rec'] : array();

        $filter_state_arr = array();
        foreach ($filter_state as $state) {
            $param_array['filter_state_arr'][] = $state['id'];
        }

        $filter_cat_arr = array();
        foreach ($filter_cat as $cat) {
            $param_array['filter_cat_arr'][] = $cat['id'];
        }

        $filter_city_arr = array();
        foreach ($filter_city as $city) {
            $param_array['filter_city_arr'][] = $city['id'];
        }

        $filter_oca_arr = array();
        foreach ($filter_oca as $oca) {
            $param_array['filter_oca_arr'][] = $oca['id'];
        }

        $filter_rec_arr = array();
        foreach ($filter_rec as $rec) {
            $param_array['filter_rec_arr'][] = $rec['id'];
        }

        // print_r($param_array);
        $res = $search_model->searchAjax($param_array);

        return json_encode($res);
        die;
    }


}
