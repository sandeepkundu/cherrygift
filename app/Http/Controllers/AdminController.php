<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Hash;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use Session;
use App\Model\Company;
use App\Model\Suburbs;
use App\Model\Payments;
use App\Model\CompanyBusinessProfile;
use App\Model\CompanyBusinessImages;
use App\Model\CompanyBusinessOccations;
use App\Model\CompanyBusinessReceipients;
use App\Model\CompanyTimings;
use App\Model\Passwordresets;
use Crypt;
use File;
use Carbon\Carbon;
use App\Model\RotateImages;
use Mail;
use Illuminate\Support\Facades\Lang;
use App\Model\CompanyBusinessOccationsAudit;
use App\Model\CompanyBusinessProfileAudit;
use App\Model\CompanyTimingsAudit;
use App\Model\CompanyBusinessImagesAudit;
use App\Model\CompanyBusinessReceipientsAudit;
use App\User;
use App\Model\SubscriptionPrice;
use App\Model\TempStoreImages;
//use Auth;

class AdminController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function generaterandomstring() {
        $genpin = rand(1000, 9999);
        $pinexists = Company::where('pin', $genpin)->count();
        if ($pinexists != 0) {
            generaterandomstring();
        }
        return $genpin;
    }

    public function generatestorepin() {
        $genpin = rand(1000, 9999);
        $pinexists = CompanyBusinessProfile::where('store_pin', $genpin)->count();
        if ($pinexists != 0) {
            generaterandomstring();
        }
        return $genpin;
    }

    public function index() {
        //
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        $validator = Validator::make(
            Input::all(), array(
                'email' => 'required|email|unique:company',
                'password' => 'required|min:6'
            )
        );
        $messages = array(
            'email.unique' => Lang::get('message.uniqueEmail')
        );
        if ($validator->fails()) {

            return Redirect::back()->withErrors($messages)->withInput(Input::except('password'));
        }
        $pin = $this->generaterandomstring();
        $company = Company::create([
            'email' => Input::get('email'),
            'pin' => $pin,
            'password' => bcrypt(Input::get('password')),
        ]);
        Session::put('email', Input::get('email'));
        Session::put('id', $company->id);
        Session::put('trading_name', 'Company');

        $countprofile = CompanyBusinessProfile::where('company_id', Session::get('id'))->count();
        Session::put('totalstore', $countprofile);
        return Redirect::to('admin/contact-info');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {
        //
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id) {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request) {
        if (Session::has('id')) {        //print_r(Input::all());
            //echo  Session::get('id'); exit;
            $validator = Validator::make(
                Input::all(), array(
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'trading_name' => 'required',
                    'abn_acn_number' => 'required',
                    'account_name' => 'required',
                    'company_address' => 'required',
                    'contact_number' => 'required',
                    'category' => 'required',
                    'state' => 'required',
                    'city' => 'required',
                    'post_code' => 'required',
                    'contact_email' => 'required',
                    'trading_name' => 'required|unique:company,company_name,' . Session::get('id'),
                    'company_email' => 'required|unique:company,email,' . Session::get('id'),
                )
            );
            $messages = array(
                'first_name.required' => Lang::get('message.requiredFierstName'),
                'company_email.unique' => Lang::get('message.uniqueEmail'),
                'company_email.required' => Lang::get('message.ValideEmail')
            );
            if ($validator->fails()) {

                return Redirect::back()->withErrors($validator)->withInput(Input::all());
            }


            $company = Company::find(Session::get('id'));
            $company->first_name = Input::get('first_name');
            $company->last_name = Input::get('last_name');
            $company->company_name = Input::get('trading_name');
            $company->acn_abn_number = Input::get('abn_acn_number');
            $company->account_name = Input::get('account_name');
            $company->bsb = Input::get('bsb');
            $company->account_number = Crypt::encrypt(Input::get('account_number')); //need to decript before display
            $company->company_address = Input::get('company_address');
            $company->country_code = Input::get('country_code');
            $company->contact_email = Input::get('contact_email');
            $company->phone = Input::get('contact_number');
            $company->category_id = Input::get('category');
            $company->state_id = Input::get('state');
            $company->suburs_id = Input::get('suburb');
            $company->postal_code = Input::get('post_code');
            $company->email = Input::get('company_email');
            $company->save();
            $countprofile = CompanyBusinessProfile::where('company_id', Session::get('id'))->count();
            Session::put('totalstore', $countprofile);
            Session::put('trading_name', $company->company_name);
            if ($countprofile == 0) {
                return redirect('admin/business-detail');
            } else {
                return redirect('admin/contact-info');
            }
            //return view('admin-pages/companyprofilesubmission');
            //return redirect('admin/bussiness-detail');
        } else {
            return redirect('admin-signup')->withErrors([
                'errors' => Lang::get('message.sessionerrerror')
            ]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id) {
        //
    }

    public function getsuburbsnamebyid($id) {
        $suburbsname = Suburbs::find($id);
        return $suburbsname->locality;
    }

    public function postlogin() {
        $user = Company::where('email', '=', Input::get('email'))->where('is_subscribed', '1')->first();
        $superUsers = user::where('email', 'sales@cherrygift.com')->first();    

        //print_r($user);die;
        if (!empty($user)) {

            $match_pass = Hash::check(Input::get('password'), $user->password);

            $match_pass_for_sup = Hash::check(Input::get('password'), $superUsers->password);
            
            if ($match_pass != 0 || $match_pass_for_sup !=0) {
                Session::put('email', $user->email);
                Session::put('id', $user->id);
                $countprofile = CompanyBusinessProfile::where('company_id', Session::get('id'))->count();
                Session::put('totalstore', $countprofile);
                ($user->company_name ? Session::put('trading_name', $user->company_name) : Session::put('trading_name', 'Company'));
                return redirect('admin/contact-info');
            } else {
                return redirect('admin-signup')->withErrors([
                    'errors' => Lang::get('message.wrongCredentials'),
                ]);
            }
        } else {
            return redirect('admin-signup')->withErrors([
                'errors' => Lang::get('message.wrongCredentials'),
            ]);
        }
    }

    public function showContactForm() {
        if (Session::has('id')) {
            $company = Company::find(Session::get('id'));
            if (!empty($company->suburs_id)) {
                $suburbsname = $this->getsuburbsnamebyid($company->suburs_id);
                $alreadyexistinglocations = CompanyBusinessProfile::where('company_id', '=', $company->id)->get();
                return view('admin-pages/contact-info', compact('company', 'suburbsname', 'alreadyexistinglocations'));
            } else {
                $alreadyexistinglocations = CompanyBusinessProfile::where('company_id', '=', $company->id)->get();
                return view('admin-pages/contact-info', compact('company', 'alreadyexistinglocations'));
            }
        } else {
            return redirect('admin-signup')->withErrors([
                'errors' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    public function showBusinessDetail($id = 0) {
        if (Session::has('id')) {
            // Check if company activated
            // $this->checkCompanyStatus(Session::get('id'));
            $subscription_price = new SubscriptionPrice();
            $amount = $subscription_price->getSubscriptionDetails();
            $subs_price = $amount->sub_price;
            $app = app();
            $recarr = array();
            $occarr = array();
            if (!empty($id)) {
                $companyprofile = new CompanyBusinessProfileAudit();
                $companybusinessprofile = $companyprofile->getStoreById($id);

                if (!empty($companybusinessprofile->subscription_amount)) {
                    $subs_price = $companybusinessprofile->subscription_amount;
                }
                $rec = new CompanyBusinessReceipientsAudit();
                $recipients = $rec->auditDetails($id);
                $oca = new CompanyBusinessOccationsAudit();
                $occations = $oca->auditDetails($id);
                if (!empty($recipients)) {
                    foreach ($recipients as $rec) {
                        $recarr[] = $rec->recipient_id;
                    }
                }
                foreach ($occations as $rec) {
                    $occarr[] = $rec->occasion_id;
                }
                $time = new CompanyTimingsAudit();
                $daystiming = $time->auditDetails($id);
                foreach ($daystiming as $rec) {
                    $daystimings[$rec->day] = $rec->timings;
                }
                $busineimg = new CompanyBusinessImagesAudit();
                $businessimages = $busineimg->auditDetails($id);
                foreach ($businessimages as $rec) {
                    $businessimagesarr[$rec->voucher_image] = $rec->image;
                }
                $suburbsname = (!empty($companybusinessprofile->city_id) ? $this->getsuburbsnamebyid($companybusinessprofile->city_id) : '');
            } else {

                $laravel_object = $app->make('stdClass');
                $fldarr = array('short_desc', 'long_desc', 'special_desc', 'logo', 'store_pin', 'category_id', 'company_id','nation_wide', 'email', 'country_code', 'phone', 'state_id', 'address', 'city_id', 'post_code', 'subscription_amount', 'website');
                foreach ($fldarr as $val) {
                    $laravel_object->$val = '';
                }

                $laravel_objr = $app->make('stdClass');
                $fldarr1 = array('company_business_profile_id', 'recipient_id');
                foreach ($fldarr1 as $val) {
                    $laravel_objr->$val = '';
                }

                $laravel_objo = $app->make('stdClass');
                $fldarr2 = array('company_business_profile_id', 'occasion_id');
                foreach ($fldarr2 as $val) {
                    $laravel_objo->$val = 0;
                }

                $laravel_object3 = $app->make('stdClass');
                $companybusinessprofile = $laravel_object;
                $recipients = $laravel_objr;

                $occations = $laravel_objo;


                $daystiming = $laravel_object3;

                $suburbsname = '';
            }

            $paypal_profile_id = "";

            $has_invoice = false;

            if (!empty($id)) {

                $payment_detail = Payments::where("user_id", $id)->where("user_type", 2)->where("transaction_type", 1)->where("status", 1)->first();

                if (!empty($payment_detail) && $companybusinessprofile->is_subscribed == 1) {
                    $paypal_profile_id = $payment_detail->susbscriber_id;
                    $has_invoice = true;
                }
                if ($payment_detail && $payment_detail->transaction_id=='INVOICE') {
                    $has_invoice = true;
                }
                elseif (empty($payment_detail)) {
                    $payment_detail = Payments::where("user_id", $id)->where("user_type", 2)->where("transaction_type", 1)->where("transaction_id", 'INVOICE')->first();
                    $has_invoice = !empty($payment_detail);                    
                }
            }
            $storepin = $this->generatestorepin();
            $company = Company::find(Session::get('id'));
            return view('admin-pages/business-detail', compact('has_invoice','company', 'storepin', 'companybusinessprofile', 'suburbsname', 'recarr', 'occarr', 'daystimings', 'businessimagesarr', 'paypal_profile_id', 'subs_price'));
        } else {
            return redirect('admin-signup')->withErrors([
                'errors' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    public function updateBusinessDetail($companyid = 0) {
        //dd(Input::all());die;
        if (Session::has('id')) {
            $validator = Validator::make(
                Input::all(), array(
                    'short_desc' => 'required|max:410',
                    'long_desc' => 'required|max:2110',
                    'logo' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                    'store_pin' => 'required|max:4|unique:company_business_profile,id,?????',
                    'category_id' => 'required',
                    'company_id' => 'required',
                    'email' => 'required|email',
                    'state' => 'required',
                    'address' => 'required',
                    'suburb' => 'required',
                    'post_code' => 'required|numeric',
                    'phone_number' => 'required',
                    'state' => 'required|numeric',
                    'address' => 'required',
                    'suburb' => 'required|numeric',
                    'post_code' => 'required|numeric',
                    'maximum_amount' => 'required|numeric',
                    'minimum_amount' => 'required|numeric',
                    'store_location' => 'required|unique:company_business_profile,store_location,' . Input::get('store_id'),
                    'first_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                    'second_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                    'third_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                    'fourth_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                    'fifth_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                    'sixth_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif'
                )
            );
            $messages = array(
                'first_name.required' => 'First name required.',
                'long_desc.max' => 'Long description does not more than 2000 characters.'
            );
            if ($validator->fails()) {

                return Redirect::back()->withErrors($validator);
            }

            $setDataArr = array('short_desc' => Input::get('short_desc'),
                'long_desc' => Input::get('long_desc'),
                'special_desc' => Input::get('special_desc'),
                'nation_wide' => Input::get('nation_wide'),
                //'logo' => Input::get('logo'),
                'store_pin' => Input::get('store_pin'),
                'category_id' => Input::get('category_id'),
                'website' => Input::get('website'),
                'company_id' => Input::get('company_id'),
                'email' => Input::get('email'),
                'country_code' => Input::get('country_code'),
                'phone' => Input::get('phone_number'),
                //'subscription_amount' => Input::get('subscription_amount'),
                'state_id' => Input::get('state'),
                'address' => Input::get('address'),
                'city_id' => Input::get('suburb'),
                'post_code' => Input::get('post_code'),
                //'subscription_amount' => Input::get('subscription_amount'),
                'merchant_pin' => Input::get('merchant_pin'),
                'max_vouchar_amount' => Input::get('maximum_amount'),
                'min_vouchar_amount' => Input::get('minimum_amount'),
                //'incremental_vouchar_amount' => Input::get('incremental_amount'),
                'store_location' => Input::get('store_location'),
                'is_approved' => 0
            );
       // dd($setDataArr);
            if (!empty($companyid)) {
                $check_Action = 'UPDATE';
                $companyprofile = new CompanyBusinessProfileAudit();
                $companyprofile->updateBussinessProfileAudit($companyid, $setDataArr);
                //CompanyBusinessProfileAudit::where('id', $companyid)->update($setDataArr);
                $companybusiness = $companyprofile->getStoreById($companyid);

                // preview
                if (!empty(Input::get('preview'))) {
                    $preview = 1;
                }

                $companybusiness->id = $companyid;
            } else {
                $check_Action = 'INSERT';
                $comp = new CompanyBusinessProfile();
                $storepinexists = $comp->store_pin(Input::get('store_pin'));
                if (!empty($storepinexists)) {
                    return Redirect::back()->withErrors(Lang::get('message.profileExists'));
                }
                $companybusiness = CompanyBusinessProfile::create($setDataArr);
                $companybussinessAudit = CompanyBusinessProfileAudit::create($setDataArr);

                $preview = 1;
            }


            $tempArr = array();
            for ($i = 0; $i < sizeof(Input::get('occation_name')); $i++) {
                $tempArr[] = array('company_business_profile_id' => $companybusiness->id,
                    'occasion_id' => Input::get('occation_name')[$i]);
            }
            if ($check_Action == 'INSERT') {
                CompanyBusinessOccations::insert($tempArr);
            } else {
                CompanyBusinessOccationsAudit::where('company_business_profile_id', $companybusiness->id)->delete();
            }
            CompanyBusinessOccationsAudit::insert($tempArr);

            $tempArr = array();
            for ($i = 0; $i < sizeof(Input::get('recpient_name')); $i++) {
                $tempArr[] = array('company_business_profile_id' => $companybusiness->id,
                    'recipient_id' => Input::get('recpient_name')[$i]);
            }

            if ($check_Action == 'INSERT') {
                $CompanyBusinessOccations = CompanyBusinessReceipients::insert($tempArr);
            } else {
                CompanyBusinessReceipientsAudit::where('company_business_profile_id', '=', $companybusiness->id)->delete();
            }
            CompanyBusinessReceipientsAudit::insert($tempArr);


            $tempArr = array();
            for ($i = 0; $i <= 6; $i++) {
                $tempArr[] = array('company_business_profile_id' => $companybusiness->id,
                    'day' => $i + 1,
                    'timings' => Input::get('days')[$i]);
            }
            if ($check_Action == 'INSERT') {
                $timings = CompanyTimings::insert($tempArr);
            } else {
                CompanyTimingsAudit::where('company_business_profile_id', '=', $companybusiness->id)->delete();
            }
            CompanyTimingsAudit::insert($tempArr);

            $store_pin = Input::get('store_pin');
            $imagepath = '/images/company/storeslogo/' . $store_pin;
            $destinationpath = getS3LocalPath().$imagepath;

            if (Input::hasFile('logo')) {
                $logopath = $destinationpath . '/logo';
                if (!empty($companybusiness->logo)) {
                    $filename = public_path() . $companybusiness->logo;
                    //                    if (File::exists($filename)) {
                    //                        File::delete($filename);
                    //                    }
                } else {
                    if(!File::exists($logopath)) {
                        File::makeDirectory($destinationpath, 0777);
                        File::makeDirectory($logopath, 0777);
                    }
                }
                $logo_img = Input::file('logo');
                $img_extn = $logo_img->getClientOriginalExtension();
                $imagename = time() . $logo_img->getFilename() . '.' . $img_extn;
                $logo_img->move($logopath, $imagename); // uploading file to given path
                RotateImages::upload_rotate_image($logopath . '/' . $imagename, $img_extn); // for iOs
                $logo_path = $imagepath . '/logo/' . $imagename;
                $s3_distination = env('s3_env') . $imagepath . '/logo/' . $imagename;
                if (env('s3_on')) {
                    RotateImages::uploadImageS3($logopath . '/' . $imagename, $s3_distination);
                }
                if ($check_Action == 'INSERT') {
                    CompanyBusinessProfile::find($companybusiness->id)->update(['logo' => $logo_path]);
                }
                CompanyBusinessProfileAudit::find($companybusiness->id)->update(['logo' => $logo_path]);
            }

            $images_arr = array('first_image' => 1, 'second_image' => 2, 'third_image' => 3, 'fourth_image' => 4, 'fifth_image' => 5, 'sixth_image' => 6);
            $i = 1;
            $tempStoreImages = new TempStoreImages();
            //Image upload and save image path in db  
            foreach ($images_arr as $imgval => $sno) {
                $tempImageId = Input::get("hidden_".$imgval);
                if (!empty($tempImageId)) {

                    if (!empty($companyid)) {
                        $first_img = CompanyBusinessImagesAudit::where('company_business_profile_id', '=', $companyid)->where('voucher_image', '=', $sno)->first();
                        /* if ($first_img) {
                        $filename = public_path() . $first_img->image;
                        if (File::exists($filename)) {
                        File::delete($filename);
                        }
                        } */
                    }

                    $tempData = $tempStoreImages->getData($tempImageId);
                    if (!empty($first_img->id)) {
                        CompanyBusinessImagesAudit::find($first_img->id)->update(['image' => $tempData->image]);
                    } else {
                        $tmpArr = array('company_business_profile_id' => $companybusiness->id,
                            'image' => $tempData->image,
                            'voucher_image' => $tempData->voucher_image);
                        if ($check_Action == 'INSERT') {
                            CompanyBusinessImages::create($tmpArr);
                        }
                        CompanyBusinessImagesAudit::create($tmpArr);
                    }
                }
                $i++;
            }

            if (isset($preview) && $preview == 1) {
                $company = new Company();
                $companydata = $company->getCompanyAuditDetails($companybusiness->id);
                @extract($companydata);
                return View::make('admin-pages.companypreview', compact('companybusiness', 'companybusinesslocation', 'companybusinessimages', 'companytiming', 'statename', 'cityname', 'companyname', 'preview'));
            } else {
                $user = new User();
                $tomail = $user->getAdminDetails();
                $storename = new CompanyBusinessProfile();
                $store = $storename->getStoreById($companyid);
                // Mail::send('admin-cms.updatestore_email', array('store_location' => $store->store_location), function($message) use($tomail,$store) {
                //     $message->to($tomail->email, $tomail->first_name . ' ' . $tomail->last_name)->subject("Changes to ".$store->store_location.' submitted for approval');
                // });
                return view('admin-pages/companyprofilesubmission');
            }
        } else {
            return redirect('admin-signup')->withErrors([
                'errors' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    public function sendresetmail() {
        $email = Input::get('email');
        $suplierexist = Company::where('email', $email)->first();

        if ($suplierexist) {
            Passwordresets::create([
                'email' => Input::get('email'),
                'token' => Input::get('_token'),
            ]);

            $mail = Mail::send('admin-pages.resetmail', array('firstname' => $suplierexist->first_name, 'token' => Input::get('_token'), 'email' => Input::get('email')), function($message) use($suplierexist) {
                $message->to($suplierexist->email, $suplierexist->first_name . ' ' . $suplierexist->last_name)->subject('cherrygift Supplier Reset password !');
            });

            if ($mail) {
                return "sucess";
            } else {
                return "fail";
            }
        }
    }

    public function supplierReset($token = null) {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }
        $suplier_password_reset = new Passwordresets();

        $reseted = $suplier_password_reset->reset($token);
        if ($reseted == 0) {
            return view('Auth.reset')->with('token', $token)->withErrors(Lang::get('message.verificationlinkexpired'));
        }
        $timeout = Passwordresets::where('token', '=', $token)
        ->where('created_at', '<', Carbon::now()->subHours(42))
        ->first();

        if ($timeout) {
            return view('admin-pages.supplierreset')->with('token', $token)->withErrors(Lang::get('message.linkExpired'));
        } else {
            return view('admin-pages.supplierreset')->with('token', $token);
        }
    }

    public function supplieremailReset() {
        $token = Input::get('token');
        $suplier_password_reset = new Passwordresets();
        $reseted = $suplier_password_reset->reset($token);
        if ($reseted == 0) {
            return view('Auth.reset')->with('token', $token)->withErrors(Lang::get('message.verificationlinkexpired'));
        }
        $timeout = $suplier_password_reset->checkExpiry($token);

        if ($timeout) {
            return view('admin-pages.supplierreset')->with('token', $token)->withErrors(Lang::get('message.linkExpired'));
        } else {

            $email = Input::get('email');
            Company::where('email', '=', $email)->update(['password' => bcrypt(Input::get('password'))]);

            $user = Company::where('email', $email)->first();
            Session::put('email', $email);
            Session::put('id', $user->id);
            ($user->company_name ? Session::put('trading_name', $user->company_name) : Session::put('trading_name', 'Company'));
            $suplier_password_reset->deleteToken($token);
            return redirect('admin/contact-info');
        }
    }

    public function updatePassword() {
        $user = Session::get('email');
        if (empty($user)) {
            return Redirect::to('admin-signup')->withErrors(Lang::get('message.loginContinue'));
        } else {
            return View('admin-pages/changepassword');
        }
    }

    public function changePassword() {
        $user = Session::get('email');
        if (empty($user)) {
            return Redirect::to('admin-signup')->withErrors(Lang::get('message.loginContinue'));
        } else {
            $rules = array(
                'old_password' => 'required',
                'password' => 'required|min:6|confirmed'
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {

                return Redirect::back()->withErrors($validator);
            }
            $currentpassword = Company::where('email', '=', $user)->pluck('password');
            if (!Hash::check(Input::get('old_password'), $currentpassword)) {
                return Redirect::back()->withErrors(Lang::get('message.passworderror'));
            } else {
                $nowpassword = bcrypt(Input::get('password'));
                Company::where('email', '=', $user)->update(['password' => $nowpassword]);
                return Redirect::back()->withMessage(Lang::get('message.passwordchanged'));
            }
        }
    }

    public function companyExists() {
        $countprofile = CompanyBusinessProfile::where('company_id', Session::get('id'))->count();
        Session::put('totalstore', $countprofile);
        return Session::get('totalstore');
    }

    public function checkCompanyStatus($comid) {
        $comp = new Company();
        $compstatus = $comp->getCompanyById($comid);

        if ($compstatus->is_subscribed == "2") {

            Session::flush();

            return Redirect::to('admin-signup')->withErrors(['errors' => Lang::get('message.sessionerrerror')]);
        }
    }

    public function deleteImageById() {
        if(!empty(Input::get('temp_img_id'))) 
        {
            $temp = new TempStoreImages();
            $temp->deleteData(Input::get('temp_img_id'));

        } elseif (!empty(Input::get('company_id')) && !empty(Input::get('voucher_img_id'))) {
            $img = new CompanyBusinessImages();
            $img->deleteById(Input::get('company_id'), Input::get('voucher_img_id'));
        }
    }

    public function checkSupplierEmail() {
        $user_email = Input::get('user_email');
        $company_id = Input::get('company_id');
        $companyobj = new Company();
        $user = $companyobj->checSupplierEmail($user_email, $company_id);
        if ($user) {
            return 1;
        } else {
            return 0;
        }
    }

    public function uploadStoreImages() {

        $validator = Validator::make(
            Input::all(), array(
                'first_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                'second_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                'third_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                'fourth_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                'fifth_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif',
                'sixth_image' => 'sometimes|required|mimes:jpeg,bmp,png,gif'
            )
        );

        if ($validator->fails()) {
            $error = '';
            $error_arr = $validator->getMessageBag()->toArray();
            foreach($error_arr as $msg)
            {
                $error = $msg[0];
            }

            return json_encode(array("status"=>0,"msg"=>$error));

        }

        $store_pin = Input::get('store_pin');
        $imagepath = '/images/company/storeslogo/' . $store_pin;
        $destinationpath = getS3LocalPath().$imagepath;
        $logopath = $destinationpath . '/logo';
        if (!File::exists($logopath)) {
            File::makeDirectory($destinationpath, 0777);
            File::makeDirectory($logopath, 0777);
        } 

        $images_arr = array('first_image' => 1, 'second_image' => 2, 'third_image' => 3, 'fourth_image' => 4, 'fifth_image' => 5, 'sixth_image' => 6);
        $i = 1;
        //Image upload and save image path in db  
        foreach ($images_arr as $imgval => $sno) {

            if (Input::hasFile($imgval)) {
                $file_img = Input::file($imgval);
                // check our file size....
                $check_img=CompanyBusinessImages::validateImage($file_img);
                if ($check_img !== true) {
                    return json_encode(['status' => 0,'msg' => $check_img]);
                }
                $img_extn = $file_img->getClientOriginalExtension();
                $imagename = time() . $file_img->getFilename() . '.' . $img_extn;
                $file_img->move($destinationpath, $imagename); // uploading file to given path
                RotateImages::upload_rotate_image($destinationpath . '/' . $imagename, $img_extn); // for iOs
                $s3_distination = env('s3_env') . $imagepath . '/' . $imagename;
                if (env('s3_on')) {
                    RotateImages::uploadImageS3($destinationpath . '/' . $imagename, $s3_distination);
                }
                $temp_table = new TempStoreImages();
                //$data['user_id'] = Auth::user()->id;
                $data['store_pin'] = $store_pin;
                $data['image'] = $imagepath . '/' . $imagename;
                $data['voucher_image'] = Input::get('voucher_image');
                $tmp_id = $temp_table->insertData($data);
                return json_encode(array("status"=>1,"msg"=>$tmp_id));

            }
            $i++;
        }
    }

    public function checkStoreName() {
        $store_name = new CompanyBusinessProfile();
        $getstorename = $store_name->checkStoreName(Input::get('store_name'),Input::get('company_id'));
        if ($getstorename) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getTradeName() {
        $company = new Company();
        $chkcompanyname = $company->checkCompanyName(Input::get('trade_name'),Input::get('company_id'));
        if ($chkcompanyname) {
            return 1;
        } else {
            return 0;
        }
    }

}
