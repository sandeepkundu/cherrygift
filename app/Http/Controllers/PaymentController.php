<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Model\Payments;
use Redirect;
use App\Model\CompanyBusinessProfile;
use App\Model\CompanyBusinessProfileAudit;
use App\Model\SubscriptionPrice;
use Mail;
use Config;
use Illuminate\Support\Facades\Lang;
use App\User;
use View;
use Log;

class PaymentController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
     public function __construct() {
        die('payment controller check for sand box account');
        //$this->middleware('App\Http\Middleware\CmsAuth',['except' => ['index']]);
    }
    public function index() {

    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {
        //
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id) {
        //
    }

    /*     * ggg
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function edit($id) {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id) {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id) {
        //
    }

    public function cancel($id) {

        return Redirect::to('admin/business-detail/' . $id)->withErrors(Lang::get('message.paymentAborted'));;

    }

    public function thanks($id) {
        if (Request::isMethod('post')) {
            $payment = new Payments();
            $data['user_type'] = 2;
            $data['user_id'] = $id;
            $data['transaction_type'] = (Request::input('txn_type') == "subscr_signup") ? 1 : 2;
            $data['susbscriber_id'] = (!empty(Request::input('subscr_id'))) ? Request::input('subscr_id') : "";
            $data['transaction_id'] = (!empty(Request::input('txn_id'))) ? Request::input('txn_id') : "";
            $data['status'] = 1;
            $payment->save_vendor_payment($data);
            $subscription = new SubscriptionPrice();
            $subs = $subscription->getSubscriptionDetails();
            $amount = $subs->sub_price;
            $updatedata = array('subscription_amount' => $amount, 'is_payment' => 1, 'is_subscribed' => 1, 'last_payment_date' => Carbon::today()->format('Y-m-d'));
            $updatepayment = new CompanyBusinessProfileAudit();
            $updatepayment->updateDetails($id, $updatedata);

            return Redirect::to('payment/thanks/' . $id);
        } else {

            $user = new User();
            $tomail = $user->getAdminDetails();
            $storename = new CompanyBusinessProfile();
            $store = $storename->getStoreById($id);
            //$store->store_location; 
            Mail::send('admin-cms.newstore_email', array('store_location' => $store->store_location), function($message) use($tomail,$store) {
                $message->to($tomail->email, $tomail->first_name . ' ' . $tomail->last_name)->subject($store->store_location.' is submitted for approval');
            });
            return view('admin-pages/paymentconfirmation');
            //exit;
            //DB::enableQueryLog(); 
            //$paymencheck= Payments::where('company_business_id','=',$id)->orderBy('id', 'desc')->first();
            //var_dump($paymencheck);
            //echo '<br>busid='.$paymencheck->company_business_id; 
            //dd(DB::getQueryLog()); exit;

            if ($id == $paymencheck->company_business_id && strtolower($paymencheck->payment_status) == "verified") {
                echo "Thank you for your submission, please allow 48 hours for your profile to be approved. look out for your confirmation email";
            } else {
                return Redirect::to('payment/cancel/' . $id);
            }
        }
    }

    public function notify($id) {
        // Send an empty HTTP 200 OK response to acknowledge receipt of the notification 
        header('HTTP/1.1 200 OK');
        // Assign payment notification values to local variables
        $item_name = $_POST['item_name'];
        $item_number = $_POST['item_number'];
        $payment_status = $_POST['payment_status'];
        $payment_amount = $_POST['mc_gross'];
        $payment_currency = $_POST['mc_currency'];
        $txn_id = $_POST['txn_id'];
        $receiver_email = $_POST['receiver_email'];
        $payer_email = $_POST['payer_email'];
        $txn_type = $_POST['txn_type'];
        $subscr_id = $_POST['subscr_id'];
        $payment = new Payments();
        // Build the required acknowledgement message out of the notification just received  
        $posted = '';
        $req = 'cmd=_notify-validate';               // Add 'cmd=_notify-validate' to beginning of the acknowledgement
        foreach ($_POST as $key => $value) {         // Loop through the notification NV pairs
            $value = urlencode(stripslashes($value));  // Encode these values
            $req .= "&$key=$value";                   // Add the NV pairs to the acknowledgement
            $posted .="$key=$value\n";
        }
        $logFd = fopen("ipn.txt", "a");
        fwrite($logFd, "****************************************************************************************************\n");
        $logStr = "Received Posted Data : " . htmlspecialchars($posted);
        fwrite($logFd, strftime("%d %b %Y %H:%M:%S ") . " $logStr\n");
        //  fclose($logFd); exit; 
        // Set up the acknowledgement request headers
        $header = "POST /cgi-bin/webscr HTTP/1.1\r\n";                    // HTTP POST request
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";

        if (!empty(env('PAYPAL_SENDBOX')))
            $header .= "Host: www.sandbox.paypal.com\r\n";
        else
            $header .= "Host: www.paypal.com\r\n";

        $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
        $header .= "Connection: Close\r\n\r\n";

        // Open a socket for the acknowledgement request
        if (!empty(env('PAYPAL_SENDBOX')))
            $fp = fsockopen('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
        else
            $fp = fsockopen('ssl://www.paypal.com', 443, $errno, $errstr, 30);
        if (!$fp) {
            // HTTP ERROR
            fwrite($logFd, "--------------------\n");
            $logStr = "IPN Listner recieved an Error:\n";

            $logStr .= $errno . '--' . $errstr;
            fwrite($logFd, strftime("%d %b %Y %H:%M:%S ") . "[IPNListner.php] $logStr\n");
            fclose($logFd);
            exit;
        } else {
            fwrite($logFd, "--------------------\n");
            $logStr = "IPN Listner recieved:\n";
            fwrite($logFd, strftime("%d %b %Y %H:%M:%S ") . "[IPNListner.php] $logStr\n");

            fputs($fp, $header . $req);

            while (!feof($fp)) {                     // While not EOF
                $res = fgets($fp, 1024);               // Get the acknowledgement response
                if (strcmp($res, "VERIFIED") == 0) {  // Response contains VERIFIED - process notification
                    // Send an email announcing the IPN message is VERIFIED
                    $payment_profile = $payment->get_vendor_payment_profile($id, $subscr_id, $txn_id);
                    if ($txn_type == "subscr_signup") {

                        if (!empty($payment_profile)) {

                            if ($payment_profile->status != 1) {
                                $payment->update_vendor_payment_status($payment_profile->id, 1);
                            }
                        } else {
                            $data['user_type'] = 2;
                            $data['user_id'] = $id;
                            $data['transaction_type'] = ($txn_type == "subscr_signup") ? 1 : 2;
                            $data['susbscriber_id'] = (!empty($subscr_id)) ? $subscr_id : "";
                            $data['transaction_id'] = (!empty($txn_id)) ? $txn_id : "";
                            $data['status'] = 1;
                            $payment->save_vendor_payment($data);
                        }
                    } else if ($txn_type == "subscr_failed") {

                        if (!empty($payment_profile)) {
                            $payment->update_vendor_payment_status($payment_profile->id, 2);
                        } else {
                            $data['user_type'] = 2;
                            $data['user_id'] = $id;
                            $data['transaction_type'] = ($txn_type == "subscr_signup") ? 1 : 2;
                            $data['susbscriber_id'] = (!empty($subscr_id)) ? $subscr_id : "";
                            $data['transaction_id'] = (!empty($txn_id)) ? $txn_id : "";
                            $data['status'] = 3;
                            $payment->save_vendor_payment($data);
                        }
                    } else if ($txn_type == "subscr_payment") {

                    }
                    //                   
                } else if (strcmp($res, "INVALID") == 0) { //Response contains INVALID - reject notification
                    // Authentication protocol is complete - begin error handling
                    // Send an email announcing the IPN message is INVALID
                    $mail_From = "IPN@example.com";
                    $mail_To = "Your-eMail-Address";
                    $mail_Subject = "INVALID IPN";
                    $mail_Body = $req;

                    //mail($mail_To, $mail_Subject, $mail_Body, $mail_From);
                    fwrite($logFd, "--------------------\n");
                    $logStr = "Invalid IPN:\n";
                    fwrite($logFd, strftime("%d %b %Y %H:%M:%S ") . "[IPNListner.php] $logStr\n");
                }
            }
            fclose($fp);  // Close the file  
        }
        fwrite($logFd, "--------------------\n");
        $logStr = "IPN Post Response:\n";
        fwrite($logFd, strftime("%d %b %Y %H:%M:%S ") . "[IPNListner.php] $logStr\n");

        fclose($logFd);
    }
    public function cancelSubscription($profile_id, $id) {
        $paypal_username = Config::get('constants.paypal_username');
        $paypal_password = Config::get('constants.paypal_password');
        $paypal_signature = Config::get('constants.paypal_signature');
        $paypal_apiurl = Config::get('constants.paypal_apiurl');
        $posturl = 'https://api-3t.' . $paypal_apiurl . 'paypal.com/nvp';
        $action = "Cancel";

        $api_request = 'USER=' . urlencode($paypal_username)
        . '&PWD=' . urlencode($paypal_password)
        . '&SIGNATURE=' . urlencode($paypal_signature)
        . '&VERSION=76.0'
        . '&METHOD=ManageRecurringPaymentsProfileStatus'
        . '&PROFILEID=' . urlencode($profile_id)
        . '&ACTION=' . urlencode($action)
        . '&NOTE=' . urlencode('Profile cancelled at cherrygift');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $posturl); // For live transactions, change to 'https://api-3t.paypal.com/nvp'
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        // Uncomment these to turn off server and peer verification
        // curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
        // curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        // Set the API parameters for this transaction
        curl_setopt($ch, CURLOPT_POSTFIELDS, $api_request);

        // Request response from PayPal
        $response = curl_exec($ch);

        // If no response was received from PayPal there is no point parsing the response
        if (!$response) {
            //echo 'Calling PayPal to change_subscription_status failed: ' . curl_error( $ch ) . '(' . curl_errno( $ch ) . ')';die;
            Redirect::back()->withErrors('Calling PayPal to change_subscription_status failed: ' . curl_error($ch) . '(' . curl_errno($ch) . ')');
            //Redirect::back()->withErrors('There is some problem on Paypal,Please try again after some time.');
        }
        // die( 'Calling PayPal to change_subscription_status failed: ' . curl_error( $ch ) . '(' . curl_errno( $ch ) . ')' );

        curl_close($ch);

        // An associative array is more usable than a parameter string
        parse_str($response, $parsed_response);

        if (isset($parsed_response["ACK"]) && $parsed_response["ACK"] == "Success") {
            $payment_profile = Payments::where("susbscriber_id", $profile_id)->first();
            $payment_profile->status = 0;
            $payment_profile->save();
            $data = array('is_subscribed' => 2, 'is_payment' => 0);
            $profile_audit = new CompanyBusinessProfileAudit();
            $profile_audit->updateDetails($payment_profile->user_id, $data);


            return Redirect::back()->withMessage(Lang::get('message.subscriptionCanceled'));
        } else {
            if (isset($parsed_response['L_SHORTMESSAGE0'])) {
                return Redirect::back()->withErrors($parsed_response['L_SHORTMESSAGE0']);
            } else {
                return Redirect::back()->withErrors(Lang::get('message.trylater'));
            }
        }
    }

    public function postDataCurl($data) {
        $paypal_apiurl = Config::get('constants.paypal_apiurl');
        $posturl = 'https://api-3t.' . $paypal_apiurl . 'paypal.com/nvp';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, $posturl);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function subscribe($id) {
        //echo date("c");die;
        $subscription = new SubscriptionPrice();
        $subs = $subscription->getSubscriptionDetails();
        $amount = $subs->sub_price;
        $paypal_apiurl = Config::get('constants.paypal_apiurl');
        $paypal_username = Config::get('constants.paypal_username');
        $paypal_password = Config::get('constants.paypal_password');
        $paypal_signature = Config::get('constants.paypal_signature');
        $cancelurl = url() . '/payment/cancel/' . $id;
        $returnurl = url() . '/payment/success/' . $id;
        $postdata = array(
            'USER' => $paypal_username,
            'PWD' => $paypal_password,
            'SIGNATURE' => $paypal_signature,
            'METHOD' => 'SetExpressCheckout',
            'VERSION' => '108',
            'LOCALECODE' => 'en_AU',
            //'PROFILESTARTDATE'=>date("c"),
            'PAYMENTREQUEST_0_AMT' => $amount,
            'PAYMENTREQUEST_0_CURRENCYCODE' => 'AUD',
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
            'PAYMENTREQUEST_0_ITEMAMT' => $amount,
            'L_PAYMENTREQUEST_0_NAME0' => 'Subscription',
            'L_PAYMENTREQUEST_0_DESC0' => '12 Months Subscription', //'1 Day Subscription',
            'L_PAYMENTREQUEST_0_QTY0' => 1,
            'L_PAYMENTREQUEST_0_AMT0' => $amount,
            'L_PAYMENTREQUEST_0_ITEMCATEGORY0' => 'Digital',
            'L_BILLINGTYPE0' => 'RecurringPayments',
            'L_BILLINGAGREEMENTDESCRIPTION0' => '12 Months Subscription', //'1 Day Subscription',
            'CANCELURL' => $cancelurl,
            'RETURNURL' => $returnurl
        );
        $response = $this->postDataCurl($postdata);
        Log::info(" SetExpressCheckout ::: ".$response);
        // Creating associative array 
        parse_str($response, $nvp);
        if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
            $query = array('cmd' => '_express-checkout', 'token' => $nvp['TOKEN']);
            $redirectURL = sprintf('https://www.' . $paypal_apiurl . 'paypal.com/cgi-bin/webscr?%s', http_build_query($query));
            return Redirect::to($redirectURL);
        } else {

            return Redirect::to('admin/contact-info')->withErrors(Lang::get('message.trylater'));
        }
    }

    /**
    * Payment option to request invoice and manual payment
    * 
    * @param mixed $id
    */
    public function requestInvoice($id) {
        // check we have not already requested an invoice
        $check = Payments::where('user_id',$id)
        ->where('transaction_id','INVOICE')
        ->first();
        if (!empty($check)) {
            return View::make(
                'admin-pages.invoice-already-sent',
                ['payment' => $check]
            );
        }
        $subscription = new SubscriptionPrice();
        $subs = $subscription->getSubscriptionDetails();
        $amount = $subs->sub_price;
        // no idea if we actually need a payment record??
        // we'll use it for our invoices in short term
        // note that the user_id DOES NOT refer to users.id, but instead the
        // actual id of the store in this instance....
        $payment = new Payments();   
        $data = [
            'user_type' => 2,
            'user_id' => $id,
            'transaction_type' => 1,
            'susbscriber_id' => '',
            'transaction_id' => 'INVOICE',
            'status' => 0
        ];
        $data['paypal_history_id']='';
        $payment->save_vendor_payment($data);
        $updatedata = [
            'subscription_amount' => $amount, 
            'is_payment' => 1,     // prevent asking them again...  
            'is_subscribed' => 1,
        ];
        $updatepayment = new CompanyBusinessProfileAudit();
        $updatepayment->updateDetails($id, $updatedata);

        // issue the invoice
        $email = $payment->sendInvoiceMail();
        // output thanks message
        return View::make(
            'admin-pages.invoice-sent',
            ['payment' => $payment, 'email' => $email]
        );

    }

    public function paymentSuccess($id) {
        $token = Request::input('token');
        $PayerID = Request::input('PayerID');
        $subscription = new SubscriptionPrice();
        $subs = $subscription->getSubscriptionDetails();
        $amount = $subs->sub_price;
        $paypal_username = Config::get('constants.paypal_username');
        $paypal_password = Config::get('constants.paypal_password');
        $paypal_signature = Config::get('constants.paypal_signature');
        //Express checkout details
        $postdata = array(
            'USER' => $paypal_username,
            'PWD' => $paypal_password,
            'SIGNATURE' => $paypal_signature,
            'METHOD' => 'GetExpressCheckoutDetails',
            'VERSION' => '108',
            'TOKEN' => $token
        );

        $response = $this->postDataCurl($postdata);
        Log::info(" GetExpressCheckoutDetails ::: ".$response);

        // Creating associative array 
        parse_str($response, $nvp);
        // $startdate = $nvp['TIMESTAMP'];
        $startdate = date('Y-m-d', strtotime('+1 years'));
        /* $postdataDoExpress = array(
        'USER' => $paypal_username,
        'PWD' => $paypal_password,
        'SIGNATURE' => $paypal_signature,
        'METHOD' => 'DoExpressCheckoutPayment',
        'VERSION' => '108',
        'TOKEN' => $token,
        'PAYERID'=>$nvp['PAYERID'],
        'PAYMENTREQUEST_0_PAYMENTACTION'=>'Sale',
        'PAYMENTREQUEST_0_AMT'=>$amount,
        'PAYMENTREQUEST_0_ITEMAMT' => $amount,
        'PAYMENTREQUEST_0_CURRENCYCODE' => 'AUD',
        );

        //$responseDoExpress = $this->postDataCurl($postdataDoExpress);
        //Log::info(" do express checkout response ::: ".json_encode($responseDoExpress));
        //parse_str($responseDoExpress, $nvpDoExpress);
        if(false){//$nvpDoExpress['ACK']=="Failure") {
        $payment = new Payments();
        $data['user_type'] = 2;
        $data['user_id'] = $id;
        $data['transaction_type'] = 1;
        $data['susbscriber_id'] = "";
        $data['transaction_id'] = $nvpDoExpress['L_ERRORCODE0'];
        $data['status'] = 2;
        $payment->save_vendor_payment($data);

        return Redirect::to('admin/contact-info')->withErrors(Lang::get('message.trylater'));
        }*/
        //print_r($responseDoExpress);die;
        //Creating recurring profile
        $postdata = array(
            'USER' => $paypal_username,
            'PWD' => $paypal_password,
            'SIGNATURE' => $paypal_signature,
            'METHOD' => 'CreateRecurringPaymentsProfile',
            'VERSION' => '108',
            'LOCALECODE' => 'en_AU',
            'TOKEN' => $token,
            'PayerID' => $PayerID,
            'PROFILESTARTDATE' => $startdate."T00:00:00Z",
            'DESC' => '12 Months Subscription', //'1 Day Subscription',
            'BILLINGPERIOD' => 'Month', //'Day',
            'BILLINGFREQUENCY' => '12', //'1',
            'AMT' => $amount,
            'INITAMT'=>$amount,
            'FAILEDINITAMTACTION'=>'ContinueOnFailure',
            'CURRENCYCODE' => 'AUD',
            'COUNTRYCODE' => 'AU',
            'MAXFAILEDPAYMENTS' => 3
        );
        $response = $this->postDataCurl($postdata);
        Log::info(" CreateRecurringPaymentsProfile ::: ".$response);   
        parse_str($response, $nvp);
        if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
            $PROFILEID = $nvp['PROFILEID'];
            return View::make('admin-pages.paysuccess', compact('PROFILEID', 'id', 'PayerID'));
        } else {
            return Redirect::to('payment/cancel/' . $id);
        }
    }

    public function isIPNValid(array $message) {
        $endpoint = 'https://www.paypal.com';

        if (!empty(env('PAYPAL_SENDBOX'))) {
            $endpoint = 'https://www.sandbox.paypal.com';
        }

        $endpoint .= '/cgi-bin/webscr?cmd=_notify-validate';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $endpoint);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($message));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        $errno = curl_errno($curl);

        curl_close($curl);
        Log::info("handshake response ::: ".json_encode($response));
        return empty($error) && $errno == 0 && $response == 'VERIFIED';
    }

    public function expressCheckoutIPN() {
        header('HTTP/1.1 200 OK');
        if (Request::isMethod('post')) {
            $post = Request::all();
            Log::info("express checkout ipn ::: ".json_encode($post));
            if (!$this->isIPNValid($post)) {
                echo "not valid";die;
                // return;
            }

            if(!empty($post['recurring_payment_id'])) {
                $payment = new Payments();
                $cbpa = new CompanyBusinessProfileAudit();
                $cbp = new CompanyBusinessProfile();
                $pay_profile = $payment->getPaymentProfileBySubsId($post['recurring_payment_id']);

                if(empty($pay_profile)) {
                    die("data not found.");
                }

                if(!empty($post['payment_status']) && $post['payment_status'] == "Completed"  && $post['txn_type'] == "recurring_payment") {

                    if(!empty($pay_profile->user_id))
                    {
                        $cbpa->updateLastPaymentDate($pay_profile->user_id,date("Y-m-d"),$post['mc_gross']);
                        $cbp->updateLastPaymentDate($pay_profile->user_id,date("Y-m-d"),$post['mc_gross']);

                        $this->afterSubscriptionRenew($pay_profile->user_id);
                    }

                }
                //            elseif($post['txn_type'] == "recurring_payment_profile_cancel") {
                //                $cbpa->updateSubscriptionStatus($pay_profile->user_id,2);
                //                  $cbp->updateSubscriptionStatus($pay_profile->user_id,2);
                //                $payment->updateSubscriptionStatus($pay_profile->id,3);
                //            } 
                elseif($post['txn_type'] == "recurring_payment_profile_cancel") {
                    $status = $this->checkSubscriptionStatus($post['recurring_payment_id']);
                    if($status == 'Cancelled' || $status == 'Suspended' || $status == 'Expired') {
                        $cbpa->updateSubscriptionStatus($pay_profile->user_id,4);
                        $cbp->updateSubscriptionStatus($pay_profile->user_id,4);
                        $payment->updateSubscriptionStatus($pay_profile->id,4);
                    }
                }


            }
        }
    }


    public function sendEmailSubscriptionRenew(){

    }

    public function checkSubscriptionStatus($subscription_id){

        $paypal_username = Config::get('constants.paypal_username');
        $paypal_password = Config::get('constants.paypal_password');
        $paypal_signature = Config::get('constants.paypal_signature');
        //Express checkout details
        $postdata = array(
            'USER' => $paypal_username,
            'PWD' => $paypal_password,
            'SIGNATURE' => $paypal_signature,
            'METHOD' => 'GetRecurringPaymentsProfileDetails',
            'VERSION' => '108',
            'PROFILEID' => $subscription_id
        );

        $response = $this->postDataCurl($postdata);
        // Creating associative array 
        parse_str($response, $nvp);
        return $nvp["STATUS"];
    }


    public function beforeSubscriptionRenew() {

        $cbp = new CompanyBusinessProfile();

        $subscriptionrenewal = $cbp->beforeSubscriptionRenew();

        foreach ($subscriptionrenewal as $renewal)
        {    
            Mail::send('emails.before-subscription-renew', array('store_location' => $renewal->store_location, 'firstname' => $renewal->first_name), function($message)use( $renewal) {
                $message->to($renewal->email, $renewal->first_name)->subject('Subscription Renewal Reminder ');
            });
        }
    }

    public function afterSubscriptionRenew($cbp_id) {
        if (!empty($cbp_id)) {

            $cbp = new CompanyBusinessProfile();
            $subscriptionpayment = $cbp->afterSubscriptionRenew($cbp_id);

            foreach ($subscriptionpayment as $payconfirm) {
                Mail::send('emails.after-subscription-renew', array('store_location' => $payconfirm->store_location, 'firstname' => $payconfirm->first_name), function($message)use($payconfirm) {
                    $message->to($payconfirm->email, $payconfirm->first_name)->subject('Cherry gift Subscription Renewed');
                });
            }
        }
    }
}
