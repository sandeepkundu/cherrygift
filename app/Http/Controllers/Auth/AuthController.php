<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Socialite;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Support\Facades\Input;
use Redirect;
use App\Model\RotateImages;
use Image;
use DB;
class AuthController extends Controller {

    /**
     * the model instance
     * @var User
     */
    protected $user;

    /**
     * The Guard implementation.
     *
     * @var Authenticator
     */
    protected $auth;

    /**
     * Create a new authentication controller instance.
     * /var/www/html/cherrgift
     * @param  Authenticator  $auth
     * @return void
     */
    public function __construct(Guard $auth, User $user) {
        $this->user = $user;
        $this->auth = $auth;

        $this->middleware('guest', ['except' => ['getLogout']]);
    }

    /**
     * Show the application registration form.
     *
     * @return Response
     */
    public function getRegister() { //echo "There"; exit;
        return view('Auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  RegisterRequest  $request
     * @return Response
     */
    public function postRegister(RegisterRequest $request) {
        //dd($request->mobile_number);
        if (Input::hasFile('image')) {
            $imagename = strtotime(date('Y-m-d H:i:s')) . $request['image']->getFilename() . '.' . $request['image']->getClientOriginalExtension();
            $destinationFolder = getS3LocalPath().'/imgs/profile/';
            $s3_distination = env('s3_env'). "/imgs/profile/".$imagename;
            $request['image']->move(
                    $destinationFolder, $imagename
            );
            // $ext = exif_imagetype($destinationFolder.$imagename);
            //print_r($request['image']->getClientOriginalExtension());
            self::upload_rotate_image($destinationFolder . $imagename, $request['image']->getClientOriginalExtension());
            if (env('s3_on'))  {
                RotateImages::uploadImageS3($destinationFolder . $imagename,$s3_distination);
            }
            //die;
        } else {
            $imagename = '';
        }
    
// You can store this but should validate it to avoid conflicts
//echo $original_name = $file->getClientOriginalName(); exit;
        //code for registering a user goes here.
        $user = User::create([
                    'email' => $request['email'],
                    'first_name' => $request['first_name'],
                    'last_name' => $request['last_name'],
                    'password' => bcrypt($request['password']),
                    'mobile_number' => $request['mobile_number'],
                    'image' => $imagename,
        ]);

                             //code written by sandeep and date- 13-10-2016
                             //this code is used for unregister gift voucher user   
                                $mobile=$request->mobile_number;
                                $user_id=$user->id;
                                $results = DB::table('unregister_gift_voucher')->where('mobile_no', '=',$mobile)->get();
                                //dd($results);
                                if($results){
                                            foreach ($results as $key => $value) {
                                                 $id=$value->id;
                                                 $mobile_no=$value->mobile_no;
                                                 $order_voucher_id=$value->order_voucher_id;
                                                $voucher_send_by=$value->voucher_send_by;

                                                 DB::table('user_gifted_voucher')->insert(
                                                        ['user_id' => $user_id,'order_voucher_id' => $order_voucher_id, 'voucher_send_by' => $voucher_send_by]
                                                    );

                                                 DB::table('unregister_gift_voucher')->where('id', '=', $id)->delete();
                                            }
                                }else{
                                    $results='';
                                }
                                //code end by sandeep
                                 
        //print_r($request);        exit();
        $this->auth->login($user);
        Session::put('new_register', '1');
        if (Session::has('befor_login_url')) {
                $url = Session::pull('befor_login_url');
                
                return redirect($url);
            }
            return redirect('/');
    }

    /**
     * Show the application login form.
     *
     * @return Response
     */
    public function getLogin() {
        return view('Auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  LoginRequest  $request
     * @return Response
     */
    public function postLogin(LoginRequest $request) {
       // echo $request['email']; exit;
        if ($this->auth->attempt(['email' => $request['email'], 'password' => $request['password'], 'status' => 'Active'])) {
           if (Session::has('befor_login_url')) {
              $url = Session::pull('befor_login_url');
                
                return redirect($url);
            }
           switch (Auth::user()->role){
               case "Admin":
        return redirect('super-admin/dashboard');
        break;
    case "User":
        return redirect('/');
        break;
    
    default:
       return redirect('/');
            }
            
            
            
        }
       return Redirect::back()->withErrors([
                    'email' => 'The credentials you entered did not match our records. Try again?',
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    public function getLogout() {
        $this->auth->logout();
        Session()->flush();                
        return redirect('/');
    }

    protected $redirectPath = '/facebookuser';

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider() {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback() {
        if (Input::get('error_reason') == 'user_denied') {
            //explain why you need them to log in
            return redirect('users/login')->withErrors([
                        'errors' => 'Something went wrong. Please try again',
            ]);
        }
        try {
            $user = Socialite::driver('facebook')->user();
            
            if(empty($user['email'])){
               
                   return redirect('users/login')->withErrors([
                        'errors' => 'Kindly signup with email id and password if you do not want to give access to your Facebook information',
            ]);
           }
           
        } catch (Exception $e) {
            return redirect('Auth/facebook');
        }
        if(!empty($user['name'])) {
            $name = explode(" ",$user['name']);
           $user['first_name'] = $name[0];
           $user['last_name'] = $name[1];
        }
        $authUser = $this->findOrCreateUser($user);
       // dd($authUser);
        Auth::login($authUser, true);


if (Session::has('befor_login_url')) {
                $url = Session::pull('befor_login_url');
                
                return redirect($url);
            }
        return redirect('/');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    private function findOrCreateUser($facebookUser) {
        //echo $facebookUser['id']; echo "<br>"; exit;
        //echo $facebookUser['first_name']; exit;
        //echo "<pre>"; print_r($facebookUser); echo"</pre>"; exit;
        $authUser = User::where('email', $facebookUser['email'])->first();

        if ($authUser) {
            //$authUser = 'Allreadyregistered';
            return $authUser;
            //print_r($authUser); 
        }
//        $user = new User();
//        $user->facebook_id = $facebookUser['id'];
//        $user->first_name = $facebookUser['first_name'];
//        $user->last_name = $facebookUser['last_name'];
//        $user->email = $facebookUser['email'];
//        $user->image = $facebookUser->avatar;
//        $user->save();
        else {
            Session::put('new_register', '1');
            $destinationFolder = getS3LocalPath().'/imgs/profile/';
            $s3_distination = env('s3_env') . "/imgs/profile/" . $facebookUser['id'].'.jpg';
            Image::make($facebookUser->avatar)->save($destinationFolder.$facebookUser['id'].'.jpg');
            //$image = Image::make($facebookUser->avatar)->save($s3_distination.$facebookUser['id'].'.jpg');
            if (env('s3_on')) {
                RotateImages::uploadImageS3($destinationFolder . $facebookUser['id'].'.jpg', $s3_distination);
            }
            return User::create([
                        'facebook_id' => $facebookUser['id'],
                        'first_name' => $facebookUser['first_name'],
                        'last_name' => $facebookUser['last_name'],
                        'email' => $facebookUser['email'],
                        'image' => $facebookUser['id'].'.jpg'
            ]);
            
        }
    }

    public static function rotate($src, $ext) {
        $deg = 0;
        if (function_exists('exif_read_data') && @exif_imagetype($src) && $ext != 'png') {
            $exif = @exif_read_data($src);
            if ($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                if ($orientation != 1) {
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                            break;
                    }
                }
            }
        }

        return $deg;
    }

    public static function upload_rotate_image($src, $ext) {
        //print_r($ext);die;
        $deg = self::rotate($src, $ext);
        //$deg = 0;
        if (strtolower($ext) == 'jpg' || strtolower($ext) == 'jpeg')
            $source_image = imagecreatefromjpeg($src);
        else
            $source_image = imagecreatefrompng($src);

        if ($deg) {
            $source_image = imagerotate($source_image, $deg, 0);
        }

        $image_saved = imagejpeg($source_image, $src);
    }

}
