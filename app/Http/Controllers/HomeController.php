<?php

namespace App\Http\Controllers;

use App\Model\States;
use App\Model\CompanyBusinessProfile;
use App\Model\HomepageSliderImages;
use App\Model\HomepageRecipientImages;
use Session;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        //dd(env('APP_KEY'));
        $locations = States::all()->toArray();
        $cherrygiftvoucher = CompanyBusinessProfile::where('cherrygift_voucher',1)
                                ->select('store_location')
                                ->orderBy('id', 'desc')
                                ->first();
       // dd($cherrygiftvoucher);
        $state_images = States::has("stateimages")->get();
        // dd($state_images);
        $state_img_arr = array();

        foreach ($state_images as $img) {
            $array['state_name'] = $img->state_name;
            $array['id'] = $img->id;
            $img_rr = array();
            foreach ($img->StateImages as $image) {
                $img_rr[] = $image->image;
            }
            $array["images"] = $img_rr;
            $state_img_arr['state_arr'][] = $array;
        }
        
        $new_register = 0;
        if (Session::has('new_register')) {
            $new_register = Session::pull('new_register');
        } 
        $state_img_arr['new_register'] = $new_register;
        $state_img_arr['states'] = $locations;
        if(!empty($cherrygiftvoucher)){
        $state_img_arr['cherrygiftvoucher'] = $cherrygiftvoucher->store_location;

        }
        
        $hsi = new HomepageSliderImages();
        $state_img_arr['slider_images'] = $hsi->getImages();
        $hri = new HomepageRecipientImages();
        $recipient_imgs = $hri->getImages();
        
        foreach($recipient_imgs as $rec_img) {
            
            if($rec_img->type == 1)
            $state_img_arr["cherrygift_voucher_images"] = $rec_img->image;
            elseif($rec_img->type == 2)
            $state_img_arr["gift_for_her_images"] = $rec_img->image;
            elseif($rec_img->type == 3)
            $state_img_arr["gift_for_him_images"] = $rec_img->image;
            elseif($rec_img->type == 4)   
            $state_img_arr["gift_for_them_images"] = $rec_img->image;    
           
        }
        
        return view('welcome', $state_img_arr);
    }

    // public function sendfootermail(){

    //     dd('hi..');
    // }

}
