<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use App\Model\Order;
use App\Model\PaypalHistory;
use App\Model\UserCart;
use App\Model\OrderVoucher;
use App\Model\UserGiftedVoucher;
use App\Model\CartVouchers;
use Session;
use View;
use Redirect;
use Illuminate\Support\Facades\Lang;
use DateTime;
use DateTimeZone;

//use Input;


class OrderController extends Controller {

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
    * Show the application dashboard to the user.
    *
    * @return Response
    */
    public function index() {
        $url = Request::url();
        if (!Auth::check()) {
            Session::put('befor_login_url', $url);
            return Redirect('users/login')->withErrors([
                'email' => Lang::get('message.login'),
            ]);
        }

        $loggedin_user = Auth::user();

        $usercartinfo = new UserCart();
        $getusercartinfo = $usercartinfo->getUserCartInfo();
        @extract($getusercartinfo);
        return View::make('cart', compact('getusercartinfo'));
    }

    public function testlink($id){

            //$company = new Company();
            //$results = $company->getCompanyById($id);
            //$first_name = $results->first_name;
            // dd($first_name);
            //$contact_email = $results->contact_email;
            //$emailid='mohd.nadeem@webnyxa.com';
               //  $emailid='mohd.nadeem@webnyxa.com';
               // //$emailid='fiona@cherrygift.com';
               //  $msendmail = Mail::send('admin-cms/confirmation_mail', array('first_name'=>$first_name,'password'=>$password), function($message) use($emailid) {
               //  $message->to($emailid)->subject('Confirmation mail');
               //  });
            dd($id);
            dd('Thanks for your confirmation');

    }
    // public function userOrderComplete($id) {
       

    //     // $id=Auth::user()->id;$
    //     if(isset($_GET['tx'])) {
    //         $tx = $_GET['tx'];
            
    //        // dd($tx);
    //         if($_GET['cm']) $user=$_GET['cm']; 

    //         $identity = 'UiVGNY_NxQvud-fBy-g3tVpl9mx1DIdFs0044XujGgw5BD7Vn3tpd1JOb0a'; 
    //         // Init curl
    //          $ch = curl_init(); 
    //         // Set request options 
    //         curl_setopt_array($ch, array ( CURLOPT_URL => 'https://www.paypal.com/cgi-bin/webscr',
    //           CURLOPT_POST => TRUE,
    //           CURLOPT_POSTFIELDS => http_build_query(array
    //             (
    //               'cmd' => '_notify-synch',
    //               'tx' => $tx,
    //               'at' => $identity,
    //             )),
    //           CURLOPT_RETURNTRANSFER => TRUE,
    //           CURLOPT_HEADER => FALSE,
    //           // CURLOPT_SSL_VERIFYPEER => TRUE,
    //           // CURLOPT_CAINFO => 'cacert.pem',
    //         ));
    //         // Execute request and get response and status code
    //         $response = curl_exec($ch);
    //         $arr = explode("\n", $response);
    //         $finalArray = array();
    //         $statusPaypal = FALSE;

    //         foreach($arr as &$vl ){
    //             $tmp = explode("=", $vl);
    //             // dd($tmp);
    //             if(isset($tmp[0])){
    //                 $finalArray[$tmp[0]] = (isset($tmp[1]) ? $tmp[1]:'');

    //             }

    //             if($statusPaypal == FALSE){
    //                 $statusPaypal = (isset($tmp[0]) ? $tmp[0]:FALSE);
    //             }

                
    //         }
    //         //dd($finalArray);
    // // For payment sccess
    //         if ($statusPaypal=='SUCCESS') {
              
    //        // Check cart voucher for custom code
    //             $cart_voucher = new CartVouchers();
    //             $voucher_cart_count = $cart_voucher->getVoucherByCode($finalArray["custom"]);
    //     // Check order for this transaction
    //             $order = new Order();
    //             $order_flag = $order->checkOrderStatus($tx);

    //             //$order_arr = array();
    //             if (!empty($voucher_cart_count) && empty($order_flag)) {
    //                 $order_arr = $order->saveOrder($finalArray);

    //             } else { 
    //             //dd('transaction id not found');                   
    //        // Update user_order table         
    //                 $user_order=Order::where('txn_id',$tx)->first();
    //                 $user_order->payer_email=$finalArray['payer_email'];
    //                 $user_order->user_id=$id;
    //                 $user_order->total_amount=$finalArray['mc_gross'];
    //                 $user_order->save(); 
    //                 $order_arr = array();
    //                 $order_arr['order_id']=$user_order->id;
                   

    //             }
    //     // Check for sms errors
    //             if (isset($order_arr['sms_error']) && !empty($order_arr['sms_error'])){
    //                 $paypalHistory = new PaypalHistory();
    //                 $finalArray['custom_message'] = 'Errors is sending sms';
    //                 $finalArray['user_id']=$id;
    //                 $saveall = $paypalHistory->saveFailTransaction($finalArray);
    //                 return Redirect('order-successful/' . $order_arr['order_id'])->withErrors('errors', $order_arr['sms_error']);
                    
    //             }

    //              // dd($order_arr['order_id']);

    //             return Redirect('order-successful/' . $order_arr['order_id']);  
    //         } else {
    //     // For failed payment
                   
    //                 $paypalHistory = new PaypalHistory();
    //                 $finalArray['custom_message'] = 'Transaction failed';
    //                 $finalArray['user_id']=$id;
    //                 $saveall = $paypalHistory->saveFailTransaction($finalArray);
    //                 $chkpay = Lang::get('message.somethingwrong')." your transation ID is- " .$tx;
                
    //                 return Redirect('cart')->withErrors(['errors' => $chkpay]);
                    
                    
    //         }
            
    //         // unset($arr);
    //         // echo $statusPaypal;        
    //         // dd($finalArray);die;

            


    //     }  else {
            
    //         $data = Request::all();

    //         $history = new PaypalHistory();
    //         $data['custom_message'] = 'Paypal do not return Txn Id ';
    //         $data['user_id']=$id;

    //         $history->saveFailTransaction($data);
    //         return Redirect('cart')->withErrors(['errors' => Lang::get('message.failedPayment')]);

    //     }


    //     //end new code=======================================================





    // }

   public function userOrderComplete($id) {
        sleep(5);

        $data = Request::all();
        // dd($data);
        //$log_file=app_path('paypal.log');
        //@file_put_contents($log_file,"\r\nStart PayPal Postback - ".date('d-M-Y g:ia')."\r\n".print_r($data,true),FILE_APPEND);
        //@file_put_contents($log_file,"\r\nurl - ".$_SERVER['REQUEST_URI'],FILE_APPEND);
        //dd($data);die;
        if (!empty($data)) {
            // looks like there is different paypal data coming back depending on how the check out happens
            // map this data across to the expected data below...
            if (empty($data['payment_status']) && empty($data['custom']) && !empty($data['st']) && !empty($data['cm'])) {
                $data['payment_status']=$data['st'];
                $data['custom']=$data['cm'];
                $data['txn_id']=$data['tx'];
                $data['mc_gross']=$data['amt'];
                // note payer_email is not available here...
            }
            @extract($data);
            if (!empty($payment_status) &&  $payment_status == "Pending") {  //Completed
                if (!empty($data['custom'])) {
                    $cart_voucher = new CartVouchers();
                // check cart_voucher for order_code returned by paypal
                    $voucher_cart_count = $cart_voucher->getVoucherByCode($data["custom"]);
                    $order = new Order();
                // check user_order for txn_id returned by paypal
                    $order_flag = $order->checkOrderStatus($data['txn_id']);
                   // dd($order_flag);
                    if (!empty($voucher_cart_count) && empty($order_flag)) {
                    // dd('flag fresh save');
                // save data if cart_voucher is not empty and user_order is empty for this txn_id 
                        $order_arr = $order->saveOrder($data);
                    } else {
                    // dd('last inserted');
                // fetch last order from user_order table
                        $order_arr['order_id'] = $order->UserLastOrder($id);
                    }
                    
                } else {
                    return Redirect('cart')->withErrors(['errors' => Lang::get('message.somethingwrong')]);
                }
                //echo "<pre>";print_r($order_arr);die();

                if (isset($order_arr['sms_error']) && !empty($order_arr['sms_error'])){
                    
                  ///dd('errors');
                    
                    
                    return Redirect('order-successful/' . $order_arr['order_id'])->withErrors('errors', $order_arr['sms_error']);
                    
                }else{
                    
                   //dd('final purchase');

                    //dd($order_arr['sms_error']);
                        return Redirect('order-successful/' . $order_arr['order_id']);   
                    }
            } else {

                 $paypalHistory = new PaypalHistory();
                    
                    $loggedin_user = Auth::user();    
                    $data['user_id']=$loggedin_user->id;
                    $saveall = $paypalHistory->saveFailTransaction($data);
                    $chkpay = Lang::get('message.somethingwrong')." your transation ID is- " .$data['txn_id'];
                
                return Redirect('cart')->withErrors(['errors' => $chkpay]);

            }
        } else {
            $order = new Order();
            $order_arr['order_id'] = $order->UserLastOrder($id);
            return Redirect('order-successful/' . $order_arr['order_id']);
        }
    }

    public function userOrderNotify($id) {
        // Send an empty HTTP 200 OK response to acknowledge receipt of the notification 
        header('HTTP/1.1 200 OK');
        $data = Request::all();
        if (!empty($data)) {
            @extract($data);
            if ($payment_status == "Completed") {
                if (!empty($data['custom'])) {
                    $cart_voucher = new CartVouchers();
                    $voucher_cart_count = $cart_voucher->getVoucherByCode($data["custom"]);
                    $order = new Order();
                    $order_flag = $order->checkOrderStatus($data['txn_id']);

                    if (!empty($voucher_cart_count) && empty($order_flag)) {
                        $order_arr = $order->saveOrder($data);
                    }
                }
            }
        }
    }

    public function userOrderCancel() {
        return Redirect('cart')->withErrors([
            'message' => Lang::get('message.ordercancel'),
        ]);
    }

    public function userPurchaseHistory() {
        if (Auth::user()) {
            try {
                $order_voucher = new OrderVoucher();
                $pushase_history = $order_voucher->purchageHistory(Auth::user()->id);
                $userGiftVoucher = new UserGiftedVoucher();
                $gift_list = $userGiftVoucher->giftHistory(Auth::user()->id);
                //dd($gift_list);die;
                @extract($pushase_history);
                @extract($gift_list);
                return View::make('history', compact("pushase_history", "gift_list"));
            } catch (Exception $ex) {

            }
        } else {
            return redirect('users/login')->withErrors([
                'email' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    public function orderDetails($order_id = 0) {
        if (Auth::user()) {
            if (!empty($order_id)) {
                try {
                    $order_voucher = new OrderVoucher();
                    $order_details = $order_voucher->orderHistory($order_id, Auth::user()->id);
                    @extract($order_details);

                    return View::make('order', compact("order_details"));
                } catch (Exception $ex) {

                }
                return Redirect::back()->withErrors(['message' => Lang::get('message.showorderrerror')]);
            }
        } else {
            return Redirect('users/login')->withErrors(['message' => Lang::get('message.sessionerrerror')]);
        }
    }

    public function schedulerForSendSMS() {

        $orderVoucher = new OrderVoucher();
        $orderVoucher->sendScheduledSms();

        //@mail("sandeep.sharma@appster.in","cron test","cron test");
    }

    public function rememberSmsUnused() {
        $orderVoucher = new OrderVoucher();
        $orderVoucher->rememberSmsUnused();
    }

    public function prePaypal($cart_id) {
        $cartVoucher = new CartVouchers();
        $uniqe_code = $cartVoucher->addUniqeCode($cart_id);
        return $uniqe_code;
    }

    public function test() {
        $str = '{"mc_gross":"2.00","outstanding_balance":"0.00","period_type":" Regular","next_payment_date":"03:00:00 May 05, 2017 PDT","protection_eligibility":"Eligible","payment_cycle":"every 12 Months","address_status":"unconfirmed","tax":"0.00","payer_id":"JTH2MCUDSA7UQ","address_street":"WZ - 64 Virender Nagar\r\nStreet No - 9","payment_date":"14:15:23 May 10, 2016 PDT","payment_status":"Completed","product_name":"12 Months Subscription","charset":"windows-1252","recurring_payment_id":"I-ND9CMXX0JPBP","address_zip":"110058","first_name":"Vaibhav","mc_fee":"0.37","address_country_code":"IN","address_name":"Vaibhav Sharma","notify_version":"3.8","amount_per_cycle":"2.00","payer_status":"unverified","currency_code":"AUD","business":"accounts@cherrygift.com","address_country":"India","address_city":"New Delhi","verify_sign":"ABp7R3R9yld3nfmt2lEiM8VCekzmA.FzXWg3dvl2xkeZKhOLdJXGEl6f","payer_email":"vaibhavsharma@runbox.com","initial_payment_amount":"0.00","profile_status":"Active","amount":"2.00","txn_id":"98H95375GB498745P","payment_type":"instant","last_name":"Sharma","address_state":"National Capital Territory of Delhi","receiver_email":"accounts@cherrygift.com","payment_fee":"","receiver_id":"EZ5MVT7R53Z3Q","txn_type":"recurring_payment","mc_currency":"AUD","residence_country":"IN","transaction_subject":"12 Months Subscription","payment_gross":"","shipping":"0.00","product_type":"1","time_created":"04:50:21 May 05, 2016 PDT","ipn_track_id":"6a0ac6828594d"}';
        $arr = json_decode($str);


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://localhost/cherry-new/cherry-gift-php/public/expressCheckoutIPN");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$arr);

        // in real life you should use something like:
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 
        //          http_build_query(array('postvar1' => 'value1')));

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);
        echo "<pre>";
        print_r($server_output);
        die;

        $o = new Order();
        $data = array( 'order_id' => 36, 'voucher_list' => array( '106' => array( '0' => array( 'price' => 100, 'mobile' => '9650 915 120' ) ) ) );
        $res = $o->sendInvoiceMail($data);
        die;
    }

    public function getUserTimezone($offset) {
        $ip = $_SERVER["REMOTE_ADDR"];
        $data = geoip_record_by_name($ip);
        //        $getip = file_get_contents("http://freegeoip.net/json/$ip");
        //        $getip = json_decode($getip);
        print_r($data);
        die;
        $tz = $this->tz_offset_to_name($offset);
        //        
        //        $fullurl = "https://maps.googleapis.com/maps/api/timezone/json?location=".$lat.",".$long."&timestamp=".time()."&key=AIzaSyBnB6wTtICXGBCHvPVZvxIlB-2FFlPAoIY";
        //        $string = file_get_contents($fullurl); // get json content

        return array("rawOffset"=>$offset,"timeZoneId"=>$tz);

    }

    public function tz_offset_to_name($offset)
    {
        echo $offset *= 60; // convert hour offset to seconds
        $abbrarray = timezone_abbreviations_list();
        foreach ($abbrarray as $abbr)
        {
            foreach ($abbr as $city)
            {
                print_r($city);
                if ($city['offset'] == $offset)
                {
                    return $city['timezone_id'];
                }
            }
        }
        return FALSE;
    }
}
