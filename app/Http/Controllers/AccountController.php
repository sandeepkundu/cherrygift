<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Input;
use Hash;
use Redirect;
use File;
use App\User;
use View;
use Session;
use Crypt;
use Response;
use App\Model\Company;
use App\Model\RotateImages;
use App\Model\Suburbs;
use App\Model\Users;
use Illuminate\Support\Facades\Lang;

class AccountController extends Controller {
    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    public function edit($id) {
        if (Auth::check()) {
            //echo $id; exit;
            // get the nerd
            $user = User::find($id);

            // show the edit form and pass the nerd
            return View::make('edit-profile')
                            ->with('user', $user);
        } else {
            return redirect('users/login')->withErrors([
                        'email' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        if (Auth::check()) {
            $id = Crypt::decrypt($id);
            $nerd = User::find($id);
            // validate
            // read more on validation at http://laravel.com/docs/validation
            $rules = array(
                //'email' => 'required|email|unique:users',
                //'password' => 'required|confirmed|min:6',
                'first_name' => 'required',
                'mobile_number' => 'required|min:8|max:21',
                'image' => 'mimes:jpeg,bmp,png,gif',
               
               
            );
            if(!empty(Input::get('now_password')))
            {
                $rules['new_password']='min:6|different:now_password';
            }else{
                $rules['new_password']='min:6';
                
            }
            
            $messages = array(
                'new_password.different' => 'The Current password and New password must be different.');
            $validator = Validator::make(Input::all(), $rules, $messages);

            // process the login
            if ($validator->fails()) {


                return Redirect::to('users/' . Crypt::encrypt($id))
                                ->withErrors($validator)
                                ->withInput(Input::except('password'));
            }
            if (!empty(Input::get('now_password')) && !empty(Input::get('new_password'))) {
                $now_password = Input::get('now_password');
                if (!Hash::check($now_password, Auth::user()->password)) {
                    return Redirect::to('users/' . Crypt::encrypt($id))
                                    ->withErrors(Lang::get('message.passworderror'))
                                    ->withInput(Input::except('password'));
                } else {
                    $nerd->password = bcrypt(Input::get('new_password'));
                }
            }

            if (!empty(Input::get('facebook_id')) && !empty(Input::get('new_password'))) {

                $nerd->password = bcrypt(Input::get('new_password'));
                $nerd->facebook_id = '';
            }

            // store

            if (Input::hasFile('image')) {
                $imagename = strtotime(date('Y-m-d H:i:s')) . Input::file('image')->getFilename() . '.' . Input::file('image')->getClientOriginalExtension();
                //$destinationFolder = public_path() . "/imgs/profile/";
                $destinationFolder = getS3LocalPath()."/imgs/profile/";
                $s3_distination = env('s3_env') . "/imgs/profile/" . $imagename;
                Input::file('image')->move($destinationFolder, $imagename);
                //Input::file('image')->move($destinationFolder, $imagename);
                // uploading file to given path
                self::upload_rotate_image($destinationFolder . $imagename, Input::file('image')->getClientOriginalExtension());
                if (env('s3_on')) {
                    RotateImages::uploadImageS3($destinationFolder . $imagename, $s3_distination);
                }
                File::delete('$destinationFolder' . $nerd->image);  // Remove previous image
            } else {
                $imagename = $nerd->image;
            }
            $nerd->mobile_number = Input::get('mobile_number');

            $nerd->first_name = Input::get('first_name');
            $nerd->last_name = Input::get('last_name');
            $nerd->image = $imagename;
            $nerd->save();

            // redirect
            //Session::flash('message', 'Successfully updated profile!');

            return Redirect::back()->with('message', Lang::get('message.updatesucess'));
        } else {
            return redirect('users/login')->withErrors([
                        'email' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        if (Auth::check()) {
            $id = Crypt::decrypt($id);
            //echo $id; die;
            $user = User::find($id);
            $user->status = 'Deleted';
            $user->save();
            Auth::logout();
            Session::flash('message', Lang::get('message.profileDeleted'));
            return Redirect::to('/');
        } else {
            return redirect('users/login')->withErrors([
                        'email' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    public function show($id) {
        $id = Crypt::decrypt($id);
        if (Auth::check()) {
            $user = User::find($id);
//print_r($user); exit;
            // show the view and pass the nerd to it
            return View::make('edit-profile')
                            ->with('user', $user);
        } else {
            return redirect('users/login')->withErrors([
                        'email' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    public static function rotate($src, $ext) {
        $deg = 0;
        if (function_exists('exif_read_data') && @exif_imagetype($src) && strtolower($ext) != 'png') {
            $exif = @exif_read_data($src);
            if ($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                if ($orientation != 1) {
                    switch ($orientation) {
                        case 3:
                            $deg = 180;
                            break;
                        case 6:
                            $deg = 270;
                            break;
                        case 8:
                            $deg = 90;
                            break;
                    }
                }
            }
        }

        return $deg;
    }

    public static function upload_rotate_image($src, $ext) {
        //print_r($ext);die;
        $deg = self::rotate($src, $ext);
        //$deg = 0;
        if (strtolower($ext) == 'jpg' || strtolower($ext) == 'jpeg')
            $source_image = imagecreatefromjpeg($src);
        else
            $source_image = imagecreatefrompng($src);

        if ($deg) {
            $source_image = imagerotate($source_image, $deg, 0);
        }

        $image_saved = imagejpeg($source_image, $src);
    }

    public function cityList() {
        $id = Input::get('state_id');
        return Suburbs::where('state_id', $id)->select('locality', 'id')->orderBy('locality', 'asc')->get();
    }

    public function suburbsList() {
        $term = Input::get('term');
        $id = Input::get('stateid');
$suburbs = new Suburbs();
 $books = array();
$books = $suburbs->getSuburbsList($id,$term);
     return $books;   
//return $citieslist;
    }

    public function checkEmail() {
        $user_email = Input::get('user_email');
        $objuser = new Users();
        $user = $objuser->getUserByEmail($user_email);
        if ($user) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checkAdminEmail() {
        $user_email = Input::get('user_email');
        $user = Company::where('email', $user_email)->count();
        if ($user == 0) {
            return 0;
        } else {
            return 1;
        }
    }

}
