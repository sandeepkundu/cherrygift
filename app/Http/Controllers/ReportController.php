<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use View;
use App\Model\CompanyBusinessProfile;
use App\Model\OrderVoucher;
use Lang;
use Carbon\Carbon;

class ReportController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $month = 0, $year = 0) {
        if (empty($month)) {
            $month = date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }
        if (Session::has('id')) {

            $outlate_summery = new CompanyBusinessProfile();
            $getredeemedsummery = $outlate_summery->getOutletSummery($id);
            $total_redeem = new OrderVoucher();
            $total_redeem = $total_redeem->totalRedeem($id, $month, $year);
            if($total_redeem!=0){
            $totalpay = $total_redeem-($total_redeem*9/100);}
            else
            {$totalpay='NA';}
            $redeemed_history = new OrderVoucher();
            $get_redeemed_history = $redeemed_history->redeemedHistory($id, $month, $year);
            $next = explode("-", Carbon::create($year, $month, 1)->addMonths(1)->format('Y-m-d'));
            $prev = explode("-", Carbon::create($year, $month, 1)->subMonth(1)->format('Y-m-d'));  
            @extract($getredeemedsummery);
            return View::make('admin-pages.dashboard', compact('id', 'nextdate', 'total_redeem', 'get_redeemed_history',"next","prev",'totalpay'));
        } else {
            return redirect('admin-signup')->withErrors([
                        'errors' => Lang::get('message.sessionerrerror'),
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
