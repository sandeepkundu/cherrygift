<?php

namespace App\Http\Controllers;

use App\Model\Enquiry;
use View;
use Response;
use Session;
use Illuminate\Http\Request;
use Mail;

class EnquiryController extends Controller {

    public function getEnquiry() 
    {
        $enquiry=new Enquiry();
        return View::make('enquiry/contact',['enquiry' => $enquiry]);
    }

    public function postEnquiry(Request $request) 
    {
        $this->validate($request,[
            'email' => 'required|email',
            'first_name'=> 'required',
            'last_name'=> 'required',
            'contact_number' => 'min:8|max:21',
            'message'=>'required'
        ]);

        $enquiry=new Enquiry($request->all());
        $enquiry->remote_ip=$_SERVER['REMOTE_ADDR'];
        $enquiry->save();
        Mail::send('enquiry.email-user', ['enquiry' => $enquiry], function($message) use($enquiry) {
            $message
            ->to($enquiry->email, $enquiry->first_name . ' ' . $enquiry->last_name)
            ->subject('Your enquiry on cherrygift ');
        });
        Mail::send('enquiry.email-admin', ['enquiry' => $enquiry], function($message) use($enquiry) {
            $message
            ->from('enquiries@cherrygift.com','CherryGift Enquiries')
            ->to(env('contact_email'))
            ->replyTo($enquiry->email)
            ->subject('New enquiry #'.sprintf('%05d',$enquiry->id).' - '.$enquiry->first_name.' '.$enquiry->last_name);
        });
        return redirect('contact-thanks');
    }

}
