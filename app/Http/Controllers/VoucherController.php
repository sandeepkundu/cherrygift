<?php

namespace App\Http\Controllers;
use Auth;
use Input;

use App\Model\CompanyBusinessProfile;
use App\Model\Order;
use App\Model\PaypalHistory;
use View;
use Response;
use App\Model\OrderVoucher;
use App\Model\Payments;
use App\Model\Users;
use App\Model\Company;
use Lang;
use Carbon\Carbon;
use Crypt;
use Request;
use Session;
use DB;
use Twilio\Rest\Client;
class VoucherController extends Controller {
    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
    public function testing() {

    echo 'This is \'test\' string $a'.'<br>';
    echo "This is \'test\' string";
    die; 
    dd('asd');
    //for sending sms on mobile using for testing
    //$code =mt_rand(100000, 999999);
    //$mobilenumber='8010783021';
    $code='https://www.cherrygift.com/';
    // $mediaUrl = "https://c1.staticflickr.com/3/2899/14341091933_1e92e62d12_b.jpg";

// dd($mediaUrl);
$ch = curl_init();
$user="mohd.nadeem62@gmail.com:9919822801";
$receipientno="8010783021";
$senderID="TEST SMS";
$msgtxt="One Time Password for Pan Card Online registration is".$code.".";
curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
$buffer = curl_exec($ch);
if(empty ($buffer))
{ echo " buffer is empty "; }
else
{ echo $buffer; }
curl_close($ch);
//////////////////////////////////////////////////////////////end testing sms code


    }
    public function edit($id) {
        //dd('as');
       // $id = Crypt::encrypt($id);
    $id = Crypt::decrypt($id);
       // $code =mt_rand(100000, 999999);
        //or$code =rand(100000, 999999);
            dd($id);
//for sending sms on mobile using for testing
       
// $ch = curl_init();
// $user="mohd.nadeem62@gmail.com:9919822801";
// $receipientno="8010783021";
// $senderID="TEST SMS";
// $msgtxt="One Time Password for Pan Card Online registration is".$code.".";;
// curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($ch, CURLOPT_POST, 1);
// curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
// $buffer = curl_exec($ch);
// if(empty ($buffer))
// { echo " buffer is empty "; }
// else
// { echo $buffer; }
// curl_close($ch);
//////////////////////////////////////////////////////////////end testing sms code

        //echo "hello";
// Get the PHP helper library from twilio.com/docs/php/install


// Your Account Sid and Auth Token from twilio.com/user/account
// $sid = "AC4be3671b99141b557a35104b48182672";
// $token = "34e2743eb2344c6f2972138817a33447";
// $client = new Client($sid, $token);

// $number = $client->incomingPhoneNumbers->create(
//     array(
//         "voiceUrl" => "http://demo.twilio.com/docs/voice.xml",
//         "phoneNumber" => "+15005550006"
//     )
// );

// echo $number->sid; 



//Get the PHP helper library from twilio.com/docs/php/install
//require app_path().'/path/to/vendor/autoload.php'; // Loads the library


//Your Account Sid and Auth Token from twilio.com/user/account
// $sid = "AC4be3671b99141b557a35104b48182672";
// $token = "34e2743eb2344c6f2972138817a33447";
// $client = new Client($sid, $token);

// $sms = $client->messages->create(
//     "+919953272512",
//     array(
//         "from" => "+15005550006",
//         "body" => "Hey Mr Nugget, you the bomb!",
//          "mediaUrl" => "https://c1.staticflickr.com/3/2899/14341091933_1e92e62d12_b.jpg",
//     )
// );

// echo $sms->sid;



}

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update() {
        $merchant_pin = Input::get('merchant_pin');
        $voucher_id = Input::get('voucher_id');
        $order_voucher = new OrderVoucher();
        $voucherdetails = $order_voucher->getOrderById($voucher_id);
        if($voucherdetails->redeemed==1 || $voucherdetails->voucher_status==0)
        {
            return Lang::get('message.failed');
        }
        $companybusinesprofile = new CompanyBusinessProfile();
          $outlates = $companybusinesprofile->getCompanyids($voucherdetails->company_business_profile_id); 
        //$outlates get the company_id       
          $company = new Company();
          $voucher_exits = $company->voucherExists($merchant_pin, $outlates[0]->company_id,$voucher_id);
          if($voucher_exits==1)
          {
              return Lang::get('message.voucherApproved');
          }
          else
          {
              return Lang::get('message.invalideMerchantPIN');
          }
        //$companydata = $company->getCompanyDetails($voucherdetails->company_business_profile_id);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $id = Crypt::decrypt($id);

        $order_voucher = new OrderVoucher();
        $voucherdetails = $order_voucher->getOrderById($id);
       
        $company = new Company();
        $companydata = $company->getCompanyDetails($voucherdetails->company_business_profile_id);
         //dd($companydata);
        //$redeemed = $voucher_details->redeemed; 
        @extract($companydata);
        return View::make('redeem', compact('companybusiness', 'companybusinesslocation', 'companybusinessimages', 'companytiming', 'statename', 'cityname', 'companyname', 'voucherdetails'));
    }

public function reSendVouchers(){
     if (!Auth::check()) {
         $url = Request::url();
            Session::put('befor_login_url', $url);
            return Redirect('users/login')->withErrors([
                        'email' => Lang::get('message.login'),
            ]);
        }
        try{
             $mobile_number =  Input::get('search_contact');
             $order = new OrderVoucher();
             $purshase_history = $order->getVouchers($mobile_number, Auth::user()->id);
            // dd($purshase_history);
             //echo "<pre>";print_r($purshase_history);die;
             @extract($purshase_history);
            return View::make('resend', compact("purshase_history","mobile_number"))->withInput(Input::all());
            
            
        } catch(ModelNotFoundException $ex) {
            //$this->model_not_found_error(Lang::get('messages.user_not_found'));
        }
       return $this->render();
    }
    public function remindRedeemVoucher()
    {
       $date = Carbon::today()->subMonth(6)->format('Y-m-d'); 
       $order = new OrderVoucher();
      $getcontact_number = $order->getPendingRedeemedVouchers($date);
    }
    
    public function reSendAll(){
     if (!Auth::check()) {
         $url = Request::url();
            Session::put('befor_login_url', $url);
            return Redirect('users/login')->withErrors([
                        'email' => Lang::get('message.login'),
            ]);
        }
        try{
             $mobile_number =  Input::get('search_contact'); 
             $order = new OrderVoucher();
             $purshase_history = $order->getVouchersDataArray($mobile_number, Auth::user()->id);
             $orderObj = new Order();
             $error = $orderObj->resendBulkSms($purshase_history);
             return $error;
        }catch(ModelNotFoundException $ex){
            //$this->model_not_found_error(Lang::get('messages.user_not_found'));
        }
       return $this->render();
    }
    
    
    
    public function resendAllVouchers($user_id,$mobile_number){
             $order = new OrderVoucher();
             $purshase_history = $order->getVouchersDataArray($mobile_number, $user_id);
             $orderObj = new Order();
             $error = $orderObj->resendBulkSms($purshase_history);
             
             return $error;
    }

   public function resendAllVouchersbyid($user_info){
            
             $order = new OrderVoucher();
             $purshase_history = $order->getVouchersDataArrayByid($user_info);
            //dd($purshase_history );
             $orderObj = new Order();
             $error = $orderObj->resendBulkSms($purshase_history);
            
             return $error;
    }

    public function resendSaveBeforePaypal() {
        
        $data = Request::all();
        
        $paypalHistory = new PaypalHistory();
        $paypalHistory->user_id=Auth::user()->id;
        $paypalHistory->item_name = $data['item_name'];
        $paypalHistory->custom = $data['custom'];
        $paypalHistory->payment_status = 'iniciated';
        $paypalHistory->payment_date = date('Y-m-d H:i:s');
        $paypalHistory->paypal_object = json_encode($data);
        $paypalHistory->save();
        session(['paypal_history_id'=>$paypalHistory->id]);


        return response()->json([
            'id'=>session('paypal_history_id')
        ]);

    }
    public function resendNotify()
    {
        
    }
    
       public function resendPaymentComplete()
    {
        // dd($data = Request::all());
        // $data = Request::all();

        //new code 
       
        

        
        if(isset($_GET['tx'])) {
            $tx = $_GET['tx'];
            $history_id = session('paypal_history_id');
           
            // dd($history_id);
            $check_txn = PaypalHistory::where('txn_id',$tx)->get();
             // dd($check_txn);
            
            
            
            if($_GET['cm']) $user=$_GET['cm']; 
            $identity = 'UiVGNY_NxQvud-fBy-g3tVpl9mx1DIdFs0044XujGgw5BD7Vn3tpd1JOb0a'; 
            // Init curl
             $ch = curl_init(); 
            // Set request options 
            curl_setopt_array($ch, array ( CURLOPT_URL => 'https://www.paypal.com/cgi-bin/webscr',
              CURLOPT_POST => TRUE,
              CURLOPT_POSTFIELDS => http_build_query(array
                (
                  'cmd' => '_notify-synch',
                  'tx' => $tx,
                  'at' => $identity,
                )),
              CURLOPT_RETURNTRANSFER => TRUE,
              CURLOPT_HEADER => FALSE,
              // CURLOPT_SSL_VERIFYPEER => TRUE,
              // CURLOPT_CAINFO => 'cacert.pem',
            ));
            // Execute request and get response and status code
            $response = curl_exec($ch);
            $arr = explode("\n", $response);
            $finalArray = array();
            $statusPaypal = FALSE;

            foreach($arr as &$vl ){
                $tmp = explode("=", $vl);
                //dd($tmp);
                if(isset($tmp[0])){
                    $finalArray[$tmp[0]] = (isset($tmp[1]) ? $tmp[1]:'');

                }

                if($statusPaypal == FALSE){
                    $statusPaypal = (isset($tmp[0]) ? $tmp[0]:FALSE);
                }

                
            }
    // For payment sccess
            if ($statusPaypal=='SUCCESS') {
                
           // Check paypal history for $tx
                if(empty($check_txn)) {

                    $payment = new Payments();
                    $user_info = urldecode($finalArray['custom']);
                  // dd(array($user_info));
                    $payment_data = array(
                        "user_type"=>1,
                        "user_id"=>Auth::user()->id,
                        "paypal_history_id"=> $history_id,
                        "transaction_type"=>3,
                        "susbscriber_id"=>$finalArray['payer_id'],
                        "transaction_id"=>$tx,
                        "status"=>1
                    );
                    $payment->save_vendor_payment($payment_data);
                // Send Sms
                    $errors = $this->resendAllVouchersbyid($user_info);
                    if(!empty($errors)) {
                        return Redirect('resend')->withErrors(['errors' => $errors]);   
                    }

                    $paypalHistory = new PaypalHistory();            
                    $paypalHistory->updatePaypalHistory($finalArray,$history_id);
                } else {

                    $history = PaypalHistory::where('id',$history_id)->first();
                    $history->custom_message = 'Paypal do not return Txn Id';
                    $history->save();
                    return Redirect('resend')->withErrors(['errors' => 'Payment for this Txn Id is allready exist']);
                }   
            } else {
    // For failed payment
                    $history = PaypalHistory::where('id',$history_id)->first();

                    $history->custom_message = 'Payment has been failed';
                    $history->save();
                    return Redirect('resend')->withErrors(['errors' => Lang::get('message.failedPayment')]);
            }
            
            // unset($arr);
            // echo $statusPaypal;        
            // dd($finalArray);die;

            


        }   else {
            $history = PaypalHistory::where('id',$history_id)->first();
            $history->custom_message = 'Paypal do not return Txn Id ';
            $history->save();
            return Redirect('resend')->withErrors(['errors' => Lang::get('message.failedPayment')]);
        } 
        //////end new code 


 
    }
}
