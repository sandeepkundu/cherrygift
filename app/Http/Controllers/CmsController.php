<?php

namespace App\Http\Controllers;

use App\Model\Company;
use App\Model\Suburbs;
use App\Model\CompanyBusinessProfileAudit;
use App\Model\CompanyBusinessReceipientsAudit;
use App\Model\CompanyBusinessReceipients;
use App\Model\CompanyBusinessOccationsAudit;
use App\Model\CompanyBusinessOccations;
use App\Model\CompanyTimingsAudit;
use App\Model\CompanyTimings;
use App\Model\CompanyBusinessImagesAudit;
use App\Model\CompanyBusinessImages;
use App\Model\Payments;
use App\Model\CompanyProfileLogs;
use App\Model\CompanyBusinessProfile;
use App\Model\Order;
use App\Model\Enquiry;
use Illuminate\Http\Request;
use Session;
use View;
use Illuminate\Support\Facades\Input;
//use yajra\Datatables\Facades\Datatables;
use Illuminate\Support\Facades\Crypt;
use Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Mail;
use File;
use Redirect;
use App\Model\OrderVoucher;
use App\Model\HomepageSliderImages;
use App\Model\Users;
use App\Model\AdminVendorPayment;
use App\Model\RotateImages;
use App\Http\Controllers\SubscriptionCmsController;
use App\Model\HomepageRecipientImages;
use App\Model\SubscriptionPrice;
use DB;
use mPDF;
use App\Model\PaypalHistory;
class CmsController extends Controller {

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct() {
        //$this->middleware('App\Http\Middleware\CmsAuth',['except' => ['index']]);
    }

    /**
    * Show the application dashboard to the user.
    *
    * @return Response
    */
    public function index() {
        //Session::flush();
        if (isset(Auth::user()->role) && (Auth::user()->role == "Admin")) {
            return redirect('super-admin/dashboard');
        } else {
            return View::make('admin-cms/login');
        }
    }

    public function dashboard() {
        return View::make('admin-cms/dashboard');
    }

    public function profile() {
        return View::make('admin-cms/profile');
    }

    public function customer() {
        return View::make('admin-cms/customer');
    }

    public function enquiry() {
        return View::make('admin-cms/enquiry');
    }

    public function store($id) {
        $company = new Company();
        $results = $company->getCompanyById($id);
        $company_name = $results->company_name;
        return View::make('admin-cms/store', compact('id', 'company_name'));
    }

     public function storeDetail($id) {
        if (!empty($id)) {
            $companyprofile = new CompanyBusinessProfileAudit();
            $OldCompanyProfile = new CompanyBusinessProfile();
            
            $changArr=array();
            $oldFArr=array();
            $companybusiness = $companyprofile->getStoreById($id);
            
            $companybusinessOld = $OldCompanyProfile->getStoreById($id)->toArray();
            $companybusinessNew = $companybusiness->toArray();
            
            foreach ($companybusinessNew as $key => $value) {
                   
                   if($value != $companybusinessOld[$key]){

                    $changArr[$key]=$value;
                    $oldFArr[$key]=$companybusinessOld[$key];
                    
                   }
                    
            }
            $subs_price = $companybusiness->subscription_amount;
            if (empty($subs_price)) {
                $subscription_price = new SubscriptionPrice();
                $amount = $subscription_price->getSubscriptionDetails();
                $subs_price = $amount->sub_price;
            }
            $store_audit = new CompanyBusinessProfileAudit();
            $companybusinessprofile = $store_audit->getStoreById($id);
            $companydb = new Company();
            $company = $companydb->getCompanyById($companybusinessprofile->company_id);
            $app = app();
            $recarr = array();
            $occarr = array();
           // $objrecipients = new CompanyBusinessReceipientsAudit();
            //$objoccationaudit = new CompanyBusinessOccationsAudit();

                        //coded by sandeep
            $objoccationaudit = new CompanyBusinessOccationsAudit();
            $Oldobjoccationaudit = new CompanyBusinessOccations();
            $occations = $objoccationaudit->auditDetails($id);
            //dd($occations);
            
            $occationsNew = $occations->toArray();
            $occationsOld = $Oldobjoccationaudit->auditDetails($id)->toArray();
                

            $oldCatArr=array();
            $newCatArr=array();

            foreach ($occationsNew as $key => $value) {
               
                $oldCatArr[] = $value['occasion_id'];
            }
            foreach ($occationsOld as $key => $value) {
               
                $newCatArr[] = $value['occasion_id'];
            }

            $posArr2=array_diff($oldCatArr,$newCatArr);
            $posArr1=array_diff($newCatArr,$oldCatArr);
            $posArr = array_merge($posArr1,$posArr2);
           
            $Oldobjrecipients = new CompanyBusinessReceipients();
            $objrecipients = new CompanyBusinessReceipientsAudit();
            $recipients = $objrecipients->auditDetails($id);
           // dd($recipients);
            $recipientsNew = $recipients->toArray();
            //dd($recipientsNew);
            $recipientsOld = $Oldobjrecipients->auditDetails($id)->toArray();
            //dd($recipientsOld);
            $oldResArr=array();
            $newResArr=array();
            

             foreach ($recipientsNew as $key => $value) {
               
                $oldResArr[] = $value['recipient_id'];
            }
            foreach ($recipientsOld as $key => $value) {
               
                $newResArr[] = $value['recipient_id'];
            }

               $posArr3=array_diff($oldResArr,$newResArr);
            $posArr4=array_diff($newResArr,$oldResArr);
            $resArr = array_merge($posArr4,$posArr3);
            //dd($resArr);

            $objcompanytimingaudit = new CompanyTimingsAudit();
            $companybussinessimageaudit = new CompanyBusinessImagesAudit();
            
            $objcompanytiming = new CompanyTimings();
            $companybussinessimage = new CompanyBusinessImages();

            $suburb = new Suburbs();
            $subres = $suburb->getSuburbById($companybusinessprofile->city_id);
            $recipients = $objrecipients->auditDetails($id);
            $occations = $objoccationaudit->auditDetails($id);
            if (!empty($recipients)) {
                foreach ($recipients as $rec) {
                    $recarr[] = $rec->recipient_id;
                }
            } 
            foreach ($occations as $rec) {
                $occarr[] = $rec->occasion_id;
            }
            $daystiming = $objcompanytimingaudit->auditDetails($id);
             
            $daystimingOld = $objcompanytiming->auditDetails($id);
            //dd($daystimingOld);
             $daystimings=array();
            foreach ($daystiming as $rec) {
                $daystimings[$rec->day] = $rec->timings;
            }
            foreach ($daystimingOld as $rec) {
                $daystimingOlds[$rec->day] = $rec->timings;
            }
            $apArr=array();

            foreach ($daystimings as $key => $value) {
                if($value != $daystimingOlds[$key]){
                   $apArr[$key]=$daystimingOlds[$key]; 
                }
            }
            $businessimages = $companybussinessimageaudit->auditDetails($id);
            //echo $businessimages;
            $businessimagesOld = $companybussinessimage->auditDetails($id);
            
            foreach ($businessimages as $rec) {


                $businessimagesarr[$rec->voucher_image] = $rec->image;
            }

            $businessimagesOld=$businessimagesOld->toArray();
            $businessimages=$businessimages->toArray();    
            //dd($businessimages);
            $changArrImgNew=array();
            $changArrImgOld=array();
            foreach ($businessimages as $key=>$value) {
            if(isset($businessimagesOld[$key]['image']) && !empty($businessimagesOld[$key]['image'])){
             if($value['image'] != $businessimagesOld[$key]['image']){

                    $changArrImgNew[$key]=$value['image'];
                    $changArrImgOld[$key]=$businessimagesOld[$key]['image'];
                    
                   }
                 }
                }
                
           // dd($businessimagesOlds[$rec->voucher_image]);


            $suburbsname = $subres->locality;
            $paypal_profile_id = "";
            $payment_detail = Payments::where("user_id", $id)->where("user_type", 2)->where("transaction_type", 1)->where("status", 1)->first();

            if (!empty($payment_detail) && $companybusinessprofile->is_subscribed == 1) {
                $paypal_profile_id = $payment_detail->susbscriber_id;
            }

            return view('admin-cms/store-detail', compact('changArrImgOld','changArrImgNew','resArr','apArr','posArr','oldFArr','changArr','company', 'companybusinessprofile', 'suburbsname', 'recarr', 'occarr', 'daystimings', 'businessimagesarr', 'paypal_profile_id','subs_price'));
        }
    }

    public function viewCustomer() {
        $id = Input::get('id');
        $company = new Company();
        $results = $company->getCompanyById($id);
        //dd($results);
        if ($results->account_number) {
            $results->account_number = Crypt::decrypt($results->account_number);
        }
        if ($results->suburs_id) {
            $suburb = new Suburbs();
            $subres = $suburb->getSuburbById($results->suburs_id);

            $results->suburbsname = $subres->locality;
        }
        else {
            $results->suburbsname='';
        }
        return $results;
    }

    public function customerAction() {

        $id = Input::get('id');
        $value = Input::get('value');
        $company = new Company();
        $results = $company->activateCompany($id, $value);
        $msg = ($value == 1) ? Lang::get('cms.companyactive') : Lang::get('cms.companydeactive');
        if ($value == 2) {
            $cmssubs = new SubscriptionCmsController();
            $cmssubs->cancelStore($id, 'company');
        }
        $data['status'] = "success";
        Session::put('success-msg', $msg);
        return json_encode($data);
    }

     public function userAction() { //code added by sandeep

        $id = Input::get('id');
        $value = Input::get('value');
       DB::table('users')
            ->where('id', $id)
            ->update(['status' => $value]);
       
}

     public function vendorAction() { //code added by sandeep

        $id = Input::get('id');
        $value = Input::get('value');
        //dd($value);
       DB::table('company')
            ->where('id', $id)
            ->update(['is_subscribed' => $value]);
       
}



    public function storeAction() {

        $id = Input::get('id');
        $value = Input::get('value');
        $companyst = new CompanyBusinessProfile();
        $companyst = $companyst->getStoreById($id);
        $isapproved_status = $companyst->is_approved;
        $store_audit = new CompanyBusinessProfileAudit();
        $results = $store_audit->activateStore($id, $value);

        if (in_array($value, array(1, 4))) {
            $data = array();
            $store_audit->updateApprovedDetails($id);
            $row = $store_audit->getStoreById($id);
            $company_id = $row->company_id;
            $company_details = new Company();
            $comprow = $company_details->getCompanyById($company_id);
            if (env('APP_DEV',false)) {
                $email_id = env('contact_email');
            }
            else {    
                $email_id = $comprow->email;
            }
            $data['merchant_pin'] = $row->merchant_pin;
            $data['store_location'] = $row->store_location;
            $data['first_name'] = $comprow->first_name;
            if ($value == 4) {
                if ($isapproved_status == 0) {
                    // send mail approved

                    Mail::send('admin-cms/approve_email', $data, function($message) use($email_id, $data) {
                        $message->to($email_id)->subject('cherrygift admin approved your store profile ' . $data['store_location']);

                    });
                    $msg = Lang::get('cms.storeapprove');
                } else {
                    Mail::send('admin-cms/approve_email_contents', $data, function($message) use($email_id, $data) {
                        $message->to($email_id)->subject('cherrygift admin approved your store profile changes ' . $data['store_location']);
                    });
                    $msg = Lang::get('cms.storeactive');
                }
            } else {
                $msg = Lang::get('cms.storeactive');
            }
        } else {
            $msg = Lang::get('cms.storedeactive');
            if ($value == 2) {
                $cmssubs = new SubscriptionCmsController();
                $cmssubs->cancelStore($id, 'store');
            }
        }

        $data['status'] = "success";
        Session::put('success-msg', $msg);
        return json_encode($data);
    }

      public function customerList() {
        $company_audit = new Company();
        $results = $company_audit->getCompanyReport();

        return \Datatables::of($results)
        ->editColumn('voucher_unique_id', function($user) {
       
        return '<span style="display:none">'.$user->voucher_unique_id.'</span>';
          })
        ->editColumn('store_location', function($user) {
       
        return '<span style="display:none">'.$user->store_location.'</span>';
          })
        ->editColumn('merchant_pin', function($user) {
       
        return '<span style="display:none">'.$user->merchant_pin.'</span>';
          })
        ->editColumn('is_subscribed', function($user) {
            $isactive = $user->is_subscribed;
            $statusArr = array("0" => 'Pending', "1" => "Active", "2" => "Deactive", "3" => "Rejected");
            return $statusArr[$isactive];
        })
        ->editColumn('phone', function($user) {
            $phone = '+61 '. $user->phone;
            return $phone;
        })
        ->editColumn('id', function($user) {
            $isactive = $user->is_subscribed;
            $html = '';
            $url = url() . '/super-admin';
            $html.= '<a id="'.$user->id .'" title= "Set Password" href= "javascript:void(0)" class="btn111"><i class="fa fa-key" style="font-size:20px;color:#8dc53e;"></i></a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';
            $html.= '<a title= "View Company Details" href= "javascript:void(0)" onClick="view_customer(' . $user->id . ')" class=""><i class="fa fa-pencil-square-o" style="font-size:20px;color:#8dc53e;"></i></a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';
            $html.= '<a title= "Delete Company" href= "javascript:void(0)" id="'. $user->id .'" class="" onClick="delete_company(' . $user->id . ')"><i class="fa fa-trash-o" style="font-size:20px;color:#8dc53e;"></i></a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';
            $html.=($isactive == 2) ? '<a title= "Activate" href= "javascript:void(0)" onClick="activate_customer(' . $user->id . ',1)" class=""><i class="fa fa-check-square-o" style="font-size:20px;color:#1B8579;"></i></a>' : '<a title= "Deactivate" href= "javascript:void(0)" onClick="activate_customer(' . $user->id . ',2)" class=""><i class="fa fa-times-circle" style="font-size:20px;color:#ec3535;"></i></a>';
            $html.= '&nbsp;&nbsp;&nbsp;&nbsp;';
            $html.= '<a title= "View Store" href= "' . $url . '/store/' . $user->id . '" style="font-size:14px;color:#ec1d23;">View Stores</a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';

            return $html;
        })
        //                ->addColumn('edit', function ($user) {
        //                return '<a href="#edit-'.$user->id.'" onclick="buttonclick('.$user->id.')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>';
        //                })
        // ->removeColumn('is_subscribed')
->make(true);
}

public function enquiryList() 
{
    $results = Enquiry::getEnquiryList()->get();

    return \Datatables::of($results)
    ->editColumn('no', function($enquiry) {
        return sprintf('%05d',$enquiry->id);
    })
    ->editColumn('name', function($enquiry) {
        return $enquiry->first_name.' '.$enquiry->last_name;
    })
    ->editColumn('date', function($enquiry) {
        return $enquiry->created_at->modify('+10 hour')->format('d-M-Y g:ia');
    })
    ->editColumn('id', function($enquiry) {
        return '<a data-id="'.$enquiry->id.'" title= "View Enquiry" href= "#"><i class="fa fa-pencil-square-o" style="font-size:20px;color:#8dc53e;"></i></a>';
    })
    ->make(true);
}

public function enquiryDetails($id)
{
    $enquiry=Enquiry::find($id);
    return View::make('admin-cms.enquiry-details',['enquiry' => $enquiry]);    
}

public function storeList($id) {
    $store_audit = new CompanyBusinessProfileAudit();
    $results = $store_audit->getStoreReport($id);

    return \Datatables::of($results)
    ->editColumn('is_approved', function($user) {
        $isactive = $user->is_approved;
        $statusArr = array("0" => 'Pending', "1" => "Active", "2" => "Deactive", "3" => "Rejected", "4" => "Active");
        return $statusArr[$isactive];
    })
    ->editColumn('phone', function($user) {
        $phone = '+61 '.$user->phone;
        return $phone;
    })
    ->editColumn('is_payment', function($user) {
            // it's not user, it's companybusinessprofileaudit...
        $payment = Payments::where('user_id',$user->id)
        ->where('transaction_type',1)
        ->where('user_type',2)
        ->orderBy('id','desc')
        ->first();
        if ($payment) {
            return $payment->invoiceNo();
        }
        return '-';
    })
    ->editColumn('id', function($user) {
        $isactive = $user->is_approved;
        $html = '';
        $url = url() . '/super-admin';
        if (!empty($user->is_payment) || $isactive == 2) {
            $html.= '<a title= "View Store Details" href= "' . $url . '/store-detail/' . $user->id . '"><i class="fa fa-pencil-square-o" style="font-size:20px;color:#8dc53e;"></i></a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';
            if (in_array($isactive, array(1, 2, 4)) && isset($user->active)) {
                $html.=($isactive == 2) ? '<a title= "Activate" href= "javascript:void(0)" onClick="activate_store(' . $user->id . ',1)" class=""><i class="fa fa-check-square-o" style="font-size:20px;color:#1B8579;"></i></a>' : '<a title= "Deactivate" href= "javascript:void(0)" onClick="activate_store(' . $user->id . ',2)" class=""><i class="fa fa-times-circle" style="font-size:20px;color:#ec3535;"></i></a>';
                $html.= '&nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $html.= '<a title= "View Vouchers" href= "' . $url . '/store-voucher/' . $user->id . '" style="font-size:14px;color:#ec1d23;">View Vouchers</a>';
        }
        return $html;
    })
->make(true);
}

public function updateBusinessDetail($companyid = 0) {
    $validator = Validator::make(
        Input::all(), array(
            'short_desc' => 'required|max:300',
            'long_desc' => 'required|max:2000',
            'logo' => 'sometimes|required|mssql_min_message_severity(severity):jpeg,bmp,png,gif',
            'store_pin' => 'required|max:4|unique:company_business_profile,id,?????',
            'category_id' => 'required',
            'company_id' => 'required',
            'email' => 'required|email',
            'state' => 'required',
            'address' => 'required',
            'suburb' => 'required',
            'post_code' => 'required|numeric',
            'phone_number' => 'required',
            'state' => 'required|numeric',
            'address' => 'required',
            'suburb' => 'required|numeric',
            'post_code' => 'required|numeric',
            'maximum_amount' => 'required|numeric',
            'minimum_amount' => 'required|numeric',
                //'incremental_amount' => 'sometimes|required|numeric',
            'store_location' => 'required',
            )
        );
$messages = array(
    'first_name.required' => 'First name required.',
    'long_desc.max' => 'Long description does not more than 2000 characters.'
    );
if ($validator->fails()) {

    return Redirect::back()->withErrors($validator);
}
if (!empty($companyid)) {
    $data = Input::all();
    $store_audit = new CompanyBusinessProfileAudit();
    $companybusinessupdate = $store_audit->updateStoreData($companyid, $data);

    $companybusiness = $companybusinessupdate;
    $companybusiness->id = $companyid;
    $vendor_id = $data['company_id'];
}
$tempArr = array();
CompanyBusinessOccationsAudit::where('company_business_profile_id', '=', $companybusiness->id)->delete();
for ($i = 0; $i < sizeof(Input::get('occation_name')); $i++) {

    $tempArr[] = array('company_business_profile_id' => $companyid,
        'occasion_id' => Input::get('occation_name')[$i]);
}
$CompanyBusinessOccations = CompanyBusinessOccationsAudit::insert($tempArr);
$tempArr = array();
CompanyBusinessReceipientsAudit::where('company_business_profile_id', '=', $companybusiness->id)->delete();
for ($i = 0; $i < sizeof(Input::get('recpient_name')); $i++) {
    $tempArr[] = array('company_business_profile_id' => $companybusiness->id,
        'recipient_id' => Input::get('recpient_name')[$i]);
}
$CompanyBusinessOccations = CompanyBusinessReceipientsAudit::insert($tempArr);
$tempArr = array();
CompanyTimingsAudit::where('company_business_profile_id', '=', $companybusiness->id)->delete();
for ($i = 0; $i <= 6; $i++) {
    $tempArr[] = array('company_business_profile_id' => $companybusiness->id,
        'day' => $i + 1,
        'timings' => Input::get('days')[$i]);
}
$CompanyTimings = CompanyTimingsAudit::insert($tempArr);

$store_pin = Input::get('store_pin');
$imagepath = '/images/company/storeslogo/' . $store_pin;
        //$destinationpath = public_path() . $imagepath;
$destinationpath = getS3LocalPath().$imagepath;

if (Input::hasFile('logo')) {
    $logopath = $destinationpath . '/logo';
    if (!empty($companybusiness->logo)) {
        $filename = public_path() . $companybusiness->logo;
                //                    if (File::exists($filename)) {
                //                        File::delete($filename);
                //                    }
    } else {
        File::makeDirectory($destinationpath, 0777);
        File::makeDirectory                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ($logopath, 0777);
    }
    $logo_img = Input::file('logo');
    $img_extn = $logo_img->getClientOriginalExtension();
    $imagename = time() . $logo_img->getFilename() . '.' . $img_extn;
            $logo_img->move($logopath, $imagename); // uploading file to given path
            RotateImages::upload_rotate_image($logopath . '/' . $imagename, $img_extn); // for iOs
            $logo_path = $imagepath . '/logo/' . $imagename;
            $s3_distination = env('s3_env') . $imagepath . '/logo/' . $imagename;
            if (env('s3_on')) {
                RotateImages::uploadImageS3($logopath . '/' . $imagename, $s3_distination);
            }
            CompanyBusinessProfileAudit::find($companybusiness->id)->update(['logo' => $logo_path]);
        }

        $images_arr = array('first_image' => 1, 'second_image' => 2, 'third_image' => 3, 'fourth_image' => 4, 'fifth_image' => 5, 'sixth_image' => 6);
        $i = 1;
        //Image upload and save image path in db  
        foreach ($images_arr as $imgval => $sno) {

            if (Input::hasFile($imgval)) {

                if (!empty($companyid)) {
                    $first_img = CompanyBusinessImagesAudit::where('company_business_profile_id', '=', $companyid)->where('voucher_image', '=', $sno)->first();
                    /* if ($first_img) {
                    $filename = public_path() . $first_img->image;
                    if (File::exists($filename)) {
                    File::delete($filename);
                    }
                } */
            }
            $file_img = Input::file($imgval);
            $img_extn = $file_img->getClientOriginalExtension();
            $imagename = time() . $file_img->getFilename() . '.' . $img_extn;
                $file_img->move($destinationpath, $imagename); // uploading file to given path
                RotateImages::upload_rotate_image($destinationpath . '/' . $imagename, $img_extn); // for iOs
                $s3_distination = env('s3_env') . $imagepath . '/' . $imagename;
                if (env('s3_on')) {
                    RotateImages::uploadImageS3($destinationpath . '/' . $imagename, $s3_distination);
                }
                if (!empty($first_img->id)) {
                    CompanyBusinessImagesAudit::find($first_img->id)->update(['image' => $imagepath . '/' . $imagename]);
                } else {
                    $tmpArr = array('company_business_profile_id' => $companybusiness->id,
                        'image' => $imagepath . '/' . $imagename,
                        'voucher_image' => $i);

                    CompanyBusinessImagesAudit::create($tmpArr);
                }
            }

            $i++;
        }

        if (in_array($companybusiness->is_approved, array("1", "4"))) {
            $updateinmaster = new CompanyBusinessProfileAudit();
            $updateinmaster->updateApprovedDetails($companybusiness->id);
        }
        Session::put('success-msg', Lang::get('cms.storedetails'));
        return Redirect('super-admin/store/' . $vendor_id);
    }

    public function saveCustomer() {
        $data = Input::all();
        //dd($data);
        $approve_company = new Company();
        $approve_company->updateCompanyData($data['id'], $data);
        Session::put('success-msg', Lang::get('cms.companydetails'));
        return Redirect('super-admin/company');
    }
    public function saveUser() {   //code added by sandeep date-14-10-2016 for save update users and js written in public/cms/script/validation.js

        $data = Input::all();
       // dd($data);
        $approve_company = new Users();
        $approve_company->updateUserData($data['id'], $data);
        //echo "save";die;
        Session::put('success-msg', Lang::get('cms.userdetails'));
        return Redirect('super-admin/users');
    }

    public function saveVendor(){//code added by sandeep date-14-10-2016 for save update users and js written in public/cms/script/validation.js
            $data = Input::all();
            $approve_company = new Company();
            $approve_company->updateVendorData($data['id'], $data);
            Session::put('success-msg', Lang::get('cms.venderdetails'));
            return Redirect('super-admin/vendors');
    }

    public function deleteCompany(){//code added by sandeep date-24-01-2017 for delete users and js written in public/cms/script/validation.js
        $id=$_POST['id'];
        $data=DB::table('company')->where('id', '=', $id)->delete();
        $users = DB::table('company_business_profile')->select('id')->where('company_id','=',$id)->first();
        if(!empty($users)){
            DB::table('company_business_profile')->where('company_id','=',$id)->delete();
            DB::table('company_business_profile_audit')->where('company_id','=',$id)->delete();
            DB::table('company_business_images')->where('company_business_profile_id','=',$users->id)->delete();
            
            DB::table('company_business_images_audit')->where('company_business_profile_id','=',$users->id)->delete();
            DB::table('company_business_occasion')->where('company_business_profile_id','=',$users->id)->delete();  
            DB::table('company_business_occasion_audit')->where('company_business_profile_id','=',$users->id)->delete(); 

            DB::table('company_business_recipient')->where('company_business_profile_id','=',$users->id)->delete();
            DB::table('company_business_recipient_audit')->where('company_business_profile_id','=',$users->id)->delete();

            DB::table('company_timings')->where('company_business_profile_id','=',$users->id)->delete();
            DB::table('company_timings_audit')->where('company_business_profile_id','=',$users->id)->delete();
        }   
      Session::put('success-msg', Lang::get('cms.companydelete'));
      return json_decode($data);

    }
    public function deleteUser(){//code added by sandeep date-14-10-2016 for delete users and js written in public/cms/script/validation.js
        $id=$_POST['id'];
       // dd($id);
        $data=  DB::table('users')->where('id', '=', $id)->delete();
        Session::put('success-msg', Lang::get('cms.userdelete'));
        return json_decode($data);

    }

    public function deleteVendor(){//code added by sandeep date-14-10-2016 for delete users and js written in public/cms/script/validation.js
        $id=$_POST['id'];
        $data=DB::table('company')->where('id', '=', $id)->delete();
        Session::put('success-msg', Lang::get('cms.venderdetails'));
        return json_decode($data);

    }

    public function rejectCustomer() {
        $data = Input::all();
        $company = new Company();
        $company->activateCompany($data['id'], '3');
        $row = $company->getCompanyById($data['id']);
        $email_id = $row->email;
        $companyLogs = new CompanyProfileLogs();
        $store_audit = new CompanyBusinessProfileAudit();
        $store_location = $store_audit->getStoreById($data['id']);
        $logData = array('company_id' => $data['id'], 'action_type' => 'Reject', 'new_data' => $data['reason']);
        $companyLogs->createLog($logData);
        $data['reason'] = nl2br($data['reason']);
        $data['first_name'] = $row->first_name;
        $data['store_location'] = $store_location->store_location;

        Mail::send('admin-cms/reject', $data, function($message) use($email_id) {
            $message->to($email_id)->subject('cherrygift admin rejected your profile');
        });
        Session::put('success-msg', Lang::get('cms.companyreject'));
        return Redirect('super-admin/customer');
    }

    public function rejectStore() {
        $data = Input::all();
        $company_store = new CompanyBusinessProfileAudit();
        $company_store->activateStore($data['id'], '3');
        $companyLogs = new CompanyProfileLogs();
        $logData = array('company_id' => $data['company_id'], 'company_business_profile_id' => $data['id'], 'action_type' => 'Reject', 'new_data' => $data['reason']);
        $companyLogs->createLog($logData);
        $store_audit = new CompanyBusinessProfileAudit();
        $store_location = $store_audit->getStoreById($data['id']);
        $data['reason'] = nl2br($data['reason']);
        $company = new Company();
        $row = $company->getCompanyById($data['company_id']);
        $email_id = $row->email;
        $data['first_name'] = $row->first_name;

        $data['store_location'] = $store_location->store_location;
        // Mail::send('admin-cms/reject', $data, function($message) use($email_id) {
        //     $message->to($email_id)->subject('cherrygift admin rejected your store profile');
        // });
        Session::flash('success-msg', Lang::get('cms.storereject'));
        return Redirect('super-admin/store/' . $data['company_id']);
    }

    public function getLogout() {
        Auth::logout();
        Session()->flush();
        return redirect(url('super-admin'));
    }

    public function storeVoucher($id) {
        $store = new CompanyBusinessProfileAudit();
        $results = $store->getStoreById($id);
        $store_location = $results->store_location;
        return View::make('admin-cms/voucher', compact('id', 'store_location'));
    }

    public function storeVoucherList($id) {
        //dd($id);
        $voucher = new OrderVoucher();
        $results = $voucher->getVoucherReport($id);

        return \Datatables::of($results)
        ->editColumn('redeemed', function($user) {
            //$date=date('Y-m-d');
           // echo date('Y-m-d', $date);
           // $date = "2016-01-01";
            $adddate=strtotime('+1 years');
            $date = strtotime($user->created_at);
            //dd($date);
            $futureDate=date('Y-m-d',strtotime('+1 year',$date));
            //dd($futureDate);
            //$isactive = $user->redeemed;
            if(strtotime($date)<strtotime($futureDate)){

            if($user->redeemed==1)
            return '<span style="color:green">Redeemed</span>';
            else if($user->voucher_status==1)
            return  '<span style="color:green">Active</span>';   

            else if($user->voucher_status==0)
            return  '<span style="color:red">Cancelled</span>';   

           

            }else{
            
                 return  '<span style="color:red">Expired</span>';
            }
            
            //$statusArr = array("0" => 'No', "1" => "Redeemed");
            //return $statusArr[$isactive];
        })
        ->editColumn('redeemed_at', function($user) {
            $isactive = $user->redeemed_at;
            return (!empty($isactive)) ? $isactive : '-';
        })
        ->editColumn('voucher_actioned_date', function($user) {
            //dd($user->voucher_actioned_date);
            $date = strtotime($user->created_at);
           $futureDate=date('d-m-Y',strtotime('+1 year',$date));
           $isactioned  = date('d-m-Y',strtotime($user->voucher_actioned_date));
            if($user->voucher_actioned_date==null){
            return '--';
            }else{
            
            if(strtotime($date)<strtotime($futureDate))
            {
                return $isactioned;

            }else{
                return $futureDate;
            }
            //dd($isactioned);
            // $statusArr = array("0" => 'Yes', "1" => "No");
            // return $statusArr[$isactive];
            // if($user->redeemed==1)
            // {

            //  return  $isactioned;  
            //   }   
            //  else if($user->voucher_status==0){
            //     return  $isactioned;
                
            //  }
            
         }
        })
         ->editColumn('sms_date', function($user) {
            $isactive = date('d-m-Y',strtotime($user->sms_date));
            return (!empty($isactive)) ? $isactive : '-';
        })
        ->editColumn('id', function($user) {
            $isactive = $user->voucher_status;
            $html = '';

            $adddate=strtotime('+1 years');
            $date = strtotime($user->created_at);
            $futureDate=date('Y-m-d',strtotime('+1 year',$date));

            if(strtotime($date)<strtotime($futureDate)){

            if ($isactive == 1 && $user->redeemed == 0) {
                $html.= '<a title= "Cancel Voucher" onClick="cancel_voucher(' . $user->id . ',0)" href="javascript:void(0)" style="font-size:14px;color:#ec1d23;">'
                . 'Cancel Voucher</a>'.'|';
                $html.= '<a title= "Redeem Voucher" onClick="redeemed_voucher(' . $user->id .',1)" href="javascript:void(0)" style="font-size:14px;color:green;">'
            . 'Redeem</a>';
            }
        }else{
                $html.= '<a title= "Cancel Voucher" onClick="return false" href="javascript:void(0)" style="font-size:14px;color:#ec1d23;">'
                . 'Cancel Voucher</a>'.'|';
                $html.= '<a title= "Redeem Voucher" onClick="return false" href="javascript:void(0)" style="font-size:14px;color:green;">'
            . 'Redeem</a>';
        }

            return $html;
        })
        ->make(true);
    }

       public function canceledVouchersbyid($user_info){
           
             $order = new OrderVoucher();
             $purshase_history = $order->getVouchersCanceledByid($user_info);
            //dd($purshase_history );
             $orderObj = new Order();
             $error = $orderObj->canceledSms($purshase_history);
             //$error='something went wrong';
             return $error;
    }

    public function voucherAction() {



        $id = Input::get('id');
        $value = Input::get('value');
        $data['voucher_status'] = $value;
        $data['voucher_actioned_date'] = date('Y-m-d h:i:s');
        $voucher = new OrderVoucher();
        $results = $voucher->updateOrderDetails($id, $data);
        $row = $voucher->getOrderById($id);
        $user_order_id = $row->user_order_id;
        $data['voucher_unique_id'] = $row->voucher_unique_id;
        $order = new Order();
        $orderrow = $order->getUserOrderById($user_order_id);
        $user = new Users();
        $userrow = $user->getUserById($orderrow->user_id);
        $email_id = $userrow->email;
        $data['first_name'] = $userrow->first_name;
        // Mail::send('admin-cms/vouchercancel', $data, function($message) use($email_id) {
        //     $message->to("mohd.nadeem@webnyxa.com")->subject('Cancelled Cherrygift Voucher');
        // });//$email_id
        // $errors = $this->canceledVouchersbyid($id);
        //             if(!empty($errors)) {
        //                 $data['status'] = "error";
        //                 Session::put('message','something went wrong in sending sms');
        //                return json_encode($data);   
        //             }
        $msg = Lang::get('cms.vouchercancel');

        $data['status'] = "success";
        Session::put('success-msg', $msg);
        return json_encode($data);




    }

        public function redeemedAction() {
       
        $id = Input::get('id');
         $value = Input::get('value');
        // dd($value);
        $voucher = new OrderVoucher();
        //dd($voucher->company_business_profile_id);
       
        $companybusinessid = DB::table('order_voucher')->select('company_business_profile_id')->where('id','=',$id)->first();
        $cid=$companybusinessid->company_business_profile_id;
        // $vid=$companybusinessid->voucher_unique_id;
        //dd($vid);
        $marchantpin=DB::table('company_business_profile')->select('merchant_pin')->where('id','=',$cid)->first();
        $mid=$marchantpin->merchant_pin;

        $data['merchant_pin'] = $mid;
        $data['voucher_unique_id'] = $id;
        $data['status'] = "success";
        //Session::put('success-msg', $msg);
        return json_encode($data);
    }



    public function homepageSliderImages() {

        $hsi = new HomepageSliderImages();
        $images = $hsi->getImages();
        //print_r($images);
        return View::make('admin-cms/slider-images', compact('images'));
    }

    public function uploadSliderImages() {

        $file = array('image' => Input::file('slider-image'));
        $rules = array('image' => 'mimes:jpeg,jpg,png|required',);

        $validator = Validator::make($file, $rules);

        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return redirect('super-admin/slider-images')->withErrors($validator);
        } else {

            if (Input::file('slider-image')->isValid()) {
                //$destinationPath = public_path() . '/hompage-slider-images'; // upload path
                $destinationPath = getS3LocalPath().'/hompage-slider-images'; // upload path
                $extension = Input::file('slider-image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file('slider-image')->move($destinationPath, $fileName); // uploading file to given path
                $s3_distination = env('s3_env') . '/hompage-slider-images' . '/' . $fileName;
                $local_drive = $destinationPath . '/' . $fileName;  

                if (env('s3_on')) {
                    RotateImages::uploadImageS3($local_drive, $s3_distination);
                }
                // sending back with message
                $img_url = "hompage-slider-images/" . $fileName;
                $hsi = new HomepageSliderImages();
                $hsi->saveImages($img_url);
                Session::flash('success', 'Upload Successful');
                return redirect('super-admin/slider-images')->with("message", "Upload Successful");
            } else {
                // sending back with error message.
                Session::flash('error', 'uploaded file is not valid');
                return redirect('super-admin/slider-images')->withErrors(['error' => 'uploaded file is not valid']);
            }
        }
    }

    public function deleteSliderImages($id) {
        try {
            $info = HomepageSliderImages::find($id);

            if (file_exists(public_path() . "/" . $info->image))
                unlink(public_path() . "/" . $info->image);

            if (!empty($info))
                $info->delete();
            return redirect('super-admin/slider-images')->with("message", "Deleted successfully");
        } catch (Exception $e) {
            throw new CustomException($e->getmessage());
        }
    }

    public function users() {
        return View::make('admin-cms/users');
    }

    public function usersdata() { //code added by sandeep(modify)

        $users = new Users();
        $results = $users->getUsers();

        return \Datatables::of($results)
        ->editColumn('status', function($user) {
        $status = $user->status;
        $html = '';
         //$url = url() . '/super-admin';
          
            //$html.= '<input type="checkbox" id="'. $user->id . '" name="my-checkbox" checked>';
             if($status=='Active'){
            $html.= '<input type="checkbox" data-on-text="Active" data-off-text="DeActive" id="'. $user->id . '" name="my-checkbox" checked>';
          }else{
            $html.= '<input type="checkbox" data-on-text="Active" data-off-text="DeActive" id="'. $user->id . '" name="my-checkbox" >';
          }

        return $html;
         })
        ->editColumn('id', function($user) {
            $isactive = $user->is_subscribed;
            $html = '';
            $url = url() . '/super-admin';
          
            $html.= '<a title= "View User Details" href= "javascript:void(0)" onClick="view_user(' . $user->id . ')" class=""><i class="fa fa-pencil-square-o" style="font-size:20px;color:#8dc53e;"></i></a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';

            $html.= '<a title= "Delete User" href= "javascript:void(0)" id="'. $user->id .'" class="" onClick="delete_user(' . $user->id . ')"><i class="fa fa-trash-o" style="font-size:20px;color:#8dc53e;"></i></a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';

            return $html;
        })
       

        ->make(true);
    }

    public function viewUser() { //code added by sandeep
        $id = Input::get('id');
        $users = new Users();
        $results = $users->getUserId($id);
       //dd($results);
        return $results;
    }

        public function viewVendor() { //code added by sandeep
        $id = Input::get('id');
        $company = new Company();
        $results = $company->getCompanyById($id);
                if ($results->account_number) {
            $results->account_number = Crypt::decrypt($results->account_number);
        }
        if ($results->suburs_id) {
            $suburb = new Suburbs();
            $subres = $suburb->getSuburbById($results->suburs_id);

            $results->suburbsname = $subres->locality;
        }
        else {
            $results->suburbsname='';
        }
       //dd($results);
        return $results;
    }   

    public function vendors() {
        return View::make('admin-cms/vendors');
    }

    public function vendorsdata() { //code added by sandeep(modify)
      
        $company = new Company();
        $results = $company->getCompanyList();

        return \Datatables::of($results)
        ->editColumn('is_subscribed', function($user) {
        $is_subscribed = $user->is_subscribed;
        $html = '';
         //$url = url() . '/super-admin';

        if($is_subscribed==1){
            
            $html.= '<input type="checkbox" data-on-text="Active" data-off-text="DeActive" id="'. $user->id . '" name="my-checkbox" checked>';
          }else if($is_subscribed==2){
            $html.= '<input type="checkbox" data-on-text="Active" data-off-text="DeActive" id="'. $user->id . '" name="my-checkbox">';
          }else{
                $html.= $is_subscribed;
          }


        
          // if($is_subscribed=='1'){
          //   $html.= '<input type="checkbox" id="'. $user->id . '" name="my-checkbox" checked>';
          // }else{
          //   $html.= '<input type="checkbox" id="'. $user->id . '" name="my-checkbox" >';
          // }
            

        return $html;
         })

         ->editColumn('id', function($user) {
            //$isactive = $user->is_subscribed;
            $html = '';
            $url = url() . '/super-admin';
          
            $html.= '<a title= "View Vendor Details" href= "javascript:void(0)" onClick="view_vendor(' . $user->id . ')" class=""><i class="fa fa-pencil-square-o" style="font-size:20px;color:#8dc53e;"></i></a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';
            $html.= '<a title= "Delete Vendor" href= "javascript:void(0)" onClick="delete_vendor(' . $user->id . ')" class=""><i class="fa fa-trash-o" style="font-size:20px;color:#8dc53e;"></i></a>'
            . '&nbsp;&nbsp;&nbsp;&nbsp;';
         

            return $html;
        })

        ->make(true);
    }

    public function resetPassword(Request $request) { //code added by sandeep

        $validation = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6|same:password'
            ]);
        if ($validation->fails()) {
            $error = $validation->errors()->all();
            echo json_encode(array('status' => 'error', 'error' => $error));
            //echo "error message";
        }else{
            $data = $request->all();
            $id = $data['id']; 
            $password = $data['password'];
           // dd($id);   
            $new_password = bcrypt($data['password']);
           // dd($new_password);

            DB::table('company')->where('id', $id)->update(array('password' => $new_password));

            $company = new Company();
            $results = $company->getCompanyById($id);
            $first_name = $results->first_name;
           // dd($first_name);
            //$contact_email = $results->contact_email;
            //$emailid='mohd.nadeem@webnyxa.com';
            $emailid='deepak.panwar@webnyxa.com';
           //$emailid='fiona@cherrygift.com';
            $msendmail = Mail::send('admin-cms/reset_password', array('first_name'=>$first_name,'password'=>$password), function($message) use($emailid) {
            $message->to($emailid)->subject('Reset Password');
            });
            //return Redirect('/site/userprofile')->with(array('status'=>'success','msg'=>'password change successfully '));
            echo json_encode(array('status' => 'success', 'message' => 'Your password has been reset successfully.'));
            //echo "save";
        }

    }

    public function downloadUsersCSV() {
        ini_set('max_execution_time', 0);
        $output = implode(",", array('Serial Number', 'First Name', 'Last Name', 'Email')) . "\n";
        $i = 1;
        $users = new Users();
        $results = $users->getUsers()->get();

        foreach ($results as $row) {
            if ($row->email != "") {
                $output .= implode(",", array($i, $row->first_name, $row->last_name, $row->email)) . "\n";
                $i++;
            }
        }

        // headers used to make the file "downloadable", we set them manually
        // since we can't use Laravel's Response::download() function
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="userlist.csv"',
            );

        // our response, this will be equivalent to your download() but
        // without using a local file
        return response()->make(rtrim($output, "\n"), 200, $headers);
    }

    public function downloadVendorCSV() {
        ini_set('max_execution_time', 0);
        $output = implode(",", array('Serial Number', 'First Name', 'Last Name', 'Email')) . "\n";
        $i = 1;
        $company = new Company();
        $results = $company->getCompanyList()->get();

        foreach ($results as $row) {
            if ($row->email != "") {
                $output .= implode(",", array($i, $row->first_name, $row->last_name, $row->email)) . "\n";
                $i++;
            }
        }

        // headers used to make the file "downloadable", we set them manually
        // since we can't use Laravel's Response::download() function
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="vendorlist.csv"',
            );

        // our response, this will be equivalent to your download() but
        // without using a local file
        return response()->make(rtrim($output, "\n"), 200, $headers);
    }

    public function adminVendorPayment($month = "", $year = "") {
        if (empty($month)) {
            $month = date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }
        $cbp = new CompanyBusinessProfile();
        $results = $cbp->getVenderPaymentList($month, $year)->get()->toArray();
        // dd($results);
        $sum=0;
        foreach ($results as $key => $value) {
            
            $sum =$sum+($value['amount']-$value['amount']*.09);
        }
        return View::make('admin-cms/admin-vendor-paymant', compact('month', 'year','sum'));
    }

    public function adminVendorPaymentList($month, $year) {
        if (empty($month)) {
            $month = date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }

        $adminvendorpayment = new AdminVendorPayment();
        //$results = $adminvendorpayment->getVendorList($month);
        $cbp = new CompanyBusinessProfile();
        $results = $cbp->getVenderPaymentList($month, $year);
        // echo $results;die;
        return \Datatables::of($results)
        ->editColumn('updated_at', function($user) {
        $updated_at = date('d-m-Y', strtotime($user->updated_at));
        return $updated_at;
         })
        ->editColumn('voucher_unique_id', function($user) {
       
        return '<span style="display:none">'.$user->voucher_unique_id.'</span>';
         })
        ->editColumn('payment_status', function($user) {

            if (!empty($user->payment_status)) {
                return "<span style='color:green'>Paid</span>";
            } else {
                return "<span class='noPaidChk' style='cursor:pointer; color:red'>Not Paid</span>";
            }
        })
        ->editColumn('amount', function($user) {
            $amount = $user->amount;
            return "$".($user->amount - ($user->amount*.09));

        })->editColumn("account_number",function($user){
            if(!empty($user->account_number))
                return Crypt::decrypt($user->account_number);
        })
        ->make(true);
    }

    
    public function pdf_form(){ //code added by sandeep

        return view('admin-cms.pdf_form');
    }
    public function voucherListPdf($id, $month = "", $year = "") { //code added by sandeep
        
        if (empty($month))
            $month = date('m');
        
        if (empty($year))
            $year = date('Y');
        $cbp = new CompanyBusinessProfile();
        $results = $cbp->getVenderPaymentListPdf($id,$month, $year)->get();

       // return View::make('admin-cms.pdf_form', compact("results","company_name","company_address"));
        $html = (string) View::make('admin-cms.pdf_form', compact("results"));
        $mpdf = new mPDF("en", "A4", "10"," ",0,0,0,0,0,0);
        $mpdf->WriteHTML($html);
        $name = "remmittedadvise.pdf";
        $pathToFile = public_path() . "/" . $name;
        $mpdf->Output($pathToFile, 'I');


    }            

    public function postmail($id, $month = "", $year = ""){

   
    if(empty($month))
        $month = date('m');
    
    if (empty($year))
        $year = date('Y');

    $cbp = new CompanyBusinessProfile();
    $results = $cbp->getVenderPaymentListPdf($id,$month, $year)->get();
    
    $company_name = $results[0]->company_name;
    $first_name = $results[0]->first_name;
    //$emailid = $results[0]->contact_email;
    $emailid = $results[0]->accounts_email;
    $html = (string) View::make('admin-cms.pdf_form', compact("results"));
    $mpdf = new mPDF("en", "A4", "10"," ",0,0,0,0,0,0);
    $mpdf->WriteHTML($html);
    $name = date('s')."_remmittedadvise.pdf";
    $pathToFile = public_path() ."/remmittedadvise/" . $name;
    $mpdf->Output($pathToFile, 'F');


    if(file_exists($pathToFile)) 
    {

        $updated_at = date('F Y', strtotime('1-'.$month.'-'.$year));
        //$emailid='deepak.panwar@webnyxa.com';
        //$emailid='mohd.nadeem@webnyxa.com';
        //$emailid=$contact_email;
        $msendmail = Mail::send('admin-cms/supplier_mail', array('first_name'=>$first_name,'updated_at'=>$updated_at), function($message) use($emailid) {
        $message->to($emailid)->subject('Remittance Advice');

        $name = date('s')."_remmittedadvise.pdf";
        $pathToFile = public_path() ."/remmittedadvise/" . $name;

         $message->attach($pathToFile, array(
            'mime' => 'application/pdf')
         );
     });
        if($msendmail)
        {

            $avp = new AdminVendorPayment();
            $avp->updateVendorMail($id, $month,$year);
            Session::flash('success', 'Mail Sent Successfully');
               return Redirect::back()->with("message", "Mail Sent Successfully");
        }else{
             Session::flash('error', 'Mail Not Sent');
                return Redirect::back()->with("message", "sending Mail failed");

        }

    }else{
             Session::flash('error', 'File not exitst');
                return Redirect::back()->with("message", "File not exitst");

        }

}

public function changeVendorPaymentStatus() {
    $data = Input::all();
    $avp = new AdminVendorPayment();
    $avp->updateVendorPayment($data['cbp_id'], $data['month'], $data["year"]);
    return "true";
}

function getRedeemedData($month, $year) {
    $redeemedlog = new OrderVoucher();

    $results = $redeemedlog->getRedeemedHistory($month, $year);
        //print_r($results);
    return \Datatables::of($results)
    ->editColumn('created_at', function($user) {
        $created_at = date('d-m-Y', strtotime($user->created_at));
        return $created_at;
    })
    ->editColumn('phone_number', function($user) {
        $phone_number = '+61 '.$user->phone_number;
        return $phone_number;
    })
    ->editColumn('redeemed', function($user) {

         $date = strtotime($user->created_at);
            $futureDate=date('Y-m-d',strtotime('+1 year',$date));
           // dd($futureDate);
            //$isactive = $user->redeemed;
            if(strtotime($date)<strtotime($futureDate)){
                if($user->redeemed==1)
                    return '<span style="color:red">Used</span>';
                else if($user->voucher_status==0){
                    return  '<span style="color:red">Inactive</span>';
                }
                else{
                    return  '<span style="color:green">Active</span>';
                }


            }else{
                

                 if($user->redeemed==1)
                    return '<span style="color:red">Used</span>';
                else if($user->voucher_status==0){
                    return  '<span style="color:red">Inactive</span>';
                }
                else{
                    return  '<span style="color:red">Inactive</span>';
                }



            }
    })
    // ->editColumn('voucher_status', function($user) {
    //     if($user->voucher_status==1)
    //         return 'Active';
    //     else
    //         return 'Cancel';

    // })

    ->editColumn('id', function($user) {
        $isactive = $user->voucher_status;
       // dd($isactive);
         $date = strtotime($user->created_at);
        $futureDate=date('d-m-Y',strtotime('+1 year',$date));
        $html = '';
         if(strtotime($date)<strtotime($futureDate)){
        if ($isactive == 1 && $user->redeemed == 0) {// js written in var/www/html/cherrygift/public/cms/script/validation.js
            $html.= '<a title= "Cancel Voucher" onClick="cancel_voucher(' . $user->id . ',0)" href="javascript:void(0)" style="font-size:14px;color:#ec1d23;">'
            . 'Cancel Voucher</a>'.'|';
            $html.= '<a title= "Redeem Voucher" onClick="redeemed_voucher(' . $user->id .',1)" href="javascript:void(0)" style="font-size:14px;color:green;">'
            . 'Redeem</a>';
        }
        else if($user->redeemed==1)
            {$html = 'Redeemed';}
         else 
            {$html = 'Cancelled';}
    }else{
        $html.='Expired '.$futureDate;
    }
        return $html;
    })
    ->filterColumn('name', function($query, $keyword) {
        $query->whereRaw("CONCAT(users.first_name,' ',users.last_name) like ?", ["%{$keyword}%"]);
    })
        /* ->editColumn('voucher_price', function($user) {
        $html = '';
        $url = url() . '/super-admin';
        if(!empty($user->voucher_price)){
        $html.= '<a title= "View Store Details" href= "' . $url . '/store-detail/' . $user->id . '"><i class="fa fa-pencil-square-o" style="font-size:20px;color:#8dc53e;"></i></a>'
        . '&nbsp;&nbsp;&nbsp;&nbsp;';

        $html.= '<a title= "View Vouchers" href= "' . $url . '/store-voucher/' . $user->id . '" style="font-size:14px;color:#ec1d23;">View Vouchers</a>';
        }
        return $html;
    }) */

->make(true);
}

public function getRedeemed($month = "", $year = "") {
    if (empty($month)) {
        $month = date('m');
    }
    if (empty($year)) {
        $year = date('Y');
    }
    return View::make('admin-cms/redeemed', compact('month', 'year'));
}

public function getRedeemedDataInCSV($month, $year) {
    $redeemedlog = new OrderVoucher();

    ini_set('max_execution_time', 0);
    $output = implode(",", array('Purchaser Name', 'Amount', 'Mobile Number', 'Voucher Number','Company Name','Store','Date')) . "\n";
    $i = 1;
    $results = $redeemedlog->getRedeemedHistory($month, $year);
    $result = $results->get();

    foreach ($result as $row) {
        $output .= implode(",", array($row->name, $row->voucher_price, '+61 ' .$row->phone_number,$row->voucher_unique_id,$row->company_name,$row->store_location, date('d-m-Y', strtotime($row->updated_at)))) . "\n";
        $i++;
    }

        // headers used to make the file "downloadable", we set them manually
        // since we can't use Laravel's Response::download() function
    $headers = array(
        'Content-Type' => 'text/csv',
        'Content-Disposition' => 'attachment; filename="redeemedlist.csv"',
        );


        // our response, this will be equivalent to your download() but
        // without using a local file
    return response()->make(rtrim($output, "\n"), 200, $headers);
}

public function voucherHistory() {
    $company = new Company();
    $getcompanylist = $company->getCompanyName();
}

public function companylist() {
    return View::make('admin-cms/companylist');
}

public function getRedeemedHistoryByCompany() {
     $company_audit = new Company();
     $results = $company_audit->getCompanyReport();
    //     $voucher = new OrderVoucher();
    //     $results = $voucher->getAllCompanyForVoucher();
     return \Datatables::of($results)
   ->editColumn('phone', function($user) {
        $phone = '+61 '.$user->phone;
        return $phone;
    })
    ->editColumn('is_subscribed', function($user) {
        $isactive = $user->is_subscribed;
        $statusArr = array("0" => 'Pending', "1" => "Active", "2" => "Deactive", "3" => "Rejected");
        return $statusArr[$isactive];
    })
    ->editColumn('id', function($user) {
        $isactive = $user->is_subscribed;
        $html = '';
        $url = url() . '/super-admin';
        $html.= '<a title= "View Vouchers" href= "' . $url . '/voucher_id/' . $user->id . '" style="font-size:14px;color:#ec1d23;">View Vouchers</a>'
        . '&nbsp;&nbsp;&nbsp;&nbsp;';

        return $html;
    })
        //                ->addColumn('edit', function ($user) {
        //                return '<a href="#edit-'.$user->id.'" onclick="buttonclick('.$user->id.')" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a>';
        //                })
        // ->removeColumn('is_subscribed')
    ->make(true);
}

public function voucher_id($id) {
    $company = new Company();
    $results = $company->getCompanyById($id);
    $company_name = $results->company_name;
    return View::make('admin-cms/voucherlist', compact('id', 'company_name'));
}

public function voucherList($id, $month = "", $year = "") {
    if (empty($month)) {
        $month = date('m');
    }
    if (empty($year)) {
        $year = date('Y');
    }
    $company = new Company();
    $results = $company->getCompanyById($id);
    $company_name = $results->company_name;
    return View::make('admin-cms/voucherlist', compact('id', 'company_name', 'month', 'year'));
}

public function voucherHistoryList($id, $month = 0, $year = 0) {
    if (empty($month)) {
        $month = date('m');
    }
    if (empty($year)) {
        $year = date('Y');
    }

    $store_audit = new orderVoucher();
    $results = $store_audit->getAllRedeemedBycompany($id, $month, $year);
    //dd($results);
    return \Datatables::of($results)
    ->editColumn('updated_at', function($user) {
        $updated_at = date('d-m-Y', strtotime($user->updated_at));
        return $updated_at;
    })
    ->make(true);
}

public function storeStatusList() {
    return View::make('admin-cms/store-status-list');
}

public function storeStatusListData() {

    $cbpa = new CompanyBusinessProfileAudit();
    $results = $cbpa->getStoreList();

    return \Datatables::of($results)
    ->editColumn('status', function($user) {
        if ($user->is_subscribed != 1)
            return "Pending for subscritpion";
        elseif ($user->is_approved == 0)
            return "Pending for approval";
        else
            return "Active";
    })
    ->make(true);
}

public function homepageRecipientImages() {

    $hsi = new HomepageRecipientImages();
    $images = $hsi->getImages();
        //print_r($images);
    return View::make('admin-cms/recipient-images', compact('images'));
}

public function uploadRecipientImages() {

    $file = array('image' => Input::file('slider-image'));
    $rules = array('image' => 'mimes:jpeg,jpg,png|required',);
    $post = Input::all();
        //print_r($post);die;
    $validator = Validator::make($file, $rules);

    if ($validator->fails()) {
            // send back to the page with the input data and errors
        return redirect('super-admin/recipient-images')->withErrors($validator);
    } else {

        if (Input::file('slider-image')->isValid()) {

                //$destinationPath = public_path() . '/hompage-recipient-images'; // upload path
                $destinationPath = getS3LocalPath().'/hompage-recipient-images'; // upload path
                $extension = Input::file('slider-image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file('slider-image')->move($destinationPath, $fileName); // uploading file to given path
                $s3_distination = env('s3_env') . '/hompage-recipient-images' . '/' . $fileName;
                $local_drive = $destinationPath . '/' . $fileName;
                if (env('s3_on')) {
                    RotateImages::uploadImageS3($local_drive, $s3_distination);
                }
                // sending back with message
                $img_url = "hompage-recipient-images/" . $fileName;
                $hsi = new HomepageRecipientImages();
                $hsi->saveImages($img_url,$post['img_type']);
                Session::flash('success', 'Upload Successful');
                return redirect('super-admin/recipient-images')->with("message", "Upload Successful");
            } else {
                // sending back with error message.
                Session::flash('error', 'uploaded file is not valid');
                return redirect('super-admin/recipient-images')->withErrors(['error' => 'uploaded file is not valid']);
            }
        }
    }


    public function saveImgUrl(){
        $post = Input::all();
        //print_r($post);die;
        $file = array('img-id' => Input::get('img_id'),"img-url"=>Input::get('img_url'));
        $rules = array('img-id' => 'required','img-url' => 'url');
        $validator = Validator::make($file, $rules);

        if ($validator->fails()) {
            // send back to the page with the input data and errors
            return redirect('super-admin/slider-images')->withErrors($validator);
        } else { 
            $hsi =  HomepageSliderImages::find($post["img_id"]);
            $hsi->img_url = $post["img_url"];
            $hsi->save();
            return redirect('super-admin/slider-images')->with("message", "Url successfully added.");
        }
        //return redirect('super-admin/recipient-images')
    }

    public function getTestInvoiceEmail()
    {
        $o = new Order();
        $data = array( 'order_id' => 7, 'voucher_list' => array( 
            '3' => array( '0' => array( 
                'price' => 100, 'mobile' => '0416081801','voucher_unique_id' => 12345678 ) ) ) );
        $res = $o->sendInvoiceMail($data);
    }

    public function paymentdetails(){
        return View::make('admin-cms/paymentdetails');
    }
    public function paymentDetailsData(){
       $paypalHistory = new PaypalHistory();
    $results = $paypalHistory->getpaypalList();

    return \Datatables::of($results)
   ->editColumn('payment_date', function($user) {
        $payment_date = date('d-m-Y', strtotime($user->payment_date));
        return $payment_date;
         })
   ->editColumn('paypal_object', function($user) {
       
        return '<div style="overflow: scroll;overflow-y: hidden;;width: 1000px;height: 150px;">'.$user->paypal_object.'</div>';
         })
    ->make(true); 

    }




}
