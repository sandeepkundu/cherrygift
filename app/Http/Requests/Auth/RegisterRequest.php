<?php namespace App\Http\Requests\Auth;
 
use Illuminate\Foundation\Http\FormRequest;
 ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('max_execution_time', 0);
class RegisterRequest extends FormRequest {
 
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { //print_r(input::all()); exit;
        
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'first_name'=> 'required',
            'mobile_number' => 'required|min:8|max:21',
            'image' => 'mimes:jpeg,bmp,png,gif',
            'term_condition'=>'required'
        ];
    }
    
    public function messages()
    {
        return [
            'email.unique' => 'This email is already registered with us'
        ];
    }

 
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
 
}