@extends('template.default')
@section('title','cherrygift - shopping vouchers on your smartphone!')
@section('description','cherrygift - the easiest way to digitially send the perfect gift for any occasion')

@section('homebanner')
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
// for now we'll switch to local due to s3 service terminated
$s3_url = 'https://www.cherrygift.com/imgs/s3/';
?>
@if(Session::has('message'))
<div class="error_message"><div class="alert alert-success">
        <h3 style='text-align: center'>{{ Session::get('message') }}</h3>
    </div>
</div>
@endif
<div id="userHomeBanner" class="carousel slide lg-container" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php $j=0;?>
        @foreach($slider_images as $s_img)
        <li data-target="#userHomeBanner" data-slide-to="{{$j}}" class="<?php if($j ==0) {?> active <?php }?>"><span></span></li>
        <?php $j++;?>
        @endforeach
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php 
        $i=0;
        $bannerAlt=[
            'Green gift giving with CherryGift vouchers',
            'Make it personal with a CherryGift voucher',
            'Send CherryGift vouchers Australia Wide',
            'Send instant happiness to friends and family with a CherryGift voucher',
        ];
        ?>
        @foreach($slider_images as $s_img)
        <?php
            if ($i < count($bannerAlt)) {
                $altText = $bannerAlt[$i];
            }
            else {
                $altText = 'CherryGift vouchers - straight to your smartphone';
            }
        ?>
        <div class="item <?php if($i ==0) {?> active <?php }?>">

            @if(!empty($s_img->img_url))
            <a href="{{$s_img->img_url}}" title="{{$s_img->img_url}}"><img src="{{$s3_url}}{{$s3_env}}/{{$s_img->image}}"></a>
            @else
            <img src="{{$s3_url}}{{$s3_env}}/{{$s_img->image}}" alt="<?= $altText ?>">
            @endif

        </div>
        <?php $i++;?>
        @endforeach
        <!--                    <div class="item active">
        <img src="{{asset('images/home-user-banner/banner-img1.png')}}">
        <div class="img-wrapper"></div>
        <div class="carousel-caption">
        <ul class="list-unstyled">
        <li class="name">TAMARIND <br>RESTAURANT</li>
        <li class="price-ctn">
        <p class="label">START AT</p>
        <p class="price-info"><sup class="sign">$</sup>50</p>
        </li>
        </ul>
        </div>
        </div>
        <div class="item">
        <img src="{{asset('images/home-user-banner/banner-img1.png')}}">
        </div>
        <div class="item">
        <img src="{{asset('images/home-user-banner/banner-img3.png')}}">
        </div>-->
    </div>
</div>
@stop
@section('content')
<!-- Country Search -->
<section class="clearfix container-fluid">
    <div class="row list">
        @if(!empty($cherrygiftvoucher))
        <a href="{{URL()}}/voucher-details/{{str_replace(' ','-',$cherrygiftvoucher)}}">
            <div class="list-item" id="still_stuck_with_idea">
                <div class="gift-section">
                    <img src="{{$s3_url}}{{$s3_env}}/{{$cherrygift_voucher_images}}" alt="Stuck for ideas? Buy a CherryGift voucher">
                    <!--                                <div class="category-text category-bg-green">
                    GIFTS</br>FOR </br>THEM
                    </div>-->
                    <div class="widget-caption">
                        <span class="pull-right widget-arrow"></span>
                    </div>
                </div>
            </div>
        </a> 
        @else
        <div class="list-item" id="still_stuck_with_idea">
            <div class="gift-section">
                <img src="{{$s3_url}}{{$s3_env}}/{{$cherrygift_voucher_images}}" alt="Stuck for ideas? Buy a CherryGift voucher">
                <!--                                <div class="category-text category-bg-green">
                GIFTS</br>FOR </br>THEM
                </div>-->
                <div class="widget-caption">
                </div>
            </div>
        </div>

        @endif
        <a href="{{URL().'/search/rec/Gifts for her/1'}}">
            <div class="list-item" id="gift_for_her">
                <div class="gift-section">
                    <img src="{{$s3_url}}{{$s3_env}}/{{$gift_for_her_images}}" alt="CherryGift vouchers for her">
                    <!--                                <div class="category-text category-bg-yellow">
                    GIFTS</br>FOR </br>HER
                    </div>-->
                    <div class="widget-caption">
                        <span class="pull-right widget-arrow"></span>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{URL().'/search/rec/Gifts for him/2'}}">
            <div class="list-item" id="gift_for_him">
                <div class="gift-section">
                    <img src="{{$s3_url}}{{$s3_env}}/{{$gift_for_him_images}}" alt="CherryGift vouchers for him">
                    <!--                                <div class="category-text category-bg-blue">
                    GIFTS</br>FOR </br>HIM
                    </div>-->
                    <div class="widget-caption">
                        <span class="pull-right widget-arrow"></span>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{URL().'/search/rec/Gifts for them/3'}}">
            <div class="list-item" id="gift_for_them">
                <div class="gift-section">
                    <img src="{{$s3_url}}{{$s3_env}}/{{$gift_for_them_images}}" alt="CherryGift vouchers for them">
                    <!--                                <div class="category-text category-bg-green">
                    GIFTS</br>FOR </br>THEM
                    </div>-->
                    <div class="widget-caption">
                        <span class="pull-right widget-arrow"></span>
                    </div>
                </div>
            </div>
        </a>


        <!--                        <div class="list-item" id="still_stuck_with_idea">
        <div class="gift-section">
        <img src="{{asset('images/gift-category/buyvouchar.png')}}" alt="">
        <div class="widget-carousel-bg full"></div>
        <div class="category-text cetegory-voucher">
        STUCK FOR IDEAS? </br>
        BUY A CHERRYGIFT</br>
        VOUCHER</br>
        </div>
        <div class="widget-caption">
        <span class="pull-right widget-arrow"></span>
        </div>
        </div>
        </div>-->

    </div>
</section>

<!-- Country Search -->
<section class="clearfix container-fluid">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <h4 class="section-heading"><span>SEARCH BY LOCATION</span></h4>
        </div>
    </div>
    <div class="row list">

        @foreach($state_arr as $state)
        <a href="{{url().'/search/state/'.$state['state_name']."/".$state['id']}}"><div class="list-item country-carousel">
        <div class="carousel slide" data-ride="carousel" id="{{$state['id']}}" data-interval="false">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @foreach($state['images'] as $key=>$img)
            <?php
            // get 480,380 and 280w versions
            $img_info = pathinfo($img);
            $img_base = $img_info['dirname'].'/'. $img_info['filename'];
            $ext = '.'.$img_info['extension'];
            ?>
            @if($key == 0)
            <div class="item active">
                @else
                <div class="item">
                    @endif    
                    <img src="<?= $img_base.'_380'.$ext ?>" alt="Browse CherryGift suppliers in <?= $state['state_name']?>" 
                    srcset="<?= $img.' 600w' ?>,<?= $img_base.'_480'.$ext.' 480w' ?>,<?= $img_base.'_380'.$ext.' 380w' ?>,<?= $img_base.'_280'.$ext.' 280w' ?>"
                    sizes="(min-width: 768px) 480px,(min-width: 992px) 380px,(min-width: 1600px) 330px, 100vw"
                    >
                </div>

                @endforeach
            </div>
            <div class="widget-carousel-bg"></div>
            <div class="widget-caption">
                <div class="display-table">
                    <div class="display-cell">
                        <h2 class="widget-title">{{$state['state_name']}}</h2>
                    </div>
                    <div class="display-cell">
                        <span class="pull-right widget-arrow"></span>
                    </div>
                </div>
            </div>
        </div></a>
    </div>

    @endforeach

    </div>

</section>

@if($new_register == 1)
<script>
    $(document).ready(function () {
        $("#tutorialModal").modal("show")
    })
</script>
@endif 
<div class="tool-tip-new">
    <div class="toll-cross-btn">X</div>
    <div class="tool-bottom-icon"></div>
    <p>Add cherrygift to Home Screen</p>   
</div>

@stop
