<?php /* THANKS POPUP PAGE HTML */ ?>

<!-- Modal -->
<div class="modal fade" id="thanksModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header clearfix">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
       
      </div>
      <div class="modal-body">
        <p>
        	Thank you for submission. Please allow 48 hours <br>
        	for your profile to be approved. Look out for <br>
        	your confirmation email.</p>
      	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block btn-lg r-blr-6" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>