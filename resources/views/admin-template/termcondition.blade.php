<?php /* TERMS & CONDITION PAGE HTML */ ?>

<!-- Modal -->
<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
        <h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
      </div>
      <div class="modal-body">
        <div class="termsCont">
                    <p>This is an agreement between You, the Supplier, (“Supplier” or “You”) and cherrygift. The terms and conditions set out in this Supplier Agreement are the basis upon which cherrygift is prepared to provide the Services. If You, the Supplier, register as a supplier/provider with cherrygift, then You are agreeing to accept and comply with the terms and conditions set out in this Agreement on and from the date of registration until your arrangements with cherrygift come to an end. </p>
                    <h4>background</h4>
                    <ul class="upperAlpha">
                    <li><span>cherrygift is a web-based business which allows customers to purchase gift vouchers from a number of different business/Suppliers/providers, with delivery of such voucher to be effected by text message to the nominated recipient’s mobile phone.</span> </li>
                    <li><span>cherrygift gives You the opportunity to take advantage of the Services it offers by registering as a Supplier and having gift vouchers for your business/product/services available for purchase by customers through the cherrygift Website. </span> </li>
                    </ul>
                    <h4>THE PARTIES AGREE:</h4>
                    <h4>DEFINITIONS</h4>
                    <p>In this Agreement unless inconsistent with the context or subject matter:</p>
                    <ul class="number">
                        <li><span>“Annual Fee” means the amount of $240.00 plus GST, or such other amount as notified to the Customer by cherrygift</span></li>
                        <li><span>“cherrygift” means Fionnghal Pty Ltd ABN 52 146 304 181 trading as cherrygift;</span></li>
                        <li><span>“cherrygift Website” means this website and any other websites associated with cherrygift’s business;</span></li>
                        <li><span>“Service Fee” means the amount payable by Supplier to cherrygift in consideration of cherrygift providing the Services, being an amount equal to 9% of the face value of the Voucher;</span></li>
                        <li><span>“Services” means the services provided by cherrygift as set out under clause headed “Services”</span></li>
                        <li><span>“Voucher” means the gift voucher for Your business/product/service purchased via the cherrygift Website;</span></li>
                        <li><span>“Voucher Funds” means the face value of the Voucher less the Service Fee;</span></li>
                    </ul>
                    <h4>registration</h4>
                    <ul class="number">
                        <li><span>If You wish to take advantage of the Services offered by cherrygift, You are required to apply to become a Supplier on the cherrygift Website. Following receipt of your application, cherrygift will consider your application and if cherrygift is satisfied to accept your application, cherrygift shall send you email notification of your successful application. Once the Annual Fee has been paid in accordance with this Agreement, You shall be registered as a Supplier of cherrygift and cherrygift shall provide the Services to you in accordance with these terms and conditions.</span> </li>
                        <li><span>cherrygift reserves the right, in its absolute discretion, to refuse, decline or deny any application to become a Supplier on whatsoever grounds cherrygift deems appropriate and cherrygift is under no obligation to provide any explanation of any such refusal, declination or denial.</span> </li>
                        <li><span>Following acceptance as a Supplier, You may be required to provide additional information to allow Voucher Funds to be paid to you following the redemption of the Voucher by the voucher holder.</span></li>
                        <li><span>It shall be the sole responsibility of the Supplier to keep the details of their registration up to date and notified to cherrygift and cherrygift takes no responsibility for loss occasioned as due to You failing to keep the details of your registration current.</span></li>
                    </ul>
                    <h4>services</h4>
                     <ul class="number">
                         <li><span>The Services to be provided by cherrygift are:</span>
                     <ul class="alphaRound">
                            <li>Operation of the cherrygift Website (and any software ancillary to);</li>
                            <li>Creating opportunities through the cherrygift Website to attract additional customers to your business;</li>
                            <li>Promotion of the cherrygift business/product;</li>
                            <li>Facilitating the online purchase of gift vouchers for cherrygift’s various suppliers; </li>
                            <li>Sending gift vouchers to nominated recipients via text message in accordance with voucher order requirements (as input by the customer);</li>
                            <li>Issue alert reminders to voucher holders in accordance with cherrygift policies;</li>
                            <li>Technical support to Suppliers in voucher redemption processes;</li>
                            <li>Marketing/advertising activities in respect of cherrygift and, where appropriate (in cherrygift’s absolute discretion), its Suppliers;</li>
                            <li>Recording the successful redemption of Vouchers through the redemption processes established by cherrygift;</li>
                            <li>(In its absolute discretion) Providing promotions, competitions and product opportunities for You to consider and participate in (at your discretion). </li>
                        </ul>
                        </li>
                    </ul>
                    <h4>fees/payment</h4>
                    <ul class="number">
                        <li><span>In consideration for cherrygift providing the Services to the Supplier, You agree to pay to cherrygift, without deduction or offset:</span>
                       <ul class="alphaRound">
                          <li>The Annual Fee;</li>
                          <li>The Service Fee.</li>
                           </ul>
</li>
                       <li><span>The Annual Fee is to be paid to cherrygift upon Your application to be a Supplier. Your registration as a Supplier with cherrygift shall be processed upon receipt of the Annual Fee.</li>
                        <li><span>The Service Fee shall be paid to cherrygift following the successful redemption of the Voucher (see heading “Voucher Redemption Process”).</span></li>
                        <li><span>cherrygift shall, pay to You the Voucher Funds on the 15th day of the month following the month in which the Voucher was successfully redeemed.</span></li>
                        <li><span>You are not entitled to any amount where a gift voucher is not successfully redeemed and all funds paid to cherrygift in relation to the Voucher shall remain with cherrygift.</span></li>
                        <li><span>You acknowledge that the Annual Fee and the Service Fee are subject to change, in cherrygift’s absolute discretion. Where the Annual Fee or Service Fee changes, cherrygift shall provide You with one months’ notice in writing.</span></li>
                        
                    </ul>
                    <h4>warranties and acknowledgements</h4>
                    <ul class="number">
                        <li><span>In registering as a Supplier and allowing the purchase of Vouchers via the cherrygift Website, You are agreeing to:-</span>
                         <ul class="alphaRound">
                        <li>Accept any Voucher purchased in respect of your business/product/services;</li>
                           <li>Pay the Annual Fee and the Service Fee to cherrygift;</li>
                           <li>Keep your registration details with cherrygift up to date;</li>
                           <li>Promptly respond to any reasonable request of cherrygift, including in relation to proofreading any advertisement or marketing materials or information, in relation to your business, which might appear on the cherrygift Website.</li>
                            </ul>
                        <li><span>In registering as a Supplier/Provider and allowing the purchase of Vouchers via the cherrygift Website, you are acknowledging that:-</span>
                                <ul class="alphaRound">
                                    <li>cherrygift will use its best endeavours to promote the Vouchers and attract new customers but does not warrant that this will occur and shall not be held responsible in any respect for the non-sale of Vouchers;</li>
                                    <li>cherrygift does not guarantee a minimum number of Voucher purchases;</li>
                                    <li>cherrygift is not responsible for any fraudulent use of the cherrygift Website, the Voucher or any cherrygift product;</li>
                                    <li>cherrygift will send a reminder alert to a voucher holder if the Voucher is not redeemed in six (6) months from the date of the Voucher is sent by cherrygift to the voucher holder;</li>
                                    <li>cherrygift will use its best endeavours to encourage a voucher holder to redeem the Voucher in store, but shall not be responsible and shall have no liability to You if this does not occur; </li>
                                </ul>

</li>
                    </ul>
                    <h4>voucher redemption process</h4>
                     <ul class="number">
                         <li><span>The Voucher purchase and redemption process is as set out below:-</span>
                         <ul class="alphaRound">
                             <li>A customer of cherrygift shall purchase the Voucher via the cherrygift Website. The customer may nominate him or herself or another party as the voucher holder (“voucher holder” or “recipient”);</li>
                             <li>Following the successful purchase of the Voucher, cherrygift shall send the Voucher to the recipient by text message, with a unique URL link to the Voucher,  to the nominated number and in accordance with the purchase directions provided by the customer;</li>
                             <li>The Voucher shall be linked, in cherrygift’s system, to You and You will be provided with a unique and secure Supplier ID PIN number;</li>
                             <li>The voucher holder (whether the customer, the nominated recipient or another party bearing a valid and unredeemed Voucher) shall redeem the Voucher by presenting to you the Voucher on the voucher holder’s smartphone;</li>
                             <li>Provided that the Voucher is not shown to be already redeemed, you will then accept presentation of the Voucher and cause it to be redeemed by inputting your unique ID;</li>
                             <li>Each Voucher is linked to your unique ID and once your ID has been entered, the Voucher will be notified as ‘redeemed’ in cherrygift’s system and can no longer be used.</li>
                             <li>The terms of the Voucher redemption are as follows:-
                             <ul class="lowerRoman">
                                 <li>The Voucher must be redeemed in one transaction; </li>
                                 <li>If the value of the Voucher is not utilised in one transaction, it is at Your discretion whether you provide cash back or refund or additional credit;</li>
                                 <li>The purchase of any product/service over and above the face value of the Voucher shall be Yours entirely;</li>
                                 <li>Each Voucher is valid for a period of twelve months from the date the Voucher is sent to its recipient unless cherrygift deems, in its absolute discretion, that an extension of this period is warranted in the circumstances.</li>
                             </ul>
                             </li>
                         </ul>
                     </li>
                    </ul>
                    <h4>advertising/marketing</h4>
                    <ul class="number">
                        <li><span>You authorise cherrygift to use your name, logo and any photographs, advertisements and materials provided by You to cherrygift by publishing the same on the cherrygift Website or any social media account associated with the promotion of the cherrygift Website. However, if you object to the use or publication of such information, you may notify cherrygift in writing and such information shall not be used.</span></li>
                        <li><span>You remain the owner of such information.</span></li>
                        <li><span>cherrygift may, in its absolute discretion, refuse to use or publish such information or any materials which it deems to be inappropriate for any reason. In this instance, cherrygift will give you the opportunity to provide appropriate material in place of any inappropriate material.</span></li>
                    </ul>
                    <h4>suspension of services</h4>
                    <ul class="number">
                        <li><span>cherrygift may suspend the Services and cease advertising You as a Supplier if, in its reasonable opinion:-</span>
                            <ul class="alphaRound">
                                <li>You are not complying with the terms of this Agreement;</li>
                                <li>Your business, product or service is no longer compatible with the cherrygift business or its philosophy;</li>
                                <li>You fail to pay any amount of money owing to cherrygift under the terms of this Agreement.</li>
                               
                            </ul>
                        </li>
                        <li><span>cherrygift shall provide you with written notice of the suspension of Services and reason for suspension and shall provide you with a reasonable period (however not more than 7 days) to respond to such notice. If, in cherrygift’s opinion, you have not addressed the concerns raised, cherrygift may terminate this Agreement by notice in writing to you. </span></li>
                        <li><span>Either party may terminate the Services (and this Agreement) without cause by providing the other party with thirty (30) days written notice of termination. In the event of termination by You, the Annual Fee shall be forfeited and you shall not be entitled to all or any part of the Annual Fee as a refund.</span></li>
                        <li><span>In the event of the termination of this Agreement, it is agreed that the following obligations shall survive termination:-</span>
                       <ul class="alphaRound">
                           <li>You will continue to honour any Voucher redeemed in the twelve (12) month period following termination; </li>
                           <li>cherrygift shall continue to account to you for the Voucher Funds following redemption;</li>
                           <li>You will continue to pay the Service Fee.</li>
                       </ul>
</li>
                    </ul>
                    <h4>No liability </h4>
                    <ul class="number">
                        <li><span>cherrygift shall not be responsible for any loss or damage caused:-</span>
                            <ul class="alphaRound">
                                <li>If You fail to follow the Voucher Redemption Process in relation to a Voucher;</li>
                                  <li>If You fail to receive Voucher Funds as a result of your failure to notify cherrygift of correct or updated details;</li>
                                    <li>By any fraudulent use of a Voucher;</li>
                                      <li>By the failure or faults with the cherrygift Website (and any associated software/platform) (although cherrygift shall take all reasonable steps to rectify such failure or fault as soon as possible);</li>
                                <li>By banking issues or delays in receiving funds</li>
                            </ul>
                        </li>
                        
                    </ul>
                    <h4>intellectual property</h4>
                    <ul class="number">
                        <li><span>All information and material on the cherrygift Website is subject to the intellectual property rights (including copyright) of cherrygift or its licensors. You may not utilise any information or material (other than material provided by You in relation to your business/product/service) on the cherrygift Website without cherrygift’s express written consent.</span> </li>
                        
                    </ul>
                    <h4>PRIVACY</h4>
                    <ul class="number">
                        <li><span>cherrygift shall at all times comply with its Privacy Policy, details of which are available on the cherrygift Website. cherrygift shall decline to provide any information or take any action if to do so would breach its Privacy Policy.</span> </li>
                    </ul>
                    <h4>GST</h4>
                    <ul class="number">
                        <li><span>All amounts referred to in this Agreement are GST exclusive unless otherwise required. The supplier shall provide a compliant tax invoice in respect of any taxable supply.
                            </span> </li>
                    </ul>
                     <h4>GENERAL</h4>
                    <ul class="number">
                        <li><span>The parties agree that:-</span>
                            <ul class="alphaRound">
                                <li>This Agreement contains the entire understanding and agreement between the parties as to the subject matter of this Agreement;</li>
                                <li>All previous negotiations, understandings, representations, warranties, memoranda or commitments about the subject matter of this Agreement are merged in this Agreement and are of no further effect.</li>
                                <li>No oral explanation or information provided by a party to another affects the meaning or interpretation of this Agreement or constitutes any collateral agreement, warranty or understanding.</li>
                            </ul>
                        </li>
                        <li><span>No waiver by a party of a provision of this Agreement is binding unless made in writing.</span></li>
                        <li><span>cherrygift may vary the terms of the Agreement by notice in writing to you, provided that you are not materially prejudiced by the variation.</span> </li>
                        <li><span>If a provision of this Agreement is void or unenforceable it must be severed from this Agreement and the provisions that are not void or unenforceable are unaffected by the severance.</span>
</li>
                        <li><span>The rights and remedies of a party to this Agreement are in addition to the rights or remedies conferred on the party at law or in equity.</span></li>
                    </ul>
                    <h4>GOVERNING LAW</h4>
                    <ul class="number">
                        <li><span>This Agreement is governed by the laws of Queensland and the Commonwealth of Australia which are in force in Queensland.</span></li>
                        <li><span>This Agreement is governed by the laws of Queensland and the Commonwealth of Australia which are in force in Queensland.</span></li>
                    </ul>
                </div></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block btn-lg r-blr-6" data-dismiss="modal" id="termconditionaccepted">OK</button>
      </div>
    </div>
  </div>
</div>