
<div class="modal fade" id="supplier_changepass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
	<header class="text-center logo-ctn">
    	<img src="../admin-images/logo.png" alt="" width="155" height="40">
    </header>
    <div class="auth-wrapper-inner">
    	<h4 class="heading">Change password</h4>
    	<form>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                    <input type="password" class="form-control" placeholder="Re-type your new password">
                </div>
            </div>
            <div class="form-group mr-b30">
                <div class="input-group">
                    <span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                    <input type="password" class="form-control" placeholder="Re-type your new password">
                </div>
            </div>
            <button class="btn btn-block btn-success btn-lg">CHANGE</button>
        </form>
       
    </div></div></div></div></div>


