<!DOCTYPE html>
<html lang="en">


    <head>
        <title>@yield('title')</title>
        @include('admin-template.header_include')

    </head>


    <body @yield('bodyclass')>
    @include('admin-template.header')



    <!--End of search Page-->
    @yield('content')
    @include('admin-template.footer')
    @include('admin-template.footer_include')

    <!-- <script type="text/javascript" src="{{ URL::asset('/scripts/validation.min.js') }}"></script>      -->
    <script type="text/javascript">
        window.__lc = window.__lc || {};
        window.__lc.license = 7576301;

        (function() {
            var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();
    </script>

    </body>
</html>


