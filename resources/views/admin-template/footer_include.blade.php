<?php /* Some Common JS includes */ ?>
<!-- Google CDN jQuery with fallback to local -->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="scripts/jquery-1.11.0.min.js"><\/script>')</script>-->
<script src="{{ URL::asset('scripts/moment.min.js')}}"></script>
<script src="{{ URL::asset('scripts/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('scripts/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{ URL::asset('scripts/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ URL::asset('scripts/bootstrap-select.min.js')}}"></script>
<script src="{{ URL::asset('scripts/admin/avatar.js')}}"></script>
<script src="{{ URL::asset('scripts/admin/script.js')}}"></script>

<script src="{{ URL::asset('scripts/owl.carousel.js') }}"></script>
<script src="{{ URL::asset('scripts/owl.carousel.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('scripts/validation.min.js') }}"></script>
<script src="{{ URL::asset('scripts/load-image.all.min.js') }}"></script>
<script src="{{ URL::asset('scripts/jquery.mask.js') }}"></script>