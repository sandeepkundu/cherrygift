<html>
<head>
<meta charset="UTF-8"><?php /* Header html of page */ ?>
<script type="text/javascript">
var APP_URL = "{{url()}}";

</script>
<header id="topHeader" class="navbar navbar-default" >
  	<div class="container-fluid">
            <link rel="shortcut icon" href="{{ URL::asset('images/cherry.ico')}}">
            <link rel="shortcut icon" href="{{ URL::asset('images/cherry.ico')}}">
    <link rel="apple-touch-icon" href="{{ URL::asset('images/cherry.ico')}}">
    	<!-- Brand and toggle get grouped for better mobile display -->
    	<div class="navbar-header">
    		<a class="navbar-brand" href="{{url()}}/admin-logout"><img src="{{ asset('images/admin-images/logo.png')}}" alt=""></a>
    	</div>
        <?php if (empty(Session::get('id')))
        { ?>
   	<ul class="nav navbar-nav navbar-right">
      		<li class="sign-login"><span><a href="{{url()}}/admin-signup" class="">SUPPLIER SIGNUP/LOGIN</a></span></li>
      	</ul>	
       <?php  }
       else
        { ?>
        <ul class="nav navbar-nav navbar-right">
            <li ><a href="{{url()}}/admin-logout" class="btn btn-info btn-lg">
          <span class="glyphicon glyphicon-log-out"></span> LOGOUT
                </a></li>	
      	</ul>
       <?php } ?>
  	</div>
</header>