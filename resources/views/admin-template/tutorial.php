<?php /* TERMS & CONDITION PAGE HTML */ ?>

<!-- Modal -->
<div class="modal fade" id="tutorialModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body pd-0">
      	 
      
      
      <div id="carousel-tutorial" class="carousel slide" data-ride="carousel" data-interval="false">
         

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="images/tutorial/tutorial.png" alt="..." class="img-responsive">
              <img src="images/tutorial/lens-zoom.png" alt="" class="img-responsive lens-zoom">
              
               <div class="item-text">
                <h4>Search for voucher by Location or Categories</h4>
                <p>sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrumexercitationem ullam 
corporis suscipit</p>
              </div>
            </div>
            <div class="item">
              <img src="images/tutorial/tutorial.png" alt="...">
               <div class="item-text">
               <h4>Search for voucher by Location or Categories</h4>
                <p>sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrumexercitationem ullam 
corporis suscipit</p>
              </div>
            </div>
            <div class="item">
              <img src="images/tutorial/tutorial.png" alt="...">
              <div class="item-text">
               <h4>Search for voucher by Location or Categories</h4>
                <p>sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrumexercitationem ullam 
corporis suscipit</p>
              </div>
            </div>
          </div>
			
          <!-- Controls -->
          <div>
          		<button type="button" class="btn btn-black col-xs-6" data-dismiss="modal">SKIP</button>
                <div class="controls-ctn col-xs-6">
                  <a class="left" href="#carousel-tutorial" role="button" data-slide="prev">
                   <span class="">Previous</span>
                  </a>
                   <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-tutorial" data-slide-to="0" class="active">1</li>
                    <li data-target="#carousel-tutorial" data-slide-to="1">2</li>
                    <li data-target="#carousel-tutorial" data-slide-to="2">3</li>
                    <li>/5</li>
                 </ol>
                 <a class="right" href="#carousel-tutorial" role="button" data-slide="next">
                    <span>Next</span>
                  </a>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>
</div>