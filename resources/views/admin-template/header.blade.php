<meta charset="UTF-8"><?php /* Header html of page */ ?>
<script type="text/javascript">
var APP_URL = "{{url()}}";

</script>
<header id="topHeader" class="navbar navbar-default" >
  	<div class="container-fluid">
            <link rel="shortcut icon" href="{{ URL::asset('images/cherry.ico')}}">
            <link rel="shortcut icon" href="{{ URL::asset('images/cherry.ico')}}">
    <link rel="apple-touch-icon" href="{{ URL::asset('images/cherry.ico')}}">
    	<!-- Brand and toggle get grouped for better mobile display -->
    	<div class="navbar-header"> 
    		<a class="navbar-brand" href="{{url()}}/admin-logout"><img src="{{ asset('images/admin-images/logo.svg')}}" alt=""></a>
    	</div>
        <?php if (empty(Session::get('id')))
        { ?>
   	<ul class="nav navbar-nav navbar-right">
      		<li class="sign-login"><span><a href="{{url()}}/admin-signup" class="">SUPPLIER SIGN UP/LOGIN</a></span></li>
      	</ul>	
       <?php  }
       else
        { ?>
        <ul class="nav navbar-nav navbar-right">
            <li class="active top-admin"><a href="{{url()}}">HOME</a></li>
        	<li class="active top-admin"><a href="{{url()}}/admin/contact-info">DASHBOARD</a></li>
                @if(Route::getCurrentRoute()->getParameter('a')!="")
                <li class="active top-admin"><a href="{{url()}}/admin/reports/{{ Route::getCurrentRoute()->getParameter('a') }}/{{date('m')}}/{{date('Y')}}">REPORTS</a></li>
                @endif
      		<!--<li class="sign-login"><span><a href="#" class="">USER SIGNUP/LOGIN</a></span></li>-->
            <li class="dropdown">
          		<a href="#" class="dropdown-toggle user-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          			
          			<?php echo str_limit(Session::get('trading_name'),10);  ?> <span class="caret"></span>
             	</a>
                <ul class="dropdown-menu">
                    <li class="top-menu" ><a href="{{url()}}" >HOME</a></li>  
               <li class="top-menu" ><a href="{{url()}}/admin/contact-info" >DASHBOARD</a></li>
               @if(Route::getCurrentRoute()->getParameter('a')!="")
                <li class="top-menu"><a href="{{url()}}/admin/reports/{{ Route::getCurrentRoute()->getParameter('a') }}/{{date('m')}}/{{date('Y')}}">REPORTS</a></li>
                @endif
            <li ><a href="{{url()}}/admin/change-password" >CHANGE PASSWORD</a></li>
            <li ><a href="{{url()}}/admin-logout" >LOGOUT</a></li> 
                </ul></li>
        </ul>
        
        
       <?php } ?>
  	</div>
</header>