<?php /* Some common CSS and head section of page */ ?>

<meta charset="utf-8">
<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />-->
<script src="<?php echo asset('scripts/jquery-1.11.0.min.js') ?>"></script>
<!--<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->


    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="apple-touch-icon-precomposed" type="image/ico" href="<?php echo  asset('/images/cherry.ico') ?>">
    <link rel="shortcut icon" href="<?php echo  asset('/images/cherry.ico') ?>">
    
    <!--FONTS-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link type='text/css' rel='stylesheet' href="{{ URL::asset('styles/font-awesome.min.css') }}" >
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('styles/bootstrap.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('styles/jquery.mCustomScrollbar.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('styles/bootstrap-select.css')}}">
    
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('styles/fonts.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('styles/admin/common-layout.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('styles/admin/style.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('styles/admin/style-responsive.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('styles/jquery-ui.min.css')}}">
    <script src="<?php echo asset('scripts/jquery-ui.js') ?>"></script>
    <script src="<?php echo asset('scripts/jquery.validate.min.js') ?>"></script>  
        <script src="<?php echo asset('scripts/validation.extended.js') ?>"></script>   
    <script type="text/javascript">
var APP_URL = "{{url()}}";
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77931905-1', 'auto');
  ga('send', 'pageview');

</script>