<?php /* TERMS & CONDITION PAGE HTML */ ?>

<!-- Modal -->
<div class="modal fade approve-modal" id="forgotApproveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bdr-n">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
      	<p class="info-text mr-0">Please follow the instructions we sent to:</p>
        <h3 class="text-center mr-b50"><strong>name@gmail.com</strong></h3>
      </div>
      <div class="modal-footer bdr-n">
        <button type="button" class="btn btn-approve btn-block"><i class="ic-tick text-hide">Tick</i></button>
      </div>
    </div>
  </div>
</div>