@extends('template.main')
@section('title','cherrygift - purchase successful')
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>

@section('content') 
    <div class="cart-wrapper">
        <div class="container">
            <div class="row">
                
            @if(Session::has('message'))
            <div class="alert alert-success">
                <h3 style='text-align: center'>{{ Session::get('message') }}</h3>
            </div>
            @endif

            @if (Session::has('errors'))
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <h3 style='text-align: center'>{{ $error }}</h3>
                    @endforeach
                </ul>
            </div>
            @endif
            
            	<div class="col-sm-10 col-sm-offset-1">
                    @if(count($order_details)!=0)
                	<h2 class="page-heading text-center">Purchase successful</h2>
                        
                        @foreach($order_details as $displayorders)
                	<div class="voucher-list mr-b50">
            			<div class="list-item clearfix">
                        	<span class="left-item product-item">
                            	<img src="{{$s3_url}}{{$s3_env}}{{$displayorders->image}}" alt="" class="img-responsive" style="height: 200px;">
                                <div class="item-upper-bg">
                            		<div class="media"> 
                                    	<div class="media-left"> 
                                        	<img class="small-circle" src="{{$s3_url}}{{$s3_env}}{{$displayorders->logo}}" style="width: 35px; height: 35px;">
                                      	</div> 
                                        <div class="media-body media-middle"> 
                                        	<h4 class="media-heading">{{$displayorders->store_location}}</h4> 
                                     	</div>
                       				</div>
                    			</div>
                            </span>
                            <div class="right-item">
                            	<ul class="list-unstyled voucher-info-list list-block">
                                	<li class="price">${{number_format($displayorders->voucher_price,0)}}</li>
                                    <li class="phone"><span class="icon-phone"></span> +61 {{$displayorders->phone_number}}</li>
                                    @if($displayorders->sms_schedule==0)
                                <li class="date"><span class="icon-calendar" custom_date_value="<?php echo strtotime($displayorders->sms_date.' '.$displayorders->sms_hour.':'.$displayorders->sms_minutes); ?>"></span> On {{ date('d F', strtotime($displayorders->sms_date)) }} 
                                    @else
                                    <li class="date"><span class="icon-calendar" custom_date_value="<?php echo strtotime($displayorders->sms_date.' '.$displayorders->sms_hour.':'.$displayorders->sms_minutes); ?>"></span> On {{ date('d F', strtotime($displayorders->sms_date)) }} at {{str_pad($displayorders->sms_hour,2,"0",STR_PAD_LEFT)}}:{{str_pad($displayorders->sms_minutes,2,"0",STR_PAD_LEFT)}} 
                                    @endif
                                </ul>
                                <p>{{substr($displayorders->sms_message, 0,strrpos($displayorders->sms_message,"http"))}}</p>
<!--                                <p>{{$displayorders->sms_message}}</p>-->
                            </div>
                            
                		</div>
            		</div>
                        @endforeach
                        @else
                        <h3 class="text-center mr-b30">There are no items in this order</h3>
                        @endif
                </div>
               
            </div>
        </div>
         <div class="container-fluid continue-shop">
        	<a href="{{url().'/search'}}">Continue shopping</a>
        </div>
  	</div>
   
    @stop
