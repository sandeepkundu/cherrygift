<!DOCTYPE html>
@extends('cms-template.default')
@section('title','Company Report')
@section('content')


          
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            Company </div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Company</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
                <div class="col-lg-12">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">Company Report</div>
                                    <div class="panel-body">
                                            @if (Session::get('message'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('message');
                                                echo $error;
                                                Session::forget('message');
                                                ?>
                                            </div>
                                            @endif

                                            @if (Session::get('success-msg'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('success-msg');
                                                echo $error;
                                                Session::forget('success-msg');
                                                ?>
                                            </div>
                                            @endif
                                            <div class="message_save"></div>
     <div class="table-responsive">                                       
    <table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
               
                <th class="no-sort">Company Name</th>
                <th class="no-sort">Company Email</th>
                <th class="no-sort">Company Address</th>
                <th class="no-sort">Contact No</th>
                <th class="no-sort">Status</th>
                <th class="no-sort">Action</th>
                 <th class="no-sort"></th>
                 <th class="no-sort"></th>
                 <th class="no-sort"></th>
            </tr>
        </thead>
 
        <tbody>
            
        </tbody>
    </table>
     </div>        
             
             
             
                                    </div>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                
  <div class="modal fade model-lg resetform"  id="view_customer_model" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content"  >
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="close-error">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"  id="myModalLabel">Company Information</h4>
            </div>
            <form role="form" id="edit_company" method="POST" enctype="multipart/form-data" action="save-customer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <br/>
                <div style="width:66%; margin: auto" class="edit-form">
                    <div class= 'error alert alert-danger' style="display:none;"></div>
                    <div class='alert-success bg-success' style="padding:10px;margin-bottom: 20px; display:none;">User Information updated Successfully.</div>
                    <div class="error_message"></div>
                    <span class='errors' id="cls_error"></span>
                    <div class="form-group">
                        <label>Company PIN</label>
                        <input type="text" class="form-control" id="pin" name="pin" readonly>
                        <span class='errors'></span>
                    </div>
                   
                    <div class="form-group">
                        <label>First Name<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" id="fname" name="first_name" required="required" >
                        <span class='errors' id="fname_error"></span>
                    </div>
                    <div class="form-group">
                        <label>Last Name<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" id="lname" name="last_name" required="required" >
                        <span class='errors' id="lname_error"></span>
                    </div>

                    <div class="form-group">
                        <label>Contact Email<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" id="conemail" name="contact_email" required="required">
                        <span class='errors' id="conemail_error"></span>
 
                    </div>

                    <div class="form-group">
                        <label>Contact Number<em><span style="color:red;">*</span></em></label>
                        <div class="row">
                            <div class="col-xs-3">
                                <input type="text" class="form-control" id="countrycode" name="country_code" value="+61" readonly style="background-color: lightgrey">
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" id="connumber" name="contact_number" required="required" maxlength="18" >
                                <span class='errors' id="contact_error"></span>
                            </div>         

                        </div>  
                    </div>
                    
                   <div class="form-group">
                        <label>Company Email<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" placeholder="Enter your company email" id="comemail" name="company_email">
                        <span class='errors'></span><span style="color: red" id="company_email_error"></span>
                    </div>
                    <div class="form-group">
                        <label>Company Name<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" placeholder="Enter your company name" id="tradname" name="trading_name" required="required" >
                        <span class='errors'></span><span id="trading_name_error" style="color: red"></span>
                    </div>
                    <div class="form-group">
                        <label>ABN / ACN numbers<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" placeholder="111 333 555 54" id="abnacnnumber" name="abn_acn_number" required="required" maxlength="14" >
                        <span class='errors' id="abnacnnumber_error"></span>
                    </div>
                    <div class="form-group">
                        <label>Company Address<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" placeholder="Enter your address" id="comaddress" name="company_address" required="required">
                        <span class='errors'></span>
                    </div>
                    <div class="form-group">
                        <label>Location<em><span style="color:red;">*</span></em></label>
                        <select id="state1" name="state" required class="form-control">
                                <option value="">Select State</option>
                                @foreach(App\Model\States::orderBy('state_name', 'asc')->get() as $location)
                                 <option value="{{$location->id}}">{{$location->state_name}}</option>                        
                                @endforeach
                            </select>
                        <span class='errors' id="state_error"></span>
                    </div>
                    <div class="form-group">
                        <label>Suburb<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" placeholder="Suburb" id="city" name="city" autocomplete="off" required="required"> 
                            
                        <input type="hidden" id="txtAllowSearch" name="suburb" >
                        <span class='errors' id="suburbs_error"></span>
                    </div>
                    <div class="form-group">
                        <label>Postcode<em><span style="color:red;">*</span></em></label>
                         <input type="text" class="form-control" placeholder="Postcode" id="postcode" name="post_code" required="required" maxlength="4" >
                        <span class='errors' id='postcode_error'></span>
                    </div>
                    <div class="form-group">
                        <label>Bank account details<em><span style="color:red;">*</span></em></label>
                        
                        <input type="text" class="form-control" placeholder="Account name" id="accountname" name="account_name"  required="required" autocomplete="off" >
                       <span class='errors' id="accountname_error"></span>
                        <div class="row" style="margin-top: 5px;">
                            <div class="col-xs-3">
                                <input type="text" class="form-control" placeholder="BSB" id="bsb" name="bsb" maxlength="6" autocomplete="off" >
                                <span class='errors' id="bsb_error"></span>
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" placeholder="Account number" id="accountnumber"  name="account_number" required="required" autocomplete="off" maxlength="10">
                                <span class='errors' id="accountnumber_error"></span>
                            </div>         

                        </div>  
                        
                    </div>
                     <div class="form-group">
                        <label>Accounts Email<em><span style="color:red;">*</span></em></label>
                         <input type="text" class="form-control" placeholder="Accounts Email" id="accmail" name="accounts_email" required="required"  >
                        <span class='errors' id='accounts_email_error'></span>
                    </div>
                    <div class="form-group">
                        <label>Accounts Email<em><span style="color:red;">*</span></em></label>
                         <input type="text" class="form-control" placeholder="Accounts Email" id="accmail" name="accounts_email" required="required">
                        <span class='errors' id='accounts_email'></span>
                    </div>
                    <div class="form-group">
                        <label>Category<em><span style="color:red;">*</span></em></label>
                        
                            <select id="category" name="category" required class="form-control">
                                <option value="">Select Category</option>
                                @foreach(App\Model\Categories::orderBy('name', 'asc')->get() as $cat)
                                   <option value="{{$cat->id}}">{{$cat->name}}</option> 
                                @endforeach
                            </select>
                                 <span class='errors' id="category_error"></span>  
                            
                                 
                    </div>

                    <input type="hidden" name="id" id="companyid" value=""/>

                    <br>
                    <div class ='form-group text-center'>
                        <input type="submit"  data-loading-text="Loading..." class="btn btn-grn" value="Save" id="savebtn" >
                        <!--input type="button"  data-loading-text="Loading..." class="btn btn-grn " value="Activate"  onclick="customerAction('1');" id='approveBtn'> 
                        <input type="button"  data-loading-text="Loading..." class="btn btn-grn " value="Reject"  onclick="companyReject();" id='rejectBtn'> 
                        <input type="button"  data-loading-text="Loading..." class="btn btn-grn " value="Deactivate"  onclick="customerAction('2');" id='deactivateBtn' -->
                        <button type="button" class="btn btn-grn close-error" data-dismiss="modal" class="close" >Close</button>
                    </div>


                </div>


            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade model-lg resetform"  id="view_reject_model" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content"  >
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="close-error">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"  id="myModalLabel">Reject Reason</h4>
            </div>
            <form role="form" id="edit_company" method="POST" enctype="multipart/form-data" action="reject-customer"><input type="hidden" name="_token" value="{{ csrf_token() }}">
                <br/>
                <div style="width:66%; margin: auto" class="edit-form">
                    <div class= 'error alert alert-danger' style="display:none;"></div>
                    
                    <div class="error_message"></div>

                     <textarea name="reason" id="reason" rows="15" cols="60"></textarea>
                     
                    <input type="hidden" name="id" id="rejcompanyid" value=""/>

                    <br>
                    <div class ='form-group text-center'>
                        <input type="submit"  data-loading-text="Loading..." class="btn btn-green" value="Send"  >
                        
                        <button type="button" class="btn btn-green close-error" data-dismiss="modal" class="close" >Close</button>
                    </div>


                </div>


            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('scripts')

<script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>    
<script type="text/javascript">
$(document).ready(function() {
  if ($('#customerlist').length) {
    $('#customerlist').DataTable({
            processing: true,
            deferRender: true,
            serverSide: true,
            responsive: true,
            //bSort: false,
            order: [[ 0, "asc" ]],
            autoWidth: true,
            bLengthChange: false,
            pageLength: 10,
            scroller: true,
            
             //scrollX: true,
            //columnDefs: [{ "orderable": false, "targets": 1 }],
            columns: [
                
                {data: 'company_name', name: 'company.company_name'},
                {data: 'email', name: 'company.email'},
                {data: 'company_address', name: 'company.company_address'},
                {data: 'phone', name: 'company.phone'}, 
                {data: 'is_subscribed', name: 'company.is_subscribed'}, 
                {data: 'id', name: 'company.id'},
                {data: 'voucher_unique_id', name: 'order_voucher.voucher_unique_id'},
                {data: 'store_location', name: 'company_business_profile.store_location'},
                {data: 'merchant_pin', name: 'company_business_profile.merchant_pin'}
                
            ],
            ajax: SITE_URL + '/super-admin/customerdata',
            // select: true,
//                'aoColumnDefs': [{
//            'bSortable': false,
//            'aTargets': [-1] /* 1st one, start by the right */
            //             }],
            //"aaSorting": [[ 1, "asc" ]] // Sort by first column descending


        });
    }    
    
} );

</script>
<script> 

$(document).ready(function() {
$('.message_save').html('');

     $(document).on('click','.btn111',function(){
       
           var a = $(this).attr('id');
          // alert(a);
           $("#divid").val(a);
           $("#myModal").modal("show");
     });
    $("#btnsave").click(function(e){
            e.preventDefault();
            var id = $("#divid").val();
            var password = $("#password").val();
            var confirm_password = $("#confirm_password").val();
           // alert(password);
       //  var hidden = $("#hidden").val();
        // var data = $("#reset_form").serialize();
        //alert(data);
      $.ajax({
                type: "post",
                dataType : 'json',
                url:"resetPassword",
                data: {
                    "_token": "{{ csrf_token() }}",
                   "id":id,
                   "password":password,
                   "confirm_password":confirm_password
                },
                success: function(result) {
                  if(result.status=="success"){
                    var msg=result.message;

                    $("#myModal").modal("hide");
                    $('.message_save').html('<div class="alert alert-success">'+msg+'</div>');
                  } else if (result.status == "error"){
                            var ty = result.error;
                                    $('#error_message_show').html('');
                                    for (var i in ty){

                            $('#error_message_show').append(ty[i] + "<br>");
                                    //toastr['error'](abcd.message, "Error");
                            }
                            }
                
                },
                 error: function(error){
                                //$btn.button("reset");
                            //console.log(error);
                            }
              })
 });


 
});

</script>
@stop

<div class="container">
 
  <!-- Trigger the modal with a button -->
  <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Modal</button>-->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reset Password</h4>
        </div>
        <div class="modal-body">
          <!--{!!Form::open(array('id'=>'resetpassword'))!!}-->
          {!! Form::model('form',['files' => true, 'method' => 'post' ,'class' => 'text-center reserform', 'required','id' =>'reset_form','accept-charset'=>'UTF-8','autocomplete'=>'off' ]) !!}
          <!--<input type="hidden" id="hidden" name="_token" value="{{csrf_token()}}">-->
         
             <div class="form-group" style="text-align:left;">
                        <label>New Password<em><span style="color:red;">*</span></em></label>
                        <!--<input type="text" class="form-control" id="newpassword" name="new_password" required="required" >-->
                        {!! Form::input('password','password','',['class'=>'form-control','placeholder'=>'Password','id'=>'password'])!!}
                        <span class='errors' id="fname_error"></span>
            </div>
             <input type="hidden" id="divid" name="id">
             <div class="form-group" style="text-align:left;">
                        <label>Confirm Password<em><span style="color:red;">*</span></em></label>
                       <!-- <input type="text" class="form-control" id="confirmpassword" name="confirmpassword" required="required" >-->
                       {!! Form::input('password','confirm_password','',['class'=>'form-control','placeholder'=>'Confirm Password','id'=>'confirm_password'])!!}
                        <span class='errors' id="fname_error"></span>

            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-grn" id="btnsave">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          
        </div>
        <div style="text-align: center;">
         <span class='errors'  id="error_message_show"></span><br>
       </div>
        {!! Form::close() !!}
      </div>
      
    </div>
  </div>
  
</div>