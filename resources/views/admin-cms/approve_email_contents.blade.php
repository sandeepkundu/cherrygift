Hi {{$first_name}}<br><br>

Your request for changes regarding your store/site <b>{{$store_location}}</b> with cherrygift has been received.<br><br>

We are very happy to advise that your profile changes have been approved.<br><br>

Your updated business page is now live so feel free to login to <a href="www.cherrygift.com">cherrygift.com</a> and check it out!<br><br>


If you have any queries regarding your subscription, check out our FAQ’s and if you don’t find your answer there, please contact our team on 1300 122 321 or email {{env('contact_email')}} <br><br>

We look forward to working with you and helping your business GROW one cherrygift at a time!<br><br>

Cheers,<br><br>

From the cherrygift team!<br><br>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">
