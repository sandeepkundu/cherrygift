<!DOCTYPE html>
@extends('cms-template.default')
@section('title','Clear Payment')
@section('content')
          
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <div class="page-title-breadcrumb" id="title-breadcrumb-option-demo">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            Clear Payment</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Clear Payment List</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
               
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
				<div class="col-lg-12">
                           		<div class="panel panel-yellow">
                            		<div class="panel-heading">Clear Payment List</div>
                            		<div class="panel-body">
                                            @if (Session::get('message'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('message');
                                                echo $error;
                                                Session::forget('message');
                                                ?>
                                            </div>
                                            @endif

                                            @if (Session::get('success-msg'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('success-msg');
                                                echo $error;
                                                Session::forget('success-msg');
                                                ?>
                                            </div>
                                            @endif   
                                        <div class="clearfix">
                                            
                                            Month : 
                                            <select name="month" id="month">
                                                <?php 
                                                $current_month = (!empty($month))?$month:date("m");
                                                for($i=1;$i<13;$i++) {?>
                                                <option <?php echo ($current_month==$i)?"selected='selected'":"";?>  value="<?php echo $i?>"><?php echo date('F', mktime(0, 0, 0, $i, 10));?></option>
                                                <?php }?>
                                            </select>

                                            Year : 
                                            <select name="year" id="year">
                                                <?php 
                                                $current_year = (!empty($year))?$year:date("Y");
                                                for($i=2016;$i<date("Y")+1;$i++) {?>
                                                <option <?php echo ($current_year==$i)?"selected='selected'":"";?>  value="<?php echo $i?>"><?php echo $i?></option>
                                                <?php }?>
                                            </select>
                                            <input type="submit" name="submit" id="date_submit" value="submit" />
                                            
                                        </div>
                     
    <table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="no-sort">Redeemed Date</th>
                <th class="no-sort">Company Name</th>
                <th class="no-sort">Store</th>
                <th class="no-sort">Payable Amount</th>
                <th class="no-sort">Account Name</th>
                <th class="no-sort">BSB</th>
                <th class="no-sort">Account Number</th>
                <th class="no-sort"></th>
                <th class="no-sort">Payment Status</th>
                <th class="no-sort">Remittance Advice </th>
                </tr>
        </thead>
 
      <!--   <tbody>
            
        </tbody>
        @if($sum!=0)

        <thead>
            <tr>
                <th></th>
                <th style="text-align:right;">Total:</th>
                <th style="padding-left:10px;">${!! $sum !!}</th>
                <th colspan="6"></th>
                
                </tr>
        </thead>
        @endif -->
        
         <tfoot>
            <tr>
                <th colspan="3" style="text-align:right">Total:</th>
                <th colspan="5"></th>
            </tr>
        </tfoot>
    </table>
             
             
             
             
                         			</div>
                        		</div>
                   		</div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                
  



@stop

@section('scripts')

<script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>    
<script type="text/javascript">
$(document).ready(function() {
    
    $("#date_submit").click(function(){
        window.location="{{url()}}/super-admin/admin-vendor-payment/"+$("#month").val()+"/"+$("#year").val();
    });
    
    function call_ajax(cbp_id) {
        $.ajax({
           url: "{{url()}}/super-admin/change-payment-status",
           data: {"cbp_id": cbp_id,"month":{{$month}},"year":{{$year}},'_token': '{{csrf_token()}}'},
           dataType: 'jsonp',
           type: 'POST',
           async:false,
           success: function(data) {
               
           }
        }); 
    }
       
  if ($('#customerlist').length) {

    var strmsg='Are you sure you want to send the mail?';
    var table = $('#customerlist').DataTable({
            processing: true,
            deferRender: true,
            serverSide: true,
            responsive: true,
            //bSort: false,
            order: [[ 0, "asc" ]],
            autoWidth: true,
            bLengthChange: false,
            pageLength: 10,
            scroller: true,
            //columnDefs: [{ "orderable": false, "targets": 1 }],
            columns: [
                {data: 'updated_at', name: 'vc.updated_at'},
                {data: 'company_name', name: 'company.company_name'},
                {data: 'store_location', name: 'company_business_profile.store_location'},
                {data: 'amount', name: 'vc.amount'},
                {data: 'account_name', name: 'company.account_name'},
                {data: 'bsb', name: 'company.bsb'},
                {data: 'account_number', name: 'company.account_number'},
                {data: 'voucher_unique_id', name: 'order_voucher.voucher_unique_id'},
                {data: 'payment_status', name: 'admin_vendor_payment.payment_status'},
                
                 {
                    "targets": 9,
                    data:null,
                    "searchable": false,
                    "mRender": function(data, type, row){
                        //return "<a id='' class='btn111'><i class='fa fa-key' aria-hidden='true'></i></a>";
                       
                    if (data.mail_sent==1) {
                        var setsec= "<span style='color:green;'>Remittance Sent</span>";
                        //return "<a style='color:black' href='{{url()}}/super-admin/pdf_voucher_id/"+data.id+"/{{$month}}/{{$year}}'  class='btn111' target='_blank'>View</a>"+'|'+setsec;
                        
                         } else {
                          var setsec= "<span style='color:red;'>Send Remittance</span>";
                        }
                          return "<a style='color:black' href='{{url()}}/super-admin/pdf_voucher_id/"+data.id+"/{{$month}}/{{$year}}'  class='btn111' target='_blank'>View</a>"+'|'+"<a style='color:black' id='"+data.id+"' onclick='return confirm("+"`"+strmsg+"`"+")' href='{{url()}}/super-admin/postmail/"+data.id+"/{{$month}}/{{$year}}'>"+setsec+"</a>";
                      
                        
                       

                    }
                 }
                  

                ],
            ajax: SITE_URL + '/super-admin/admin-vendor-payment-list/'+'{{$month}}'+'/{{$year}}',
            fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

            $('td .noPaidChk', nRow).on('click', function() {
            
                if(aData['payment_status'] == "<span class='noPaidChk' style='cursor:pointer; color:red'>Not Paid</span>")
                    var con = confirm("Are you sure You want to clear payment?");
                
                if(con) {
                         var res = call_ajax(aData['id']);
                         location.reload();
                 }
             
            });
            
            },
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 3 ).footer() ).html(
                '$'+pageTotal
            );
        },

        });
    }  

} );
//console.log('Col Clicked.', this, aData, iDisplayIndex, iDisplayIndexFull);
</script>
@stop