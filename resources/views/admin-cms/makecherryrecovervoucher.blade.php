@extends('cms-template.default')
@section('title','Company Report')
@section('content')
<style>



/* ------------------------------------------------------------------------------------- PRODUCT VOUCHER COMMON */  
.product-item{
    position:relative;  
}
.product-item > img:not(.index){ width:100%; height: 200px;}
.product-item.multi{
    margin-bottom:20px; 
    overflow: hidden;
        height: 200px;
}
.product-item.multi:hover{
-webkit-box-shadow: 1px 1px 1px 1px #888;
    -moz-box-shadow: 1px 1px 1px 1px #888;
    box-shadow: 1px 1px 1px 1px #888;
}
.product-item.multi .full-background {
    width: 100%;
    padding:10px;
    top: -100%;
    background-color: rgba(0,0,0,0.6);
    position: absolute;
    color: #fff;
    z-index: 10;
    transition: top 0.25s ease-in-out 0s;
    left: 0;
}
.product-item.multi .full-background.hover {
   top: 0;
    height: auto;
    max-height: 100%;
}
.product-item .item-upper-bg{
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    box-shadow: inset 0px -27px 56px 0px rgba(0,0,0,0.1);
    padding: 15px;
     z-index: 11;
}
.product-item .item-upper-bg .media-body .media-heading{
    color:#fff;
    font-size:16px;
    font-weight:bolder;
    margin:0;   
}

/* ------------------------------------------------------------------------------------- BUY VOUCHER PAGE */


/* ------------------------------------------------------------------------------------- HISTORY PAGE */
.white-bg-header{
    background-color:#fff;
    text-align:center;  
    padding:50px 0;
}
.white-bg-header .title{
    font-size:32px;
    font-weight:bolder;
    display:inline-block;   
    margin:0 40px 0 0;
}
.voucher-list + .voucher-list{
    margin-top:15px;
}
.voucher-list .list-item{
    background-color:#fff;
    border:1px solid #ecedf3;
    padding:20px;
    -webkit-box-shadow: 1px 1px 1px 1px rgba(236,237,243,0.5);

}
.voucher-list .list-item .left-item{
    display:block;  
    margin:0 0 20px 0;
}
.voucher-list .list-item .right-item{
    overflow:hidden;    
}
.voucher-info-list{
    margin-bottom:20px; 
}
.voucher-info-list li{
    display:inline-block;
    padding:0 15px;
    vertical-align:middle;  
    line-height:46px;
}
.voucher-info-list.list-block li{
    display:block;
    padding:0;
    border:none;
    line-height:100%;
}
.voucher-info-list.list-block li .action-btn-group .btn{
    padding:10px 15px;
    font-weight:600;
}
.voucher-info-list.list-block li +li{
    margin-top:25px;
}
.voucher-info-list li + li{
    border-left:1px solid #eeeff1;
}
.voucher-info-list li.price{
    font-size:46px;
    font-weight:bold;
    color:#ec1d23 /* red color */;
    padding-left:0;
}
.voucher-info-list li.phone{
    font-size:14px;
    font-weight:bolder;
    color:#000;
}
.voucher-info-list li span[class^="icon-"]{
    width:20px;
    height:22px;
    display:inline-block;
    background-image:url(../images/voucher/voucher-sprite.png);
    vertical-align:middle; 
    margin-right:10px;
}
.icon-phone{background-position:0 0;}
.icon-calendar{background-position:0 -27px;}





  
    
    
    /* --------------------------------------------------------------------------------------------- FOOTER RESPONSIVE */



    /* --------------------------------------------- AUTH */
    .auth-header{
        border-bottom:1px solid #dbdbdb;
        padding-bottom:40px;
    }
    .auth-header .head-left{
        float:left;
        margin-bottom:0;
    }
    .auth-header .head-right{
        float:right;
    }
    .auth-wrapper .submit-ctn{
        margin-bottom:45px;
    }
    .list .list-item {
        width: 50%;
     }
     
     .voucher-list .list-item .left-item{
        float:left;
        width:300px;
        margin:0 20px 0 0;  
    }
    
    #page-search #page-search-option.collapse, #catalog-filter #search-accordion.collapse{
        display:block;  
    }
    
    




    
    
    .redeem-wrapper .page-heading{
        text-align:center;
    }
    
    .redeem-main{
        background-color: #fff;
        padding: 20px 15px;
        -webkit-box-shadow: 1px 1px 1px 1px rgba(236,237,243,0.5);
    }
    
    .redeem-info-list .general-info{
        border-left:1px solid #eeeff1;  
    }
    .redeem-info-list .general-info .column-common{
        padding:15px;   
    }
    .redeem-v-code{
        padding-top:25px;   
    }
    .redeem-v-code-ctn{
        padding:80px 15px;  
    }
    
    /* --------------------------------------------- BUY VOUCHER */
    .buy-voucher-wrapper{
        padding-bottom:30px;
    }
    
    /* --------------------------------------------- SEARCH PAGE */
    .catalog-product {
        margin: 0 0 0 250px;
    }
    .catalog-filter {
        width: 250px;
        float:left;
    }
    
   

        
}



.voucher-list .list-item {
    background-color: #fff;
    border: 1px solid #ecedf3;
    padding: 20px;
    -webkit-box-shadow: 1px 1px 1px 1px rgba(236,237,243,0.5);
}
.white-bg-header > div {
    margin-bottom: -29px;
    margin-left: -46px;
    margin-top: 40px;

}
.white-bg-header{

    padding:30px 0;
}
.history{

  margin: -8px 8px 10px;
}
.common{
    font-size: 14px;
    font-weight: bolder;
    color: #000;

}
.redeem{
 font-size: 14px;
    font-weight: bolder;
    color: #8dc53e;

}
.cancel{
font-size: 14px;
    font-weight: bolder;
    color: red;

}
.voucher-info-list li {
       line-height:normal; 
       padding-right: 10px;
       padding-left: 8px;
  }
  .listdate{
        /*margin-right: 44px;*/
    margin-top: 10px;
    border-left-color: #eeeff1;
    border-left-style: solid;
    border-left-width: 1px;
    height: 40px;
    
  }
.voucher-info-list li.phone {

        height: 36px;
}

/*.voucher-info-list li {

        padding: 0 20px;
}*/
</style>
<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Recover Voucher</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

            <li class="active">Recover Voucher</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class=" mbl">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                             Cherrygift Recover Voucher</div>
                        <div class="panel-body pan">
                        
                            @if(Session::has('message'))
                            <div class="alert alert-success">
                                <h3 style='text-align: center'>{{ Session::get('message') }}</h3>
                            </div>
                            @endif

                            @if (Session::has('errors'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <h3 style='text-align: center'>{{ $error }}</h3>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                                
                           
                            <form action="cherrygift-recover-voucher-update" method="post" id="frm_subscription">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <div class="form-body pal">
                                    <div class="form-group"></div> 
                                    <div class="row">
                                        <div class="col-md-5">
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                
                                                <input type="number" id="cherrygift_voucher" placeholder="Enter mobile number or voucher number" name="cherrygift_voucher" value="{{ @$mobile_number}}" class="form-control"  minlength="9" maxlength="15" required/></div>
                                                <p class="errorvoucher"></p>
                                                <span class="resenderrors" style="color:red"></span>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group text-right">
                                                
                                               
                                                <input type="submit"  value="Search" id="search_vouchers"/>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                
                            </form>

            @if(Request::isMethod('post'))
            @if(count($purshase_history)!=0)
           
            <div class="col-sm-12 mr-b50" id="resend-vocher-id clearfix">
                
              
                <form name="" action="postresendvouchers" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="vouchervalue" name="voucher_ids" value="">
                    <input type="hidden" id="mobileno" name="mobileno" value="{{$purshase_history[0]['phone_number']}}">
                    
                     <div class="form-actions text-right pal resendblock" >
                        <button type="submit" class="btn btn-success" id="resendbtn">
                         Resend</button>
                    </div>
                    
                </form>
                 @foreach($purshase_history as $history)
                 <div class="pd-tab-15">
                    <br>
          <div class="voucher-list">
                            <div class="list-item clearfix">
                                <span class="left-item product-item">
                                    <img class="img-responsive" alt="" src="http://localhost:8000/s3/pro/images/company/storeslogo/2730/1465276197phpmhNfYE.png" style="height: 200px;">
                                    <div class="item-upper-bg">
                                        <div class="media"> 
                                            <div class="media-left"> 
                                                <img style="width: 35px; height: 35px;" class="small-circle" src="http://localhost:8000/s3/pro/images/company/storeslogo/2730/logo/1462409173phpYvU9oa.png">
                                            </div> 
                                            <div class="media-body media-middle"> 
                                                <h4 class="media-heading">{{$history->store_location}}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                </span>
                                <div class="right-item">
                                    <ul class="list-unstyled voucher-info-list">
                                        <li class="price">${{number_format($history->voucher_price,0)}}</li>
                                        <li class="phone"><span class="icon-phone"></span> +61 {{$history->phone_number}}
                                          
                                        </li>
                                        @if($history->sms_schedule==0)
                                        <div class="pull-right listdate">
                                        <li class="date" style="float:left; display:block;">
                                          <span class="icon-calendar"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }}
                                      
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                                  <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?> 
                                        </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                          
                                        @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 
                                         
                                      </li>
                                        </div>
                                      </div>
                                       @else<!--sheduler ifend-->

                                        <div class="pull-right listdate">
                                        <li class="date" style="float:left; display:block;">
                                          <span class="icon-calendar"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }} at {{str_pad($history->sms_hour,2,"0",STR_PAD_LEFT)}}:{{str_pad($history->sms_minutes,2,"0",STR_PAD_LEFT)}}
                                      
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                                  <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?> 
                                        </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                          
                                        @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 
                                         
                                      </li>
                                        </div>
                                      </div>



                                       @endif 

  
                                    </ul>   
                                
                                    <p>{{substr($history->sms_message, 0,strrpos($history->sms_message,"http"))}}
                                     
                                    </p><br><br>

                                    <p style="float:right;margin: 0px 10px 10px;"><b >Voucher:&nbsp; {{$history->voucher_unique_id}}</b> &nbsp;&nbsp;&nbsp;<span style="border-right-color: #eeeff1;border-right-style: solid;border-right-width: 1px;font-size:33px"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                      @if($history->voucher_status==1 && $history->redeemed==0)
                                      <input type="checkbox" class="chkbox" id="{{$history->id}}">
                                      @elseif($history->redeemed==1)
                                      <input type="checkbox" class="chkbox" id="" disabled>
                                      @else
                                      <input type="checkbox" class="chkbox" id="" disabled>
                                      @endif
                                    </p>
                                </div>

                            </div>
                        </div><br>
                 </div>
                 @endforeach
                 @else
                 <div class="col-lg-12">
                   <p style="text-align: center;"> NO VOUCHER FOUND</p>
                    
                </div>

            </div>
            @endif
            @endif




                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!--END CONTENT-->

</div>
<!--END PAGE WRAPPER-->




@stop

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){


$('.chkbox').click(function(){

      
    var id=$(this).attr('id');


var matches = [];
$(".chkbox:checked").each(function() {
    matches.push(this.id);
});
$('#vouchervalue').val(matches);

  
 });

    $("#resendbtn").click(function(){
       // alert('sdfsdf');
       $checkbox = $(".chkbox").is(":checked");
       if($checkbox == true) {
         var checked_boxes = $('input:checkbox:checked').length
       
        var total=3.85*checked_boxes;
        //$('#amountvalue').val(total);
        
        
       
        return true;
    
} else {
        $('.resenderrors').html('Please select at least one checkbox');
    
       return false;
}
       




    });
});
</script>