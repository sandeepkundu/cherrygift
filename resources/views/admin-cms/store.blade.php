<!DOCTYPE html>
@extends('cms-template.default')
@section('title','Company Store Report')
@section('content')


          
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            {{$company_name}} Store List</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        
                        <li class="active">Store List</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
				<div class="col-lg-12">
                           		<div class="panel panel-yellow">
                            		<div class="panel-heading">Store Report</div>
                            		<div class="panel-body">
                                            @if (Session::get('message'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('message');
                                                echo $error;
                                                Session::forget('message');
                                                ?>
                                            </div>
                                            @endif

                                            @if (Session::get('success-msg'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('success-msg');
                                                echo $error;
                                                Session::forget('success-msg');
                                                ?>
                                            </div>
                                            @endif
                                    	<table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                
                <th class="no-sort">Site/Store Name</th>                
                <th class="no-sort">Email Id</th>
                <th class="no-sort">Address</th>
                <th class="no-sort">Contact No</th>
                <th class="no-sort">Site/Store Pin</th>
                <th class="no-sort">Merchant PIN</th>
                <th class="no-sort">Invoice</th>
                <th class="no-sort">Status</th>
                <th class="no-sort">Action</th>
            </tr>
        </thead>
 
        <tbody>
            
        </tbody>
         <div class="loader-ctn" id="loader">
        <div class="loader">Loading...</div>
    </div>
    </table>
             
             
             
             
                         			</div>
                        		</div>
                   		</div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                


@stop

@section('scripts')

<script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>    
<script type="text/javascript">
$(document).ready(function() {
  if ($('#customerlist').length) {
    $('#customerlist').DataTable({
            processing: true,
            deferRender: true,
            serverSide: true,
            responsive: true,
            //bSort: false,
            order: [[ 0, "asc" ]],
            autoWidth: true,
            bLengthChange: false,
            pageLength: 10,
            scroller: true,
            //columnDefs: [{ "orderable": false, "targets": 1 }],
            columns: [
                {data: 'store_location', name: 'store_location'},
                {data: 'email', name: 'email'},
                {data: 'address', name: 'address'},
                 {data: 'phone', name: 'phone'},
                {data: 'store_pin', name: 'store_pin'},
                {data: 'merchant_pin', name: 'merchant_pin'},
                {data: 'is_payment', name: 'is_payment'},
                {data: 'is_approved', name: 'is_approved'},
                 {data: 'id', name: 'id'}
                
            ],
            ajax: SITE_URL + '/super-admin/storedata/'+{{$id}},
            // select: true,
//                'aoColumnDefs': [{
//            'bSortable': false,
//            'aTargets': [-1] /* 1st one, start by the right */
            //             }],
            //"aaSorting": [[ 1, "asc" ]] // Sort by first column descending


        });
    }    
    
} );

</script>
@stop