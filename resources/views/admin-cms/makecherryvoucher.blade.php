@extends('cms-template.default')
@section('title','Company Report')
@section('content')

<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Make cherrygift voucher</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

            <li class="active">Make cherrygift voucher</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class=" mbl">
            <div class="row">
                <div class="col-lg-8 col-md-offset-2">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            Update cherrygift voucher</div>
                        <div class="panel-body pan">
                            @if (Session::get('message'))
                            <div class="alert alert-success">
                                <?php
                                $error = Session::get('message');
                                echo $error;
                                Session::forget('message');
                                ?>
                            </div>
                            @endif

                            @if (Session::get('success-msg'))
                            <div class="alert alert-success">
                                <?php
                                $error = Session::get('success-msg');
                                echo $error;
                                Session::forget('success-msg');
                                ?>
                            </div>
                            @endif
                            <form action="{{url()}}/super-admin/cherrygift-voucher-update" method="post" id="frm_subscription">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <div class="form-body pal">
                                    <div class="form-group"></div> 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <b>Merchant PIN</b></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="number" id="cherrygift_voucher" placeholder="" name="cherrygift_voucher" value="" class="form-control"  minlength="8" maxlength="8" required/></div>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-actions text-right pal">
                                    <button type="submit" class="btn btn-grn" id="update_cherrygift_voucher">
                                        Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!--END CONTENT-->

</div>
<!--END PAGE WRAPPER-->



@stop

