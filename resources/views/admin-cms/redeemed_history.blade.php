<!DOCTYPE html>
@extends('cms-template.default')
@section('title','Company Store Report')
@section('content')



<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Redeemed History</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

            <li class="active">Store List</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div id="tab-general">
            <div class="row mbl">
                <div class="col-lg-12">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">Store Report</div>
                        <div class="panel-body">
                            @if (Session::get('message'))
                            <div class="alert alert-success">
                                <?php
                                $error = Session::get('message');
                                echo $error;
                                Session::forget('message');
                                ?>
                            </div>
                            @endif

                            @if (Session::get('success-msg'))
                            <div class="alert alert-success">
                                <?php
                                $error = Session::get('success-msg');
                                echo $error;
                                Session::forget('success-msg');
                                ?>
                            </div>
                            @endif
                            <table border="0" cellpadding="5" cellspacing="5">
    <tbody>
        
</tbody></table><a href="{{url()}}/super-admin/getRedeemedDataInCSV"> Download Redeemed List </a>
                            <table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort">Purchaser Name</th>                
                                        <th class="no-sort">Amount</th>
                                        <th class="no-sort">Mobile Number</th>
                                        <th class="no-sort">Redeemed To</th>
                                        <th class="no-sort">Date</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END CONTENT-->



    @stop

    @section('scripts')

    <script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>
<!--    <script type="text/javascript" src="{{URL::asset('cms/script/buttons.flash.min.js')}}"></script>-->
    <script type="text/javascript" src="{{URL::asset('cms/script/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('cms/script/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('cms/script/vfs_fonts.js')}}"></script>
  <!--  <script type="text/javascript" src="{{URL::asset('cms/script/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('cms/script/buttons.print.min.js')}}"></script>-->
    <script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.columnFilter.js')}}"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        if ($('#customerlist').length) {
            
            $('#customerlist').DataTable({
                processing: true,
                deferRender: true,
                serverSide: true,
                responsive: true,
                //bSort: false,
                order: [[0, "asc"]],
                autoWidth: true,
                bLengthChange: false,
                pageLength: 10,
                scroller: true,
                //columnDefs: [{ "orderable": false, "targets": 1 }],
                columns: [
                    {data: 'first_name', name: 'users.first_name'},
                    {data: 'voucher_price', name: 'order_voucher.voucher_price'},
                    {data: 'phone_number', name: 'order_voucher.phone_number'},
                    {data: 'store_location', name: 'company_business_profile.store_location'},
                    {data: 'updated_at', name: 'order_voucher.updated_at'}


                ],
                ajax: SITE_URL + '/super-admin/redeemeddata',
                // select: true,
    //                'aoColumnDefs': [{
    //            'bSortable': false,
    //            'aTargets': [-1] /* 1st one, start by the right */
                //             }],
                //"aaSorting": [[ 1, "asc" ]] // Sort by first column descending


            });
        }

    });

    </script>
    
    @stop