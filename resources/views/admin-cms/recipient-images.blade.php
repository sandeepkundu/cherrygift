<!DOCTYPE html>
@extends('cms-template.default')
@section('title','Homepage Slider Images')
@section('content')
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>
<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Homepage Recipient Images </div>
        </div>
        
        <ol class="breadcrumb page-breadcrumb pull-right">
            
            <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">Homepage Recipient Images</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div id="tab-general">
            <div class="row mbl">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
<!--                                <strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if(Session::has('message'))
                <div class="alert alert-success">
                    <h3 style='text-align: center'>{{ Session::get('message') }}</h3>
                </div>
                @endif
                <div class="alert alert-danger" id="div_error" style="display: none" >
                    <ul id="h3_error"></ul>
                    
                </div>
                <div  style=" margin: 0 auto 20px;
                      width: 80%;"><span>*For better UI, please add images with similar dimensions<br />
                    
                    </span></div>
                <div id="image_cherrygift">
                <form action="{{url()}}/super-admin/upload-recipient-images" method="post" id="slider-image-form" name="slider-image-form"  enctype="multipart/form-data" >
                    <input type="hidden" name="img_type" value="1"  />
                    <div class="col-sm-6" style="margin-top: 50px;">
                     Image cherrygift voucher :   
                    <div class="input-group">
               
                <input type="text" class="form-control" readonly>
                 <span class="input-group-btn">
                    <span class="btn btn-col btn-primary btn-file">
                        Browse <input type="file" multiple name="slider-image" id="fileUpload" >
                    </span>
                </span>
            </div>
                    </div>
<!--                    <input type="file" name="slider-image" id="fileUpload" />-->
<!--                    <input type="submit" name="upload" value="Upload" onclick="return Upload()"/>-->
                    <div class="col-sm-2" style="margin-top: 70px;"><button class="btn btn-grn" name="upload" type="submit" onclick="return Upload()" >Upload</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="col-sm-4">
                         <div class="img-uploder">
                    <ul class="image-uplodeicon">
                        @foreach($images as $img)
                        @if($img->type == 1)
                        <li><img src="{{$s3_url}}{{$s3_env}}/{{$img->image}}"  width="100%" /> </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
                        
                    </div>
                </form>
                
               
            </div> 
                
            
                <div id="image_cherrygift">
                <form action="{{url()}}/super-admin/upload-recipient-images" method="post" id="slider-image-form" name="slider-image-form"  enctype="multipart/form-data" >
                    <input type="hidden" name="img_type" value="2"  />
                    <div class="col-sm-6" style="margin-top: 50px;">
                     Image gift for her :   
                    <div class="input-group">
               
                <input type="text" class="form-control" readonly>
                 <span class="input-group-btn">
                    <span class="btn btn-col btn-primary btn-file">
                        Browse <input type="file" multiple name="slider-image" id="fileUpload" >
                    </span>
                </span>
            </div>
                    </div>
<!--                    <input type="file" name="slider-image" id="fileUpload" />-->
<!--                    <input type="submit" name="upload" value="Upload" onclick="return Upload()"/>-->
                    <div class="col-sm-2" style="margin-top: 70px;"><button class="btn btn-grn" name="upload" type="submit" onclick="return Upload()" >Upload</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="col-sm-4">
                         <div class="img-uploder">
                    <ul class="image-uplodeicon">
                        @foreach($images as $img)
                        @if($img->type == 2)
                        <li><img src="{{$s3_url}}{{$s3_env}}/{{$img->image}}"  width="100%" /> </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
                        
                    </div>
                </form>
                
               
            </div> 
                
            
                <div id="image_cherrygift">
                <form action="{{url()}}/super-admin/upload-recipient-images" method="post" id="slider-image-form" name="slider-image-form"  enctype="multipart/form-data" >
                    <input type="hidden" name="img_type" value="3"  />
                    <div class="col-sm-6" style="margin-top: 50px;">
                     Image gift for him:   
                    <div class="input-group">
               
                <input type="text" class="form-control" readonly>
                 <span class="input-group-btn">
                    <span class="btn btn-col btn-primary btn-file">
                        Browse <input type="file" multiple name="slider-image" id="fileUpload" >
                    </span>
                </span>
            </div>
                    </div>
<!--                    <input type="file" name="slider-image" id="fileUpload" />-->
<!--                    <input type="submit" name="upload" value="Upload" onclick="return Upload()"/>-->
                    <div class="col-sm-2" style="margin-top: 70px;"><button class="btn btn-grn" name="upload" type="submit" onclick="return Upload()" >Upload</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="col-sm-4">
                         <div class="img-uploder">
                    <ul class="image-uplodeicon">
                        @foreach($images as $img)
                        @if($img->type == 3)
                        <li><img src="{{$s3_url}}{{$s3_env}}/{{$img->image}}"  width="100%" /> </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
                        
                    </div>
                </form>
                
               
            </div> 
                
                <div id="image_cherrygift">
                <form action="{{url()}}/super-admin/upload-recipient-images" method="post" id="slider-image-form" name="slider-image-form"  enctype="multipart/form-data" >
                    <input type="hidden" name="img_type" value="4"  />
                    <div class="col-sm-6" style="margin-top: 50px;">
                       Image gift for them:   
                    <div class="input-group">
               
                <input type="text" class="form-control" readonly>
                 <span class="input-group-btn">
                    <span class="btn btn-col btn-primary btn-file">
                        Browse <input type="file" multiple name="slider-image" id="fileUpload" >
                    </span>
                </span>
            </div>
                    </div>
<!--                    <input type="file" name="slider-image" id="fileUpload" />-->
<!--                    <input type="submit" name="upload" value="Upload" onclick="return Upload()"/>-->
                    <div class="col-sm-2" style="margin-top: 70px;"><button class="btn btn-grn" name="upload" type="submit" onclick="return Upload()" >Upload</button>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="col-sm-4">
                         <div class="img-uploder">
                    <ul class="image-uplodeicon">
                        @foreach($images as $img)
                        @if($img->type == 4)
                        <li><img src="{{$s3_url}}{{$s3_env}}/{{$img->image}}"  width="100%" /> </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
                        
                    </div>
                </form>
                
               
            </div>     
                <script type="text/javascript">
                    
                    function Upload() {
//                        var error_on = 0;
//                        //Get reference of FileUpload.
//                        var fileUpload = document.getElementById("fileUpload");
//                        $("#h3_error").html('');
//                        $("#div_error").hide();
//                        //Check whether the file is valid Image.
//                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
//                        if (regex.test(fileUpload.value.toLowerCase())) { 
//
//                            //Check whether HTML5 is supported.
//                            if (typeof (fileUpload.files) != "undefined") { 
//                                //Initiate the FileReader object.
//                                var reader = new FileReader();
//                                //Read the contents of Image File.
//                                reader.readAsDataURL(fileUpload.files[0]);
//                                reader.onload = function (e) {
//                                    //Initiate the JavaScript Image object.
//                                    var image = new Image();
//
//                                    //Set the Base64 string return from FileReader as source.
//                                    image.src = e.target.result;
//
//                                    //Validate the File Height and Width.
//                                    image.onload = function () {
//                                        var height = this.height;
//                                        var width = this.width;
//                                         //console.log(height+"//"+width);
//                                        if (height <= 500 || width <= 1200) {
//                                            error_msg = "Minimum Height 500px and Width must be 1200px.";
//                                            show_error(error_msg,false);
//                                            error_on = 1;
//                                            return false;
//                                        } else {
//                                        $("#slider-image-form").submit();
//                                        return true;
//                                    }
//                                    };
//
//                                }
//                            } else {
//                                error_msg ="This browser does not support HTML5.";
//                                error_on = 1;
//                                show_error(error_msg,false);
//                                return false;
//                            }
//                        } else {
//                            error_msg = "Please select a valid Image file.";
//                            show_error(error_msg,false);
//                            return false;
//                        }
//                        console.log(error_on);
//                        return false;
                    }
                    
                    function show_error(error_msg,error){
                        console.log(error_msg);
                        if(!error)
                        {   
                            $("#div_error").show();
                            $("#h3_error").html("<li>"+error_msg+"</li>");
                            
                        } else {
                            $("#div_error").hide();
                            $("#h3_error").html('');
                        }
                    }
</script>              
            </div>
        </div>
    </div>
    <!--END CONTENT-->



    @stop

