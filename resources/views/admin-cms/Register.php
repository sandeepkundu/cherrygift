<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <?php include("header_include.php"); ?>
</head>
<body class="log_screen">
<div class="login-wrapper">
	<div class="login_outer">
    	<div class="login_form">
    	 	<a id="logo"  class="login-logo"><img src="images/login_logo.png" class="img-responsive" alt="" /></a>
       		<form action="#" class="form-horizontal">
               	<div class="form-group">
                    <div class="input-group margin-bottom-sm">
  						<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
  						<input class="form-control" type="text" placeholder="Username">
					</div>
				</div>
                <div class="form-group">
                    <div class="input-group">
  						<span class="input-group-addon"><i class="fa fa fa-envelope fa-fw"></i></span>
  						<input class="form-control" type="text" placeholder="Email">
					</div>
                </div>
				<div class="form-group">
                    <div class="input-group">
  						<span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
  						<input class="form-control" type="password" placeholder="Password">
					</div>
                </div>
                <div class="form-group">
                    <div class="input-group">
  						<span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
  						<input class="form-control" type="password" placeholder="Confirm Password">
					</div>
                </div>
                <div class="form-group">
                    <a href="index.php" value="Sign In" class="btn btn-block btn-lg  btn-black">Register
                    	<span><i class="fa fa-chevron-right"></i></span>
                   	</a>
              	</div>
          	</form>
            <p class="text-center">
                Already have an account? <a href="Login.php" style="color:#428bca; font-weight:bold"> Login</a>
            </p>
   		</div>
    </div>
</div>
</body>
</html>
