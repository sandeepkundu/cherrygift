@extends('cms-template.default')
@section('title','cherrygift admin login')
@include('admin-template.header_include')

@section('bodyclass','class="bg-grey-login login-page"')
@section('content')
@include('template/forgot')
<style>
.home-btn a:hover, a:focus {
        color: #8dc53e !important;
     text-decoration: underline !important;
    }
</style>
    <div class="auth-wrapper">
	<header class="text-center logo-ctn">
    	<img src="{{ asset('images/logo.png') }}" alt="">
    </header>
    @if (Session::has('errors'))
    <div class="alert alert-danger">
       <ul>
                                    @foreach ($errors->all() as $error)
                                    <li style="align: center;">{{ $error }}</li>
                                    @endforeach
                                </ul>
    </div>
@endif

    @if(Session::has('message'))
<div class="alert alert-success">
       {{Session::get('message')}}
    </div>
    @endif
    <div class="auth-tabs-ctn">
        
    	
        <!-- Tab panes -->
        <div class="tab-content">
          	<div role="tabpanel" class="tab-pane active" id="admin-login">
                    <form action="{{url()}}/users/login" method="post" id="cms-admin-login">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                	<div class="form-group">
                    	<div class="input-group">
                        	<span class="input-group-addon"><i class="ic-16 ic-user"></i></span>
                                <input type="email" class="form-control" placeholder="Email" name="email" id="username" required="required">
                        </div><label for="username" generated="true" class="errors" style="display: none;"></label>
             		</div>
                	<div class="form-group">
                       	<div class="input-group">
                        	<span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                                <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="required">
                      	</div>  <label for="password" generated="true" class="errors" style="display: none;"></label>
                    </div>
                 	<div class="form-group auth-link-ctn">
                    	<a href="javascript:void(0);" class="auth-link" data-toggle="modal" data-target="#forgotModal" data-backdrop="static" data-keyboard="false" id="forgat_password">Forgot password?</a>
                  	</div>	
         			<button class="btn btn-block btn-success btn-lg" id="cms-loginbtn">LOGIN</button>
                </form>
          	</div>
          	
      	</div>

	</div>
    <div class="clearfix home-btn"><a href="{{url()}}"><i class="fa fa-home"></i>Home</a></div>
</div>
@stop
