Hi {{$first_name}}<br><br>

Thank you for registering your store/site  <b>{{$store_location}}</b> with cherrygift.<br><br>

We are very happy to advise that your profile has been approved.<br><br>

Your business page is now live so feel free to login to <a href="http://www.cherrygift.com">cherrygift.com</a> and check it out!<br><br>

Your <b>MERCHANT PIN</b> is – <b>{{$merchant_pin}}</b> <br><br>

Please keep this number accessible to all ‘front end staff’ as this needs to be entered into your customer’s smartphone to redeem both <b>{{$store_location}}</b> and <b>cherrygift</b> vouchers.<br><br>

If you have any queries regarding your subscription, check out our FAQ’s and if you don’t find your answer there, please contact our team on 1300 122 321 or email {{env('contact_email')}} <br><br>

We look forward to working with you and helping your business GROW one cherrygift at a time!<br><br>

Cheers,<br><br>

From the cherrygift team!<br><br>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">