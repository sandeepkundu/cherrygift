<!DOCTYPE html>
@extends('cms-template.default')
@section('title','Enquiries')
@section('content')



<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
<!--BEGIN TITLE & BREADCRUMB PAGE-->
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">
            Enquiry </div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Enquiries</li>
    </ol>
    <div class="clearfix">
    </div>
</div>
<!--END TITLE & BREADCRUMB PAGE-->
<!--BEGIN CONTENT-->
<div class="page-content">
    <div id="tab-general">
        <div class="row mbl">
            <div class="col-lg-12">
                <div class="panel panel-yellow">
                    <div class="panel-heading">Enquiry Report</div>
                    <div class="panel-body">
                        @if (Session::get('message'))
                        <div class="alert alert-success">
                            <?php
                            $error = Session::get('message');
                            echo $error;
                            Session::forget('message');
                            ?>
                        </div>
                        @endif

                        @if (Session::get('success-msg'))
                        <div class="alert alert-success">
                            <?php
                            $error = Session::get('success-msg');
                            echo $error;
                            Session::forget('success-msg');
                            ?>
                        </div>
                        @endif
                        <table id="enquirylist" class="display nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="no-sort">Enquiry #</th>
                                    <th class="no-sort">Date</th>
                                    <th class="no-sort">Name</th>
                                    <th class="no-sort">Phone</th>
                                    <th class="no-sort">Email</th>
                                    <th class="no-sort">Action</th>
                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>




                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END CONTENT-->

<div class="modal fade model-lg resetform"  id="view_enquiry_modal" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content"  >
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="close-error">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"  id="myModalLabel">Enquiry Information</h4>
            </div>
            <div class="details">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@stop

@section('scripts')

<script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>    
<script type="text/javascript">
    $(document).ready(function() {
        if ($('#enquirylist').length) {
            $('#enquirylist').DataTable({
                processing: true,
                deferRender: true,
                serverSide: true,
                responsive: true,
                bSort: false,
                autoWidth: true,
                bLengthChange: false,
                pageLength: 10,
                scroller: true,
                columns: [
                    {data: 'no', name: 'no'},
                    {data: 'date', name: 'date'},
                    {data: 'name', name: 'name'},
                    {data: 'contact_number', name: 'contact_number'}, 
                    {data: 'email', name: 'email'},
                    {data: 'id', name: 'id'},

                ],
                ajax: SITE_URL + '/super-admin/enquirydata',
            }).on(
                'click','a[data-id]',function(event) {
                    event.preventDefault();
                    $.get('enquirydata/'+$(this).data('id'),function(data) {
                        var $m=$('#view_enquiry_modal');
                        $m.find('.details').html(data);
                        $m.modal({
                            backdrop: 'static',
                            keyboard: true
                        });    
                    });
                }
            );
        }    

    } );

</script>
@stop