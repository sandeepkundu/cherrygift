<?php
use App\Model\CompanyBusinessImages;
?> 
@extends('cms-template.default')
@section('content')
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>


<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <div class="page-content">
        <!--BEGIN TITLE & BREADCRUMB PAGE-->
        <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
            <div class="page-header pull-left">
                <div class="page-title">
                    Site/Store Details</div>
            </div>
            <ol class="breadcrumb page-breadcrumb pull-right">
                <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

                <li class="active">Site/Store Details</li>
            </ol>
            <div class="clearfix">
            </div>
        </div>
        <!--END TITLE & BREADCRUMB PAGE-->
        <!--BEGIN CONTENT-->
        @if (Session::has('errors'))
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li style="align: center;">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if (Session::has('message'))
        <div class="alert alert-success">
            <p style="align-content: center;">{{ Session::get('message') }}</p>
        </div>
        @endif
        @section('bodyclass','class=""')

        <div class="container-fluid">
            <div class="row">
                <div class=" col-sm-12">
                    <div class="page-wrapper" id="business-detail">


                        <form class="form-horizontal" method="post" action="{{url()}}/super-admin/update-business-details/{{@$companybusinessprofile->id}}" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group ">
                            <label class="col-sm-offset-4 col-sm-8 control-label" style="align:center;"><span align="center" style="color: red" id="cls_error"></span></label>

                        </div>
                        <div class="form-pd-wrapper">
                            <div class="form-group mr-b30">
                                <div class="col-sm-6 col-sm-offset-4">
                                    <div class="upload-logo-ctn">
                                        <img id="img-logo-id" @if(empty($companybusinessprofile->logo)) src="{{ asset('images/admin-images/demo-avatar.jpg')}}" @else src="{{$s3_url}}{{$s3_env}}{{$companybusinessprofile->logo}}" @endif  class="img-rounded" alt="" width="90" height="90">
                                        <span class="btn btn-file btn-link"> 
                                            <input type="file" class="business-logo-upload" name="logo" id="business-logo" @if(empty($companybusinessprofile->logo)) value="" @else value="{{ asset($companybusinessprofile->logo)}}" @endif > Change Logo
                                            <input type="hidden" name="category_id" value="{{$company->category_id}}">
                                            <input type="hidden" name="company_id" id="company_id" value="{{$company->id}}">
                                            <input type="hidden" name="company_pin" id="company_pin" value="{{$company->pin}}">
                                          
                                            <em><span style="color:red;">*</span></em></span>
                                    </div>
                                          @if(isset($changArr['logo']) && $changArr['logo'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['logo'] }}</p>
                                     @endif
                                    <p class="logo-info">Please provide as JPEG or PNG. The minimum dimensions 200 X 200.</p>
                               
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Site/Store Name<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    
                                    <input type="text" class="form-control" placeholder="Enter Trading Name" id="store_location" name="store_location" value="{{@$companybusinessprofile->store_location}}">
                                    @if(isset($changArr['store_location']) && $changArr['store_location'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['store_location'] }}</p>
                                     @endif

                                    <span style="color: red" id="store_location_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Brief description<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <textarea class="form-control custom_number" id="descript1" rows="6" placeholder="Brief description (300 characters allowed)" maxlength="300" name="short_desc" id="brief_description">{{ $companybusinessprofile->short_desc }}</textarea>
                                    @if(isset($changArr['short_desc']) && $changArr['short_desc'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['short_desc'] }}</p>
                                     @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Long description<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <textarea class="form-control custom_number" id="descript2" rows="6" placeholder="Long description (2000 characters allowed)" maxlength="2000" name="long_desc" id="long_desc">{{ $companybusinessprofile->long_desc }}</textarea>
                                @if(isset($changArr['long_desc']) && $changArr['long_desc'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['long_desc'] }}</p>
                                     @endif
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 control-label">Special Instructions</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" id="descript3" rows="6" placeholder="Special Instructions (2000 characters allowed)" maxlength="2000" name="special_desc" id="special_desc">{{ $companybusinessprofile->special_desc }}</textarea>
                                    @if(isset($changArr['special_desc']) && $changArr['special_desc'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['special_desc'] }}</p>
                                     @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="mr-b30 clearfix">
                                    <label class="col-sm-6 control-label">Upload company pictures - <?= CompanyBusinessImages::$IMAGE_WIDTH ?> X <?= CompanyBusinessImages::$IMAGE_HEIGHT ?></label>
                                </div>
                                <div class="mr-b20 clearfix img-error"  id="img_error" style="display: none;">

                                </div>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <div class="col-sm-6 upload-company-ctn">
                                            @if(!empty($businessimagesarr[1]))
                                            <?php
                                            $check_img=CompanyBusinessImages::validateImage("{$s3_url}{$s3_env}{$businessimagesarr[1]}");
                                            ?>
                                            <div class="business-company-upload1 load-img">
                                                <div class="company-img">
                                                    <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{$businessimagesarr[1]}}">
                                                </div>
                                            </div>
                                            <a class="close-img fa fa-times" id="comp-img-1" img-tag='first_image' style="display:block"></a>
                                            <div class="show_img" style="display: none">
                                                <span class="btn btn-file upload-company-btn" id="file1"><i class="upload-icon"></i>Upload
                                                    <input type="file" id="business-company-upload1" class="store_ajax_img_upload" loader-id='temp_loader_first_image'  number="1" name="first_image">
                                                </span>
                                            </div>
                                            @else
                                            <?php
                                            $check_img=true;
                                            ?>
                                            <div class="business-company-upload1 load-img" style="display: none;">

                                            </div>
                                            <a class="close-img fa fa-times" id="comp-img-1" img-tag='first_image' ></a>
                                            <div class="show_img" >
                                                <span class="btn btn-file upload-company-btn" id="file1"><i class="upload-icon"></i>Upload
                                                    <input type="file" id="business-company-upload1" class="store_ajax_img_upload" loader-id='temp_loader_first_image' number="1" name="first_image" >
                                                </span>
                                            </div>
                                            @endif
                                            <p class="upload-info-label">This will be your voucher image</p>
                                            <p class="img-error"><?= $check_img===true ? '&nbsp;' : $check_img ?></p>
                                        </div>
                                        <div class="col-sm-6 upload-company-ctn">
                                            @if(!empty($businessimagesarr[2]))
                                            <?php
                                            $check_img=CompanyBusinessImages::validateImage("{$s3_url}{$s3_env}{$businessimagesarr[2]}");
                                            ?>
                                            <div class="business-company-upload2 load-img">
                                                <div class="company-img">
                                                    <!--                                                    <div id="temp_loader_second_image" class="img-loader-ctn">
                                                    <i class="fa fa-spinner img-spinner fa-2x fa-spain"></i>
                                                    </div>-->
                                                    <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{$businessimagesarr[2]}}">
                                                </div></div>
                                            <a class="close-img fa fa-times" id="comp-img-2" img-tag='second_image' style="display:block" custom_val="{{$companybusinessprofile->id}}"></a>
                                            <div class="show_img" style="display: none">
                                                <span class="btn btn-file upload-company-btn" id="file2"><i class="upload-icon"></i>Upload
                                                    <input type="file" id="business-company-upload2" class="store_ajax_img_upload" loader-id='temp_loader_second_image' number="2" name="second_image">
                                                </span>
                                            </div>
                                            @else
                                            <?php
                                            $check_img=true;
                                            ?>
                                            <div class="business-company-upload2 load-img"> </div>
                                            <a class="close-img fa fa-times" id="comp-img-2" img-tag='second_image' ></a>
                                            <div class="show_img" >
                                                <span class="btn btn-file upload-company-btn" id="file2"><i class="upload-icon"></i>Upload
                                                    <input type="file" id="business-company-upload2" class="store_ajax_img_upload" loader-id='temp_loader_second_image' number="2" name="second_image">
                                                </span>
                                            </div>
                                            @endif
                                            <p class="img-error"><?= $check_img===true ? '&nbsp;' : $check_img ?></p>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 upload-company-ctn">

                                            @if(!empty($businessimagesarr[3]))
                                            <?php
                                            $check_img=CompanyBusinessImages::validateImage("{$s3_url}{$s3_env}{$businessimagesarr[3]}");
                                            ?>
                                            <div class="business-company-upload3 load-img">
                                                <div class="company-img">
                                                    <!--                                                    <div id="temp_loader_third_image" class="img-loader-ctn">
                                                    <i class="fa fa-spinner img-spinner fa-2x fa-spain"></i>
                                                    </div>-->
                                                    <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{$businessimagesarr[3]}}">
                                                </div></div>
                                            <a class="close-img fa fa-times" img-tag='third_image' id="comp-img-3" style="display:block" custom_val="{{$companybusinessprofile->id}}"></a>
                                            <div class="show_img" style="display: none">
                                                <span class="btn btn-file upload-company-btn" id="file3"><i class="upload-icon"></i>Upload
                                                    <input type="file" id="business-company-upload3" class="store_ajax_img_upload" loader-id='temp_loader_third_image' number="3" name="third_image">
                                                </span>
                                            </div>
                                            @else
                                            <?php
                                            $check_img=true;
                                            ?>
                                            <div class="business-company-upload3 load-img"> </div>
                                            <a class="close-img fa fa-times" img-tag='third_image' id="comp-img-3" ></a>
                                            <div class="show_img" >
                                                <span class="btn btn-file upload-company-btn" id="file3"><i class="upload-icon"></i>Upload
                                                    <input type="file" id="business-company-upload3" loader-id='temp_loader_third_image' class="store_ajax_img_upload" number="3" name="third_image">
                                                </span>
                                            </div>
                                            @endif
                                            <p class="img-error"><?= $check_img===true ? '&nbsp;' : $check_img ?></p>

                                        </div>    
                                        <div class="col-sm-6 upload-company-ctn">

                                            @if(!empty($businessimagesarr[4]))
                                            <?php
                                            $check_img=CompanyBusinessImages::validateImage("{$s3_url}{$s3_env}{$businessimagesarr[4]}");
                                            ?>

                                            <div class="business-company-upload4 load-img">
                                                <div class="company-img">
                                                    <!--                                                    <div id="temp_loader_fourth_image" class="img-loader-ctn">
                                                    <i class="fa fa-spinner img-spinner fa-2x fa-spain"></i>
                                                    </div>-->
                                                    <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{$businessimagesarr[4]}}">
                                                </div>
                                            </div>
                                            <a class="close-img fa fa-times" id="comp-img-4" img-tag='fourth_image' style="display:block" custom_val="{{$companybusinessprofile->id}}"></a>
                                            <div class="show_img" style="display: none">
                                                <span class="btn btn-file upload-company-btn" id="file4"><i class="upload-icon"></i>Upload
                                                    <input type="file" id="business-company-upload4" loader-id='temp_loader_fourth_image' class="store_ajax_img_upload" number="4" name="fourth_image">
                                                </span>
                                            </div>
                                            @else
                                            <?php
                                            $check_img=true;
                                            ?>
                                            <div class="business-company-upload4 load-img"> </div>
                                            <a class="close-img fa fa-times" id="comp-img-4" img-tag='fourth_image' ></a>
                                            <div class="show_img" >
                                                <span class="btn btn-file upload-company-btn" id="file4"><i class="upload-icon"></i>Upload
                                                    <input type="file" id="business-company-upload4" loader-id='temp_loader_fourth_image' class="store_ajax_img_upload" number="4" name="fourth_image">
                                                </span>
                                            </div>
                                            @endif
                                            <p class="img-error"><?= $check_img===true ? '&nbsp;' : $check_img ?></p>

                                        </div>


                                                                         
                                    </div>
                                    <div class="row">



                                        <div class="col-md-12">
                                        <div class="business-company-upload4 load-img">
                                                <div class="company-img">
                                                    @if(isset($changArrImgNew) && $changArrImgNew !='')
                                                    <p style="color: red">Old was =></p>
                                             @foreach($changArrImgNew as $key=>$val)
                                          <div class="col-md-3">  <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{@$changArrImgOld[$key]}}" width="200px"></div>
                                             @endforeach
                                             @endif 
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if (@$companybusinessprofile->is_payment == "1")
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Merchant PIN</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control country-pin" placeholder="139382" readonly  name="merchant_pin" id="merchant_pin"  value="{{$companybusinessprofile->merchant_pin}}" style="background-color: lightgrey">
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Site/Store PIN</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control country-pin" placeholder="139382" readonly  name="store_pin" style="background-color: lightgrey" id="store_pin" @if(empty($companybusinessprofile->store_pin)) value="{{$storepin}}" @else value="{{$companybusinessprofile->store_pin}}" @endif>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" placeholder="email@email.com" name="email" id="email" value="{{$companybusinessprofile->email}}">
                                     @if(isset($changArr['email']) && $changArr['email'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['email'] }}</p>
                                     @endif
                                    <span style="color: red" id="email_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Business website</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" placeholder="www.abc.com" name="website" id="website" value="{{$companybusinessprofile->website}}">
                                    @if(isset($changArr['website']) && $changArr['website'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['website'] }}</p>
                                     @endif
                                    <span style="color: red" id="website_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Phone Number<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <input type="text" class="form-control" id="countrycode" name="country_code" value="+61"  readonly style="background-color: lightgrey">

                                        </div>
                                        <div class="col-xs-6">
                                            <input type="text" class="form-control custom_number" placeholder="444 768 984" name="phone_number"  id="phone_number" maxlength="18" value="{{$companybusinessprofile->phone}}">
                                            @if(isset($changArr['phone']) && $changArr['phone'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['phone'] }}</p>
                                     @endif
                                            <span style="color: red" id="contact_error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Address<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control custom_number" placeholder="Enter your address" id="address" name="address" value="{{$companybusinessprofile->address}}">
                                      @if(isset($changArr['address']) && $changArr['address'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['address'] }}</p>
                                     @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Location<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <select class="selectpicker" data-width="100%" id="state1" name="state">
                                        <option value="">Select State</option>
                                        @foreach(App\Model\States::orderBy('state_name', 'asc')->get() as $location)
                                        @if ($location->id == $companybusinessprofile->state_id) 
                                        <option value="{{$location->id}}" selected="selected">{{$location->state_name}}</option>
                                        @else
                                        <option value="{{$location->id}}">{{$location->state_name}}</option>
                                        </label>
                                        @endif

                                        @endforeach
                                    </select>
                                      @if(isset($changArr['state_id']) && $changArr['state_id'] !='')
                                    <p style="color: red">Old Value =>  
                                      <?php echo App\Model\States::getState($oldFArr['state_id']); ?>
                                      

                                    </p>
                                     @endif
                                    <span style="color: red" id="state_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Suburb<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control custom_number" placeholder="Suburb" id="city" name="city" autocomplete="off" value="{{$suburbsname }}"> 
                                     @if(isset($changArr['city_id']) && $changArr['city_id'] !='')
                                    <p style="color: red">Old Value =>  
                                      <?php echo App\Model\Suburbs::getCity($oldFArr['city_id']); ?>
                                      

                                    </p>
                                     @endif
                                    <span style="color: red" id="suburbs_error"></span>
                                    <input type="hidden" id="txtAllowSearch" name="suburb" value="{{$companybusinessprofile->city_id}}">
                               
                                </div>
                                <div id="suggesstion-box"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Postcode<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control custom_number" placeholder="Postcode" name="post_code" id="post_code" maxlength="4" value="{{$companybusinessprofile->post_code}}"><span style="color: red" id="postcode_error"></span>
                                 @if(isset($changArr['post_code']) && $changArr['post_code'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['post_code'] }}</p>
                                     @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Occasion<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <div class="row">
                                        @foreach(App\Model\Occasion::orderBy('occation_name', 'asc')->get() as $oca)
                                        <div class="col-sm-6">
                                            <div class="ck-button">
                                                @if((in_array($oca->id,$occarr)))
                                              
                                                <label>
                                                    <input type="checkbox" value="{{$oca->id}}" name="occation_name[]" checked="checked"><span class="ck-btn">{{$oca->occation_name}} </span>
                                                </label>
                                                @else
                                                <label>
                                                    <input type="checkbox" value="{{$oca->id}}" name="occation_name[]"><span class="ck-btn">{{$oca->occation_name}} </span>
                                                </label> @endif
                                            </div>
                                        </div>

                                        @endforeach
                                    </div>
                                    
                                     @foreach(App\Model\Occasion::orderBy('occation_name', 'asc')->get() as $oca)
                                      @if((in_array($oca->id,$posArr)))
                                    
                                   
                                     <p style="color: red" >Old Value =>  {{$oca->occation_name}}</p>
                                   
                                     @endif  
                                      @endforeach
                                 


                                    <span style="color: red" id="occation_error"></span>
                                     </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Recipient<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <div class="row">
                                        @foreach(App\Model\Recipient::where('is_top', '=', '1')->get() as $rec)
                                        <div class="col-sm-6">
                                            <div class="ck-button">

                                                @if (in_array($rec->id,$recarr)) 
                                                <label>
                                                    <input type="checkbox" value="{{$rec->id}}" name="recpient_name[]" checked="checked"><span class="ck-btn">{{$rec->name}} </span>
                                                </label> 
                                                @else
                                                <label>
                                                    <input type="checkbox" value="{{$rec->id}}" name="recpient_name[]"><span class="ck-btn">{{$rec->name}} </span>
                                                </label> 
                                                @endif



                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                       @foreach(App\Model\Recipient::where('is_top', '=', '1')->get() as $rec)
                                      @if((in_array($rec->id,$resArr)))
                                    
                                   
                                     <p style="color: red" >Old Value =>  {{$rec->name}}</p>
                                   
                                     @endif  
                                      @endforeach
                                    <span style="color: red" id="receipient_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Subscription</label>
                                <div class="col-sm-6">
                                    <div class="pos-rel">
                                        <input type="text" class="form-control" name="subscription_amount" value="${{$subs_price}} inc GST" readonly style="background-color: lightgrey">


                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Set minimum limit<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <input type="number" min="1" class="form-control custom_number" name="minimum_amount" id="minimum_amount" placeholder="$5" @if(!empty($companybusinessprofile->min_vouchar_amount)) value="{{$companybusinessprofile->min_vouchar_amount}}" @endif><span style="color: red" id="minimum_amount_error"></span>
                                     @if(isset($changArr['min_vouchar_amount']) && $changArr['min_vouchar_amount'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['min_vouchar_amount'] }}</p>
                                     @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Set maximum limit<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6">
                                    <input type="number" min="1" class="form-control custom_number" name="maximum_amount" id="maximum_amount" placeholder="$500" @if(!empty($companybusinessprofile->max_vouchar_amount)) value="{{$companybusinessprofile->max_vouchar_amount}}" @endif ><span style="color: red" id="maximum_amount_error"></span>
                                    @if(isset($changArr['max_vouchar_amount']) && $changArr['max_vouchar_amount'] !='')
                                    <p style="color: red">Old Value =>  {{ @$oldFArr['max_vouchar_amount'] }}</p>
                                     @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Increment amount</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="incremental_amount" id="incremental_amount" value="$10" readonly="readonly" style="background-color: lightgrey"><span style="color: red" id="incremental_amount_error"></span>
                                </div>
                            </div>
                            <div class="form-group mr-b30">
                                <label class="col-sm-4 control-label">Opening hours<em><span style="color:red;">*</span></em></label>
                                <div class="col-sm-6 opening-hours-ctn">
                                    <div class="row">
                                        <div class="col-sm-6">

                                            @for($j=1; $j<=7; $j++)

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">{{Config::get('constants.days.'.$j)}}</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control custom_number" placeholder="08:00 - 18:00" name="days[]" @if(isset($daystimings) && !empty($daystimings)) value="{{$daystimings[$j]}}" @endif>
                                                     <p style="color: red">{{ @ $apArr[$j] }}</p>
                                                </div>
                                            </div>

                                            @if($j==4)                                     
                                        </div>
                                        <div class="col-sm-6">
                                            @endif

                                           
                                                       

                                            @endfor
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class ='form-group text-center'>
                            <input type="submit"  class="btn btn-grn" value="Save" id="editUpdatebtn" style="width: 85px;">
                            @if ($companybusinessprofile->is_approved == 0)
                            <input type="button"  class="btn btn-grn " value="Approve"  onclick="activate_store('{{$companybusinessprofile->id}}','4');" id='approveBtn' style="width: 100px;"> 

                            <input type="button"  class="btn btn-grn " value="Reject"  onclick="$('#view_reject_model').modal('show');"  style="width: 100px;"> 
                            @endif
                            <button type="button" class="btn btn-grn close-error" class="close" style="width: 85px;" onclick="javascript:history.back('0')">Go Back</button>
                        </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div><div class="loader-ctn" id="loader">
        <div class="loader">Loading...</div>
    </div>
</div>          <!--END CONTENT-->


<div class="modal fade model-lg resetform"  id="view_reject_model" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"  aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content"  >
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="close-error">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"  id="myModalLabel">Reject Reason</h4>
            </div>
            <form role="form" id="edit_company" method="POST" enctype="multipart/form-data" action="{{url()}}/super-admin/reject-store"><input type="hidden" name="_token" value="{{ csrf_token() }}">
                <br/>
                <div style="width:66%; margin: auto" class="edit-form">
                    <div class= 'error alert alert-danger' style="display:none;"></div>

                    <div class="error_message" style="color: red"></div>

                    <textarea name="reason" id="reason" rows="8"  placeholder="Enter reason to reject"></textarea>

                    <input type="hidden" name="id" id="store_id"  value="{{$companybusinessprofile->id}}"/>
                    <input type="hidden" name="company_id"  value="{{$companybusinessprofile->company_id}}"/>
                    <br>
                    <div class ='form-group text-center'>
                        <input type="submit"  data-loading-text="Loading..." class="btn btn-grn" value="Send" id="send-reject"  >

                        <button type="button" class="btn btn-grn close-error" data-dismiss="modal" class="close" >Close</button>
                    </div>


                </div>


            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop