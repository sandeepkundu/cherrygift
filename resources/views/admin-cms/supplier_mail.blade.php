Hi {{$first_name}},<br><br>

Congratulations on redeeming your cherrygift vouchers.<br><br>

Attached is your remittance advice for {{$updated_at}}.<br><br>

Payments will be processed on the 15th of this month.<br><br>

If you have any queries regarding your payments, please contact our accounts team on 1300 122 321 or email accounts@cherrygift.com<br><br>

We look forward to continue working with you and helping your business GROW one cherrygift at a time!<br><br>

Cheers,<br>
From the cherrygift team!<br><br>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">


   