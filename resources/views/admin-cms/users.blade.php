<!DOCTYPE html>
@extends('cms-template.default')
@section('title','User / Vendor List')
@section('content')


          
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            User / Vendor List </div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Users</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
				<div class="col-lg-12">
                           		<div class="panel panel-yellow">
                            		<div class="panel-heading">Users Report</div>
                            		<div class="panel-body">
                                            <select onchange="change_list(this.value)" id="list_type">
                                                <option  <?php (Request::is('super-admin/users'))?"selected='selected'":'';?> value="1">Users</option>
                                                <option <?php (Request::is('super-admin/vendors'))?"selected='selected'":'';?> value="2">Vendors</option>
                                            </select>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="{{url()}}/super-admin/users-csv-download" style="color: #ec1d23;"> Download User List </a>
                                    	

                                             @if (Session::get('success-msg'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('success-msg');
                                                echo $error;
                                                Session::forget('success-msg');
                                                ?>
                                            </div>
                                            @endif


                                              @if (Session::get('success-msg'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('success-msg');
                                                echo $error;
                                                Session::forget('success-msg');
                                                ?>
                                            </div>
                                            @endif
                                            

                                        <table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="no-sort">First Name</th>
                                                <th class="no-sort">Last Name</th>
                                                <th class="no-sort">Email</th>
                                                <th class="no-sort">Status</th>
                                                <th class="no-sort">Action</th>
                                                </tr>
                                        </thead>
                                 
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
             
             
             
             
                         			</div>
                        		</div>
                   		</div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                
  <!--Start Model -->
    <div class="modal fade model-lg resetform"  id="view_user_model" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content"  >
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="close-error">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"  id="myModalLabel">User Information</h4>
            </div>
            <form role="form" id="edit_company" method="POST" enctype="multipart/form-data" action="save-user">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <br/>
                <div style="width:66%; margin: auto" class="edit-form">
                    <div class= 'error alert alert-danger' style="display:none;"></div>
                    <div class='alert-success bg-success' style="padding:10px;margin-bottom: 20px; display:none;">User Information updated Successfully.</div>
                    <div class="error_message"></div>
                    <span class='errors' id="cls_error"></span>
                   
                   
                    <div class="form-group">
                        <label>First Name<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" id="fname" name="first_name" required="required" >
                        <!--<span class='errors' id="fname_error"></span>-->
                    </div>
                    <div class="form-group">
                        <label>Last Name<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" id="lname" name="last_name" required="required" >
                       <!-- <span class='errors' id="lname_error"></span>-->
                    </div>

                    <div class="form-group">
                        <label>Contact Email<em><span style="color:red;">*</span></em></label>
                        <input type="text" class="form-control" id="email" name="email" required="required">
                        <!--<span class='errors' id="conemail_error"></span>-->
 
                    </div>

                    <div class="form-group">
                        <label>Contact Number<em><span style="color:red;">*</span></em></label>
                        <div class="row">
                            <div class="col-xs-3">
                                <input type="text" class="form-control" id="countrycode" name="country_code" value="+61" readonly style="background-color: lightgrey">
                            </div>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" id="connumber" name="mobile_number" required="required" maxlength="18" >
                               <!-- <span class='errors' id="contact_error"></span>-->
                            </div>         

                        </div>  
                    </div>
                  <!--    <div class="form-group">
                        <label>Password<em><span style="color:red;">*</span></em></label>
                        <input type="password" class="form-control" id="password" name="password" required="required">
                        <span class='errors' id="password_error"></span>
 
                    </div> -->
                 
                 
                    <input type="hidden" name="id" id="userid" value=""/>

                    <br>
                    <div class ='form-group text-center'>
                        <input type="submit"  data-loading-text="Loading..." class="btn btn-grn" value="Save" id="savebtn12" >
                        <!--input type="button"  data-loading-text="Loading..." class="btn btn-grn " value="Activate"  onclick="customerAction('1');" id='approveBtn'> 
                        <input type="button"  data-loading-text="Loading..." class="btn btn-grn " value="Reject"  onclick="companyReject();" id='rejectBtn'> 
                        <input type="button"  data-loading-text="Loading..." class="btn btn-grn " value="Deactivate"  onclick="customerAction('2');" id='deactivateBtn' -->
                        <button type="button" class="btn btn-grn close-error" data-dismiss="modal" class="close" >Close</button>
                    </div>


                </div>


            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



@stop

@section('scripts')

<script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>    
<script type="text/javascript">
    function change_list(val){
        if(val == 1) {
            window.location = "{{url()}}/super-admin/users";
        } else {
            window.location = "{{url()}}/super-admin/vendors";
        }
    }
$(document).ready(function() {
  if ($('#customerlist').length) {
    
   var userDataTable =  $('#customerlist').DataTable({
            processing: true,
            deferRender: true,
            serverSide: true,
            responsive: true,
             stateSave: true,
            //bSort: false,
            order: [[ 0, "asc" ]],
            autoWidth: true,
            bLengthChange: false,
            pageLength: 10,
            scroller: true,
            //columnDefs: [{ "orderable": false, "targets": 1 }],
            columns: [
                {data: 'first_name', name: 'first_name'},
                {data: 'last_name', name: 'last_name'},
                {data: 'email', name: 'email'},
                {data: 'status', name: 'status'},
              //  {data: 'Action', name: 'Action'},
                {data: 'id', name: 'id'},

                ],
    
            ajax: SITE_URL + '/super-admin/usersdata',
            // select: true,
//                'aoColumnDefs': [{
//            'bSortable': false,
//            'aTargets': [-1] /* 1st one, start by the right */
            //             }],
            //"aaSorting": [[ 1, "asc" ]] // Sort by first column descending

 "drawCallback": function( settings ) {

    $("[name='my-checkbox']").bootstrapSwitch();
    $("[name='my-checkbox']").on('switchChange.bootstrapSwitch', function(event, state) {

    var value;
   var id= this.id;
    if(state==true)
    {

     value ='Active';
    }
    if(state==false)
        {
            value = 'Deleted';
             }
    if (value!="") {
    var dataString = { id: id,value: value};
    $.ajax({
        type: "POST",
        url: SITE_URL + "/super-admin/user-action",
        data: dataString,
        dataType: "html",
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
        success: function (data)
        {
            //obj = JSON.parse(data);
            // if (obj.status == 'success')
            // {
            //     //location.reload();
            // }
        }
    });
}

});

 


  },


        });
    }    


} );


</script>
@stop