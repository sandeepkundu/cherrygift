<!DOCTYPE html>
@extends('cms-template.default')
@section('title','PymentDetails')
@section('content')
          
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            Payment Details List</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="hidden"><a href="#">Tables</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        <li class="active">Payment Details List</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
				<div class="col-lg-12">
                           		<div class="panel panel-yellow">
                            		<div class="panel-heading">Payment Details List</div>
                            		<div class="panel-body">
                                        <table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="no-sort">User Id</th>
                <th class="no-sort">Item Name</th>
                <th class="no-sort">Custom Message</th>
                <th class="no-sort">Transaction Id</th>
                <th class="no-sort">Payer Email</th>
                <th class="no-sort">Payment Status</th>
                <th class="no-sort">Payer Id</th>
                
                <th class="no-sort">Payment Date</th>
                <th class="no-sort">Paypal History Id</th>
                <th class="no-sort">Status</th>
                <th class="no-sort">Paypal Object</th>
                </tr>
        </thead>
 
        <tbody>
            
        </tbody>
    </table>
             
             
             
             
                         			</div>
                        		</div>
                   		</div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                
  



@stop

@section('scripts')

<script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>    
<script type="text/javascript">
    
$(document).ready(function() {
  if ($('#customerlist').length) {
    $('#customerlist').DataTable({
            processing: true,
            deferRender: true,
            serverSide: true,
            responsive: true,
            //bSort: false,
            order: [[ 0, "asc" ]],
            autoWidth: true,
            bLengthChange: false,
            pageLength: 10,
            scroller: true,
            //columnDefs: [{ "orderable": false, "targets": 1 }],
            columns: [
                {data: 'user_id', name: 'user_id'},
                {data: 'item_name', name: 'item_name'},
                {data: 'custom_message', name: 'custom_message'},
                {data: 'txn_id', name: 'txn_id'},
                {data: 'payer_email', name: 'payer_email'},
                {data: 'payment_status', name: 'payment_status'},
                {data: 'payer_id', name: 'payer_id'},
                
                {data: 'payment_date', name: 'payment_date'},
                {data: 'paypal_history_id', name: 'paypal_history_id'},
                {data: 'status', name: 'status'},
                {data: 'paypal_object', name: 'paypal_object'},
                ],
            ajax: SITE_URL + '/super-admin/paymentDetailsData',
            // select: true,
//                'aoColumnDefs': [{
//            'bSortable': false,
//            'aTargets': [-1] /* 1st one, start by the right */
            //             }],
            //"aaSorting": [[ 1, "asc" ]] // Sort by first column descending


        });
    }    
    
} );

</script>
@stop