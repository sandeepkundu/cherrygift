Hi {{$first_name}},<br><br>
Your login password for cherrygift.com/business has been reset.<br><br>

Your new password is - {{$password}}<br><br>

Please reset this password on your first login to maintain privacy.<br><br>

If you have any queries regarding your account, please contact our team on 1300 122 321 or email accounts@cherrygift.com<br><br>

We look forward to continue working with you and helping your business GROW one cherrygift at a time!<br><br>

Cheers,<br>
From the cherrygift team!<br><br>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">
