<!DOCTYPE html>
@extends('cms-template.default')
@section('title','Company Store Report')
@section('content')


          
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            {{$store_location}} Voucher List</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        
                        <li class="active">Voucher List</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
				<div class="col-lg-12">
                           		<div class="panel panel-yellow">
                            		<div class="panel-heading">Voucher Report</div>
                            		<div class="panel-body">
                                            @if (Session::get('message'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('message');
                                                echo $error;
                                                Session::forget('message');
                                                ?>
                                            </div>
                                            @endif

                                            @if (Session::get('success-msg'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('success-msg');
                                                echo $error;
                                                Session::forget('success-msg');
                                                ?>
                                            </div>
                                            @endif
                                    	<table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                
                <th class="no-sort">Voucher No</th>                
                <th class="no-sort">Voucher Amount</th>
                <th class="no-sort">Status</th>
                <th class="no-sort">Redeemed Store Pin</th>
                <th class="no-sort">Date Purchased</th>
                <th class="no-sort">Date Actioned</th>
                
                
                <th class="no-sort">Action</th>
            </tr>
        </thead>
 
        <tbody>
            
        </tbody>
    </table>
             
             
             
             
                         			</div>
                        		</div>
                   		</div>
                        </div>
                    </div>
                </div>
     <div class="loader-ctn" id="loader">
        <div class="loader">Loading...</div>
    </div>           
                <!--END CONTENT-->
                
    <div class="modal fade" id="redeemed_voucher_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title" id="myModalLabel" style="padding-bottom:5px ">Merchant PIN</h4>
       
        <p id="errMsges" style="color:red; text-align:center;"></p>
      </div>


        <form class="form-horizontal" role="form" method="" action="">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="voucher_id" value="" id="voucherid">
                        <input type="text" class="form-control f_24 mob-f15" name="merchant_pin" id="merchant_pin"  placeholder="Enter your 8 digit Merchant PIN" value="" maxlength="8">
                <button type="button" class="btn btn-success btn-block btn-lg" id="submit-redeemvoucher"><span></span>REDEEM</button>


        </form>

                             
    </div>
  </div>
</div>

<div class="modal fade approve-modal" id="approved_voucher_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bdr-n">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        
      </div>
      <div class="modal-body">
        
        <h3 class="text-center mr-b50"><strong id="myHeader">VOUCHER APPROVED</strong></h3>
      </div>
     <div class="modal-footer bdr-n">
        <button id="redeemconfirmed" class="btn btn-approve btn-block" type="button"><i class="fa fa-check" aria-hidden="true"></i>
</button>
      </div>
    </div>
  </div>
</div> 


@stop

@section('scripts')

<script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>    
<script type="text/javascript">
$(document).ready(function() {
  if ($('#customerlist').length) {
    $('#customerlist').DataTable({
            processing: true,
            deferRender: true,
            serverSide: true,
            responsive: true,
            //bSort: false,
            order: [[ 0, "asc" ]],
            autoWidth: true,
            bLengthChange: false,
            pageLength: 10,
            scroller: true,
            //columnDefs: [{ "orderable": false, "targets": 1 }],
            columns: [
                {data: 'voucher_unique_id', name: 'voucher_unique_id'},
                {data: 'voucher_price', name: 'voucher_price'},
                {data: 'redeemed', name: 'redeemed'},
                {data: 'redeemed_at', name: 'redeemed_at'},
                {data: 'sms_date', name: 'sms_date'},
                {data: 'voucher_actioned_date', name: 'voucher_actioned_date'},
                
                
                {data: 'id', name: 'id'}
                
            ],
            ajax: SITE_URL + '/super-admin/voucherdata/'+{{$id}},
            // select: true,
//                'aoColumnDefs': [{
//            'bSortable': false,
//            'aTargets': [-1] /* 1st one, start by the right */
            //             }],
            //"aaSorting": [[ 1, "asc" ]] // Sort by first column descending


        });
    }    
    
} );

</script>
@stop