<table class="table table-bordered table-striped">
    <tbody>
        <tr>
            <th>Enquiry</th>
            <td><?= sprintf('#%05d',$enquiry->id) ?></td>
        </tr>
        <tr>
            <th>Submitted</th>
            <td><?= $enquiry->created_at->modify('+10 hour')->format('d-M-Y g:ia') ?></td>
        </tr>
        <tr>
            <th>First name</th>
            <td>{{ $enquiry->first_name }}</td>
        </tr>
        <tr>
            <th>Last name</th>
            <td>{{ $enquiry->last_name }}</td>
        </tr>
        <tr>
            <th>Contact Number</th>
            <td>{{ $enquiry->contact_number }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ $enquiry->email }}</td>
        </tr>
        <tr>
            <th valign="top"><strong>Message</th>
            <td valign="top"><?= nl2br(htmlspecialchars($enquiry->message)) ?></td>
        </tr>
    </tbody>
</table>
