<!DOCTYPE html>
<html lang="en">
<head>
    <title>CherryGift</title>
   
   <style>

       *{margin: 0; padding: 0;}
       img{max-width: 100%;}
       body{margin:0; padding:0; font-family: arial;}
       

 table-cellble td, table th {
    display: table-cell;
    padding: 18px 8px;
    text-align: left;
    vertical-align: top;
  }  
      
    </style>
</head>
<body>
<div style="max-width: 1200px; width: 100%; margin: 0 auto; overflow: hidden;">
    
    <table width="100%" style="padding:0 40px;">
            <tr>
                <td style="width:30%; text-align:left; padding: 30px 0;">

                    <img src="{{url().'/images/invoice-logo.png'}}" alt="cherrygift logo">
                    
                </td>
                 <td style="width:30%;">
                </td>
                <td style="width:40%; text-align:left;padding: 30px 0">
                <h6>&nbsp;</h6>
                <h1>Remittance Advice</h1>
                <h3 style="text-align:left; color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;">ABN:  {{ @env(INVOICE_ABN)}}</h3>
                    

                </td>

                <!--td  style="width:20%; text-align:left; margin-right: 0px; padding: 40px 0 30px;">

                    <h1 style="color: #000; text-align:left; font-size: 30px; margin-top: 0;">Name here</h1>
                    <h3 style="color: #000; text-align:left; font-size: 18px; margin-top: 5px; font-weight: normal;">ABN: {{env('INVOICE_ABN')}}</h3>

                </td-->
            </tr>
            <tr>
                <td style="width:30%; text-align:left; ">
                    <h3 style="color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;"><b>{{@$results[0]->store_location}}</b><br>
                    {{@$results[0]->address}}<br>
                    {{App\Model\Suburbs::getCity(@$results[0]->city_id)}},
                    {{App\Model\States::getState(@$results[0]->state_id)}}
                    {{@$results[0]->post_code}}<br>
                    
                    </h3></td>
                     <td style="width:30%;">
                </td>

                <td style="width:40%; text-align:left;">
                    <h3 style=" text-align:left; color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;">Date: {{ @date('d-m-Y')}}</h3></td>
            </tr>
        </table>
    
    <table style="border-collapse: collapse; border-spacing: 0; display: table; width: 85%; margin: 60px auto 40px; float: left;">
<tbody>
<tr style="background: #8dc73f!important;">
  <th>Date Redeemed  </th>
  <th> Voucher ID</th>
  <th>Voucher Type</th>
  <th>Voucher Amount </th>
  <th><center>Commission<br>9%</th>
  <th>Payment</th>
</tr>
@foreach($results as $val)
<tr style="background: #e1e1ed;">
  
  <th> {{date('d-m-Y',strtotime($val->updated_at))}}</th>
  <th>{{$val->voucher_unique_id}}</th>
  <th>{{App\Model\CompanyBusinessProfile::getloc($val->redeemed_at)}}</th>
  <th>${{ number_format($val->amount ,2)}}</th>
  <th>${{number_format($val->amount*.09,2)}}</th>
  <th>${{number_format($val->amount -$val->amount*.09 ,2)}}</th>
  

</tr>
@endforeach
<tr style="background: #8dc73f!important;">
<td colspan="3" style="background-color:#fff;"></td>  
  <th>Total</th>
  <?php 

  $sum =0;     
  $Allsum =0; 
   foreach($results as $val):
    $sum  =  $sum + ($val->amount*.09);
    $Allsum  =  $Allsum + ($val->amount -$val->amount*.09);
  endforeach;
  ?>
  <th>${{ number_format($sum,2) }}</th>
  <th>${{ number_format($Allsum,2) }}</th>
</tr>
<tr style="background: #8dc73f!important;">
<td colspan="3" style="background-color:#fff;"></td>  
  <th  colspan="3">Payment scheduled for the 15th of this month</th>
</tr>
  </tbody>
</table>
    
   
</div>
    <div style="background: #ed1b24; width: 100%; float: left; margin-top: 150px; padding: 0 0; position: absolute; bottom: 0;">
        <div style=" width: 100%; margin: 0 auto; overflow: hidden;">
            <img src="{{url().'/images/invoice_img_footer.jpg'}}" alt="cherrygift logo">
        </div>
    </div>
 
</body>
</html>

