<!DOCTYPE html>

@extends('cms-template.default')
@section('title','cherrygift vouchers promo')
@section('content')
<style>
.clearfix a{
color:#000;
}
</style>
<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Promo Vouchers</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

            <li class="active">Promo Vouchers</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div id="tab-general">
            <div class="row mbl">
                <div class="col-lg-12">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">Promo Vouchers</div>
                        <div class="panel-body">
                            @if (Session::get('message'))
                            <div class="alert alert-success"><!--alert alert-warning-->
                                <?php
                                $error = Session::get('message');
                                echo $error;
                                Session::forget('message');
                                ?>
                            </div>
                            @endif

                            @if (Session::get('success-msg'))
                            <div class="alert alert-success">
                                <?php
                                $error = Session::get('success-msg');
                                echo $error;
                                Session::forget('success-msg');
                                ?>
                            </div>
                            @endif
                            <input type="hidden" value="{{$month}}" name="month_val" id="month_val">
                            <input type="hidden" value="{{$year}}" name="year_val" id="year_val">
                            <table border="0" cellpadding="5" cellspacing="5">
    <tbody>
        
    </tbody></table><a href="javascript:void(0)" id="Download_Redeemed_List" style="color: #ec1d23;"> Download Purchase List </a>
<div class="clearfix">
                                            
                                            Month : 
                                            <select name="month" id="month">
                                                <?php 
                                                $month = Route::getCurrentRoute()->getParameter('id');
                                                $year = Route::getCurrentRoute()->getParameter('year');
                                                $current_month = (!empty($month))?$month:date("m");
                                                for($i=1;$i<13;$i++) {?>
                                                <option <?php echo ($current_month==$i)?"selected='selected'":"";?>  value="<?php echo $i?>"><?php echo date('F', mktime(0, 0, 0, $i, 10));?></option>
                                                <?php }?>
                                            </select>

                                            Year : 
                                            <select name="year" id="year">
                                                <?php 
                                                $current_year = (!empty($year))?$year:date("Y");
                                                for($i=2016;$i<date("Y")+1;$i++) {?>
                                                <option <?php echo ($current_year==$i)?"selected='selected'":"";?>  value="<?php echo $i?>"><?php echo $i?></option>
                                                <?php }?>
                                            </select>
                                            <input type="submit" name="submit" id="promovoucher_submit" value="submit" />
                                            
                                        </div>
                            <div class="clearfix" style="float:right;margin-top:-35px;">
                            <!-- <form action="{{url()}}/post_buy_promovoucher" method="post" id="buy_promovoucher_form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            Select Store : 
                            <select id="location" name="location" >
                                <option value="">Select Store</option>
                                @foreach(App\Model\CompanyBusinessProfile::orderBy('store_location', 'asc')->get() as $location)
                                 <option value="{{$location->id}}">{{$location->store_location}}</option>                        
                                @endforeach
                            </select>
                            
                            <a><input type="submit" name="submit" id="store_submit" value="Send Store" /></a>
                            </form> -->
                         
                            <a href="{{url()}}/super-admin/buypromovoucher" ><input type="submit" name="submit" value="Buy Voucher"/></a>
                            </div>         
                             <div class="table-responsive" style="width:100%">          
                            <table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort">Purchaser Name</th>                
                                        <th class="no-sort">Amount</th>
                                        <th class="no-sort">Mobile Number</th>
                                        <th class="no-sort">Voucher Number</th>
                                        <th class="no-sort">Description</th>
                                        <th class="no-sort">Company Name</th>
                                        <th class="no-sort">Store</th>
                                        <th class="no-sort">Date</th>
                                        <th class="no-sort">Status</th>
                                        <th class="no-sort">Action</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>

                        </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END CONTENT-->
    <div class="modal fade" id="redeemed_voucher_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title" id="myModalLabel" style="padding-bottom:5px ">Merchant PIN</h4>
       
        <p id="errMsges" style="color:red; text-align:center;"></p>
      </div>


        <form class="form-horizontal" role="form" method="" action="">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="voucher_id" value="" id="voucherid">
                        <input type="text" class="form-control f_24 mob-f15" name="merchant_pin" id="merchant_pin" placeholder="Enter your 8 digit Merchant PIN" value="" minlength="8" maxlength="8">
                <button type="button" class="btn btn-success btn-block btn-lg" id="submit-redeemvoucher"><span></span>REDEEM</button>


        </form>

                             
    </div>
  </div>
</div>

<div class="modal fade approve-modal" id="approved_voucher_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bdr-n">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        
      </div>
      <div class="modal-body">
        
        <h3 class="text-center mr-b50"><strong id="myHeader">VOUCHER APPROVED</strong></h3>
      </div>
     <div class="modal-footer bdr-n">
        <button id="redeemconfirmed" class="btn btn-approve btn-block" type="button"><i class="fa fa-check" aria-hidden="true"></i>
</button>
      </div>
    </div>
  </div>
</div> 


    @stop

    @section('scripts')

    <script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        if ($('#customerlist').length) {
            
            $('#customerlist').DataTable({
                processing: true,
                deferRender: true,
                serverSide: true,
                responsive: true,
                //bSort: false,
                order: [[0, "asc"]],
                autoWidth: true,
                bLengthChange: false,
                pageLength: 10,
                //scroller: true,
              //columnDefs: [{ "orderable": false, "targets": 1 }],
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'voucher_price', name: 'order_voucher.voucher_price'},
                    {data: 'phone_number', name: 'order_voucher.phone_number'},
                    {data: 'voucher_unique_id', name: 'order_voucher.voucher_unique_id'},
                    {data: 'description', name: 'order_voucher.description'},
                    {data: 'company_name', name: 'company.company_name'},
                    {data: 'store_location', name: 'company_business_profile.store_location'}, 
                    {data: 'created_at', name: 'order_voucher.created_at'},
                    {data: 'redeemed', name: 'order_voucher.redeemed'},
                    //{data: 'voucher_status', name: 'order_voucher.voucher_status'},
                    {data: 'id', name: 'order_voucher.id'}

                ],
                ajax: SITE_URL + '/super-admin/redeemeddatabypromo/'+$('#month_val').val()+'/'+$('#year_val').val(),
                // select: true,
    //                'aoColumnDefs': [{
    //            'bSortable': false,
    //            'aTargets': [-1] /* 1st one, start by the right */
                //             }],
                //"aaSorting": [[ 1, "asc" ]] // Sort by first column descending


            });
        }

    });

    </script>
    
    @stop