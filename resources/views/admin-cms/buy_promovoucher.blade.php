<!DOCTYPE html>

@extends('cms-template.default')
@section('title','cherrygift vouchers promo')
@section('content')
<style>
.media{
margin-top: -70px;
margin-left: 15px;

}
.media-heading{
color:#fff; 
margin-left: 70px;
margin-top: -70px;
}
.small-circle{
margin-left: 10px;
margin-top: -100px;   
}
.price{
    color:red;
}
.mediadiv {
    width: 80%;
}
.count-input{
        margin-left: 13px;
    margin-top: -25px;
}
</style>
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>
<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Promo Vouchers</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

            <li class="active">Promo Vouchers</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div id="tab-general">
        <div class="row mbl">
        <div class="col-lg-12">
            <div class="row">
                            <div class="col-lg-2">
                            Select Store : 
                            </div>
                            <div class="col-lg-4">
                            <form method="post" action="">
                            <select id="location" name="location" class="form-control">
                                <option value="">Select Store</option>
                                @foreach(App\Model\CompanyBusinessProfile::orderBy('store_location', 'asc')->get() as $location)
                                 <option value="{{$location->id}}">{{$location->store_location}}</option>                        
                                @endforeach
                            </select>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           </form>
                       </div>
            </div>
            <!--h1 class="heading mr-b50" style="">Buy voucher</h1--><br>
<div class="hiddenfields" style="display:none">
    
    <div class="row">
        <div class="col-md-2">Select Price:</div>
        <form class="section-voucher" method="post" action="buypromovoucher">
            
            <div class="col-md-2">
                <span>$</span>
                <input type="text" name="quantity" id="quantity" class="count-input form-control" min="10">
                <div><span id="error_max_amount" style="color: red;"></span></div>
                <span id="errmsg"></span>
                <input type="hidden" name="max_value"  id="max_value">   
                <input type="hidden" name="min_value"  id="min_value"> 
                <input type="hidden" name="voucher_id" id="voucher_id">
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <!--button type="submit" class="btn btn-success btn-sm" id="add_to_cart" value="Submit">Continue</button-->
        </form>
    </div><!--second row end-->
    <br><br>

    <div class="row">
        <div class="col-lg-4">
            <div>
                <div style="background-color:white !important;height:190px;width:290px">
                <img src="" width="300px" alt="" class="img-responsive img-rounded bord-radius index" id="storeimage">
                </div>
                <input type="hidden" value="{{$s3_url}}{{$s3_env}}" id="forbaseurl">
                <img class="small-circle" src="" style="width: 50px; height: 50px;border-radius: 20%;" id="companylogo">
            </div>
            
            <div class="mediadiv" style="width:290px !important;">
                <h4 class="media-heading"></h4> 
            </div>
        </div>

        <div class="col-lg-2">
            <h1 class="price" style="margin-top: 78px;font-weight:600"></h1>
        </div>
    </div>
   
        
</div>
<div class="clearfix">
        </div>
<br><br><br>
    <form method="post" action="sendvoucherwithoutpayment">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                 <div class="col-xs-4">&nbsp;
                  <input type="text" class="form-control valid" id="country_code" name="country_code" value="+61" readonly="" style="background-color: lightgrey">
                </div>
                 <div class="col-xs-8"><span style="padding-left: 1px">Recipient's mobile number here</span>
                    <input type="text" maxlength="15" placeholder="Mobile number" id="mobile_number" name="phone_number" class="form-control">
                  </div>
                </div>
             </div>
        </div>
    </div>
            
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <textarea rows="5" placeholder="Your message here.From Dave x (max 1600 characters)" class="form-control" maxlength="1600" id="sms_txt" name="sms_txt"></textarea>
                    </div>
                    <div class="form-group">
                    <textarea rows="5" placeholder="Description" class="form-control" maxlength="500" id="desc_txt" name="desc_txt"></textarea>
                    </div>
                    <button class="btn btn-block btn-success mr-b20" value="send_new" name="send_now" id="send_now">SEND NOW</button>
                    <span id="error_send_now" style="color: red;"></span>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" value="" id="price" name="price">
                    <input type="hidden" value="" id="companybusinessid" name="companybusinessid">

                </div>
              </div>
          </form>



		</div>
        </div>
        </div>
        </div>
    </div>
    <!--END CONTENT-->





    @stop

    @section('scripts')

  
    
    @stop