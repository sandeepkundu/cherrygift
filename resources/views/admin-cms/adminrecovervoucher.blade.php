
@section('title','Company Report')
@section('content')

<!--BEGIN PAGE WRAPPER-->
<div id="page-wrapper">
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">
                Make cherrygift recover voucher</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>

            <li class="active">Make cherrygift recover voucher</li>
        </ol>
        <div class="clearfix">
        </div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->
    <!--BEGIN CONTENT-->
    <div class="page-content">
        <div class=" mbl">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                             Cherrygift Recover Voucher</div>
                        <div class="panel-body pan">
                        	
<!--                             @if (Session::get('message'))
                            <div class="alert alert-success">
                                <?php
                               // $error = Session::get('message');
                               // echo $error;
                               // Session::forget('message');
                                ?>
                            </div>
                            @endif

                            @if (Session::get('success-msg'))
                            <div class="alert alert-success">
                                <?php
                               // $error = Session::get('success-msg');
                               // echo $error;
                               // Session::forget('success-msg');
                                ?>
                            </div>
                            @endif -->
                            <form action="" method="post" id="frm_subscription">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <div class="form-body pal">
                                    <div class="form-group"></div> 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group text-right">
                                                
                                               
                                                <input type="button"  value="Search" id="search_vouchers"/>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                
                                                <input type="number" id="cherrygift_voucher" placeholder="" name="cherrygift_voucher" value="" class="form-control"  minlength="9" maxlength="15" required/></div>
                                                <p class="errorvoucher"></p>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-actions text-right pal resendblock"  style="display:none">
                                    <button type="submit" class="btn btn-success" id="update_cherrygift_voucher">
                                        Resend</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!--END CONTENT-->

</div>
<!--END PAGE WRAPPER-->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){


    $("#search_vouchers").click(function(){

    var contact_no = $('#cherrygift_voucher').val();
    //alert(contact_no);
    var dataString = {contact_no: contact_no};
     $.ajax({
            type: "POST",
            url: SITE_URL+"/super-admin/cherrygift-recover-voucher-update",
            data: dataString,
            dataType: "json",
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
            success: function (data)
            {
                //console.log(data);
                if(data=="")
                {
                 $('.errorvoucher').html('NO VOUCHER FOUND ');  
                //console.log('voucher not found');
                }else{
                $('.resendblock').show();
                }

               
            }
       });
    

    });
});
</script>

@stop

