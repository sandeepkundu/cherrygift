<!DOCTYPE html>
@extends('cms-template.default')
@section('title','Company Store Report')
@section('content')


          
            <!--BEGIN PAGE WRAPPER-->
            <div id="page-wrapper">
                <!--BEGIN TITLE & BREADCRUMB PAGE-->
                <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                    <div class="page-header pull-left">
                        <div class="page-title">
                            {{$company_name}} redeemed voucher list</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a href="{{url()}}/super-admin/dashboard">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                        
                        <li class="active">Redeemed voucher list</li>
                    </ol>
                    <div class="clearfix">
                    </div>
                </div>
                <!--END TITLE & BREADCRUMB PAGE-->
                <!--BEGIN CONTENT-->
                <div class="page-content">
                    <div id="tab-general">
                        <div class="row mbl">
				<div class="col-lg-12">
                           		<div class="panel panel-yellow">
                            		<div class="panel-heading">Redeemed voucher list</div>
                            		<div class="panel-body">
                                            @if (Session::get('message'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('message');
                                                echo $error;
                                                Session::forget('message');
                                                ?>
                                            </div>
                                            @endif

                                            @if (Session::get('success-msg'))
                                            <div class="alert alert-success">
                                                <?php
                                                $error = Session::get('success-msg');
                                                echo $error;
                                                Session::forget('success-msg');
                                                ?>
                                            </div>
                                            @endif
                                            Month : 
                                            <select name="month" id="month">
                                                <?php 
                                                $month = Route::getCurrentRoute()->getParameter('month');
                                                $year = Route::getCurrentRoute()->getParameter('year');
                                                $current_month = (!empty($month))?$month:date("m");
                                                for($i=1;$i<13;$i++) {?>
                                                <option <?php echo ($current_month==$i)?"selected='selected'":"";?>  value="<?php echo $i?>"><?php echo date('F', mktime(0, 0, 0, $i, 10));?></option>
                                                <?php }?>
                                            </select>

                                            Year : 
                                            <select name="year" id="year">
                                                <?php 
                                                $current_year = (!empty($year))?$year:date("Y");
                                                for($i=2016;$i<date("Y")+1;$i++) {?>
                                                <option <?php echo ($current_year==$i)?"selected='selected'":"";?>  value="<?php echo $i?>"><?php echo $i?></option>
                                                <?php }?>
                                            </select>
                                            <input type="hidden" value="{{$id}}" name="company_id" id="company_id">
                                            <input type="submit" name="submit" id="redeemed_history_submit" value="submit" />
    <table id="customerlist" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                
                <th class="no-sort">Voucher ID</th>                
                <th class="no-sort">Amount</th>
                <th class="no-sort">Purchased At</th> 
                <th class="no-sort">Redeemed At</th>
                <th class="no-sort">Redeemed Date</th>  
            </tr>
        </thead>
 
        <tbody>
            
        </tbody>
    </table>
             
             
             
             
                         			</div>
                        		</div>
                   		</div>
                        </div>
                    </div>
                </div>
                <!--END CONTENT-->
                


@stop

@section('scripts')

<script type="text/javascript" src="{{URL::asset('cms/script/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('cms/script/dataTables.responsive.js')}}"></script>    
<script type="text/javascript">
$(document).ready(function() {
  if ($('#customerlist').length) {
    $('#customerlist').DataTable({
            processing: true,
            deferRender: true,
            serverSide: true,
            responsive: true,
            //bSort: false,
            order: [[ 0, "asc" ]],
            autoWidth: true,
            bLengthChange: false,
            pageLength: 10,
            scroller: true,
            //columnDefs: [{ "orderable": false, "targets": 1 }],
            columns: [
                {data: 'voucher_unique_id', name: 'order_voucher.voucher_unique_id'},
                {data: 'voucher_price', name: 'order_voucher.voucher_price'},
                {data: 'purshased_at', name: 'cbp1.store_location'},
                {data: 'redeemed_at', name: 'cbp2.store_location'},
                {data: 'updated_at', name: 'order_voucher.updated_at'},
                
                 
                
            ],
            ajax: SITE_URL + '/super-admin/voucherhistorydata/'+{{$id}}+'/'+$('#month').val()+'/'+$('#year').val(),
            // select: true,
//                'aoColumnDefs': [{
//            'bSortable': false,
//            'aTargets': [-1] /* 1st one, start by the right */
            //             }],
            //"aaSorting": [[ 1, "asc" ]] // Sort by first column descending


        });
    }    
    
} );

</script>
@stop