Hi <b>{{$first_name}}</b><br><br>

Thank you for your purchase with cherrygift.<br><br>

Your recently purchased voucher number <b>{{$voucher_unique_id}}</b> has been cancelled.<br><br>

If you believe this is a mistake, please contact one of our team on 1300 122 321 or email {{env('contact_email')}}.<br><br>

Don't forget to follow us on Facebook, Twitter, Instagram and You Tube!<br><br>

Cheers,<br><br>

From the cherrygift team!<br><br>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">
