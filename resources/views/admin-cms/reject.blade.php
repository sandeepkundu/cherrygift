Hi {{$first_name}}<br><br>

Thank you for registering your store/site <b>{{$store_location}}</b> with cherrygift.<br><br>

Unfortunately, your profile does not meet our criteria.<br><br>

<?php echo $reason; ?><br><br>
If you have any queries regarding your subscription, please contact our team on 1300 122 321 or email {{env('contact_email')}} <br><br>

We look forward to working with you and helping your business GROW one cherrygift at a time!<br><br>

Cheers,<br><br>

From the cherrygift team!<br><br>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">
