Hi cherrygift,<br><br>

Store/site  <b>{{$store_location}}</b> profile has requested changes and are being submitted for approval.<br><br>

Please action and advise customer.<br><br>

Cheers,<br><br>

From the cherrygift team!<br><br>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">
