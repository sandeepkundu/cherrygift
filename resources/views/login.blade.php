<!DOCTYPE html>
<html lang="en">
<head>
    <title>cherrygift</title>
    @include('template.header_include')
    
</head>
<body>
	@include('template.header') 
<!--    {!! csrf_field() !!}-->
 	<div class="auth-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                    <header class="auth-header clearfix">
                    	<div class="head-left">
                        	<h1 class="auth-title">Login </h1><span class="sub-title">Sign up</span>
                        </div>
                        <div class="head-right">
                            <a href="auth/facebook" class="btn btn-primary btn-block"> Connect with your Facebook</a>
                        </div>
                    </header>
                    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                    <div class="row">
                    	<div class="col-sm-8 col-sm-offset-2">
                            
                            <form class="form-horizontal" action="login" method="POST">
<!--                                <input type="hidden" name="_token" value="{{ csrf_token() }}">-->
                                
                                <div class="form-group">
                                  <label for="" class="col-sm-3 control-label">Email</label>
                                  <div class="col-sm-9">
                                      <input type="email" class="form-control"  name="email" id="" placeholder="Email" value="{{ old('email') }}">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="" class="col-sm-3 control-label">Password</label>
                                  <div class="col-sm-9">
                                      <input type="password" class="form-control" id="" name="password" placeholder="Password" value="{{ old('password') }}">
                                  </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                    <a href="#" class="forgot-link" data-toggle="modal" data-target="#forgotModal">Forgot password?</a>
                                    </div>
                                </div>
                                <div class="form-group mr-b0">
                                    <div class="col-sm-offset-3 col-sm-5 submit-ctn">
                                      <button type="submit" class="btn btn-danger btn-block">LOGIN</button>
                                    </div>
                                </div>
                            </form>
                      	</div>
                    </div>
                </div>
            </div>
        </div>
  	</div>
        @include('template.termcondition')
    @include('template.forgot')
    @include('template.forgot_a')
    @include('template.tutorial')
    @include('template.footer')
    @include('template.footer_include')
<script>
		(function($){
			$(window).load(function(){
				
				$("#termsModal .modal-body").mCustomScrollbar({
					setHeight:340,
					theme:"minimal-dark"
				});
				
			});
			
			$("#carousel-tutorial").carousel({
  interval: false,
  wrap: false
});
			var checkitem = function() {
  var $this;
  $this = $("#carousel-tutorial");
  if ($("#carousel-tutorial .carousel-inner .item:first").hasClass("active")) {
    $this.find(".left").hide();
    $this.find(".right").show();
  } else if ($("#carousel-tutorial .carousel-inner .item:last").hasClass("active")) {
    $this.find(".right").hide();
    $this.find(".left").show();
  } else {
    $this.find(".left").show();
	$this.find(".right").show();
  }
};

checkitem();

$("#carousel-tutorial").on("slid.bs.carousel", "", checkitem);

			
			
		})(jQuery);
		
		
		
	</script>
</body>
</html>