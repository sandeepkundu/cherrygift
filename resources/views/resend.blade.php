@extends('template.main')
@section('title','cherrygift - resend vouchers')
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>
@section('content') 

<style>
.white-bg-header > div {
    margin-bottom: -29px;
    margin-left: -46px;
    margin-top: 40px;

}
.white-bg-header{

    padding:30px 0;
}
.history{

  margin: -8px 8px 10px;
}
.common{
    font-size: 14px;
    font-weight: bolder;
    color: #000;

}
.redeem{
 font-size: 14px;
    font-weight: bolder;
    color: #8dc53e;

}
.cancel{
font-size: 14px;
    font-weight: bolder;
    color: red;

}
.voucher-info-list li {
       line-height:normal; 
       padding-right: 10px;
       padding-left: 8px;
  }
  .listdate{
        /*margin-right: 44px;*/
    margin-top: 10px;
    border-left-color: #eeeff1;
    border-left-style: solid;
    border-left-width: 1px;
    height: 40px;
    
  }
/*.voucher-info-list li {

        padding: 0 20px;
}*/
</style>

    <div class="wrapper"><!-- PAGE WRAPPER -->
        <header class="white-bg-header"><!-- PAGE HEADING -->
            <h2 class="title">Recover Voucher</h2>
        <div><span><b>•</b></span> Please note that $3.85 fee applies per voucher</div>
        @if(!Auth::check())
          <div><span><b>•</b></span> You must login or sign up to recover vouchers</div>  
        @endif
        </header><!-- PAGE WRAPPER -->
       
        <section class="clearfix container mr-t15"> <!-- MAIN SECTION -->
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 clearfix">
            @if(Session::has('message'))
            <div class="alert alert-success">
                <h3 style='text-align: center'>{{ Session::get('message') }}</h3>
            </div>
            @endif

            @if (Session::has('errors'))
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <h3 style='text-align: center'>{{ $error }}</h3>
                    @endforeach
                </ul>
            </div>
            @endif
                
            </div>

            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">

                <form method="post" name="voucher_search" id="voucher_search" action="resend">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                <div class="search-cont">
                    <div class="input-group">
  <span class="input-group-addon">+61</span>
    <input type="text" class="form-control input-sm" placeholder="Search by contact number" name="search_contact" id="search_contact" value="{{Input::get('search_contact')}}" maxlength="15" autocomplete="false"/>
    <button type="submit" class="search-btn btn-primary btn-sm" name="search_vouchers">Search</button>
</div>
                    
                    
                    <label class="errors" for="search_contact"></label>
                </div>
                </form></div>
           
            @if(Request::isMethod('post'))
            @if(count($purshase_history)!=0)
           
            <div class="col-sm-8 col-sm-offset-2 mr-b50" id="resend-vocher-id clearfix">
                
                @if(!empty(env('PAYPAL_SENDBOX')))
    <form name="resendform" id="resendform" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
        @else
        <form name="resendform" id="resendform"  action="https://www.paypal.com/cgi-bin/webscr" method="post">
            @endif

            <input type="hidden" name="cmd" value="_xclick">  
            <input type="hidden" name="business" value="{{Config::get('constants.paypal_user_acc')}}">
            <input type="hidden" name="upload" value="1">


            <input type="hidden" name="item_name" value="Resend Cherrygift Vouchers">
<!--            <input type="hidden" name="item_number" value="Designer">-->
               <!-- <input type="hidden" name="amount" value="" id="amountvalue">-->



            <input type="hidden" id="vouchervalue" name="custom" value="">
            <input type="hidden" name="amount" value="" id="amountvalue">
           <!-- <input type="hidden" name="voucherid" value="" id="vouchervalue">-->
            <input type="hidden" name="shipping" value="0.00">
            <input type="hidden" name="no_shipping" value="0">
            <input type="hidden" name="no_note" value="1">
            <input type="hidden" name="currency_code" value="AUD">
<!--            <input type="hidden" name="tax" value="0.00">-->
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="paypal_history_id" id="paypal_history_id" value="">
            <input type="hidden" name="lc" value="">
            <input type="hidden" name="notify_url" value="{{url()}}/resendNotify" />
            <input type="hidden" name="cancel_return" value="{{url()}}/resend" />
            <input type="hidden" name="return" value="{{url('resendPaymentComplete')}}" id="resendcomplete"/>
            <button class="btn btn-success btn-sm btn-block" id="resendbtn">RESEND</button>
            <span class="resenderrors" style="color:red"></span>
        </form>
<!--                id="resend_voucher"-->
            </div>
            
            @foreach($purshase_history as $history)
            <div class="redeem-voucher-list">
            <div class="row">
                <div class="col-md-12 col-lg-10 col-lg-offset-1">
              
                    <div class="pd-tb-15">
                        <div class="voucher-list">
                            <div class="list-item clearfix">
                                <span class="left-item product-item">
                                    <img class="img-responsive" alt="" src="{{$s3_url}}{{$s3_env}}{{$history->image}}" style="height: 200px;">
                                    <div class="item-upper-bg">
                                        <div class="media"> 
                                            <div class="media-left"> 
                                                <img style="width: 35px; height: 35px;" class="small-circle" src="{{$s3_url}}{{$s3_env}}{{$history->logo}}">
                                            </div> 
                                            <div class="media-body media-middle"> 
                                                <h4 class="media-heading">{{$history->store_location}}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                </span>
                                <div class="right-item">
                                    <ul class="list-unstyled voucher-info-list">
                                        <li class="price">${{number_format($history->voucher_price,0)}}</li>
                                        <li class="phone"><span class="icon-phone" ></span> +61 {{$history->phone_number}}
                                          <span><br>&nbsp</span>
                                        </li>
                                        @if($history->sms_schedule==0)

                                        <div class="pull-right listdate">
                                        <li class="date" style="float:left; display:block;">
                                          <span class="icon-calendar"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }}
                                      
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                              <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?>
                                         </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                         @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 


                                        </span>
                                      </li>
                                        </div>
                                      </div>
                                       


                                        
                                    @else
                                         <div class="pull-right listdate">
                                        <li class="date" style="float:left; display:block;">
                                          <span class="icon-calendar"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }} at {{str_pad($history->sms_hour,2,"0",STR_PAD_LEFT)}}:{{str_pad($history->sms_minutes,2,"0",STR_PAD_LEFT)}}
                                      
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                              <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?>
                                         </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                         @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 


                                        </span>
                                      </li>
                                        </div>
                                      </div>

    
                                    @endif  
                                    </ul>   
                                
                                    <p>{{substr($history->sms_message, 0,strrpos($history->sms_message,"http"))}}
                                     
                                    </p><br><br>

                                    <p style="float:right;margin: 0px 10px 10px;"><b >Voucher:&nbsp; {{$history->voucher_unique_id}}</b> &nbsp;&nbsp;&nbsp;<span style="border-right-color: #eeeff1;border-right-style: solid;border-right-width: 1px;font-size:33px"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                      @if($history->voucher_status==1 && $history->redeemed==0)
                                      <input type="checkbox" class="chkbox" id="{{$history->id}}" style="width: 20px;height: 20px;">
                                      @elseif($history->redeemed==1)
                                      <input type="checkbox" class="chkbox" id="" style="width: 20px;height: 20px;" disabled>
                                      @else
                                      <input type="checkbox" class="chkbox" id="" style="width: 20px;height: 20px;" disabled>
                                      @endif
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-sm-8 col-sm-offset-2 mr-b50">
                    NO VOUCHER FOUND
                </div>
        </div>
                @endif
@endif
 </section><!-- MAIN SECTION -->
            </div>
       
    </div>
<!-- PAGE WRAPPER -->

  
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){


    $('.chkbox').click(function(){

          
        var id=$(this).attr('id');


        var matches = [];
        $(".chkbox:checked").each(function() {
        matches.push(this.id);
        });
        $('#vouchervalue').val(matches);

          
    });

    $("#resendbtn").click(function(e){
      e.preventDefault();
    

        $checkbox = $(".chkbox").is(":checked");
        if($checkbox == true) {

            $('#resendbtn').prop('disabled',true);
            setTimeout(function (){
                $('#resendbtn').prop('disabled',false);
            },10000);

            var checked_boxes = $('input:checkbox:checked').length
            var total=3.85*checked_boxes;
            $('#amountvalue').val(total);
            var form=$("#resendform");
            $.ajax({
              type: "POST",
              url: "resendSaveBeforePaypal",
              data:form.serialize(),
              dataType: "json",
              headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
              success: function (response) {     
                //$('#vouchervalue').val(response.id);
                document.resendform.submit();               
              }
          });        
        } else {
          $('.resenderrors').html('Please select at least one checkbox');    
          return false;
        }
      
    });      
});
</script>