@extends('template.main')
@section('title','cherrygift - purchase history')
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>
    @section('content')

<style>
.white-bg-header > div {
    margin-bottom: -29px;
    margin-left: -46px;
    margin-top: 40px;

}
.white-bg-header{

    padding:30px 0;
}
.history{

  margin: -8px 8px 10px;
}
.common{
    font-size: 14px;
    font-weight: bolder;
    color: #000;

}
.redeem{
 font-size: 14px;
    font-weight: bolder;
    color: #8dc53e;

}
.cancel{
font-size: 14px;
    font-weight: bolder;
    color: red;

}
.voucher-info-list li {
       line-height:normal; 
       padding-right: 10px;
        padding-left: 8px;
  }
  .listdate{
        /*margin-right: 44px;*/
    margin-top: 10px;
    border-left-color: #eeeff1;
    border-left-style: solid;
    border-left-width: 1px;
    height: 40px;
    
  }
  .voucher-info-list li + li {

     height: 36px;
  }
/*.voucher-info-list li {

        padding: 0 20px;
}*/
</style>



    <div class="wrapper"><!-- PAGE WRAPPER -->
    	<header class="white-bg-header"><!-- PAGE HEADING -->
            <h2 class="title his-mob-30">My History</h2><a href="{{url()}}/resend"><button class="btn btn-default">RECOVER VOUCHER VIA SMS</button></a>
        </header><!-- PAGE WRAPPER -->
    	
    	<section class="clearfix container mr-t15"> <!-- MAIN SECTION -->
    		<div class="row">
            	<div class="col-md-12 col-lg-10 col-lg-offset-1">
                    <div class="row">
  						<div class="col-sm-8 col-sm-offset-2">
    						<h4 class="section-heading"><span>VOUCHERS SENT</span></h4>
           	 			</div>
       				</div>
                   <?php $con=0; ?>
                    @if(count($pushase_history)!=0)
                    
                     @foreach($pushase_history as $history)
                     
                     @if($history->sms_status==1)
                     <?php $con++; ?>
                     @endif
                     @endforeach
                     @endif
                	
                     @if($con==0)
                     <div class="row">
  						<div class="col-sm-8 col-sm-offset-2">
    						<h3 class="text-center mr-b30">No voucher to display</h3>
           	 			</div>
       				</div>
                     @endif
                     @if(count($pushase_history)!=0)
                     @foreach($pushase_history as $history)
                     @if($history->sms_status==1 OR $history->sms_status==2)
                    <div class="pd-tb-15">
                        <div class="voucher-list">
                            <div class="list-item clearfix">
                                <span class="left-item product-item">
                                    <img src="{{$s3_url}}{{$s3_env}}{{$history->image}}" alt="" class="img-responsive" style="height: 200px;" width="300" height="200">
                                    <div class="item-upper-bg">
                                        <div class="media"> 
                                            <div class="media-left"> 
                                                <img class="small-circle" src="{{$s3_url}}{{$s3_env}}{{$history->logo}}" style="width: 35px; height: 35px;">
                                            </div> 
                                            <div class="media-body media-middle"> 
                                                <h4 class="media-heading">{{$history->store_location}}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                </span>
                                <div class="right-item">
                                    <ul class="list-unstyled voucher-info-list">
                                        <li class="price">${{number_format($history->voucher_price,0)}}</li>
                                        <li class="phone"><span class="icon-phone"></span>+61 {{$history->phone_number}}
                                        
                                        </li>
                                         @if($history->sms_schedule==0)
                                        <div class="pull-right listdate">
                                        <li class="date" style="float:left; display:block;">
                                          <span class="icon-calendar"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }}
                                      
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                              <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?>
                                         </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                         @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 


                                        </span>
                                      </li>
                                        </div>
                                      </div>




                                    @else

                                 <div class="pull-right listdate">
                                        <li class="date" style="float:left; display:block;">
                                          <span class="icon-calendar" custom_date_value="<?php echo strtotime($history->sms_date.' '.$history->sms_hour.':'.$history->sms_minutes); ?>"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }} at {{str_pad($history->sms_hour,2,"0",STR_PAD_LEFT)}}:{{str_pad($history->sms_minutes,2,"0",STR_PAD_LEFT)}} 
                                     
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                              <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?>
                                         </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                         @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 


                                        </span>
                                      </li>
                                        </div>
                                      </div>



                                   
                                        @endif
                                        
                                    </ul>
                                    <p>{{substr($history->sms_message, 0,strrpos($history->sms_message,"http"))}}
                                       
                                    </p><br><br>
                                    <p style="float:right;margin: 0px 10px 10px;"><b >Voucher:&nbsp; {{$history->voucher_unique_id}}</b> </p>
                                </div>
                                
                            </div>
                        </div>
                        
                        @endif
                        @endforeach
                        
                        @endif
                    
                     <div class="row">
  						<div class="col-sm-8 col-sm-offset-2">
    						<h4 class="section-heading"><span>VOUCHERS IN PROGRESS</span></h4>
           	 			</div>
       				</div>
                        
                     <?php $con=0; ?>
                    @if(count($pushase_history)!=0)
                    
                     @foreach($pushase_history as $history)
                     
                     @if($history->sms_status==0)
                     <?php $con++; ?>
                     @endif
                     @endforeach
                     @endif
                      @if($con==0)
                     <div class="row">
  						<div class="col-sm-8 col-sm-offset-2">
    						<h3 class="text-center mr-b30">No voucher to display</h3>
           	 			</div>
       				</div></div>
            
                     @endif
                     @foreach($pushase_history as $history)
                     @if($history->sms_status==0)
                    <div class="pd-tb-15">
                        <div class="voucher-list">
                            <div class="list-item clearfix">
                                <span class="left-item product-item">
                                    <img src="{{$s3_url}}{{$s3_env}}{{$history->image}}" alt="" class="img-responsive" style="height: 200px;">
                                    <div class="item-upper-bg">
                                        <div class="media"> 
                                            <div class="media-left"> 
                                                 <img class="small-circle" src="{{$s3_url}}{{$s3_env}}{{$history->logo}}" style="width: 35px; height: 35px;"> 
                                            </div> 
                                            <div class="media-body media-middle"> 
                                                <h4 class="media-heading">{{$history->store_location}}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                </span>
                                <div class="right-item">
                                    <ul class="list-unstyled voucher-info-list">
                                        <li class="price">${{number_format($history->voucher_price,0)}}</li>
                                        <li class="phone"><span class="icon-phone"></span>+61 {{$history->phone_number}}</li>
                                        @if($history->sms_schedule==0)

                                        <div class="pull-right listdate">
                                        <li class="date f-red" style="float:left; display:block;">
                                          <span class="icon-calendar"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date f-red" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }}
                                      
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                              <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?>
                                         </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                         @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 


                                        </span>
                                      </li>
                                        </div>
                                      </div>

                                    @else
                                   
                                   
                                    <div class="pull-right listdate">
                                        <li class="date f-red" style="float:left; display:block;">
                                          <span class="icon-calendar"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date f-red" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }}  at {{str_pad($history->sms_hour,2,"0",STR_PAD_LEFT)}}:{{str_pad($history->sms_minutes,2,"0",STR_PAD_LEFT)}}
                                      
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                              <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?>
                                         </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                         @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 


                                        </span>
                                      </li>
                                        </div>
                                      </div>


                                    @endif
                                    </ul>
                                    <p>{{substr($history->sms_message, 0,strrpos($history->sms_message,"http"))}}
                                                                  
                                    </p><br><br>
                                    <p style="float:right;margin: 0px 10px 10px;"><b >Voucher:&nbsp; {{$history->voucher_unique_id}}</b> </p>
                                </div>
                                
                            </div>
                        </div>
                        
                        
                    </div>
                     @endif
                        @endforeach
                        
                        
                        <div class="row">
  						<div class="col-sm-8 col-sm-offset-2">
    						<h4 class="section-heading"><span>GIFT VOUCHERS RECEIVED</span></h4>
           	 			</div>
       				</div>
                        
                     <?php $con_gift=0; ?>
                    @if(count($gift_list)!=0)
                    
                     @foreach($gift_list as $history)
                     <?php $con_gift++; ?>
                     @endforeach
                     
                     @endif
                      @if($con_gift==0)
                     <div class="row">
  						<div class="col-sm-8 col-sm-offset-2">
    						<h3 class="text-center mr-b30">No voucher to display</h3>
           	 			</div>
       				</div></div>
            
                     @endif
                     @foreach($gift_list as $history)
                     
                    <div class="pd-tb-15">
                        <div class="voucher-list">
                            <div class="list-item clearfix">
                                <span class="left-item product-item">
                                    <img src="{{$s3_url}}{{$s3_env}}{{$history->image}}" alt="" class="img-responsive" style="height: 200px;">
                                    <div class="item-upper-bg">
                                        <div class="media"> 
                                            <div class="media-left"> 
                                                 <img class="small-circle" src="{{$s3_url}}{{$s3_env}}{{$history->logo}}" style="width: 35px; height: 35px;"> 
                                            </div> 
                                            <div class="media-body media-middle"> 
                                                <h4 class="media-heading">{{$history->store_location}}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                </span>
                                <div class="right-item">
                                    <ul class="list-unstyled voucher-info-list">
                                        <li class="price">${{number_format($history->voucher_price,0)}}</li>
                                        <li class="phone"><span class="icon-phone"></span>+61 {{$history->phone_number}}</li>
                                        
                                        <div class="pull-right listdate">
                                        <li class="date f-red" style="float:left; display:block;">
                                          <span class="icon-calendar"></span>
                                        </li>
                                        <div style="float:left; display:block;">
                                        <li class="date f-red" style="float:left; display:block;">
                                          
                                         <span class="common">Sent:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ date('d F Y', strtotime($history->sms_date)) }} @if($history->sms_hour!=0 && $history->sms_minutes!=0) at {{str_pad($history->sms_hour,2,"0",STR_PAD_LEFT)}}:{{str_pad($history->sms_minutes,2,"0",STR_PAD_LEFT)}}@endif
                                      
                                        </li><br>
                                          <li style="float:left; display:block;">
                                            <span style="float:right">
                                              <?php
                                        $date = strtotime($history->created_at);

                                        $futureDate=date('d F  Y',strtotime('+1 year',$date));
                                         echo '<span class="common">'.'Expires:'.'&nbsp;&nbsp;&nbsp;'.'</span>'. $futureDate;

                                       ?>
                                         </span>
                                       </li><br>
                                       <li style="float:left; display:block;">
                                         @if($history->redeemed==1)
                                        <span class="common">
                                          
                                          Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Redeemed</span>

                                      
                                       @elseif($history->voucher_status==1) 
                                      <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="redeem">Active</span>
                                       @elseif($history->voucher_status==0)
                                       <span class="common">Status:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="cancel">Cancelled</span>
                                       
                                       @endif 


                                        </span>
                                      </li>
                                        </div>
                                      </div>


                                    </ul>
                                    <p>{{substr($history->sms_message, 0,strrpos($history->sms_message,"http"))}}
                                   
                                    </p><br><br>
                                    <p style="float:right;margin: 0px 10px 10px;"><b >Voucher:&nbsp; {{$history->voucher_unique_id}}</b> </p>
                                </div>
                                
                            </div>
                        </div>
                        
                        
                    </div>
                     
                        @endforeach
             	</div>
            </div>
            
            
            
           
                	
                    
                    
             	
           
               
        </section><!-- MAIN SECTION -->
    </div><!-- PAGE WRAPPER -->
  		
  @stop