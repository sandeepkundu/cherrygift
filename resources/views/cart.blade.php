@extends('template.main')
@section('title','cherrygift - cart')
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>

@section('content') 

<div class="cart-wrapper">
    <div class="container">
        <div class="row">
            @if(Session::has('message'))
            <div class="alert alert-success">
                <h3 style='text-align: center'>{{ Session::get('message') }}</h3>
            </div>
            @endif

            @if (Session::has('errors'))
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <h3 style='text-align: center'>{{ $error }}</h3>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <h2 class="page-heading">Your cart</h2>
                @if(empty($getusercartinfo) || count($getusercartinfo)==0)
                <h3 class="text-center mr-b30">There are no items in this cart</h3>
                @else
                <?php $cart_item = 0;?>
                @foreach($getusercartinfo as $cartinfo)
                <?php $cart_item++;?>
                <div class="voucher-list" id="voucher-list{{$cartinfo->id}}">
                    <div class="list-item clearfix">
                        <span class="left-item product-item">
                            <img src="{{$s3_url}}{{$s3_env}}{{$cartinfo->image}}" alt="" class="img-responsive" style="height: 200px; width: 300px" >
                            <div class="item-upper-bg">
                                <div class="media"> 
                                    <div class="media-left"> 
                                       <img class="small-circle" src="{{$s3_url}}{{$s3_env}}{{$cartinfo->logo}}" style="width: 35px; height: 35px;">
                                    </div> 
                                    <div class="media-body media-middle"> 
                                        <h4 class="media-heading">{{$cartinfo->store_location}}</h4> 
                                    </div>
                                </div>
                            </div>
                        </span>
                        <div class="right-item">
                            <ul class="list-unstyled voucher-info-list list-block">
                                <input type="hidden" value="{{$cartinfo->price}}" class="sum_price"> <li class="price">$ {{$cartinfo->price}} 
                                    <div class="btn-group pull-right action-btn-group" role="group">
                                        <button type="button" class="btn btn-default" value="{{$cartinfo->id}}" id="cart_id_del"><span class="ic-16 ic-trash mr-r10"></span><span class="iconTxt">Delete</span></button>
                                        <button type="button" class="btn btn-default" value="{{$cartinfo->id}}" onclick="window.location.href ='{{url()}}/edit-voucher-details/{{$cartinfo->id}}'" id="cart_id_edit"><span class="ic-16 ic-edit mr-r10"></span><span class="iconTxt">Edit</span></button>
                                    </div>
                                </li>  
                                <li class="phone"><span class="icon-phone"></span> +61 {{$cartinfo->phone_number}}</li>
                            @if($cartinfo->sms_schedule==0)
                            
                                <li class="date">
                                    
                                    <span class="icon-calendar" sms_schedule="{{$cartinfo->sms_schedule}}" custom_date_value="<?php echo strtotime(gmdate($cartinfo->sms_date.' '.$cartinfo->sms_hour.':'.$cartinfo->sms_minutes)); ?>"></span> On {{ date('d F Y', strtotime($cartinfo->sms_date)) }} 
                                    <input type="hidden" name="sms_schedule" id="sms_schedule" value="{{$cartinfo->sms_schedule}}">
                                    @else
                                    <li class="date"><span class="icon-calendar" sms_schedule="{{$cartinfo->sms_schedule}}" custom_date_value="<?php echo strtotime(gmdate($cartinfo->sms_date.' '.$cartinfo->sms_hour.':'.$cartinfo->sms_minutes)); ?>"></span> On {{ date('d F Y', strtotime($cartinfo->sms_date)) }} at {{str_pad($cartinfo->sms_hour,2,"0",STR_PAD_LEFT)}}:{{str_pad($cartinfo->sms_minutes,2,"0",STR_PAD_LEFT)}} 
                                    @endif
                                    <span id="error_cls" class="error_cls" style="color: red"></span>
                                </li>
                            </ul> 
                            <p>{{$cartinfo->sms_message}}</p>
                        </div>

                    </div>
                </div>
                @endforeach
                <input type="hidden" id="curdate_time" value="{{strtotime(gmdate("Y-m-d H:i:s", time()))}}">
                <input type="hidden" id="curdate_date" value="{{strtotime(gmdate("Y-m-d", time()))}}">
                <h2 class="page-heading" id="payment">Payment</h2>
                <ul class="list-unstyled cart-payment-list">
                    <li class="clearfix">
                        VOUCHERS <span class="voucher-price"></span>
                    </li>
                    <li class="clearfix">
                        TRANSACTION FEE <span  style="display:none"  id="fee">{{env('CART_PER_ITEM_TAX')}}</span><span class="fee">${{env('CART_PER_ITEM_TAX')*$cart_item}} </span>
                    </li>
                    <li class="clearfix">
                        TOTAL <span style="font-size: 18px;"><br>(including GST)</span><span id="total_price" style="margin-right: -116px;"></span>
                    </li>
                </ul>
                @endif
            </div>
        </div>
    </div>
   
    @if(empty($getusercartinfo) || count($getusercartinfo)!=0)
    @if(!empty(env('PAYPAL_SENDBOX')))
        <form name="frm_payment_method" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
    @else
        <form name="frm_payment_method" action="https://www.paypal.com/cgi-bin/webscr" method="post">
    @endif

            <input type="hidden" name="cmd" value="_xclick">  
            <input type="hidden" name="business" value="{{Config::get('constants.paypal_user_acc')}}">
            <input type="hidden" name="upload" value="1">


            <input type="hidden" name="item_name" value="Cherrygift Vouchers">
<!--            <input type="hidden" name="item_number" value="Designer">-->

            <input type="hidden" name="custom" id="custom" value="">
            <input type="hidden" name="cart_id" id="cart_id" value="{{$cart_id}}">
            <input type="hidden" name="amount" id="paypal_amt" value="{{$total_cart_price}}">
            <input type="hidden" name="shipping" value="0.00">
            <input type="hidden" name="no_shipping" value="0">
            <input type="hidden" name="no_note" value="1">
            <input type="hidden" name="currency_code" value="AUD">
<!--            <input type="hidden" name="tax" value="0.00">-->
            <input type="hidden" name="lc" value="">
            <input type="hidden" name="notify_url" value="{{url()}}/userOrderNotify/{{Auth::user()->id}}" />
            <input type="hidden" name="cancel_return" value="{{url()}}/userOrderCancel"/>
            <input type="hidden" name="return" value="{{url()}}/userordercomplete/{{Auth::user()->id}}"/>
@if(!empty($getusercartinfo))
            <div class="payPal-ctn ">
                <a  class="payPal-btn-img" id="paypal_pay_now"></a>
            </div>
@endif
        </form>

        @endif
        <div class="container-fluid continue-shop">


            <a href="{{url().'/search'}}">Continue shopping</a>
        </div>
</div>


@stop

