@extends('template.default')
@section('title','cherrygift - search')
@section('description','CherryGift vouchers search results.  Find the perfect voucher here.')
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>	
       
 @section('content') 
  <script type="text/javascript">
            var filter_json = '';
            var city_state_json = '<?php echo json_encode($state_city_map) ?>';
            var s3_url = '<?php echo $s3_url;?>';
            var s3_env = '<?php echo $s3_env;?>';
            var url_param_type = '{{$url_param_type}}';
            var url_param_id = '{{$url_param_id}}';
            var url_param_name = "<?php echo $url_param_name?>";
            
 </script>      
    <section class="search-wrapper lg-container clearfix">
        <aside  class="catalog-filter" id="catalog-filter">
            <h2 class="catalog-heading hidden-xs">Filter Results by:</h2>

            <div class="visible-xs">
                <a class="main-search-accordion" id="filter-toggle" data-toggle="collapse" href="#search-accordion">
                    FILTER RESULTS BY<span class="glyphicon"></span>
                </a>
            </div>
            <div class="collapse" id="search-accordion" aria-multiselectable="true">

                <div class="accordion-ctn panel">
                    <h4 class="accordion-title">
                        <a role="button" data-toggle="collapse" data-parent="#search-accordion"  href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            CATEGORIES<span class="glyphicon"></span>
                        </a>
                    </h4>
                    <div id="collapseOne" class="panel-collapse  collapse in accordion-body">

                        <ul class="list-unstyled options scrollbar-macosx">
                            @foreach(App\Model\Categories::orderBy('name', 'asc')->get() as $cat)
                           
                            <li><a class="search_filter_action" filter_type="cat" filter_id="{{$cat->id}}" filter_name="{{$cat->name}}" href="javascript:void(0);">{{$cat->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="accordion-ctn panel">
                    <h4 class="accordion-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#search-accordion"  href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            STATE<span class="glyphicon"></span>
                        </a>
                    </h4>
                    <div id="collapseTwo" class="panel-collapse  collapse accordion-body">

                       	<ul class="list-unstyled options scrollbar-macosx">
                            @foreach(App\Model\States::orderBy('state_name', 'asc')->get() as $location)
                            <li><a class="search_filter_action" filter_type="state" filter_id="{{$location->id}}" filter_name="{{$location->state_name}}"  href="javascript:void(0);">{{$location->state_name}}</a></li>
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
                <div class="accordion-ctn panel">
                    <h4 class="accordion-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#search-accordion"  href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                           SUBURB<span class="glyphicon"></span>
                        </a>
                    </h4>
                    <div id="collapseThree" class="panel-collapse  collapse accordion-body">

                       	<ul class="list-unstyled options scrollbar-macosx" id="filter_city_list">
                            @foreach(App\Model\Suburbs::orderBy('locality', 'asc')->get() as $suburbs)
                            <li><a class="search_filter_action" filter_type="city" filter_id="{{$suburbs->id}}" filter_name="{{$suburbs->locality}}" href="javascript:void(0);">{{$suburbs->locality}}</a></li>
                            @endforeach
                        </ul>

                    </div>
                </div>
                <div class="accordion-ctn panel">
                    <h4 class="accordion-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#search-accordion"  href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            RECIPIENT<span class="glyphicon"></span>
                        </a>
                    </h4>
                    <div id="collapseFour" class="panel-collapse  collapse accordion-body">

                       	<ul class="list-unstyled options scrollbar-macosx">
                            @foreach(App\Model\Recipient::where('is_top', '=', '1')->orderBy('name', 'asc')->get() as $rec)
                            <li><a class="search_filter_action" filter_type="rec" filter_id="{{$rec->id}}" filter_name="{{$rec->name}}" href="javascript:void(0);">{{$rec->name}}</a></li>
                            @endforeach
                        </ul>

                    </div>
                </div>
                <div class="accordion-ctn panel">
                    <h4 class="accordion-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#search-accordion"  href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            OCCASION<span class="glyphicon"></span>
                        </a>
                    </h4>
                    <div id="collapseFive" class="panel-collapse  collapse accordion-body">

                       	<ul class="list-unstyled options scrollbar-macosx">
                            @foreach(App\Model\Occasion::orderBy('occation_name', 'asc')->get() as $oca)
                            <li><a class="search_filter_action" filter_type="oca" filter_id="{{$oca->id}}" filter_name="{{$oca->occation_name}}" href="javascript:void(0);">{{$oca->occation_name}}</a></li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </aside>
        <section class="catalog-product">

            <div class="col-md-12">
                <div class="tag-wrapper" >
                    <div id="filter_tag_html" class="pull-left"></div>
                        <?php   if (!empty($params) || (Request::input('search_state')))
                        {  ?>
                            <a class="clear-all" id="clear_all_filter" href="{{URL().'/search'}}">Clear All</a>
                        <?php } else
                        {
                            ?> <a class="clear-all" id="clear_all_filter" href="{{URL().'/search'}}" style="display: none;">Clear All</a>
                            <?php }
                        ?>					
                </div>

                <h2 class="heading">Search results : <?php echo @$_POST['search_txt']; ?></h2>
                <div class="row">
                    <ul class="list-unstyled catalog-product-list" id="display_data">
                        @if(!empty($voucher_arr))
                        @foreach($voucher_arr as $voucher)
                        <li class="col-lg-4 col-md-6">
                            <div class="product-item multi bck_grey">
                            	<a href="{{url()}}/voucher-details/{{str_replace(' ', '-',$voucher->store_location)}}" class="dis-block auto-height">
                                    @if(!empty($voucher->image))
                                    <img  src="{{$s3_url}}{{$s3_env}}{{$voucher->image}}" alt="" class="img-responsive image-proportion">
                                    @else
                                    <img  src="{{asset('images/cherry gift.png')}}" alt="" class="img-responsive image-proportion">
                                    @endif
                                    <div class="item-upper-bg">
                                        <div class="media"> 
                                            <div class="media-left"> 
                                               <img class="media-object small-circle " src="{{$s3_url}}{{$s3_env}}{{$voucher->logo}}" style="width: 35px; height: 35px;">  
                                            </div> 
                                            <div class="media-body media-middle"> 
                                                <h4 class="media-heading">{{$voucher->store_location}}</h4> 
                                            </div>
                                        </div>
                                    </div>
                                
                                <span class="full-background"><p>{{$voucher->short_desc}}</p></span></a>
                            </div>
                        </li>
                        @endforeach
                        @else
                        <li class="col-sm-12 text-center"> Sorry, no vouchers found</li>
                        @endif 

                    </ul>
                </div>
            </div>
        </section>
    </section>
    <div class="loader-ctn" id="loader">
        <div class="loader">Loading...</div>
    </div>

 @stop
 