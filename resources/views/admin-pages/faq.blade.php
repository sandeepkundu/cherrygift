@include('admin-template.header_include')
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>cherrygift FAQ</title>
        </head>
@include('admin-template.header')
@section('content')
     <div class="page-wrapper clearfix" id="innerContact">
     	<div class="container">
        	<div class="row">
            	<div class="col-sm-10 col-sm-offset-1">
                    <header class="text-center">
                        <h2 class="page-heading">FAQ</h2>
                    </header>
                    <section class="counter-section">
                        <h2>Where can I buy cherrygift vouchers?</h2>
                        <p>Via our website. cherrygift.com</p>  
                        
                        <h2>Do I need to activate the cherrygift voucher?</h2>
                        <p>No, it is ready to use immediately.</p> 
                        
                        <h2>Can I buy the cherrygift voucher now and send later?</h2>
                        <p>Yes, you can schedule your cherrygift voucher to be sent at any date or time in the future. </p>
                        
                        <h2>What if I don’t use the whole amount?</h2>
                        <p>Unfortunately no change is given. Spend up!</p> 
                        
                        <h2>Can I exchange my cherrygift Voucher for cash?</h2>
                        <p>No, it’s a gift. Treat yourself!</p> 
                        
                        <h2>When does the cherrygift voucher expire?</h2>
                        <p>You have 12 months validity from the date the voucher is received. You will receive a reminder after 6 months.</p> 
                        
                        <h2>Are my details safe?</h2>
                        <p>Yes, this is a secure site. Your personal details remain with cherrygift and will not be sent or sold to any third party. All payments are made via Paypal secure site. </p> 
                        
                        <h2>How do I use my cherrygift voucher?</h2>
                        <p>Simply take your smartphone shopping and show your cherrygift voucher at the register. The assistant will then validate your voucher directly on your smartphone. Make sure you advise the retailer of your cherrygift Voucher when pre-booking.</p>
                        
                        <h2>Do I have to print out my cherrygift voucher?</h2>
                        <p>No, simply take your smartphone shopping!</p>
                        
                        <h2>What if the recipient doesn’t have a smartphone?</h2>
                        <p>Seriously? Who are they? Instead of a voucher, buy them a smartphone! Problem solved!</p>
                        
                        
                        <h2>What do I do if my cherrygift voucher is lost?</h2>
                        <p>Send us a quick email to redemptions@cherrygift.com and we will resend it to you via SMS.</p>
                        
                        <h2>Can I top up the cherrygift Voucher?</h2>
                        <p>No, you can’t top up but you can buy as many as you like!</p>
                        
                        <h2>How much does it cost to send?</h2>
                        <p>Each cherrygift voucher will only cost you $3.85! That’s less than a cup of coffee!</p>
                        
                        <h2>Will I get a receipt or tax invoice?</h2>
                        <p>Yes, it will be sent to your email address that you registered.</p>
                        
                        <h2>Can I order from outside Australia?</h2>
                        <p>Yes, we accept orders from overseas but can only be delivered to an Australian mobile number.</p>
                    </section>
              	</div>
          	</div>
        </div>
    
     </div>
 	
@include('admin-template.footer_include')
@include('admin-template.footer')    
