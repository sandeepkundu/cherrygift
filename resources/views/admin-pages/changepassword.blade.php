@extends('admin-template.defaultadmin')

                <title>cherryGift - Change password</title>
 
        
	@section('content')
<div class="container-fluid">
    <div class="page-wrapper">
<div class="row">
 <div class="col-sm-6 col-sm-offset-3">
 <div class="panel panel-default reset-cont">
     <div class="panel-heading" style="text-align:center"><b>Change your password</b></div>
 <div class="panel-body">
<style type="text/css">  
 .errors {color:#900}; 
 
</style> 
 @if (count($errors) > 0)
    <div class="alert alert-danger">
    
    <ul>
        @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
        @endforeach
         </ul>
        </div>
@endif

@if(Session::has('message'))
<div class="alert alert-success">
       {{Session::get('message')}}
    </div>
    @endif
<form class="form-horizontal" role="form" method="POST" action="{{url()."/admin/change-password"}}" novalidate="novalidate" id="register-form">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                    <input type="password" class="form-control" placeholder="Type your current password" name="old_password" id="old_password">
                </div>
        <label class="errors" for="old_password" generated="true"></label>
            </div>

<div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                    <input type="password" class="form-control" placeholder="Type your new password" name="password" id="password">
                </div>
    <label class="errors" for="password" generated="true"></label>
            </div>

            <div class="form-group mr-b30">
                <div class="input-group">
                    <span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                    <input type="password" class="form-control" placeholder="Re-type your new password" name="password_confirmation" id="password_confirmation">
                </div>
                <label class="errors" for="password_confirmation" generated="true"></label>
            </div>

            <button class="btn btn-block btn-success btn-lg">RESET PASSWORD</button>
 </form>

 </div>
 </div>
 </div>
 </div>
    </div>
</div>
@stop