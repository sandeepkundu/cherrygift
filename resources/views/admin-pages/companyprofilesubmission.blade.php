<!doctype html>
<html>
<head>
<meta charset="UTF-8">
@include('admin-template.header_include')
 <title>
            @yield('title') cherrygift - payment confirm
        </title>
</head>

<body class="bg-grey-login">
<div class="auth-wrapper">
	<header class="text-center logo-ctn">
    	<img src="{{ asset('images/admin-images/login-logo.png') }}" alt="">
    </header>
    @if(Session::has('message'))
<div class="alert alert-success">
       {{Session::get('message')}}
    </div>
    @endif
    
</div>
    
    <div id="PaymentConfirmationModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header ok_model">
        <button type="button" class="close" data-dismiss="modal" id="sucess-pay"></button>
      </div>
      <div class="modal-body text-center">
        <p>Your profile is submitted for approval to cherrygift.</p>
        <p>  Once approved, you will get an automated email.</p>
        
      </div>
      <div class="modal-footer">
          <button type="button" id="companyprofile_submit" class="btn btn-success ok-btn" >OK!</button>
      </div>
    </div>
  </div>
</div>
@include('admin-template.footer_include')
<script>
	(function($){
            $('#PaymentConfirmationModal').modal({backdrop: 'static', keyboard: false});
		$(window).load(function(){
			$('#PaymentConfirmationModal').modal('show');  
		});
			
	})(jQuery);
		
		
		
	</script>
        
</body>
</html>
