@include('admin-template.header_include')
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>cherrygift privacy policy</title>
        </head>
        @include('admin-template.header')
@section('content')
<div class="page-wrapper clearfix" id="termsCondition">
     	<div class="container">
        	<div class="row">
            	<div class="col-sm-10 col-sm-offset-1">
                          <header class="text-center">
                        <h2 class="page-heading text-center">PRIVACY POLICY</h2>

                    </header>
                <div class="termsCont">
                    <p>This web site is owned and operated by Fionnghal Pty Ltd ABN 52 146 304 181 (trading as ‘cherrygift’) and will be referred to as "We", "our" and "us" in this Internet Privacy Policy. By using this site, you agree to the Internet Privacy Policy of this web site ("the web site"), which is set out on this web site page. The Internet Privacy Policy relates to the collection and use of personal information you may supply to us through your conduct on the web site.</p>
<p>We reserve the right, at our discretion, to modify or remove portions of this Internet Privacy Policy at any time. This Internet Privacy Policy is in addition to any other terms and conditions applicable to the web site. We do not make any representations about third party web sites that may be linked to the web site.</p> 
<p>We recognise the importance of protecting the privacy of information collected about visitors to our web site, in particular information that is capable of identifying an individual ("personal information"). This Internet Privacy Policy governs the manner in which your personal information, obtained through the web site, will be dealt with. This Internet Privacy Policy should be reviewed periodically so that you are updated on any changes. We welcome your comments and feedback.</p>
                    <h4>Personal Information</h4>
                     <ul class="number">
                         <li><span>Personal information about visitors to our site is collected only when knowingly and voluntarily submitted. For example, we may need to collect such information to provide you with further services or to answer or forward any requests or enquiries. It is our intention that this policy will protect your personal information from being dealt with in any way that is inconsistent with applicable privacy laws in Australia. </span>
</li>
                  <h4>Use of Information</h4>
                         <li><span>Personal information that visitors submit to our site is used only for the purpose for which it is submitted or for such other secondary purposes that are related to the primary purpose, unless we disclose other uses in this Internet Privacy Policy or at the time of collection. Copies of correspondence sent from the web site, that may contain personal information, are stored as archives for record-keeping and back-up purposes only. </span></li>
                   <h4>Collecting information on registered users</h4>
                         <li><span>As part of registering with us, we collect personal information about you in order for you to take full advantage of our services. To do this it may be necessary for you to provide additional information to us as detailed below.</span></li>
                         <li><span>Registration<br>
                             Registration may include submitting your name, email address, address, telephone numbers, option on receiving updates and promotional material and and other information. You may access this information at any time by logging in and going to your account.</span> </li>
                         <h4>Credit Card Details</h4>
                         <li><span>Credit Card details are only stored for the processing of payment and will be deleted once payment is processed.Any payment required shall be facilitated through ‘Paypal’ and we will not request or have access to credit card details.</span> </li>
                         <h4>Disclosure</h4>
                         <li><span>Apart from where you have consented or disclosure is necessary to achieve the purpose for which it was submitted, personal information may be disclosed in special situations where we have reason to believe that doing so is necessary to identify, contact or bring legal action against anyone damaging, injuring, or interfering (intentionally or unintentionally) with our rights or property, users, or anyone else who could be harmed by such activities. Also, we may disclose personal information when we believe in good faith that the law requires disclosure.</span> </li>
                         <li><span>We may engage third parties to provide you with goods or services on our behalf. In that circumstance, we may disclose your personal information to those third parties in order to meet your request for goods or services. </span></li>
                       <h4>Security</h4>
                         <li><span>We strive to ensure the security, integrity and privacy of personal information submitted to our sites, and we review and update our security measures in light of current technologies. Unfortunately, no data transmission over the Internet can be guaranteed to be totally secure.</span> </li>
                         <li><span>However, we will endeavour to take all reasonable steps to protect the personal information you may transmit to us or from our online products and services. Once we do receive your transmission, we will also make our best efforts to ensure its security on our systems.</span> </li>
                         <li><span>In addition, our employees and the contractors who provide services related to our information systems are obliged to respect the confidentiality of any personal information held by us. However, we will not be held responsible for events arising from unauthorised access to your personal information.</span></li>
                        <h4>Collecting Information from Users</h4>
                        <li><span>IP Addresses<br>
                            Our web servers gather your IP address to assist with the diagnosis of problems or support issues with our services. Again, information is gathered in aggregate only and cannot be traced to an individual user.</span></li>
                   <li><span>Cookies and Applets<br>
We use cookies to provide you with a better experience. These cookies allow us to increase your security by storing your session ID and are a way of monitoring single user access.<br>
                       This aggregate, non-personal information is collated and provided to us to assist in analysing the usage of the site. </span></li>
                   <h4>Access to Information</h4>
                         <li><span>We will endeavour to take all reasonable steps to keep secure any information which we hold about you, and to keep this information accurate and up to date. If, at any time, you discover that information held about you is incorrect, you may correct that information by, in the first instance, logging in and going to your account and correcting such information or, if you are unable to log in and go to your account, you may contact us to have the information corrected.</span> </li>
                         <li><span>In addition, our employees and the contractors who provide services related to our information systems are obliged to respect the confidentiality of any personal information held by us.</span></li>
                   <h4>Links to other sites</h4>
                         <li><span>We provide links to Web sites outside of our web sites, as well as to third party Web sites. These linked sites are not under our control, and we cannot accept responsibility for the conduct of companies linked to our website. Before disclosing your personal information on any other website, we advise you to examine the terms and conditions of using that Web site and its privacy statement.</span> </li>
                   <h4>Problems or questions</h4>
                         <li><span>If we become aware of any ongoing concerns or problems with our web sites, we will take these issues seriously and work to address these concerns. If you have any further queries relating to our Privacy Policy, or you have a problem or complaint, please contact us.</span></li>
                         <li><span>For more information about privacy issues in Australia and protecting your privacy, visit the Australian Federal Privacy Commissioner's web site; <a href="http://www.privacy.gov.au/" target="_blank">http://www.privacy.gov.au/</a>.</span></li>
                    </ul>
                    
                </div>

            </div>

        </div>
          </div>
    </div>
 	
@include('admin-template.footer_include')
@include('admin-template.footer')    
