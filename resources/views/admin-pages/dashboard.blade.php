@extends('admin-template.defaultadmin')
@section('title','supplier dashboard')


@section('content')
<?php 
$monthNum= Route::getCurrentRoute()->getParameter('month'); 
$yearNum= Route::getCurrentRoute()->getParameter('year');
$monthName = date("F", mktime(0, 0, 0, $monthNum, 10));

$nextmonthNum= Route::getCurrentRoute()->getParameter('month'); 
$nextyearNum= Route::getCurrentRoute()->getParameter('year');
//$nextmonthName = date("F", mktime(0, 0, 0, $nextmonthNum, 10));
$date = mktime( 0, 0, 0, $nextmonthNum, 1, $nextyearNum );
$nextMnthYr = strftime( '%B, %Y', strtotime( '+1 month', $date ) );

 $date =  $yearNum.'-'.$monthNum;  $now = date("Y")."-".date("m");
?>
<section class="dashboard-top-table lg-container">
	<div class="dashboard-table-cell plan-detail">
            <div class="left-arrow"><a href="{{url()}}/admin/reports/{{ Route::getCurrentRoute()->getParameter('a') }}/{{$prev[1]}}/{{$prev[0]}}"><span class="fa fa-angle-left fa-2x" aria-hidden="true"></span></a></div>
    	<p class="period">{{$monthName}}, {{$yearNum}}</p>
        <p>MONTHLY REDEEMS</p>
        <h2 class="bold-text">${{number_format($total_redeem,0)}}</h2>
         <div class="right-arrow visible-xs"> @if($date < $now)
            <a href="{{url()}}/admin/reports/{{ Route::getCurrentRoute()->getParameter('a') }}/{{$next[1]}}/{{$next[0]}}"><span class="fa fa-angle-right fa-2x" aria-hidden="true"></span></a>
            @endif
        </div>
    </div>
    <div class="dashboard-table-cell">
    	<div id="carousel-dashbaord" class="" data-ride="carousel">
 			<!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active"><p>&nbsp;</p>
                  <p class="heading">REDEEM HISTORY</p>
                  <p style="color:black">
                  	Please note that the amount credit to your account will be minus<strong> 9% commission</strong> as per terms and conditions
				
                </p>
              </div>

            </div>
          
<!--             Controls 
            <a class="left carousel-control" href="{{url()}}/admin/reports/{{ Route::getCurrentRoute()->getParameter('a') }}/{{$prev[1]}}/{{$prev[0]}}" role="button" data-slide="prev">
              <span class="fa fa-angle-left fa-2x" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="{{url()}}/admin/reports/{{ Route::getCurrentRoute()->getParameter('a') }}/{{$next[1]}}/{{$next[0]}}" role="button" data-slide="next">
              <span class="fa fa-angle-right fa-2x" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>-->
		</div>
    </div>
    <div class="dashboard-table-cell due-date"><p>&nbsp;</p><p>&nbsp;</p>
         <p>NEXT PAYMENT</p>
    	<h2 class="bold-text">@if($totalpay!='NA') ${{number_format($totalpay,2)}} @else {{$totalpay}} @endif </span></h2>
        <p class="period">15th {{$nextMnthYr}}</p>
     <div class="right-arrow hidden-xs"> @if($date < $now)
            <a href="{{url()}}/admin/reports/{{ Route::getCurrentRoute()->getParameter('a') }}/{{$next[1]}}/{{$next[0]}}"><span class="fa fa-angle-right fa-2x" aria-hidden="true"></span></a>
            @endif
        </div>
    </div>
</section>

<section class="container dashboard-purchaser-section page-wrapper clearfix">
	<div class="">
    	<div class="col-sm-offset-1 col-sm-10">
        	<ul class="purchaser-list list-unstyled">
            	<li class="header clearfix">
                	<ul class="list-unstyled">
                    	<li class="col-sm-3 col-xs-3">
                        	Purchaser
                        </li>
                        <li class="col-sm-3 col-xs-3">
                            Voucher ID
                        </li>
                        <li class="col-sm-3 col-xs-3">
                        Redeemed Date
                        </li>
                        <li class="col-sm-3 col-xs-3">
                        	Total Voucher Amount
                        </li>
                    </ul>
                </li>
                
                
                @if(count($get_redeemed_history)!=0)
                @foreach($get_redeemed_history as $redeemed_history)
                <li class="clearfix">
                	<ul class="list-unstyled">
                    	<li class="col-sm-3 col-xs-3 purchaser">
                        	{{$redeemed_history->first_name}} {{$redeemed_history->last_name}}
                            <p class="redeem-date">{{date('d F  Y', strtotime($redeemed_history->updated_at))}}</p>
                        </li>
                        <li class="col-sm-3 col-xs-3 purchaser">
                            {{$redeemed_history->voucher_unique_id}}
                        </li>
                        <li class="col-sm-3 redeem-col redeem-date">
                        	 {{date('d F  Y', strtotime($redeemed_history->updated_at))}}
                        </li>
                        <li class="col-sm-3 col-xs-3 price ">
                        	${{number_format($redeemed_history->voucher_price,0)}}
                        </li>
                    </ul>
                </li>
                    @endforeach
                    @else
                    
                        <li class=" purchaser brd_0 text-center">History not available</li>
                    @endif
                
                
                
                
                
                
                
            </ul>
        </div>
 	</div>
</section>
@stop