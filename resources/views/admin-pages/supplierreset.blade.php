@extends('admin-template.defaultadmin')

                <title>cherryGift - Change password</title>
 
        
	@section('content')
<div class="container-fluid">
    <div class="page-wrapper">
<div class="row">
 <div class="col-sm-6 col-sm-offset-3">
 <div class="panel panel-default reset-cont">
     <div class="panel-heading" style="text-align:center"><b>Reset password</b></div>
 <div class="panel-body">

 @if (count($errors) > 0)
    <div class="alert alert-danger">
    
    <ul>
        @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
        @endforeach
         </ul>
        </div>
@endif

@if(Session::has('message'))
<div class="alert alert-success">
       {{Session::get('message')}}
    </div>
    @endif
<form class="form-horizontal" role="form" method="POST" action="{{url()."/supplier/password/reset"}}" novalidate="novalidate" id="register-form">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" class="form-control" name="email" value="<?php echo $_GET['mail']; ?>">
    <input type="hidden" class="form-control" name="token" value="{{$token}}">
   <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                    <input type="password" class="form-control" placeholder="Type your new password" name="password" required="required" id="password"><span style="color: red" id="password_error"></span>

                </div>
     <label class="errors" for="password" generated="true"></label>
            </div>
<div class="form-group mr-b30">
                <div class="input-group">
                    <span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                    <input type="password" name="password_confirmation" required="required" id="password_confirmation" class="form-control" placeholder="Re-type your new password">
                </div>
    <label class="errors" for="password_confirmation" generated="true"></label>
            </div>
    <button class="btn btn-block btn-success btn-lg" type="submit" id="send_resetpass">RESET PASSWORD</button>
 </form>

 </div>
 </div>
 </div>
</div>
 </div>
</div>
@stop