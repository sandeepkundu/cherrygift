<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        @include('admin-template.header_include')
        <title>
            @yield('title') cherrygift - invoice sent
        </title>
    </head>

    <body class="bg-grey-login">
        <div class="auth-wrapper">
            <header class="text-center logo-ctn">
                <img src="{{ asset('images/admin-images/login-logo.png') }}" alt="">
            </header>
            @if(Session::has('message'))
            <div class="alert alert-success">
                {{Session::get('message')}}
            </div>
            @endif

        </div>

        <div id="PaymentConfirmationModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header ok_model">
                        <button type="button" class="close" data-dismiss="modal" id="sucess-pay"></button>
                    </div>
                    <div class="modal-body text-center">
                        <h2>Invoice #{{$payment->invoiceNo()}}</h2>
                        <p>
                            Thank you for your submission.  An invoice has been forwarded to your 
                            email address <strong>{{$email}}</strong> for payment.  
                        </p>
                        <p>
                            Check your SPAM and Junk mail folders if you have trouble locating the email.<br />
                            Please also add 
                            {{env('contact_email')}} to your contact list to ensure delivery.</p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{url()}}/admin/contact-info"><button type="button" class="btn btn-success ok-btn" >OK!</button></a>
                    </div>
                </div>
            </div>
        </div>
        @include('admin-template.footer_include')
        <script>
            (function($){
                $(window).load(function(){
                    $('#PaymentConfirmationModal').modal({backdrop: 'static', keyboard: false});
                    $('#PaymentConfirmationModal').modal('show');  
                });

            })(jQuery);



        </script>

    </body>
</html>
