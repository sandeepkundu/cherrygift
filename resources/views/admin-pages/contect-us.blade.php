@include('admin-template.header_include')
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>cherrygift contact us</title>
        </head>
@include('admin-template.header')
@section('content')
<div class="page-wrapper clearfix" id="innerContact">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <header class="text-center">
                    <h2 class="page-heading">CONTACT US</h2>
                </header>
                <section>
                    <h2>Have a question for us? Check out our <a href="{{url()}}/admin/faq">Frequently Asked Questions</a> and if your answer isn’t there...</h2>
                </section>
                <section class="company-section">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6 company-info">
                                <h4 class="heading mr-0">CONTACT US</h4>
                                <ul class="info-list contact-info">
                                    <li><span class="email"></span><a href="mailto:{{env('contact_email')}}">{{env('contact_email')}}</a></li>
                                    <li><span class="telephone"></span>1300 122 321</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 company-info">
                                <h4 class="heading mr-0">WE ARE OPEN</h4>
                                <ul class="info-list time-info">
                                    <li><span class="day">Monday - Friday</span>09:00 - 17:00 (AEST)</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>
@include('admin-template.footer_include')
@include('admin-template.footer')




