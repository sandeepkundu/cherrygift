<!doctype html>

@include('admin-template.header_include')
<title>cherrygift supplier login/sign up</title>
@include('admin-template.header')
@section('content')

<style>
.heading{
        margin-top: 0px; 
}
.heading-color{
   color:#F13F31 !important; 
   font-weight: 600;
}
.main-heading{
    text-align:center;background-color:#F13F31;color:#fff;

}
.main-heading h1,h3{
  font-weight: 700;
    
}
</style>

<body class="body-suplier">
    <div class="supplierImg lg-container">
        <!-- <img src="{{ asset('images/admin-images/cherry-banner.jpg') }}" alt=""> -->
        <img src="{{ asset('images/admin-images/business-banner.png') }}" alt="">
        <div class="main-heading" style="">
            <br>
            <h1 class="heading">CONGRATULATIONS!</h1>
            <h3 class="heading">You have just made a great business decision.</h3><br>
        </div>
    </div>
    <div class="container">
        
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                <div class="page-wrapper landing-wrapper pad-top0">
                    <section class="landing-top-section clearfix">
                        <!--        			<img src="../admin-images/landing-img.png" alt="" >-->
                        
                        <h2 class="heading heading-color">Why?</h2>
                        <p class="mr-b20"><b>Because we are about to make your life a whole lot easier!</b> </p>
                        <p class="mr-b20">Love the business you get from Gift Vouchers but hate the work associated with them? Paper trails, purchasing plastic cards, mail-outs, correspondence regarding lost vouchers, time consuming, the list is endless!</p> 
                        <p class="mr-b20">Imagine there was a way to have all the hard work done for you. From advertising your business, selling your digital vouchers, tracking their usage and the money in your account each month. Imagine no more! All this for only $20 + GST per month registration fee, plus 9% service fee per voucher redeemed. No hidden costs.</p>
                        <p class="mr-b10">Anyone online worldwide can purchase a voucher from any device for your business and send it via SMS to a loved one anywhere in Australia. How easy is that! They can send it immediately or schedule for up to twelve months in advance.</p>
                    </section>
                    <section class="landing-about-section clearfix">
                        <h2 class="heading heading-color">Need the stats?</h2>
                        <p class="mr-b20">*87% of all Australians aged 14 to 49 own a smartphone<br> 
                            *94% of Australian text messages are opened within 5 minutes<br> 
                            *$2.5b worth of vouchers purchased annually in Australia </p>
                        <p>*source Roy Morgan Research (Aug 2015)</p>

                        <h2 class="heading heading-color">Let’s go!</h2>
                        <ul class="item-list">
                            <p style="margin-left: -18px;">You have two options..</p>
                            <li><span> Either email sales@cherrygift.com with your approval to go your listing and a sales agent will organise this for you. </span></li>
                           <br> <p style="margin-left: -18px;">Or, DIY </p><br>
                            <li><span> Click 'Supplier Sign up/Login' at the top right hand corner or bottom of this page </span></li>
                            <li><span>Enter your email address (this will be your login email) and create a password</span></li>
                            <li><span>Enter your Company Contact Information and take note of your Company PIN</span></li>
                            <li><span>Enter your Site/Store Details and take note of your Site/Store PIN (These numbers combined are your unique 8 digit Merchant PIN) </span></li>
                            <li><span>Click ‘Submit and Preview’ (This is a preview of what your customers will see so check all details are correct)</span></li>
                            <li><span> Either pay annual subscription fee via PayPal or simply request an invoice </span></li>
                            <li><span>Please allow up to 48 hours for your profile to be approved and live</span></li>
                            <li><span>You will then receive a confirmation email with your 8 digit MERCHANT PIN (combination of Company PIN and Site/Store PIN)</span></li>              
                            <li><span>Please look out for your welcome pack (via snail mail so allow some time) </span></li>
                        </ul>
                        <h2 class="heading heading-color">How to Redeem</h2>
                        <ul class="item-list">
                            <li><span>Your customers will approach your registers with their cherrygift voucher displayed on their smartphone (data or wifi is required)</span></li>
                            <li><span>Your staff member will press the gift icon on your customers phone and enter your 8 digit MERCHANT PIN </span></li>
                            <li><span>Ensure the balance displays $0.00.</span></li>        
                        </ul>
                        <h2 class="heading heading-color">Important Information</h2>
                        <ul class="item-list">
                            <li><span>Please ensure that all service staff are aware of your MERCHANT PIN</span></li>
                            <li><span>All vouchers are to be redeemed in full</span></li>
                            <li><span>Suppliers must accept both company branded and universal cherrygift vouchers</span></li>  
                            <li><span>Companies will be paid for all vouchers redeemed monthly </span></li>        
                        </ul>
                        <h2 class="heading heading-color">Need more?</h2>
                        <p class="mr-b20">cherrygift is an Australian owned and operated company with our head office based in Cairns, Queensland.<br> 
                            We are partnered with PayPal to ensure your financial information has the highest level of online security.<br>
                            We are saving the planet one cherrygift voucher at a time. No paper vouchers, no plastic cards simply 100% digital!<br>
                            We also send a six monthly reminder via SMS for unredeemed vouchers.
                        </p>
                        <h2 class="heading heading-color">Ready?</h2>
                        <p>To get started, you will need:</p>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <ul class="item-list">
                                    <li><span>Your Logo (200x200px)</span></li>
                                    <li><span>4 Images of your business/products (600x400px exact) This will also include one hero image for your gift voucher</span></li>
                                    <li><span>ABN/ACN</span></li>
                                    <li><span>Bank Account details (so we can pay you)</span></li>
                                    <!--<li><span>PayPal Account</span></li>-->
                                </ul>
                            </div>
                        </div><br>
                        <center><div class="sign-login" style="width: 40%;">
                            <span style="text-align:center;">
                                <a href="{{url()}}/admin-signup" style="font-size:20px !important;font-weight:700" class="">Click here to sign up!</a>
                            </span>
                        </div>
                        </center><br>
                    </section>
                </div>
            </div>
        </div>
    </div>
</body>
@include('admin-template.footer_include')
@include('admin-template.footer')


