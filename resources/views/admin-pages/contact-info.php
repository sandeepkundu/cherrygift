<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>cherrygift</title>
<?php include("../admin-template/header_include.php"); ?>
</head>
<body>
<?php include("../admin-template/header.php"); ?>
<div class="container">
	<div class="row">
    	<div class="col-sm-offset-2 col-sm-8">
        	<h3 class="page-heading">Contact Information</h3>
            <form class="form-horizontal">
            	<div class="form-group">
                    <label class="col-sm-4 control-label">First Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Enter your first name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Last Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Enter your last name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Contact Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="Enter your email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Contact Number</label>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-5">
                                <select class="selectpicker" data-width="100%">
                                    <option>+1</option>
                                </select>
                            </div>
                            <div class="col-xs-7">
                                <input type="text" class="form-control" placeholder="444 768 984">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Company Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" placeholder="Enter your company email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Trading Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Enter your trading name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">ABN / ACN numbers</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="111 333 555 54">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Company Address</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Enter your address">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Location</label>
                    <div class="col-sm-8">
                        <select class="selectpicker" data-width="100%">
                            <option>state</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Suburb</label>
                    <div class="col-sm-8">
                        <select class="selectpicker" data-width="100%">
                            <option>suburb</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Postcode</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Postcode">
                    </div>
                </div>
                
                <div class="form-group">
                	<label class="col-sm-12 control-label pd-tb-30">Already signed off locations</label>
              	</div>
                
                <div class="form-group">
                    <label class="col-sm-4 control-label">Signed off location #1</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="ABC">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Signed off location #2</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="CDE">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Account name</label>
                    <div class="col-sm-8">
                    	<div class="form-group">
                        	<div class="col-sm-12">
                        		<input type="text" class="form-control" placeholder="Account name">
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-xs-6">
                        		<input type="text" class="form-control" placeholder="BSB">
                            </div>
                            <div class="col-xs-6">
                        		<input type="text" class="form-control" placeholder="Account number">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mr-b30">
                    <label class="col-sm-4 control-label">Category</label>
                    <div class="col-sm-8">
                    	<div class="row">
                       		<div class="ck-button col-sm-6">
                              <label>
                                  <input type="checkbox" value="1"><span class="ck-btn">Category name <span class="check-tick"></span></span>
                              </label>
                              </div>
                              <div class="ck-button col-sm-6">
                              <label>
                                  <input type="checkbox" value="1"><span class="ck-btn">Category name <span class="check-tick"></span></span>
                              </label>
                              </div>
                              <div class="ck-button col-sm-6">
                              <label>
                                  <input type="checkbox" value="1"><span class="ck-btn">Category name <span class="check-tick"></span></span>
                              </label>
                              </div>
                              <div class="ck-button col-sm-6">
                              <label>
                                  <input type="checkbox" value="1"><span class="ck-btn">Category name <span class="check-tick"></span></span>
                              </label>
                              </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-sm-8 col-sm-offset-2">
                		<button class="btn btn-block btn-success submit-btn btn-lg">Next</button>
                    </div>
                </div>
          	</form>
        </div>
    </div>
</div>

<?php include("../admin-template/footer.php"); ?>
<?php include("../admin-template/footer_include.php"); ?>
<body>
</body>
</html>