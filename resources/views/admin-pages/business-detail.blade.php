<?php
use App\Model\CompanyBusinessImages;
?> 
@extends('admin-template.defaultadmin')
@section('title','Site store details')
<?php
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>

@section('content')

@if (Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li style="align: center;">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (Session::has('message'))
<div class="alert alert-success">
    <p style="align-content: center;">{{ Session::get('message') }}</p>
</div>
@endif
@section('bodyclass','class=""')

<div class="container">
    <div class="row">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="page-wrapper" id="business-detail">
                <h3 class="page-heading">Site/Store Details</h3>
                @if(empty($companybusinessprofile->id))
                <form class="form-horizontal" method="post" action="{{url()}}/admin/update-business-details" enctype="multipart/form-data">
                <input type="hidden" name="merchant_pin" id="merchant_pin"  value="{{$company->pin}}{{$storepin}}">

                @else
                <form class="form-horizontal" method="post" action="{{url()}}/admin/update-business-details/{{@$companybusinessprofile->id}}" enctype="multipart/form-data">
                <input type="hidden" name="merchant_pin" id="merchant_pin"  value="{{$companybusinessprofile->merchant_pin}}">
                <input type="hidden" name="store_id" id="store_id"  value="{{$companybusinessprofile->id}}">
                @endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group ">
                    <label class="col-sm-offset-4 col-sm-8 control-label" style="align:center;"><span align="center" style="color: red;" id="cls_error" class="error"></span></label>

                </div>
                <div class="form-pd-wrapper">
                    <div class="form-group mr-b30">
                        <div class="col-sm-6 col-sm-offset-4">
                            <div class="upload-logo-ctn">

                                <img id="img-logo-id" @if(empty($companybusinessprofile->logo)) src="{{ asset('images/admin-images/demo-avatar.jpg')}}" @else src="{{$s3_url}}{{$s3_env}}{{$companybusinessprofile->logo}}" @endif  class="img-rounded" alt="" width="90" height="90">
                                <span class="btn btn-file btn-link">
                                    <input type="file" class="business-logo-upload" name="logo" id="business-logo" @if(empty($companybusinessprofile->logo)) value="" @else value="{{$s3_url}}{{$s3_env}}{{$companybusinessprofile->logo}}" @endif >

                                    <input type="hidden" name="category_id" value="{{$company->category_id}}">
                                    <input type="hidden" name="company_id" id="company_id" value="{{$company->id}}">
                                    <input type="hidden" name="company_pin" id="company_id" value="{{$company->pin}}">
                                    Upload Logo<em><span style="color:red;">*</span></em></span>
                            </div>
                            <p class="logo-info">Please provide as JPEG or PNG. The minimum dimensions 200 X 200.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Site/Store Name<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control custom_number" placeholder="Enter Trading Name" id="store_location" name="store_location" value="{{@$companybusinessprofile->store_location}}">
                            <span style="color: red" id="store_location_error" class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Brief description<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="6" placeholder="Brief description (300 characters allowed)" maxlength="300" name="short_desc" id="brief_description">{{ $companybusinessprofile->short_desc }}</textarea>
                            <span style="color: red" id="brief_description_error" class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Long description<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="6" placeholder="Long description (2000 characters allowed)" maxlength="2000" name="long_desc" id="long_desc">{{ $companybusinessprofile->long_desc }}</textarea>
                            <span style="color: red" id="long_desc_error" class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Special Instructions <br/>(Optional)</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="6" placeholder="Special Instructions (2000 characters allowed)" maxlength="2000" name="special_desc" >{{ $companybusinessprofile->special_desc }}</textarea>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mr-b20 clearfix">
                            <label class="col-sm-6 control-label">Upload company pictures</label>
                            <p class="col-sm-12 upload-company-text">
                                Please upload images of max size 3MB and with dimensions <?= CompanyBusinessImages::$IMAGE_WIDTH ?> X <?= CompanyBusinessImages::$IMAGE_HEIGHT ?>
                            </p>
                        </div>
                        <div class="mr-b20 clearfix img-error"  id="img_error" style="display: none;">

                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6 upload-company-ctn">
                                    <input type="hidden" name="hidden_first_image" id="hidden_first_image" />
                                    <input type="hidden" name="hidden_second_image" id="hidden_second_image" />
                                    <input type="hidden" name="hidden_third_image" id="hidden_third_image" />
                                    <input type="hidden" name="hidden_fourth_image" id="hidden_fourth_image" />
                                    @if(!empty($businessimagesarr[1]))
                                    <?php
                                    $check_img=CompanyBusinessImages::validateImage("{$s3_url}{$s3_env}{$businessimagesarr[1]}");
                                    ?>
                                    <div class="business-company-upload1 load-img">
                                        <div class="company-img">
                                            <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{$businessimagesarr[1]}}">
                                        </div>
                                    </div>
                                    <a class="close-img fa fa-times" id="comp-img-1" img-tag='first_image' style="display:block"></a>
                                    <div class="show_img" style="display: none">
                                        <span class="btn btn-file upload-company-btn" id="file1"><i class="upload-icon"></i>Upload
                                            <input type="file" id="business-company-upload1" class="store_ajax_img_upload" loader-id='temp_loader_first_image'  number="1" name="first_image">
                                        </span>
                                    </div>
                                    @else
                                    <?php
                                    $check_img=true;
                                    ?>
                                    <div class="business-company-upload1 load-img" style="display: none;">

                                    </div>
                                    <a class="close-img fa fa-times" id="comp-img-1" img-tag='first_image' ></a>
                                    <div class="show_img" >
                                        <span class="btn btn-file upload-company-btn" id="file1"><i class="upload-icon"></i>Upload
                                            <input type="file" id="business-company-upload1" class="store_ajax_img_upload" loader-id='temp_loader_first_image' number="1" name="first_image" >
                                        </span>
                                    </div>
                                    @endif
                                    <p class="upload-info-label">This will be your voucher image</p>
                                    <p class="img-error"><?= $check_img===true ? '&nbsp;' : $check_img ?></p>
                                </div>
                                <div class="col-sm-6 upload-company-ctn">
                                    @if(!empty($businessimagesarr[2]))
                                    <?php
                                    $check_img=CompanyBusinessImages::validateImage("{$s3_url}{$s3_env}{$businessimagesarr[2]}");
                                    ?>
                                    <div class="business-company-upload2 load-img">
                                        <div class="company-img">
                                            <!--                                                    <div id="temp_loader_second_image" class="img-loader-ctn">
                                            <i class="fa fa-spinner img-spinner fa-2x fa-spain"></i>
                                            </div>-->
                                            <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{$businessimagesarr[2]}}">
                                        </div></div>
                                    <a class="close-img fa fa-times" id="comp-img-2" img-tag='second_image' style="display:block" custom_val="{{$companybusinessprofile->id}}"></a>
                                    <div class="show_img" style="display: none">
                                        <span class="btn btn-file upload-company-btn" id="file2"><i class="upload-icon"></i>Upload
                                            <input type="file" id="business-company-upload2" class="store_ajax_img_upload" loader-id='temp_loader_second_image' number="2" name="second_image">
                                        </span>
                                    </div>
                                    @else
                                    <?php
                                    $check_img=true;
                                    ?>
                                    <div class="business-company-upload2 load-img"> </div>
                                    <a class="close-img fa fa-times" id="comp-img-2" img-tag='second_image' ></a>
                                    <div class="show_img" >
                                        <span class="btn btn-file upload-company-btn" id="file2"><i class="upload-icon"></i>Upload
                                            <input type="file" id="business-company-upload2" class="store_ajax_img_upload" loader-id='temp_loader_second_image' number="2" name="second_image">
                                        </span>
                                    </div>
                                    @endif
                                    <p class="img-error"><?= $check_img===true ? '&nbsp;' : $check_img ?></p>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 upload-company-ctn">

                                    @if(!empty($businessimagesarr[3]))
                                    <?php
                                    $check_img=CompanyBusinessImages::validateImage("{$s3_url}{$s3_env}{$businessimagesarr[3]}");
                                    ?>
                                    <div class="business-company-upload3 load-img">
                                        <div class="company-img">
                                            <!--                                                    <div id="temp_loader_third_image" class="img-loader-ctn">
                                            <i class="fa fa-spinner img-spinner fa-2x fa-spain"></i>
                                            </div>-->
                                            <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{$businessimagesarr[3]}}">
                                        </div></div>
                                    <a class="close-img fa fa-times" img-tag='third_image' id="comp-img-3" style="display:block" custom_val="{{$companybusinessprofile->id}}"></a>
                                    <div class="show_img" style="display: none">
                                        <span class="btn btn-file upload-company-btn" id="file3"><i class="upload-icon"></i>Upload
                                            <input type="file" id="business-company-upload3" class="store_ajax_img_upload" loader-id='temp_loader_third_image' number="3" name="third_image">
                                        </span>
                                    </div>
                                    @else
                                    <?php
                                    $check_img=true;
                                    ?>
                                    <div class="business-company-upload3 load-img"> </div>
                                    <a class="close-img fa fa-times" img-tag='third_image' id="comp-img-3" ></a>
                                    <div class="show_img" >
                                        <span class="btn btn-file upload-company-btn" id="file3"><i class="upload-icon"></i>Upload
                                            <input type="file" id="business-company-upload3" loader-id='temp_loader_third_image' class="store_ajax_img_upload" number="3" name="third_image">
                                        </span>
                                    </div>
                                    @endif
                                    <p class="img-error"><?= $check_img===true ? '&nbsp;' : $check_img ?></p>

                                </div>	
                                <div class="col-sm-6 upload-company-ctn">

                                    @if(!empty($businessimagesarr[4]))
                                    <?php
                                    $check_img=CompanyBusinessImages::validateImage("{$s3_url}{$s3_env}{$businessimagesarr[4]}");
                                    ?>

                                    <div class="business-company-upload4 load-img">
                                        <div class="company-img">
                                            <!--                                                    <div id="temp_loader_fourth_image" class="img-loader-ctn">
                                            <i class="fa fa-spinner img-spinner fa-2x fa-spain"></i>
                                            </div>-->
                                            <img alt="" class="img-rounded" src="{{$s3_url}}{{$s3_env}}{{$businessimagesarr[4]}}">
                                        </div></div>
                                    <a class="close-img fa fa-times" id="comp-img-4" img-tag='fourth_image' style="display:block" custom_val="{{$companybusinessprofile->id}}"></a>
                                    <div class="show_img" style="display: none">
                                        <span class="btn btn-file upload-company-btn" id="file4"><i class="upload-icon"></i>Upload
                                            <input type="file" id="business-company-upload4" loader-id='temp_loader_fourth_image' class="store_ajax_img_upload" number="4" name="fourth_image">
                                        </span>
                                    </div>
                                    @else
                                    <?php
                                    $check_img=true;
                                    ?>
                                    <div class="business-company-upload4 load-img"> </div>
                                    <a class="close-img fa fa-times" id="comp-img-4" img-tag='fourth_image' ></a>
                                    <div class="show_img" >
                                        <span class="btn btn-file upload-company-btn" id="file4"><i class="upload-icon"></i>Upload
                                            <input type="file" id="business-company-upload4" loader-id='temp_loader_fourth_image' class="store_ajax_img_upload" number="4" name="fourth_image">
                                        </span>
                                    </div>
                                    @endif
                                    <p class="img-error"><?= $check_img===true ? '&nbsp;' : $check_img ?></p>

                                </div>                                
                            </div>

                        </div>
                    </div>

                    @if (@$companybusinessprofile->is_payment == "1")
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Merchant PIN</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control country-pin" placeholder="139382" readonly  name="merchant_pin" id="merchant_pin"  value="{{$companybusinessprofile->merchant_pin}}" style="background-color: lightgrey">
                        </div>
                    </div>

                    @endif
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Site/Store PIN</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control country-pin" placeholder="139382" readonly  name="store_pin" style="background-color: lightgrey" id="store_pin" @if(empty($companybusinessprofile->store_pin)) value="{{$storepin}}" @else value="{{$companybusinessprofile->store_pin}}" @endif>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Are you nation wide?<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <div class="row">
                               
                                <div class="col-sm-12">
                                    <div class="ck-button">

                                      <div class="col-sm-6">
                                        <label>
                                            <input type="radio" value="Yes" class="" name="nation_wide" checked="checked" <?php echo ($companybusinessprofile->nation_wide=='Yes')?'checked':'' ?>><span class="ck-btn">Yes </span>
                                        </label>
                                      </div>
                                        
                                       
                                         
                                       <div class="col-sm-6">
                                        <label>
                                            <input type="radio" value="No" class="" name="nation_wide" <?php echo ($companybusinessprofile->nation_wide=='No')?'checked':'' ?>><span class="ck-btn">No </span>
                                        </label> 
                                      
                                    </div>


                                    </div>
                                </div>
                                
                            </div>
                            <span style="color: red" id="nation_error" class="error"></span>
                        </div>
                    </div>




                    <div class="form-group">
                        <label class="col-sm-4 control-label">Email<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" placeholder="email@email.com" name="email" id="email" value="{{$companybusinessprofile->email}}">
                            <span style="color: red" id="email_error" class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Business website</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="www.abc.com" name="website" id="website" value="{{$companybusinessprofile->website}}">
                            <span style="color: red" id="website_error" class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Phone Number<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-xs-4">
                                    <input type="text" class="form-control" id="countrycode" name="country_code" value="+61"  readonly style="background-color: lightgrey">

                                </div>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control custom_number" placeholder="4444 768 984" name="phone_number"  id="phone_number" maxlength="20" value="{{$companybusinessprofile->phone}}">
                                    <span style="color: red" id="contact_error" class="error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Address<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control custom_number" placeholder="Enter your address" id="address" name="address" value="{{$companybusinessprofile->address}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Location<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <select class="selectpicker" data-width="100%" id="state1" name="state">
                                <option value="">Select State</option>
                                @foreach(App\Model\States::orderBy('state_name', 'asc')->get() as $location)
                                @if ($location->id == $companybusinessprofile->state_id) 
                                <option value="{{$location->id}}" selected="selected">{{$location->state_name}}</option>
                                @else
                                <option value="{{$location->id}}">{{$location->state_name}}</option>
                                </label>
                                @endif

                                @endforeach
                            </select><span style="color: red" id="state_error" class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Suburb<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control custom_number" placeholder="Suburb" id="city" name="city" autocomplete="off" value="{{$suburbsname }}"> 
                            <span style="color: red" id="suburbs_error" class="error"></span>
                            <input type="hidden" id="txtAllowSearch" name="suburb" value="{{$companybusinessprofile->city_id}}">
                        </div>
                        <div id="suggesstion-box"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Postcode<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control custom_number" placeholder="Postcode" name="post_code" id="post_code" maxlength="4" value="{{$companybusinessprofile->post_code}}"><span style="color: red" id="postcode_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Occasion<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <div class="row">
                                @foreach(App\Model\Occasion::orderBy('occation_name', 'asc')->get() as $oca)
                                <div class="col-sm-6">
                                    <div class="ck-button">
                                        @if((in_array($oca->id,$occarr)))
                                        <label>
                                            <input type="checkbox" value="{{$oca->id}}" name="occation_name[]" checked="checked"><span class="ck-btn">{{$oca->occation_name}} </span>
                                        </label>
                                        @else
                                        <label>
                                            <input type="checkbox" value="{{$oca->id}}" name="occation_name[]"><span class="ck-btn">{{$oca->occation_name}} </span>
                                        </label> @endif
                                    </div>
                                </div>

                                @endforeach
                            </div>
                            <span style="color: red" id="occation_error" class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Recipient<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <div class="row">
                                @foreach(App\Model\Recipient::where('is_top', '=', '1')->get() as $rec)
                                <div class="col-sm-6">
                                    <div class="ck-button">

                                        @if (in_array($rec->id,$recarr)) 
                                        <label>
                                            <input type="checkbox" value="{{$rec->id}}" name="recpient_name[]" checked="checked"><span class="ck-btn">{{$rec->name}} </span>
                                        </label> 
                                        @else
                                        <label>
                                            <input type="checkbox" value="{{$rec->id}}" name="recpient_name[]"><span class="ck-btn">{{$rec->name}} </span>
                                        </label> 
                                        @endif



                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <span style="color: red" id="receipient_error" class="error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Subscription</label>
                        <div class="col-sm-8">
                            <div class="pos-rel">
                                <input type="text" class="form-control" name="subscription_amount" value="${{$subs_price}} inc GST" readonly style="background-color: lightgrey">
                                @if(!empty($paypal_profile_id)) 
                                <span class="cancel-txt"><a style="color: red;" href="{{url()}}/payment/cancel/{{$paypal_profile_id}}/{{$companybusinessprofile->id}}" onclick="return confirm('Are you sure you want to cancel your subscription?')"> CANCEL SUBSCRIPTION</a></span>

                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Set minimum limit<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <div class="right-inner-addon">
                                <span>$</span>
                                <input type="number" min="1" class="form-control custom_number" name="minimum_amount" id="minimum_amount" placeholder="5" @if(!empty($companybusinessprofile->min_vouchar_amount)) value="{{$companybusinessprofile->min_vouchar_amount}}" @endif><span style="color: red" id="minimum_amount_error"></span>
                            </div></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Set maximum limit<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8"><div class="right-inner-addon">
                                <span>$</span>
                                <input type="number" min="1" class="form-control custom_number" name="maximum_amount" id="maximum_amount" placeholder="500" @if(!empty($companybusinessprofile->max_vouchar_amount)) value="{{$companybusinessprofile->max_vouchar_amount}}" @endif ><span style="color: red" id="maximum_amount_error"></span>
                            </div></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Increment amount</label>
                        <div class="col-sm-8"><div class="right-inner-addon">
                                <span>$</span>
                                <input type="text" class="form-control" name="incremental_amount" id="incremental_amount" value="10" readonly="readonly" style="background-color: lightgrey"><span style="color: red" id="incremental_amount_error"></span>
                            </div></div>
                    </div>
                    <div class="form-group mr-b30">
                        <label class="col-sm-4 control-label">Opening hours<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8 opening-hours-ctn">
                            <div class="row">
                                <div class="col-sm-6">

                                    @for($j=1; $j<=7; $j++)

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">{{Config::get('constants.days.'.$j)}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control custom_number" placeholder="08:00 - 18:00" name="days[]" @if(isset($daystimings)) value="{{$daystimings[$j]}}" @endif>
                                        </div>
                                    </div>

                                    @if($j==4)                                     
                                </div>
                                <div class="col-sm-6">
                                    @endif
                                    @endfor
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 control-label">
                        @if(!empty($companybusinessprofile->id) && ($companybusinessprofile->is_subscribed==2))

                        <div class="col-sm-6 control-label mr-b30"> <button class="btn btn-block btn-success btn-lg disabledButton" id="save-bussiness-info" >SAVE</button></div> 
                        <div class="col-sm-6 control-label mr-b30"> <button class="btn btn-block btn-success btn-lg" id="save-bussiness-info" name="preview" value="preview">PREVIEW</button></div>
                        <!--           <div class="col-sm-4 control-label mr-b30 text-center"><a href="{{url()}}/payment/subscribeagain/{{$companybusinessprofile->id}}"><img src="{{asset('images/payPal-button.png')}}" alt="Pay using Paypal" title="Subscribe again"></a></div>
                        <div class="col-sm-4 control-label text-center"><a class="btn btn-success btn-block btn-lg" href="{{url()}}/payment/subscribeagain/{{$companybusinessprofile->id}}"><img src="src="{{asset('images/paypal.png')}}" alt="Pay using Paypal" title="Subscribe again""></a></div>-->
                        @elseif(!empty($companybusinessprofile->id))
                        <!--                                <div class="col-sm-6 control-label mr-b30"> <button class="btn btn-block btn-success btn-lg disabledButton" id="save-bussiness-info" >SAVE</button></div>-->
                        @if($companybusinessprofile->is_subscribed == 0 || $companybusinessprofile->is_payment == 0)
                        <div class="col-sm-8 control-label mr-b30 col-sm-offset-4"> 
                        <button class="btn btn-block btn-success btn-lg" id="save-bussiness-info" name="preview" value="preview">
                        PREVIEW<?= empty($has_invoice) ? ' AND PAY' : null ?>
                        </button>
                        </div>
                        @else
                        <div class="col-sm-6 control-label mr-b30"> <button class="btn btn-block btn-success btn-lg disabledButton" id="save-bussiness-info" >SAVE</button></div>
                        <div class="col-sm-6 control-label mr-b30"> <button class="btn btn-block btn-success btn-lg" id="save-bussiness-info" name="preview" value="preview">PREVIEW</button></div>
                        @endif
                        @else
                        <div class="col-sm-8 control-label mr-b30 col-sm-offset-4"><button class="btn btn-success btn-lg  btn-block" id="save-bussiness-info-preview">SUBMIT AND PREVIEW</button></div>
                        @endif
                    </div>
                </div>
            </div>

            </form>
        </div>
    </div>
</div>
</div>

<script>
    (function ($) {
        $(window).load(function () {
            $("#termsModal .modal-body").mCustomScrollbar({
                setHeight: 340,
                theme: "minimal-dark"
            });
        });
    })(jQuery);

</script>

@stop

