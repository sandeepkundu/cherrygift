@extends('admin-template.defaultadmin')       
@section('title','Company Contact Information')
@section('content') 
@if (Session::has('errors'))
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li style="align: center;">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-lg-offset-2 col-lg-8 col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10">
            <div class="page-wrapper">
                <h3 class="page-heading">Company Contact Information</h3>
                
                <form class="form-horizontal" action="{{url()}}/admin/business-details" method="post" id="contactinfo-frm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="company_id" id="company_id" value="{{ $company->id }}">
                    <div class="form-group ">
                        <label class="col-sm-offset-4 col-sm-8 control-label" style="align:center;"><span align="center" style="color: red" id="cls_error"></span></label>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-xs-4 control-label">Company PIN</label>
                        <div class="col-sm-8 col-xs-4">
                            <p class="form-control-static country-pin">{{ $company->pin }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">First Name<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Enter your first name" id="fname" name="first_name" required="required" value="{{ $company->first_name }}">
                            <span style="color: red" id="fname_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Last Name<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Enter your last name" id="lname" name="last_name" required="required" value="{{ $company->last_name }}">
                            <span style="color: red" id="lname_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Contact Email<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Enter your email" id="conemail" name="contact_email" required="required" value="{{ $company->contact_email }}">
                            <span style="color: red" id="conemail_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Contact Number<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-xs-5">
                                    <input type="text" class="form-control" id="countrycode" name="country_code" value="+61" readonly style="background-color: lightgrey">

                                </div>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" placeholder="4444 768 9841" id="connumber" name="contact_number" required="required" maxlength="19" value="{{ $company->phone }}">
                                    <span style="color: red" id="contact_error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Company Email<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Enter your company email" id="comemail" name="company_email" value="{{ $company->email }}">
                        <span style="color: red" id="company_email_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Company Name<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Enter your company name" id="tradname" name="trading_name" required="required" value="{{$company->company_name}}">
                            <span style="color: red" id="trading_name_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">ABN / ACN numbers<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="111 333 555 54" id="abnacnnumber" name="abn_acn_number" required="required" maxlength="14" value="{{$company->acn_abn_number}}">
                            <span style="color: red" id="abnacnnumber_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Company Address<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Enter your address" id="comaddress" name="company_address" required="required" value="{{$company->company_address}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Location<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <select class="selectpicker" id="state1" name="state" required>
                                <option value="">Select State</option>
                                @foreach(App\Model\States::orderBy('state_name', 'asc')->get() as $location)
                                @if ((!empty($company->state_id)) && ($location->id == $company->state_id)) 
                                <option value="{{$location->id}}" selected="selected">{{$location->state_name}}</option>
@else
      <option value="{{$location->id}}">{{$location->state_name}}</option>
                                        </label>
@endif
                                
                                @endforeach
                            </select><span style="color: red" id="state_error"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Suburb<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Suburb" @if(empty($suburbsname)) value= "" @else value="{{$suburbsname}}" @endif id="city" name="city" autocomplete="off" required="required"> 
                            <span style="color: red" id="suburbs_error"></span>
                            <input type="hidden" id="txtAllowSearch" name="suburb" value="{{$company->suburs_id}}">
                        </div>
                        <div id="suggesstion-box"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Postcode<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Postcode" id="postcode" name="post_code" required="required" maxlength="4" @if(empty($company->postal_code)) value="" @else value="{{$company->postal_code}}" @endif>
                            <span style="color: red" id="postcode_error"></span>
                        </div>
                    </div>
                    @if(!empty($company->postal_code && $company->is_subscribed!=2))
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Add New Site/Store</label>
                        <div class="col-sm-8">
                            <label class="control-label"><a class="red-col" href="{{url()}}/admin/business-detail"> + Add</a></label>
                            
                        </div>
                    </div>
                    @endif
                    @if(count($alreadyexistinglocations)!=0)
                    
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Edit existing Site/Store</label>
                        <div class="col-sm-8">@foreach($alreadyexistinglocations as $newlocation)
                            <div style="position: relative;">
                             <input type="text" class="form-control input-edit mr-b20"  readonly="readonly" value="{{$newlocation->store_location}}">
                             @if(($newlocation->is_approved!=2) && (!empty($company->is_subscribed) && $company->is_subscribed!=2))<span class="edit-icons edit-store"><a href="{{url()}}/admin/business-detail/{{$newlocation->id}}" class="btn btn-xs btn-default"><i class="fa fa-pencil-square-o"></i> edit store</a>@endif
</span>
                            </div>                      
                        
                     
                    @endforeach
                            </div>
                    </div>
                    
                    @endif
                     
                    <div class="form-group">

                        <label class="col-sm-4 control-label">Bank account details<em><span style="color:red;">*</span></em>&nbsp;&nbsp;<i class="fa fa-question-circle " data-toggle="tooltip" data-placement="top" title="Why do I need to provide this? So we can pay you!"></i></label>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Account name" id="accountname" name="account_name"  value="{{$company->account_name}}" required="required" autocomplete="off" title="Why do I need to provide this? So we can pay you!">
                                    <span style="color: red" id="accountname_error"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" placeholder="BSB" id="bsb" name="bsb" maxlength="6" autocomplete="off" value="{{$company->bsb}}">
                                    <span style="color: red" id="bsb_error"></span>
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" placeholder="Account number" id="accountnumber" @if(empty($company->account_number)) value="" @else value="{{Crypt::decrypt($company->account_number)}}" @endif name="account_number" required="required" autocomplete="off" maxlength="10">
                                    <span style="color: red" id="accountnumber_error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mr-b30">
                        <label class="col-sm-4 control-label">Category<em><span style="color:red;">*</span></em></label>
                        <div class="col-sm-8">
                            <div class="row">
                                @foreach(App\Model\Categories::orderBy('name', 'asc')->get() as $cat)
                                <div class="col-sm-6">
                                    <div class="ck-button">
                                        
                                            @if (!empty($company->category_id) && ($cat->id == $company->category_id))
                                            <label><input type="radio" name="category" id="optionsRadios1" value="{{$cat->id}}" checked="checked"><span class="ck-btn">{{$cat->name}} </span>
                                        </label>
@else
      <label><input type="radio" name="category" id="optionsRadios1" value="{{$cat->id}}"><span class="ck-btn">{{$cat->name}} </span>
                                        </label>
@endif
                                         
                                    </div>
                                </div>
                                @endforeach
                                 <span style="color: red" id="category_error"></span>  
                            </div>
                        </div>


                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">
                            @if(!empty($company->postal_code))
                            <button class="btn btn-block btn-success btn-lg disabledButton" id="contect-info-save" >SAVE</button>
                            @else
                            <button class="btn btn-block btn-success btn-lg" id="contect-info-save">SAVE AND CONTINUE</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
