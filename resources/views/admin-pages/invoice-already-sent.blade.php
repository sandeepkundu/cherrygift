<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        @include('admin-template.header_include')
        <title>
            @yield('title') cherrygift - invoice sent
        </title>
    </head>

    <body class="bg-grey-login">
        <div class="auth-wrapper">
            <header class="text-center logo-ctn">
                <img src="{{ asset('images/admin-images/login-logo.png') }}" alt="">
            </header>
            @if(Session::has('message'))
            <div class="alert alert-success">
                {{Session::get('message')}}
            </div>
            @endif

        </div>

        <div id="PaymentConfirmationModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header ok_model">
                        <button type="button" class="close" data-dismiss="modal" id="sucess-pay"></button>
                    </div>
                    <div class="modal-body text-center">
                        <h2>Already Invoiced #{{$payment->invoiceNo()}}</h2>
                        <p>
                            An invoice for your subscription has already been invoiced and emailed.  
                        </p>
                        <p>
                            Check your SPAM and Junk mail folders if you have trouble locating the email.<br />
                            Contact <a href="mailto:{{env('account_email')}}"><strong>{{env('account_email')}}</strong></a> 
                            if you require the invoice to be resent.</p>
                    </div>
                    <div class="modal-footer">
                        <a href="{{url()}}/admin/contact-info"><button type="button" class="btn btn-success ok-btn" >OK!</button></a>
                    </div>
                </div>
            </div>
        </div>
        @include('admin-template.footer_include')
        <script>
            (function($){
                $(window).load(function(){
                    $('#PaymentConfirmationModal').modal({backdrop: 'static', keyboard: false});
                    $('#PaymentConfirmationModal').modal('show');  
                });

            })(jQuery);



        </script>

    </body>
</html>
