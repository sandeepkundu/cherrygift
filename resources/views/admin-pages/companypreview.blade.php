@extends($preview==1? 'admin-template.defaultadmin': 'template.main')
<link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/style.css') ?>">
@if($preview==1)
@section('title','Voucher preview')
@else
@section('title','Voucher details')
@endif
@section('content')
@if(!empty($invalid))
<div class="invalid-error">Invalid Voucher</div>
@else
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>
<div id="company-carousel" class="clearfix lg-container">

    <?php 
    $i=0;
    $total_images = 0;
    ?>
    @foreach($companybusinessimages as $companyimage)
    <?php 
    if ($i++ == 4) {
      break;    
    }
    $total_images++;
    ?>
    <div class="item"><img src="{{$s3_url}}{{$s3_env}}{{$companyimage->image}}" alt=""></div>
    @endforeach

    <?php for(;$i<4;$i++) {?>
        <div class="item"><img src="{{asset('images/cherry gift.png')}}" alt=""></div>
        <?php }?>

</div>
<div class="page-wrapper ld-pad">
    <section class="company-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="media company-logo-ctn" id="comp-profile"> 
                        <div class="media-left pd-r30"> 
                            @if(empty($companylogo))
                            <img class="media-object img-rounded"  alt="" src="{{$s3_url}}{{$s3_env}}{{$companybusiness->logo}}" data-holder-rendered="true" style="width:auto; height: 148px;"> 
                            @else
                            <img class="media-object img-rounded"  alt="" src="{{asset($companylogo->logo)}}" data-holder-rendered="true" style="width:auto; height: 148px;">
                            @endif
                        </div> 
                        <div class="media-body media-middle media-middletxt"> 
                            <h2 class="media-heading">{{$companybusiness->store_location}}</h2>
                            <p class="h4 location map-txt"><i class="fa fa-map-marker map-marker"></i>{{$companybusiness->address}}, {{$cityname}}, {{$statename}} {{$companybusiness->post_code}}</p> 
                        </div> 
                    </div>
                </div>
                <div class="col-sm-3 company-info email-info">
                    <h4 class="heading mr-0">CONTACT US</h4>
                    <ul class="email-list contact-info">
                        @if(!empty($companybusiness->email))<a href="mailto:{{$companybusiness->email}}"><li title="{{$companybusiness->email}}"><span class="email"></span>Send an Email</li></a>@endif

                        @if(!empty($companybusiness->phone))<li><span class="telephone"></span> @if(substr($companybusiness->phone, 0, 4)!="1300") {{$companybusiness->country_code}} @endif {{$companybusiness->phone}}</li>@endif
                        @if(!empty($companybusiness->website))<a href="http://{{$companybusiness->website}}" title="{{$companybusiness->website}}" target="_blank"><li><span class="website"></span>Visit Website</li></a>@endif

                    </ul>{{--*/ @$count = 0;  /*--}}
                    <!--ul class="info-list contact-info">
                        @foreach($companybusinesslocation as $locations)
                        @if($locations->id!=$companybusiness->id)
                        {{--*/ @$count++;  /*--}}
                        <li><span class="location" id="all_locations"></span>{{$locations->address}}, {{$locations->state_name}}, {{$locations->locality}}</li>
                        @endif
                        @endforeach
                    </ul-->
                    @if($count>=3)
                    <div class="loadMore">See All</div>
                    <div class="showLess" id="showLesslocation">See Less</div>
                    @endif
                </div>
                <div class="col-sm-3 company-info">
                    <h4 class="heading mr-0">WE ARE OPEN</h4>
                    <ul class="info-list time-info">
                        @for($j=1; $j<=7; $j++) 
                        <li><span class="day">{{ucfirst(strtolower(Config::get('constants.dayname.'.$j)))}} - </span>{{$companytiming[$j]}}</li>

                        @endfor
                    </ul>
                    <div class="loadMore">See All</div>
                    <div class="showLess" id="showLessday">See Less</div>
                </div>
            </div>
        </div>
    </section>

    <section class="company-section section-information">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="text-center">
                    <h2 class="text-center heading">Information</h2>
                </div>
                <p class="information-text text-center">
                    {{$companybusiness->long_desc}}    
                </p>

            </div>
        </div>
    </section>
        @if(isset($companybusiness->special_desc) && !empty($companybusiness->special_desc))
        <section class="company-section section-information" id="special_inst">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="text-center">
                    <h2 class="text-center heading">Special Instructions</h2>
                </div>
                <p class="information-text text-center">
                    {{$companybusiness->special_desc}}    
                </p>

            </div>
        </div>
    </section>
    @endif
    <section class="company-section section-voucher">
        <div class="container">
        <div class="col-md-10 col-md-offset-1">
        <div class="row">
        <div class="col-sm-4 mob_pad0">
            <div class="product-item">
                @if(!empty($total_images))
                <?php $v_img = 0;?>
                @foreach($companybusinessimages as $voucharimage)

                @if($voucharimage->voucher_image==1)
                <?php $v_img=1;?>
                <img src="{{$s3_url}}{{$s3_env}}{{$voucharimage->image}}" width="300" alt="" class="img-responsive img-rounded bord-radius index">
                @endif
                @endforeach
                @if(empty($v_img))
                <img src="{{asset('images/cherry gift.png')}}" style="height: 198px;" width="100%" alt="" class="img-responsive img-rounded bord-radius">
                @endif
                @else
                <img src="{{asset('images/cherry gift.png')}}" style="height: 198px;" width="100%" alt="" class="img-responsive img-rounded bord-radius">
                @endif
                <div class="item-upper-bg">
                    <div class="media"> 
                        <div class="media-left"> 
                            @if(empty($companylogo))
                            <img class="small-circle" src="{{$s3_url}}{{$s3_env}}{{$companybusiness->logo}}" style="width: 50px; height: 50px;"> 
                            @else
                            <img class="small-circle" src="{{asset($companylogo->logo)}}" style="width: 50px; height: 50px;">
                            @endif  

                        </div> 
                        <div class="media-body media-middle"> 
                            <h4 class="media-heading">{{$companybusiness->store_location}}</h4> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-7 discrip-sect">
            <h2 class="">{{$companybusiness->store_location}}</h2>
            <p class="info-text">{{$companybusiness->short_desc}}</p> 
        </div>
        <div class="col-sm-10 col-sm-offset-2 col-md-9 col-md-offset-2 text-center">
        <h2 class="text-center heading mr-b50">Buy voucher</h2>
        @if($preview==1)

        <div class="amt-btn">
            <div class="col-sm-6"><input type="text" class="vouchar-preview" readonly="" value="${{$companybusiness->min_vouchar_amount}}" size="5"><div class="range-value">Min Amount</div></div>

            <!--                            <div class="col-sm-4 inc-amt"><input type="text" class="vouchar-preview" readonly="" value="${{$companybusiness->incremental_vouchar_amount}}" size="5"><div class="range-value">Inc Amount</div></div>   -->
            <div class="col-sm-6"><input type="text" class="vouchar-preview" readonly="" value="${{$companybusiness->max_vouchar_amount}}" size="5" style="width:50%"><div class="range-value">Max Amount</div></div>

        </div>
        <!--                            <div class="form-group clearfix">
        <div class="col-sm-6">
        <button class="btn btn-default btn-sm btn-block"><span class="ic-bag-l mr-r10"></span>ADD TO CART</button>
        </div>
        <div class="col-sm-6">
        <button class="btn btn-success btn-sm btn-block" >BUY</button>
        </div>
        </div>-->




        <!--form name="frm_payment_method" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="business" value="{{Config::get('constants.paypal_user_acc')}}" />
        <input type="hidden" name="notify_url" value="{{url()}}/payment/notify/{{$companybusiness->id}}" />
        <input type="hidden" name="cancel_return" value="{{url()}}/payment/cancel/{{$companybusiness->id}}" />
        <input type="hidden" name="return" value="{{url()}}/payment/thanks/{{$companybusiness->id}}" />
        <input type="hidden" name="rm" value="2" />
        <input type="hidden" name="custom" value="{{$companybusiness->id}}" />
        <input type="hidden" name="lc" value="" />
        <input type="hidden" name="no_shipping" value="1" />
        <input type="hidden" name="no_note" value="1" />
        <input type="hidden" name="currency_code" value="AUD" />
        <input type="hidden" name="page_style" value="paypal" />
        <input type="hidden" name="charset" value="utf-8" />
        <input type="hidden" name="item_name" value="Supplier Subscription" />
        <input type="hidden" name="cmd" value="_xclick-subscriptions" />
        <input type="hidden" name="src" value="1" />
        <input type="hidden" name="srt" value="0" /> 
        <input type="hidden" name="a1" value="198" />
        <input type="hidden" name="p1" value="1" />
        <input type="hidden" name="t1" value="Y"/>
        <input type="hidden" name="a3" value="198" />
        <input type="hidden" name="p3" value="1" />
        <input type="hidden" name="t3" value="Y" />
        <!-- PayPal reattempts failed recurring payments. 
        <input type="hidden" name="sra" value="1">-->
        @if($companybusiness->is_payment==1) 
    </section>
    <div  class="footer-last-section center-part">
        <a href="{{url()}}/admin/business-detail/{{$companybusiness->id}}"><img src="{{asset('images/go_back_btn.png')}}" /></a>
    </div>
    @else
    </section>     
    <div  class="footer-last-section center-part">
        <a href="{{url()}}/payment/subscribe/{{$companybusiness->id}}" class=" btn btn-lg submit-pay">Confirm and Pay With PayPal</a>
        <a href="{{url()}}/payment/request-invoice/{{$companybusiness->id}}" class=" btn btn-lg submit-pay">Confirm and Request an Invoice</a>
        <a href="{{url()}}/admin/business-detail/{{$companybusiness->id}}" class="back-btn">GO BACK</a>

    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        var updated = false;
        $('.submit-pay').on('click',function(event) {
            if (!updated) {
                $(
                '<p>Please wait while you are transferred, may take a few moments</p>'
                ).css({
                    'background-color': '#fff',
                    padding: '10px',
                    'font-weight': 'bold',
                    'font-size': '1.1em' 
                }).appendTo($(this).parent());
                updated = true;
            }   
            else {
                event.preventDefault(true);
            }    
        });    
    });
    </script>
    @endif

    <!--/form-->
    @else
    <form class="section-voucher">
        <div class="form-group mr-b50">
            <button type="button" class="cal-btn count-minus" field='quantity'>-</button>
            <div class="count-input-wrapper">
                <span class="count-label">$</span>
                <input type='text' name='quantity' id='quantity' class='count-input'  @if(!empty($cart_voucher_price)) value="{{$cart_voucher_price}}" @elseif(!empty($companybusiness->min_vouchar_amount)) value="{{$companybusiness->min_vouchar_amount}}" min="{{$companybusiness->min_vouchar_amount}}" @else value="0" min="0" @endif>
                <span id="errmsg"></span>
                <input type="hidden" name="max_value" @if(!empty($companybusiness->max_vouchar_amount)) value="{{$companybusiness->max_vouchar_amount}}" @else value="" @endif id="max_value">   
                <input type="hidden" name="min_value" @if(!empty($companybusiness->min_vouchar_amount)) value="{{$companybusiness->min_vouchar_amount}}" @else value="" @endif id="min_value"> 
                <input type="hidden" name="voucher_id" value="{{$companybusiness->id}}" id="voucher_id">
            </div>
            <button class="cal-btn count-plus" field='quantity'>+</button>
            <div><span id="error_max_amount" style="color: red;"></span></div>
        </div>

        <div class="form-group">

        <input type="hidden" @if(!empty($edit_cart_voucher)) value="{{$edit_cart_voucher}}" @else value="0" @endif id="edit_cart_voucher" />
        <input type="hidden" @if(!empty($cart_voucher_id)) value="{{$cart_voucher_id}}" @else value="0" @endif id="cart_voucher_id" />
        <div class="col-sm-12">

            <div class="col-sm-6">
                <button class="btn btn-success btn-sm btn-block mob-30" id="back-to-search" onclick="">GO BACK</button>
            </div>
            <div class="col-sm-6">

                <button class="btn btn-success btn-sm btn-block" id="add_to_cart">CONTINUE</button>
            </div>
        </div>
    </form>
    @endif





</div>
</div>
</div>
</div>
</section>
</div>
@endif	
@stop
