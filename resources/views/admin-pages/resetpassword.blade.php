<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Cherry Gift</title>
<?php include("../admin-template/header_include.php"); ?>
</head>

<body class="bg-grey">
<div class="auth-wrapper">
	<header class="text-center logo-ctn">
    	<img src="../admin-images/logo.png" alt="" width="155" height="40">
    </header>
    <div class="auth-wrapper-inner">
    	<h4 class="heading">Reset password</h4>
    	<form>
            <div class="form-group mr-b30">
                <div class="input-group">
                    <span class="input-group-addon"><i class="ic-16 ic-user"></i></span>
                    <input type="password" class="form-control" placeholder="Re-type your new password">
                </div>
            </div>
           
            <button class="btn btn-block btn-success btn-lg">RESET PASSWORD</button>
        </form>
       
	</div>

<?php include("../admin-template/footer_include.php"); ?>

</body>
</html>