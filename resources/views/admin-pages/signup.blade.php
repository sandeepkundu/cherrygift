<!doctype html>
<html>
<head>
<meta charset="UTF-8">
@include('admin-template.header_include')
@include('admin-template.termcondition')
@include('admin-template.forgot')
 <title>
            @yield('title') cherrygift - supplier Login/Sign UP
        </title>
	
       
 @section('content')
 
 
</head>

<body class="bg-grey-login login-page">
<div class="auth-wrapper">
	<header class="text-center logo-ctn">
            <img src="{{ asset('images/admin-images/CG_Logo.svg') }}" alt="" width="167" height="43">
    </header>
    @if (Session::has('errors'))
    <div class="alert alert-danger">
       <ul>
                                    @foreach ($errors->all() as $error)
                                    <li style="align: center;">{{ $error }}</li>
                                    @endforeach
                                </ul>
    </div>
@endif

    @if(Session::has('message'))
<div class="alert alert-success">
       {{Session::get('message')}}
    </div>
    @endif
    <div class="auth-tabs-ctn">
        
    	<!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li role="presentation" id="business_login"><a href="#admin-login" role="tab" data-toggle="tab">Business login</a></li>
          <li role="presentation" id="business_sign_up"><a href="#admin-signup" role="tab" data-toggle="tab">Business sign up</a></li>
        </ul>
      
        <!-- Tab panes -->
        <div class="tab-content">
          	<div role="tabpanel" class="tab-pane active" id="admin-login">
                    <form action="admin/login" method="post">

                	<div class="form-group">
                    	<div class="input-group">
                        	<span class="input-group-addon"><i class="ic-16 ic-user"></i></span>
                                <input type="email" class="form-control" placeholder="Email" name="email" id="email" required="required">
                     	</div><span style="color: red" id="email_error"></span>
             		</div>
                	<div class="form-group">
                       	<div class="input-group">
                        	<span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                                <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="required">
                      	</div><span style="color: red" id="password_error"></span>
                    </div>
                 	<div class="form-group auth-link-ctn">
                    	<a href="#" class="auth-link" data-toggle="modal" data-target="#forgotModal" id="forgat_password">Forgot password?</a>
                  	</div>	
         			<button class="btn btn-block btn-success btn-lg" id="admin_login">LOGIN</button>
                </form>
          	</div>
          	<div role="tabpanel" class="tab-pane" id="admin-signup">
                    <form action="admin/create" method="POST">
                	<div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="ic-16 ic-user"></i></span>
                            <input type="text" class="form-control" placeholder="Enter your email" name="email" id="signupemail">
                        </div><span style="color: red" id="signup_email_error"></span>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="ic-16 ic-key"></i></span>
                            <input type="password" class="form-control" placeholder="Create your password" name="password" id="signup_password">
                        </div><span style="color: red" id="signup_password_error"></span>
                    </div>
                 	<div class="form-group auth-link-ctn">
            			<label class="auth-link">
                                    <input type="checkbox" id="term_condition" name="term_condition"> I agree <a href="#" class="auth-links" data-toggle="modal" data-target="#termsModal">Terms and Conditions</a>
                                                <p style="color: red; display: none;" id="termcondition_error"></p>
    					</label>
                 	</div>
                    <button class="btn btn-block btn-success btn-lg" id="admin_signup">SIGN UP</button>
            	</form>
          </div>
      	</div>
	</div>
    <div class="clearfix home-btn"><a href="{{url()}}"><i class="fa fa-home"></i>Home</a></div>
</div>
@include('admin-template.footer_include')
<script>
	(function($){
		$(window).load(function(){
			$("#termsModal .modal-body,#forgotModal .modal-body").mCustomScrollbar({
				setHeight:340,
				theme:"minimal-dark"
			});
		});
			
	})(jQuery);
		
		
		
	</script>
</body>
</html>
