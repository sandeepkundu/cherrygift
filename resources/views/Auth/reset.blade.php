@if(!empty($role) && $role=="User")
@include('template.header_include')
    @include('template.header')
    @else
    @include('cms-template.header_include')
    @include('cms-template.header')
    @endif 
	<title>cherrygift reset password</title>
    {!! csrf_field() !!}

<div class="container-fluid">
<div class="row">
 <div class="col-md-8 col-md-offset-2">
     <div class="panel panel-default" style="margin-top: 80px;">
     <div class="panel-heading" style="text-align:center"><b>Reset your password</b></div>
 <div class="panel-body">

 @if (count($errors) > 0)
    <div class="alert alert-danger">
<!--    <strong>Whoops!</strong> There were some problems with your input.<br><br>-->
    <ul>
        @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
        @endforeach
         </ul>
        </div>
@endif

<form class="form-horizontal" role="form" method="POST" id="reset_password_frm" action="users/password/reset">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="token" value="{{ $token }}">
<input type="hidden" class="form-control" name="email" value="<?php echo $_GET['mail']; ?>">
<!--<div class="form-group">
<label class="col-md-4 control-label">E-Mail Address</label>
<div class="col-md-6">
<input type="email" class="form-control" name="email" value="{{ old('email') }}">
</div>
</div>-->

<div class="form-group">

<label class="col-md-4 control-label">New Password<em><span style="color:red;">*</span></em></label>

<div class="col-md-6">
    <input type="password" class="form-control" name="password" id="password">
</div>
 </div>

 <div class="form-group">
 <label class="col-md-4 control-label">Confirm password<em><span style="color:red;">*</span></em></label>
 <div class="col-md-6">
     <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
 </div>
  </div>

 <div class="form-group">
 <div class="col-md-4 col-md-offset-4">
 <button type="submit" class="btn btn-danger" id="sendresetpass" style="display: block; width: 100%;">
             RESET PASSWORD
 </button>
 </div>
 </div>
 </form>

 </div>
 </div>
 </div>
 </div>
</div>
    @if(!empty($role) && $role=="User")
@include('template.footer_include') 
    @include('template.footer')
    @else
    @include('cms-template.footer_include')
    <script src="{{URL::asset('scripts/validation.extended.js')}}"></script>
    @include('cms-template.footer')
    @endif 