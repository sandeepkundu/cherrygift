<!DOCTYPE html>
<html lang="en">
    <head>
        <title>cherrygift - login</title>
         @include('template.header_include')
        @include('template.headerlogo')
       

    </head>
    <body>
         
        {!! csrf_field() !!}
        <div class="auth-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                        <header class="auth-header clearfix">
                            <div class="head-left">
                                <h1 class="auth-title">Login </h1><span class="sub-title"><a href='register'>Sign up</a></span>
                            </div>
                            <div class="head-right">
                                <a href="facebook"> <button class="btn btn-primary btn-block">Connect with your Facebook </button></a>
                            </div>
                        </header>
                        <div>
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
<!--                                <strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">

                                    <form class="form-horizontal" action="login" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Email<em><span style="color:red;">*</span></em></label>
                                            <div class="col-sm-9">
                                                <input type="email" class="form-control"  name="email" id="" placeholder="Email" value="{{ old('email') }}"><span style="color: red" id="uerror"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Password<em><span style="color:red;">*</span></em></label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" id="" name="password" placeholder="Password" value="{{ old('password') }}"><span style="color: red" id="upass"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <a href="#" class="forgot-link auth-links" id="forgarepass" data-toggle="modal" data-target="#forgotModal">Forgot password?</a>
                                            </div>
                                        </div>
                                        <div class="form-group mr-b0">
                                            <div class="col-sm-offset-3 col-sm-5 submit-ctn">
                                                <button type="submit" class="btn btn-success btn-block" id="login">LOGIN</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></div></body>
    @include('template.termcondition')
    @include('template.forgot')
    @include('template.forgot_a')
    @include('template.tutorial')
    @include('template.footer')
    @include('template.footer_include')
    <script type="text/javascript" src="{{ URL::asset('/scripts/validation.min.js') }}">
        (function ($) {
            $(window).load(function () {

                $("#termsModal .modal-body").mCustomScrollbar({
                    setHeight: 340,
                    theme: "minimal-dark"
                });

            });

            $("#carousel-tutorial").carousel({
                interval: false,
                wrap: false
            });
            var checkitem = function () {
                var $this;
                $this = $("#carousel-tutorial");
                if ($("#carousel-tutorial .carousel-inner .item:first").hasClass("active")) {
                    $this.find(".left").hide();
                    $this.find(".right").show();
                } else if ($("#carousel-tutorial .carousel-inner .item:last").hasClass("active")) {
                    $this.find(".right").hide();
                    $this.find(".left").show();
                } else {
                    $this.find(".left").show();
                    $this.find(".right").show();
                }
            };

            checkitem();

            $("#carousel-tutorial").on("slid.bs.carousel", "", checkitem);



        })(jQuery);
</script>
    
</html>