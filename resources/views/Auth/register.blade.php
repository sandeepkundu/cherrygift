<!DOCTYPE html>
<html lang="en">
    <head>
        <title>cherrygift - sign up</title>
        @include('template.header_include')
         @include('template.headerlogo')
    </head>
    <body>

        <div class="auth-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                        <header class="auth-header clearfix">
                            <div class="head-left">
                                <h1 class="auth-title">Sign up </h1><span class="sub-title"><a href='login'>Login</a></span>
                            </div>
                            <div class="head-right">
                                <a href="facebook"> <button class="btn btn-primary btn-block">Connect with your Facebook </button></a>
                            </div>
                        </header>
                        <div>
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
<!--                                <strong>Whoops!</strong> There were some problems with your input.<br><br>-->
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-sm-12 col-md-10 col-md-offset-1">
                                    <form class="form-horizontal" action="register" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-8">
                                                <div class="" id="sign-upload-ctn">
                                                    <img src="{{asset('/images/profile-img.png')}}" alt="" width="97" height="97" class="img-circle" id="dvPreview">
                                                    <span class="btn btn-file btn-link">Upload profile picture
                                                        <input type="file" name="image" id="fileupload" class="signup-upload">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">First name<em><span style="color:red;">*</span></em></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="" placeholder="First name" name="first_name" value="{{ old('first_name') }}"><span style="color: red" id="fname_error"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Last name<em><span style="color:red;">*</span></em></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="" placeholder="Last name" name='last_name' value="{{ old('last_name') }}"><span style="color: red" id="lname_error"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Mobile number<em><span style="color:red;">*</span></em></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="mobile_number" placeholder="Mobile number" maxlength="15" name="mobile_number" value="{{ old('mobile_number') }}"><span style="color: red" id="mobile_error"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Email<em><span style="color:red;">*</span></em></label>
                                            <div class="col-sm-8">
                                                <input type="email" class="form-control" id="user_email" placeholder="Email" name="email" value="{{ old('email') }}"><span style="color: red" id="email_error"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Password<em><span style="color:red;">*</span></em></label>
                                            <div class="col-sm-8">
                                                <input type="password" class="form-control" id="" name="password" placeholder="Password" value="{{ old('password') }}" autocomplete='off'><span style="color: red" id="password_error"></span>
                                            </div>
                                        </div>
                                        <div class="form-group mr-b30">
                                            <label for="" class="col-sm-4 control-label">Re-type password<em><span style="color:red;">*</span></em></label>
                                            <div class="col-sm-8">
                                                <input type="password" class="form-control" id="" placeholder="Password" name="password_confirmation" autocomplete='off'><span style="color: red" id="confirmpassword_error"></span>
                                            </div>
                                        </div>


                                        <div class="checkbox mr-b50">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                <label>
                                                    <input type="checkbox" name="term_condition"> I agree <a href="#" class="auth-links" data-toggle="modal" data-target="#termsModal"> Terms and Conditions</a><span style="color:red;">*</span>
                                                    <p style="color: red" id="termcondition_error"></p>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group mr-b0">
                                            <div class="col-sm-offset-4 col-sm-5 submit-ctn">
                                                <button type="submit" class="btn btn-success btn-block" data-toggle="modal" id="signup">SIGN UP</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('template.termcondition')
            @include('template.forgot')
            @include('template.forgot_a')
            @include('template.tutorial')
            @include('template.footer')
            @include('template.footer_include')
            <script src="{{ URL::asset('/scripts/jquery-migrate-1.0.0.js')}}"></script>
            <script type="text/javascript" src="{{ URL::asset('/scripts/validation.min.js') }}"></script>
            <script>
				$('#tutorialModal').on('hidden.bs.modal',function(){
    				$('#carousel-tutorial').carousel(0)
				})
				$('#carousel-tutorial').on('slid.bs.carousel', checkitem);
				checkitem()
				function checkitem(){
					 var $this = $("#carousel-tutorial");
					$this.find('.controls-ctn .tutorial-done-btn').hide();
					if($('#carousel-tutorial .carousel-inner .item:first').hasClass('active')) {
						$this.find('.controls-ctn .left').hide();
					} else {
						$this.find('.controls-ctn .left').show();
					}
					if($('.carousel-inner .item:last').hasClass('active')) {
						$this.find('.controls-ctn .right').hide();
						$this.find('.controls-ctn .tutorial-done-btn').show();
					} else {
						$this.find('.controls-ctn .right').show();
						$this.find('.controls-ctn .tutorial-done-btn').hide();
					}
				}
			</script>
    </body>
</html>
