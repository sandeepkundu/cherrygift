<!DOCTYPE html>
<html lang="en">
<head>
    <title>CherryGift</title>
   <style>
       *{margin: 0; padding: 0;}
       img{max-width: 100%;}
       body{margin:0; padding:0; font-family: arial;}
       

 table td, table th {
    display: table-cell;
    padding: 18px 8px;
    text-align: left;
    vertical-align: top;
}  
      
    </style>
</head>
<body>
<div style="max-width: 1200px; width: 100%; margin: 0 auto; overflow: hidden;">
    
    <table width="100%" style="padding:0 40px;">
            <tr>
                <td style="width:30%; text-align:left; padding: 30px 0;">

                    <img src="{{public_path().'/images/invoice-logo.png'}}" alt="cherrygift logo">

                </td>
                <td  style="width:20%; text-align:left; margin-right: 0px; padding: 40px 0 30px;">

                    <h1 style="color: #000; text-align:left; font-size: 30px; margin-top: 0;">Tax Invoice</h1>
                    <h3 style="color: #000; text-align:left; font-size: 18px; margin-top: 5px; font-weight: normal;">ABN: {{env('INVOICE_ABN')}}</h3>

                </td>
            </tr>
            <tr>
                <td style="width:30%; text-align:left; ">
                    <h3 style="color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;"><b>{{$order_user_info[0]['first_name']}} {{$order_user_info[0]['last_name']}}</b></h3></td>
                <td style="width:20%; text-align:left;">
                    <h3 style="text-align:left; color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;">Invoice #{{$order_id}}</h3>
                    <h3 style=" text-align:left; color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;">Date: {{date("d-m-Y")}}</h3></td>
            </tr>
        </table>
    
    <table style="border-collapse: collapse; border-spacing: 0; display: table; width: 92%; margin: 60px auto 40px; float: left;">
<tbody>
<tr style="background: #8dc73f!important;">
	<th>Description</th>
	<th>Voucher amount</th>
	<th>Service Fee*</th>		
    <th>Total</th>
</tr>
<?php $i=0;
$total_price=0;
?>
@foreach($result as $res)
<?php $i++;?>

<tr style="background: #e1e1ed;">
	<td>{{$res['location']."    ".$res['mobile']}}<br />#{{$res['voucher_unique_id']}}</td>
	<td>${{$res['price']}}</td>
	<td>${{env('INVOICE_PER_ITEM_TAX')}}</td>		
	<td>${{number_format($res['price']+env('INVOICE_PER_ITEM_TAX'), 2, '.', '')}}</td>
        <?php $total_price +=  $res['price']+env('CART_PER_ITEM_TAX')?>
</tr>
@endforeach

<tr style="background: #eeedf5;">
<td colspan="2" style="background-color:#fff;">*Service fee attracts GST</td>	
	<td>GST</td>		
	<td>${{number_format((env('CART_PER_ITEM_TAX')-env('INVOICE_PER_ITEM_TAX'))*$i, 2, '.', '')}}</td>
</tr>
<tr style="background: #8dc73f!important;">
<td colspan="2" style="background-color:#fff;"></td>	
	<td>Total</td>		
	<td>${{number_format($total_price, 2, '.', '')}}</td>
</tr>
</tbody></table>
   
    <table width="92%" style="margin:0 auto;">
    <tr>
        <td style="text-align:right; width:100%;"><img src="{{public_path().'/images/paid.jpg'}}"></td>
    </tr>
</table>
    
   
</div>
    <div style="background: #ed1b24; width: 100%; float: left; margin-top: 150px; padding: 0 0; position: absolute; bottom: 0;">
        <div style=" width: 100%; margin: 0 auto; overflow: hidden;">
<!--            <table width="100%">
                <tr>
                    <td style="width:20%; position:relative;">
                        <span align="left"> <img src="{{public_path().'/images/img_phone.jpg'}}" align="left" style="float:left;"></span>
                        <span style="color: #fff; font-size: 14px;  left: 60px; position: absolute; top: 8px;">{{env('contact_number')}}</span>
                    </td>
                    <td style="width:27%; position:relative;">
                        <img src="{{public_path().'/images/img-email.jpg'}}" align="left" style="float:left;">
                        <span style="color: #fff; font-size: 14px;  left: 60px; position: absolute; top: 8px;">{{env('account_email')}}</span>


                    </td>
                    <td style="width:27%; position:relative;">

                        <img src="{{public_path().'/images/img-website.jpg'}}" align="left" style="float:left;">
                        <span style="color: #fff; font-size: 14px;  left: 60px; position: absolute; top: 8px;">qa.cherrygift.com</span>

                    </td>
                    <td style="width:26%; position:relative;">

                        <img src="{{public_path().'/images/img_address.jpg'}}" align="left" style="float:left;">
                        <span style="color: #fff; font-size: 14px;  left: 60px; position: absolute; top: 18px;">{{env('address_1')}}<br>{{env('address_2')}}</span>

                    </td>
                </tr>
            </table>-->
            <img src="{{public_path().'/images/invoice_img_footer.jpg'}}" alt="">
        </div>
    </div>
 
</body>
</html>
