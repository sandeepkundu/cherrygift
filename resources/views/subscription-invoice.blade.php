<!DOCTYPE html>
<html lang="en">
    <head>
        <title>CherryGift</title>
        <style>
            *{margin: 0; padding: 0;}
            img{max-width: 100%;}
            body{margin:0; padding:0; font-family: arial;}


            table td, table th {
                display: table-cell;
                padding: 18px 8px;
                text-align: left;
                vertical-align: top;
            }  

        </style>
    </head>
    <body>
        <div style="max-width: 1200px; width: 100%; margin: 0 auto; overflow: hidden;">

            <table width="100%" style="padding:0 40px;">
                <tr>
                    <td style="width:30%; text-align:left; padding: 30px 0;">

                        <img src="{{public_path().'/images/invoice-logo.png'}}" alt="cherrygift logo">

                    </td>
                    <td  style="width:20%; text-align:left; margin-right: 0px; padding: 40px 0 30px;">

                        <h1 style="color: #000; text-align:left; font-size: 30px; margin-top: 0;">Tax Invoice</h1>
                        <h3 style="color: #000; text-align:left; font-size: 18px; margin-top: 5px; font-weight: normal;">ABN: {{env('INVOICE_ABN')}}</h3>

                    </td>
                </tr>
                <tr>
                    <td style="width:30%; text-align:left; ">
                        <h3 style="color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;">
                            <b>
                                {{$company->first_name}} {{$company->last_name}}<br />
                                {{$company->company_name}}<br />
                                {{$company->company_address}}<br />
                                {{$suburb}} {{$state}} {{$company->postal_code}}
                            </b>
                        </h3>
                    </td>
                    <td style="width:20%; text-align:left;">
                        <h3 style="text-align:left; color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;">Invoice #{{$payment->invoiceNo()}}</h3>
                        <h3 style=" text-align:left; color: #000; font-size: 18px; margin-top: 5px; font-weight: normal;">
                            Date: {{$payment->created_at->format('d-m-Y')}}
                        </h3>
                    </td>
                </tr>
            </table>
            <?php
            $gst = round($profile->subscription_amount / 11.0,2);
            $exGST = round($profile->subscription_amount - $gst,2);

            ?>
            <table style="border-collapse: collapse; border-spacing: 0; display: table; width: 92%; margin: 60px auto 40px; float: left;">
                <tbody>
                    <tr style="background: #8dc73f!important;">
                        <th style="width:84%" colspan="2">Description</th>
                        <th style="width:16%; text-align: right;">Price (exGST)</th>
                    </tr>
                    <tr style="background: #e1e1ed;">
                        <td colspan="2">12 Month cherrygift Subscription</td>
                        <td style="text-align: right;">${{number_format($exGST,2)}}</td>
                    </tr>
                    <tr style="background: #8dc73f!important;">
                        <td rowspan="2" style="width:76%;">Bank details for EFT:<br />
                            <strong>Account Name:</strong> cherrygift<br />
                            <strong>BSB:</strong> 484 799<br />
                            <strong>Acc:</strong> 606248276
                        </td>        
                        <td style="text-align: right; width:10%">GST</td>        
                        <td style="text-align: right;">${{number_format($gst, 2, '.', '')}}</td>
                    </tr>
                    <tr style="background: #8dc73f!important;">
                        <td style="text-align: right;font-weight:bold;">Total</td>		
                        <td style="text-align: right;font-weight:bold;">${{number_format($profile->subscription_amount, 2, '.', '')}}</td>
                    </tr>
                </tbody>
            </table>

            <?php
            if ($payment->status == 1) {
                ?>
                <table width="92%" style="margin:0 auto;">
                    <tr>
                        <td style="text-align:right; width:100%;"><img src="{{public_path().'/images/paid.jpg'}}"></td>
                    </tr>
                </table>
                <?php
            }
            ?>

        </div>
        <div style="background: #ed1b24; width: 100%; float: left; margin-top: 150px; padding: 0 0; position: absolute; bottom: 0;">
            <div style=" width: 100%; margin: 0 auto; overflow: hidden;">

                <img src="{{public_path().'/images/invoice_img_footer.jpg'}}" alt="">
            </div>
        </div>

    </body>
</html>
