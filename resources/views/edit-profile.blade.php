
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>cherrygift</title>
        @include("template.header_include")
    </head>
    <body>
        @include("template.header")
        @if(Session::has('message'))

        <div class="alert alert-success">
            <h3 style='text-align: center'>{{ Session::get('message') }}</h3>
        </div>
        @endif
        <div class="auth-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
                        <header class="auth-header clearfix">
                            <div class="head-left">
                                <h1 class="auth-title">Edit Profile</h1>
                            </div>

                        </header>
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-12 col-md-10 col-md-offset-1">

                                {!! Form::model($user,array('method' => 'PATCH','class' => 'form-horizontal','route'=>array('users.update',Crypt::encrypt(Auth::user()->id)),'files' => true, 'autocomplete'=>'off')) !!}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <div class="" id="sign-upload-ctn">



                                            <?php 
                                            $destinationFolder = env('s3_url') . env('s3_env'). '/imgs/profile/' . $user->image; ?>
                                            @if($user->image=='')

                                            <img src="{{asset('/images/profile-img.png')}}" alt="" width="97" height="97" class="img-circle" id="dvPreview">
                                            @endif
                                            @if($user->image!='')
                                            <img src="{{$destinationFolder}}" alt="" width="97" height="97" class="img-circle" id="dvPreview">
                                            @endif
                                            <span class="btn btn-file btn-link">Update profile picture
                                                <input type="file" class="signup-upload" name="image" id="fileupload">
                                            </span>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">First name<em><span style="color:red;">*</span></em></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="First name" value="{{ $user->first_name }}" name="first_name" autocomplete='off'><span style="color: red" id="fname_error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Last name<em><span style="color:red;">*</span></em></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="Last name" value="{{ $user->last_name }}" name="last_name" autocomplete='off'><span style="color: red" id="lname_error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Mobile number<em><span style="color:red;">*</span></em></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="mobile_number" placeholder="Mobile number" value="{{ $user->mobile_number }}" maxlength="15" name="mobile_number" autocomplete='off'><span style="color: red" id="mobile_error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control" id="" placeholder="Email" value="{{ $user->email }}" name="email" disabled="disabled"> 
                                    </div>
                                </div>
                                @if(!empty($user->facebook_id))
                                <input type="hidden" name="facebook_id" value="{{$user->facebook_id}}" id="facebook_id">
                                @else
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Current password&nbsp;&nbsp;</label>
                                    <div class="col-sm-8">
                                        <input type="password" id="prevent_autofill" style="display:none"/>
                                        <input type="password" name="now_password" class="form-control" id="now_password" value="" placeholder="Password"  autocomplete="off"><span style="color: red" id="password_error"></span>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group mr-b50">
                                    <label for="" class="col-sm-4 control-label">New password</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" id="" placeholder="Password" name="new_password"  autocomplete="off"><span style="color: red" id="confirmpassword_error"></span>
                                    </div>
                                </div>
                                <div class="form-group mr-b50">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <p class="mr-b20"><a href="#" class="auth-links" data-toggle="modal" data-target="#termsModal">Read Terms and Conditions</a></p>          
                                        <p class="mr-b20"><a href="#" class="auth-links" data-toggle="modal" data-target="#tutorialModal">View tutorial</a></p>
                                        <p><a href="{{url()}}/destroy/{{Crypt::encrypt(Auth::user()->id)}}" class="auth-links" onclick="return confirm('Are you sure you want to leave us? Any scheduled vouchers can not be delivered if your profile has been deleted.')">Delete Profile</a></p>

                                    </div>
                                </div>


                                <div class="form-group mr-b0">
                                    <div class="col-sm-offset-4 col-sm-5 submit-ctn">
                                        <input type="submit" class="btn btn-success btn-block" name="upadate" id="upadate" value="SUBMIT" disabled="disabled">
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('template.termcondition')
        @include('template.forgot')
        @include('template.forgot_a')
        @include('template.tutorial')
        @include('template.footer')
        @include('template.footer_include')

        <script src="{{ URL::asset('/scripts/jquery-migrate-1.0.0.js')}}"></script>
        <script type="text/javascript" src="{{ URL::asset('/scripts/validation.min.js') }}"></script>
        <script>
            $('#tutorialModal').on('hidden.bs.modal',function(){
                $('#carousel-tutorial').carousel(0)
            })
            $('#carousel-tutorial').on('slid.bs.carousel', checkitem);
            checkitem()
            function checkitem(){
                var $this = $("#carousel-tutorial");
                $this.find('.controls-ctn .tutorial-done-btn').hide();
                if($('#carousel-tutorial .carousel-inner .item:first').hasClass('active')) {
                    $this.find('.controls-ctn .left').hide();
                } else {
                    $this.find('.controls-ctn .left').show();
                }
                if($('.carousel-inner .item:last').hasClass('active')) {
                    $this.find('.controls-ctn .right').hide();
                    $this.find('.controls-ctn .tutorial-done-btn').show();
                } else {
                    $this.find('.controls-ctn .right').show();
                    $this.find('.controls-ctn .tutorial-done-btn').hide();
                }
            }
        </script>
        <script type="text/javascript">
            window.__lc = window.__lc || {};
            window.__lc.license = 7576301;

            (function() {
                var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
            })();
        </script>

    </body></html>