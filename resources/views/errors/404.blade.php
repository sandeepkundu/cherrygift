<!DOCTYPE html>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
	<head>
		<title>cherrygift error 404</title>
			
                        <style>
           .content p{
	margin: 18px 0px 45px 0px;
}
.content p{
	font-family: "Century Gothic";
	font-size:2em;
	color:#666;
	text-align:center;
}
.content p span,.logo h1 a{
	color:#e54040;
}
*{ margin:0px; padding:0px;}

.content{
	text-align:center;
	padding:115px 0px 0px 0px;
}
.content a{
	color:#fff;
	font-family: "Century Gothic";
	background: #666666; 
	background: -moz-linear-gradient(top,  #666666 0%, #666666 100%); 
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#666666), color-stop(100%,#666666)); 
	background: -webkit-linear-gradient(top,  #666666 0%,#666666 100%); 
	background: -o-linear-gradient(top,  #666666 0%,#666666 100%); 
	background: -ms-linear-gradient(top,  #666666 0%,#666666 100%); 
	background: linear-gradient(to bottom,  #666666 0%,#666666 100%); 
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#666666', endColorstr='#666666',GradientType=0 ); 
	padding: 15px 20px;
	border-radius: 1em;
        text-decoration: none;
}
.content a:hover{
	color:#e54040;
}
.logo{
	text-align:center;
	-webkit-box-shadow: 0 8px 6px -6px rgb(97, 97, 97);
	-moz-box-shadow: 0 8px 6px -6px  rgb(97, 97, 97);
	box-shadow: 0 8px 6px -6px  rgb(97, 97, 97);
}
.logo h1{
	font-size:2em;
	font-family: "Century Gothic";
	background: #666666;
	background: -moz-linear-gradient(top,  #666666 0%, #666666 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#666666), color-stop(100%,#666666)); 
	background: -webkit-linear-gradient(top,  #666666 0%,#666666 100%); 
	background: -o-linear-gradient(top,  #666666 0%,#666666 100%); 
	background: -ms-linear-gradient(top,  #666666 0%,#666666 100%); 
	background: linear-gradient(to bottom,  #666666 0%,#666666 100%); 
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#666666', endColorstr='#666666',GradientType=0 );
	padding: 10px 10px 18px 10px;
}
.logo h1 a{
	font-size:1em;
}

/*------responive-design--------*/
@media screen and (max-width: 1366px)	{
	.content {
		padding: 58px 0px 0px 0px;
	}
}
@media screen and (max-width:1280px)	{
	.content {
		padding: 58px 0px 0px 0px;
	}
}
@media screen and (max-width:1024px)	{
	.content {
		padding: 58px 0px 0px 0px;
	}
	.content p {
		font-size: 1.5em;
	}
	.copy-right p{
		font-size:0.9em;
		
	}
}
@media screen and (max-width:640px)	{
	.content {
		padding: 58px 0px 0px 0px;
	}
	.content p {
		font-size: 1.3em;
	}
	.copy-right p{
		font-size:0.9em;
	}
}

@media screen and (max-width:320px)	{
	.content {
		padding:30px 0px 0px 0px;
		margin:0px 12px;
	}
	.content a {
		padding:10px 15px;
		font-size:0.8em;
	}
	.content p {
		margin: 18px 0px 22px 0px;
	}
       
}</style>
	</head>
	<body>
		<!--start-wrap--->
		<div class="wrap">
			<!---start-header---->
				<div class="header">
					<div class="logo">
						<h1><a href="http://www.cherrygift.com"><img src="{{ URL::asset('images/logo.png')}}" ></a></h1>
					</div>
				</div>
			<!---End-header---->
			<!--start-content------>
			<div class="content">
				
				<p style="text-align:center;"><span style="display:block; font-size:100px;">404</span>Page not found</p>
				<a href="http://www.cherrygift.com">Back To Home</a>
				
   			</div>
			<!--End-Cotent------>
		</div>
		<!--End-wrap--->
	</body>
</html>

