<?php /* Some Common JS includes */ ?>
<script type="text/javascript">
var APP_URL = "{{url()}}";
</script>
    <script src="{{URL::asset('cms/script/jquery-1.10.2.min.js')}}"></script>
 	<script src="{{URL::asset('cms/script/jquery-migrate-1.2.1.min.js')}}"></script>
    <script src="{{URL::asset('cms/script/jquery-ui.js')}}"></script>
    <script src="{{URL::asset('cms/script/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('cms/script/bootstrap-hover-dropdown.js')}}"></script>
    <script src="{{URL::asset('cms/script/html5shiv.js')}}"></script>
    <!--<script src="{{URL::asset('cms/script/respond.min.js')}}"></script>-->
    <script src="{{URL::asset('cms/script/jquery.metisMenu.js')}}"></script>
    <!--<script src="{{URL::asset('cms/script/jquery.slimscroll.js')}}"></script>
    <script src="{{URL::asset('cms/script/jquery.cookie.js')}}"></script>-->
    <script src="{{URL::asset('cms/script/icheck.min.js')}}"></script>
   <script src="{{URL::asset('cms/script/avatar.js')}}"></script>
    <!--<script src="{{URL::asset('cms/script/jquery.news-ticker.js')}}"></script>-->
   	<script src="{{URL::asset('cms/script/jquery.menu.js')}}"></script>   
    <!--<script src="{{URL::asset('cms/script/pace.min.js')}}"></script>-->
    <!--<script src="{{URL::asset('cms/script/holder.js')}}"></script>-->
    <!--<script src="{{URL::asset('cms/script/responsive-tabs.js')}}"></script>-->
   <!-- <script src="{{URL::asset('cms/script/jquery.flot.js')}}"></script>
    <script src="{{URL::asset('cms/script/jquery.flot.categories.js')}}"></script>-->
    <!--<script src="{{URL::asset('cms/script/jquery.flot.pie.js')}}"></script>-->
    <!--<script src="{{URL::asset('cms/script/jquery.flot.tooltip.js')}}"></script>
    <script src="{{URL::asset('cms/script/jquery.flot.resize.js')}}"></script>-->
    <!--<script src="{{URL::asset('cms/script/jquery.flot.fillbetween.js')}}"></script>-->
    <!--<script src="{{URL::asset('cms/script/jquery.flot.stack.js')}}"></script>-->
    <!--<script src="{{URL::asset('cms/script/jquery.flot.spline.js')}}"></script>-->
    <!--<script src="{{URL::asset('cms/script/zabuto_calendar.min.js')}}"></script>
    <script src="{{URL::asset('cms/script/index.js')}}"></script>-->
    <!--LOADING SCRIPTS FOR CHARTS-->
    <!--<script src="{{URL::asset('cms/script/highcharts.js')}}"></script>
    <script src="{{URL::asset('cms/script/data.js"')}}></script>
    <script src="{{URL::asset('cms/script/drilldown.js')}}"></script>
    <script src="{{URL::asset('cms/script/exporting.js')}}"></script>
    <script src="{{URL::asset('cms/script/highcharts-more.js')}}"></script>
    <script src="{{URL::asset('cms/script/charts-highchart-pie.js')}}"></script>
    <script src="{{URL::asset('cms/script/charts-highchart-more.js')}}"></script>-->
    <!--CORE JAVASCRIPT-->
    <script src="{{URL::asset('cms/script/main.js')}}"></script>
    <script src="{{URL::asset('cms/script/validation.js')}}"></script>
<!--    <script src="{{URL::asset('cms/script/jquery-ui.min.js')}}"></script>-->
    <script src="{{URL::asset('scripts/jquery.validate.min.js')}}"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
    <!--<script src="{{URL::asset('cms/scripts/bootstrap-switch.min.js')}}"></script>-->
    <!-- starting progress indicator script code-->
    <script type="text/javascript">
            $(document).ready(function () {
            $(window).scroll(function () {
                var s = $(document).scrollTop(),
                    d = $(document).height() - $(window).height();          
                $("#progressbar").attr('max', d);
                $("#progressbar").attr('value', s)
             });
         });
    </script>
    
    @yield('scripts')