<!DOCTYPE html>
<html lang="en">
    
    <head>
        <title>@yield('title')</title>
        <meta name="_token" content="{!! csrf_token() !!}"/>
        @include('cms-template.header_include')
    </head>
    

    <body @yield('bodyclass')>
        @if (!empty(Auth::user()->id))
        
        @include('cms-template.header')
        
        <div id="wrapper">
         @include('cms-template.sidebar')
         
        @endif    
        @yield('content')
        @include('cms-template.footer')
        @if (Session::has('cms_admin_id'))
        </div>     
        @endif
        @include('cms-template.footer_include')
    </body>
</html>


