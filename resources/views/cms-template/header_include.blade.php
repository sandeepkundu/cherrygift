<?php /* Some common CSS and head section of page */ ?>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{URL::asset('/images/cherry.ico')}}">
    <link rel="apple-touch-icon" href="{{URL::asset('/images/cherry.ico')}}">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
<!--    <link type="text/css" rel="stylesheet" href="{{URL::asset('cms/styles/jquery-ui-1.10.4.custom.min.css')}}">-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.css">
    
    <link type="text/css" rel="stylesheet" href="{{URL::asset('cms/styles/font-awesome.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{URL::asset('cms/styles/bootstrap.min.css')}}">
     <!--layout, header, footer, sidebar css-->
    <link type="text/css" rel="stylesheet" href="{{URL::asset('cms/styles/common-layout.css')}}">
    <!--checkbox icheck plugin css-->
    <link type="text/css" rel="stylesheet" href="{{URL::asset('cms/styles/all.css')}}">
    <link type="text/css" rel="stylesheet" href="{{URL::asset('cms/styles/main.css')}}">
    <link type="text/css" rel="stylesheet" href="{{URL::asset('cms/styles/style-responsive.css')}}">
    <link type="text/css" rel="stylesheet" href="{{URL::asset('cms/styles/custom-element1.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('cms/styles/jquery.dataTables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('cms/styles/dataTables.responsive.css')}}">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/jquery-ui.min.css') ?>">
    <style>
		td.details-control:before {
			content: "\f055";
			font-size:18px;
			text-align:center;
			color:#31b131;
			font-family: FontAwesome;
    		cursor: pointer;
			
		}
		tr.shown td.details-control:before {
    		content: "\f056";
			color:#d33333;
		}
                .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
    /* add padding to account for vertical scrollbar */
    padding-right: 20px;
}
	</style>															