<?php /* Sidebar navigation of page */ ?>
@if (Auth::user()->role=='Admin')
<!--BEGIN SIDEBAR MENU-->
<nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;" data-position="right" class="navbar-default navbar-static-side">
    <div class="sidebar-collapse">
        <ul id="side-menu" class="nav">
            <div class="clearfix"></div>


            <!--            <li>
                                    <a href="{{url()}}/super-admin/profile"><i class="fa fa-user"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Profile</span></a>
                        </li>-->
            <li>
                <a href="{{url()}}/super-admin/company" class="{{Request::is('super-admin/store/*') || (Request::is('super-admin/store-voucher/*')) ? 'active' : ''}}"><i class="fa fa-th-list fa-fw"> <div class="icon-bg bg-blue"></div></i></i>			                <span class="menu-title">Company / Store List</span></a>
            </li>

            <li>
                <a href="{{url()}}/super-admin/users"><i class="fa fa-th-list fa-fw"> <div class="icon-bg bg-blue"></div></i></i>			                <span class="menu-title">User / Vendor List</span></a>
            </li>
            
            <li>
                <a href="{{url()}}/super-admin/storeStatusList"><i class="fa fa-th-list fa-fw"><div class="icon-bg bg-blue"></div></i><span class="menu-title">Store Status List</span></a>
            </li>
            <!--            <li>
                            <a href="{{url()}}/super-admin/companylist"><i class="fa fa-th-list fa-fw"> <div class="icon-bg bg-blue"></div></i></i>			                <span class="menu-title">Company List</span></a>
                        </li>-->

            <li>
                <a href="{{url()}}/super-admin/voucher_history" class="{{Request::is('super-admin/voucher_id/*') ? 'active' : ''}}"><i class="fa fa-th-list fa-fw"> <div class="icon-bg bg-blue"></div></i></i>			                <span class="menu-title">Voucher redeemed history</span></a>
            </li>
            <li>
                <a href="{{url()}}/super-admin/redeemed_history" class="{{Request::is('super-admin/redeemed_history/*') ? 'active' : ''}}"><i class="fa fa-th-list fa-fw"> <div class="icon-bg bg-blue"></div></i></i>			                <span class="menu-title">Voucher purchase history</span></a>
            </li>
            
            <li>
                <a href="{{url()}}/super-admin/admin-vendor-payment"><i class="fa fa-th-list fa-fw"> <div class="icon-bg bg-blue"></div></i></i>			                <span class="menu-title">Clear supplier payment</span></a>
            </li>

            <li>
                <a href="{{url()}}/super-admin/slider-images"><i class="fa fa-th-list fa-fw"> <div class="icon-bg bg-blue"></div></i></i>			                <span class="menu-title">Homepage Slider Images</span></a>
            </li>
            <li>
                <a href="{{url()}}/super-admin/recipient-images"><i class="fa fa-th-list fa-fw"> <div class="icon-bg bg-blue"></div></i></i>			                <span class="menu-title">Homepage Recipient Images</span></a>
            </li>
            <li>
                <a href="{{url()}}/super-admin/subscription-fee"><i class="fa fa-edit fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Manage Subscription Fee</span></a>
            </li>
            <li>
                <a href="{{url()}}/super-admin/make-cherrygift-voucher"><i class="fa fa-edit fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Make cherrygift voucher</span></a>
            </li>
            <li>
                <a href="{{url()}}/super-admin/recover-voucher"><i class="fa fa-edit fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Recover Voucher</span></a>
            </li>

            <li>
                <a href="{{url()}}/super-admin/enquiry"><i class="fa fa-th-list fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Enquiries</span></a>
            </li>
            <li>
                <a href="{{url()}}/super-admin/payment-details"><i class="fa fa-th-list fa-fw"><div class="icon-bg bg-violet"></div></i><span class="menu-title">Payment Details</span></a>
            </li>
            
       	</ul>
    </div>
</nav> 
<!--END SIDEBAR MENU-->
@endif