<!DOCTYPE html>
<html lang="en">

    <head>
        <title>@yield('title')</title>
        @include('template.header_include')
    </head>
    <body>
        <?php        
        $currentRoute = Route::current();
        $params = $currentRoute->parameters();
        if(Request::server('HTTP_REFERER') == url() . '/') {
            $currentRoute->type;
            $currentRoute->id;
        } else {
            $currentRoute->type = '';
            $currentRoute->id = '';
        }
        ?> 
        @include('template.header')  
        @yield('homebanner')

        @yield('content')
        @include('template.footer')
        @include('template.footer_include')

        <script type="text/javascript" src="{{ URL::asset('/scripts/validation.min.js') }}"></script>  

        <script type="text/javascript">
            window.__lc = window.__lc || {};
            window.__lc.license = 7576301;

            (function() {
                var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
            })();
        </script>
    </body>
</html>


