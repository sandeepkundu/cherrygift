<div class="search-ctn lg-container clearfix" id="page-search">
            <div class="visible-xs">
                <a class="main-search-accordion" id="search-toggle" data-toggle="collapse" href="#page-search-option">
                    SEARCH<span class="glyphicon"></span>
                </a>

            </div>
            <form action="{{url()}}/search" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                @if(@$url_param_type)
                <input type="hidden" id="url_param_type" name="url_param_type" value="{{ $url_param_type }}">
                @endif
                @if(@$url_param_id)
                <input type="hidden" name="url_param_id" id="url_param_id" value="{{ $url_param_id }}">
                @endif
                <div class="collapse" id="page-search-option">
                    <div class="col-sm-4">
                        <div class="row left-inner-addon">
                            <i class="fa fa-search fa-lg input-lg"></i>
                            <input type="text" id="search_txt" name="search_txt" value="<?php echo @$_POST['search_txt']; ?>" class="form-control input-lg" placeholder="Search for Vouchers">
                        </div>
                    </div>
                    <div class="loader-ctn" id="loader">
        <div class="loader">Loading...</div>
    </div>
                    <div class="col-sm-3">
                        <div class="row ">
                            <select class="selectpicker state" name="search_state" title="State" data-width="100%" id="state-list">
                                <option value="" data-hidden="true">State</option>
                                @foreach(App\Model\States::orderBy('state_name', 'asc')->get() as $location)
                                @if(!empty($_POST['search_state']))
                                @if ($location->id == @$_POST['search_state'])
                                <option value="{{$location->id}}" selected>{{$location->state_name}}</option>
                                @else
                                <option value="{{$location->id}}">{{$location->state_name}}</option>
                                @endif

                                @else
                                <option value="{{$location->id}}">{{$location->state_name}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="row">
                            <a href="#suburblink" id="suburblink_id"></a>
                            <a name="suburblink" id="suburblink"></a>
                            <select class="selectpicker city" name="search_city" title="Suburb" data-width="100%" id="city-list" data-live-search="true" mobile="true" data-size="5">
                                <option value="" data-hidden="true">City</option>
                                @if($currentRoute->type=='state' || !empty($_POST['search_state']))
                                @foreach(App\Model\Suburbs::where('state_id',$currentRoute->id ? $currentRoute->id : @$_POST['search_state'])->orderBy('locality', 'asc')->get()

                                as $suburbs)

                                @if (($suburbs->id == $currentRoute->cid) || ($suburbs->id == @$_POST['search_city']))
                                <option value="{{$suburbs->id}}" selected>{{$suburbs->locality}}</option>
                                @else
                                <option value="{{$suburbs->id}}">{{$suburbs->locality}}</option>
                                @endif
                                @endforeach
                                @else
                                <option value="" disabled="disabled" style="color: red">Please select a State first</option>
                                @endif
                                
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2 bdr-n">
                        <div class="row">
                            <button class="btn btn-black btn-block btn-lg">SEARCH</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>