<?php /* Some Common JS includes */ 
$useMinified = true;
if ($useMinified) {
    ?>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="/scripts/cherry-gift-footer.min.js"></script>
    <?php
}
else {
    ?>
    <!-- Google CDN jQuery with fallback to local -->
    <script src="{{ URL::asset('scripts/jquery-1.11.0.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="{{ URL::asset('scripts/moment.min.js') }}"></script>
    <script src="{{ URL::asset('scripts/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('scripts/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ URL::asset('scripts/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ URL::asset('scripts/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ URL::asset('scripts/bootstrap-select.min.js') }}"></script>
    <script src="{{ URL::asset('scripts/avatar.js') }}"></script>
    <script src="{{ URL::asset('scripts/script.js') }}"></script>
    <script src="{{ URL::asset('scripts/load-image.all.min.js') }}"></script>
    <script src="{{ URL::asset('scripts/jquery.mask.js') }}"></script>
    <script src="{{ URL::asset('scripts/bootstrap-select.min.js') }}"></script>
    <?php
}
