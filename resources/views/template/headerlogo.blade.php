<?php /* Header html of page */ ?>
<script type="text/javascript">
var APP_URL = "{{url()}}";
</script>
<header id="topHeader" class="navbar navbar-default" >
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">


<!--            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>-->
            <?php
                if (Auth::check()) {?>
            <span class="bag visible-xs visible-sm">
                <a href="#">
                    <span class="ic-bag">
<!--                        <span class="badge">2</span>-->
                            
                    </span>
                </a>
            </span> 
                <?php }?>
            <a class="navbar-brand" href="{{URL::to('/home')}}"><img src="{{asset('/images/logo.png')}}" alt=""></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</header>
