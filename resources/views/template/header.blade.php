<?php /* Header html of page */ ?>
<script type="text/javascript">
var APP_URL = "{{url()}}";
</script>
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>
<header id="topHeader" class="navbar navbar-default" >
    <div class="lg-container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">


            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>
            <?php
                if (Auth::check()) { ?>
            <span class="bag visible-xs visible-sm">
                <a href="{{url()}}/cart">
                    <span class="{{Request::is('cart') ? 'ic-bag active' : 'ic-bag'}}" id="ic-bag-id">
                        @if(App\Model\UserCart::getCartCountView(Auth::user()->id) > 0)
                        <span class="badge" id="badge_item">{{App\Model\UserCart::getCartCountView(Auth::user()->id)}}</span>
                        @endif 
<!--                        <span class="badge">2</span>-->
                    </span>
                </a>
            </span> 
                <?php } ?>
            <a class="navbar-brand" href="{{url()}}"><img src="{{asset('/images/logo.svg')}}" alt="CherryGift - vouchers to your smartphone"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">


                <?php
                if (Auth::check()) {
                    $user = Auth::user();
                    $site_url = explode("public", url());
                    $user_image = url() . '/images/user.png';
                    
                    if (!empty($user->image)) { 
                        $user_image = $s3_url . $s3_env. '/imgs/profile/' . $user->image;
                        //$user_image = url(). '/imgs/profile/' . $user->image;}
                    }
                        ?>

                    <li class="profile dropdown visible-xs visible-sm">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="{{$user_image}}" alt="" class="img-circle" width="48" height="48">
                            <span>{{$user->first_name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{url()}}/user/history">My History</a></li>

<!--                            <li><a href="#">Edit Profile</a> </li>-->

                                                       <li><a href="{{url()}}/users/{{Crypt::encrypt(Auth::user()->id)}}">Edit Profile</a></li>

                            <li><a href="{{url()}}/users/logout">Logout</a></li>
                        </ul>

                    </li>

                    <?php
                } else {
                    ?>
                    <li class="sign-login visible-xs visible-sm clearfix"><span><a href="{{url()}}/users/login" class="btn btn-success">USER SIGN UP/LOGIN</a></span></li>

                    <?php }
                ?>



                <li class="{{Request::is('/' ) || Request::is('home') ? 'active' : ''}}"><a href="{{url()}}">HOME</a></li>
                <li class="{{Request::is('search/state/*') ? 'active dropdown' : 'dropdown'}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">LOCATION 	 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @foreach(App\Model\States::orderBy('state_name', 'asc')->get() as $location)
                        <li class="{{Request::is('search/state/'.$location->state_name.'/'.$location->id) ? 'active' : ''}}"><a href="{{url()}}/search/state/{{$location->state_name}}/{{$location->id}}">{{$location->state_name}}</a></li>
                        @endforeach
                        <!--            <li><a href="#">Northern Territory</a></li>
                                    <li><a href="#">Australian Capital Territory</a></li>-->
                    </ul>
                </li>
                <li class="{{Request::is('search/cat/*') ? 'active dropdown' : 'dropdown'}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CATEGORIES 	 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @foreach(App\Model\Categories::all() as $cat)
                        <li class="{{Request::is('search/cat/'.$cat->name.'/'.$cat->id) ? 'active' : ''}}"><a href="{{url()}}/search/cat/{{$cat->name}}/{{$cat->id}}">{{$cat->name}}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="{{Request::is('search/oca/*') ? 'active dropdown' : 'dropdown'}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">OCCASION 	 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @foreach(App\Model\Occasion::all() as $oca)
                        <li class="{{Request::is('search/oca/'.$oca->occation_name.'/'.$oca->id) ? 'active' : ''}}"><a href="{{url()}}/search/oca/{{$oca->occation_name}}/{{$oca->id}}">{{$oca->occation_name}}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="{{Request::is('search/rec/*') ? 'active dropdown' : 'dropdown'}}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">RECIPIENT 	 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @foreach(App\Model\Recipient::where('is_top', '=', '1')->get() as $rec)
                        <li class="{{Request::is('search/rec/'.$rec->name.'/'.$rec->id) ? 'active' : ''}}"><a href="{{url()}}/search/rec/{{$rec->name}}/{{$rec->id}}">{{$rec->name}}</a></li>
                        @endforeach
                    </ul>
                </li>
				
                <?php
                if (Auth::check()) {
                    $user = Auth::user();
                    $site_url = explode("public", url());
                    $user_image = url() . '/images/user.png';


                    if (!empty($user->image)) {
                        
                            $user_image = $s3_url . $s3_env. '/imgs/profile/' . $user->image;
                            //$user_image = url(). '/imgs/profile/' . $user->image;
                            
                    }
                    ?>
                    <li class="bag">
                        <a href="{{url()}}/cart">
                            <span class="{{Request::is('cart') ? 'ic-bag active' : 'ic-bag'}}" id="ic-bag-id">

                                @if(App\Model\UserCart::getCartCountView(Auth::user()->id) > 0)
                                
                                    <span class="badge">{{App\Model\UserCart::getCartCountView(Auth::user()->id)}}</span>
                                    
                                @endif    

                            </span>

                        </a>
                    </li> 
                    <li class="profile dropdown hidden-sm hidden-xs">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="{{asset($user_image)}}" alt="" class="img-circle" width="48" height="48">
                            <span>{{$user->first_name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{url()}}/user/history">My History</a></li>

                                                   <!--<li><a href="#">Edit Profile</a> </li>-->
                        <li><a href="{{url()}}/users/{{Crypt::encrypt(Auth::user()->id)}}">Edit Profile</a></li>

                            <li><a href="{{url()}}/users/logout">Logout</a></li>
                        </ul>
                        <?php
                    } else {
                        ?>
                    <li class="sign-login hidden-sm hidden-xs mr-r10"><span><a href="{{url()}}/users/login" class="btn btn-success">USER SIGN UP/LOGIN</a></span></li>

                    <?php
                }
                ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</header>
