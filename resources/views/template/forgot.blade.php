<?php /* TERMS & CONDITION PAGE HTML */ ?>

<!-- Modal -->
<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Reset password</h4>
        <p class="info-text">Enter your registered email and we’ll send instructions<br> to get you back on your feet.</p>
        <span class="danger" text-align="center" style="color:red; padding-top:30px;padding-left:192px;font-weight:bold;"></span>
      </div>


        <form class="form-horizontal" role="form" method="POST" action="password/email">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="email" class="form-control" name="useremail" id="useremail" placeholder="Enter your email" value="{{ old('email') }}">
			<button type="button" class="btn btn-success btn-block btn-lg" id="sendresetpasslink">RESET PASSWORD</button>


		</form>

<!--      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#forgotApproveModal" data-dismiss="modal">RESET PASSWORD</button>
      </div>-->
    </div>
  </div>
</div>
<div class="modal fade approve-modal" id="forgotApproveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bdr-n">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
      	<p class="info-text mr-0">Please follow the instructions we sent to:</p>
        <h3 class="text-center mr-b50"><strong id="myHeader"></strong></h3>
      </div>
      <div class="modal-footer bdr-n">
        <button type="button" class="btn btn-approve btn-block" id="emailconfirmed"><i class="ic-tick text-hide">Tick</i></button>
      </div>
    </div>
  </div>
</div>
