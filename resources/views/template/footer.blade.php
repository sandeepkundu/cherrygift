<?php /* Footer html of page */ ?>
<!--<footer class="footer">
  <div class="container-fluid">
        <a href="#" class="pull-left">
        <img  alt="logo" src="{{ asset('images/footer-logo.png') }}">
        </a>
            <ul class="footer-social-nav inline-block">
                <li><a href="#" class="facebook"></a></li>
                <li><a href="#" class="twitter"></a></li>
                <li><a href="#" class="instagram"></a></li>
            </ul>
  </div>
</footer>-->

<style>

.footer .footer-left {
    float: none;
}
.footer {
      margin-bottom: -90px !important;
      padding: 0px;
}

.footer-social-nav {
border-left:none;
margin:-28px;
  }


 ul li{list-style-type:none;}

.footer_head{color:#ED1C24;}
.listitems{color:#fff;}
.paragraph{color:#ED1C24}
.mail_add{border-radius: 0px;border: 1px solid rgb(255, 255, 255);padding: 6px; width: 80%;}
.maill{background: #ED1C24;
color: #fff;
font-size: 22px;
padding: :6px;
padding-top: 4px;
padding-bottom: 8px;
padding-right: 7px;
padding-left: 5px;
margin: -4px;
}
.fa-linkedin-square{
      color: #fff;
    font-size: 22px;
    background-color: #0077B5;
    border-radius: 50%;
    margin-left: -15px;
    padding: 12px 15px;
    
}
.fa-youtube-play{
color: #fff;
    font-size: 22px;
    background-color: #c4302b;
    border-radius: 50%;
    
    padding: 12px 13px;
}

#insta{
  color:#fff;
  background: #B43694;
  border-radius: 50%;
  padding: 8px 33px 37px 15px;
  font-size:22px;
}
#tw{
  color:#fff;
  background: #0084b4;
  border-radius: 50%;
  padding: 8px 32px 36px 15px;
  font-size:22px;
   margin: 0px;
}
  #fb{
    color:#fff;
    background: #4169e1;
    border-radius: 50%;
    padding: 9px 29px 34px 15px;
    font-size:22px;
    margin-left: 5px;
      }

      a:hover, a:focus {
    color: #fff;
    text-decoration: none;
}
</style>
 <script type = "text/javascript" 
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">

  </script>
<script>
$(document).ready(function() {
  $( ".sendmail" ).click(function() {
         var input_value=$('.mail_add').val();
          if(input_value==''){
          $('.errorsmailinput').html('Please enter your email id');
          return false; 
          } 
       
        var atpos = input_value.indexOf("@");
        var dotpos = input_value.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=input_value.length) {
           $('.errorsmailinput').html('Not a valid e-mail address');
            return false;
        }    
      $( "#target" ).submit();
  });
});
</script>
<br><br><br><br><br>
<footer class="footer">
  <div class="container-fluid">
    <br>
   <br>
      <!--<div class="footer-left clearfix">-->
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1">
      <div class="col-md-3">
       
          <ul>
            <li class="footer_head">Customer Service</li><br>
            <li ><a href="{{url()}}/faq" class="listitems">FAQs</a></li>
            <li ><a href="{{url()}}/contact-us" class="listitems">Contact Us</a></li>
            <li ><a href="{{url()}}/user/term-condition" class="listitems">Terms & Conditions</a></li>
            <li ><a href="{{url()}}/user/privacy" class="listitems"> Privacy Policy</a></li>
          </ul>
      </div>
      <div class="col-md-3">

        <ul>
                <li class="footer_head">Customers</li><br>
                <li><a href="{{url()}}/users/login" class="listitems">Customer Login</a></li>
                <li><a href="{{url()}}/search" class="listitems">Shop for Vouchers</a></li>                
                <li><a href="{{url()}}/resend" class="listitems"> Recover Voucher</a></li>
             </ul>
         
      </div>
      <div class="col-md-3">
        <ul>
                <li class="footer_head">About Cherrygift</li><br>
                <li><a href="#" class="listitems">The Company</a></li>
                <li><a href="#" class="listitems">Blog</a></li>                
                <li><a href="#" class="listitems">Meet Our Founder</a></li>
             </ul>
        
      </div>
      <div class="col-md-3">
        <ul>
                <li class="footer_head">Suppliers</li><br>
                <li><a href="{{url()}}/admin-signup" class="listitems">Supplier Login</a></li>
                <li><a href="{{url()}}/business" class="listitems">Become a Supplier</a></li>                
                <li><a href="{{url()}}/admin/term-condition" class="listitems">Terms & Conditions</a></li>
             </ul>
      </div>
     </div>
   </div><!--end 1 st row-->
   <br>
   <div class="row">
    <div class="col-lg-10 col-lg-offset-1">
    <div class="col-md-3">
      <ul><li class="paragraph">
      Sign up cherrygift news
    </li>
    </div>
    <div class="col-md-3">
      <form method="POST" class="em_wfe_form" id="target" name="" action="http://email.gatewaymedia.com.au/em/forms/subscribe.php?db=353760&s=89758&a=57063&k=d02426b&va=5" enctype="multipart/form-data">
     
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
     
      <input type="email" value="" class="mail_add" id="em_wfs_formfield_3045093" name="em_wfs_formfield_3045093" validation="1" ftype="1" maxlength=80 size=30 placeholder="Email" required />
      <a href="javascript:void(0)" class="sendmail"><i class="fa fa-envelope maill" aria-hidden="true"></i></a>
    <span class="errorsmailinput" style="color:red"></span>
    </form>
    </div>
    <div class="col-md-3">
      <ul><li class="paragraph">
     Get social with #cherrygift
   </li>
   </ul>
    </div>
    <div class="col-md-3">
      <ul class="footer-social-nav inline-block">
                <li><a href="https://www.facebook.com/cherrygifted/" id="fb" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i>
                 </a></li>
                <li><a href="https://twitter.com/cherrygifted" id="tw" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="https://www.instagram.com/cherrygifted/" id="insta" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="http://www.linkedin.com/company/cherrygift" class="bg_none" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UCgBThtQi5kd137kNU9qYnJQ" class="bg_none" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                
            </ul>
    </div>
  </div>
   </div><!--end 2 nd row-->
      <div class="row">
      <div class="col-lg-10 col-lg-offset-1">
        <hr style="margin-left:15px"/>
      <div class="col-lg-6">
        
        <p style="color:#fff;">&copy; <?php echo date('Y');?> cherrygift</p>
      </div>
      <div class="col-lg-6 text-right">
        <a href="{{url()}}">
        <img alt="" src="http://localhost:8000/images/logo.png">
      </a>
      </div>
      </div>  
     </div><!--end 3 rd row--><br>
  </div>
</footer>
