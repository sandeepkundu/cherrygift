<?php /* TERMS & CONDITION PAGE HTML */ ?>

<!-- Modal -->
<div class="modal fade" id="tutorialModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body pd-0">
      	 
      
      
      <div id="carousel-tutorial" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
         

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="{{ asset('images/tutorial/tutorial.jpg') }}" alt="Find the perfect gift" class="img-responsive">
              <img src="{{ asset('images/tutorial/lens-zoom.png') }}" alt="" class="img-responsive lens-zoom">
              
               <div class="item-text">
                <h4>Find the perfect gift</h4>
                <p>Browse from our collection of gifts by location, category, occasion or recipient. Once you’ve found the most perfect gift for your loved one, choose your amount!</p>
              </div>
            </div>
            <div class="item">
              <img src="{{ asset('images/tutorial/carousel2.jpg') }}" alt="Make it personal">
               <div class="item-text">
               <h4>Make it personal</h4>
                <p>Sweet! You’ve found the perfect gift, now let's send it. Add your recipients's Australian mobile number and your personal message. Don’t forget to let them know who sent it.</p>
              </div>
            </div>
            <div class="item">
              <img src="{{ asset('images/tutorial/carousel3.jpg') }}" alt="Check out your CherryGift">
              <div class="item-text">
               <h4>Time to check out</h4>
                <p>cherrygift is partnered with PayPal so your dollars are secure. You will be emailed a tax invoice and your gift voucher will either be sent immediately or it’s sitting in the queue as per your request.</p>
              </div>
            </div>
              <div class="item">
              <img src="{{ asset('images/tutorial/carousel4.jpg') }}" alt="Reem your CherryGift voucher">
              <div class="item-text">
               <h4>Redeem your voucher</h4>
                <p>Congrats! You are the lucky recipient of a cherrygift voucher. Simply take your smartphone to redeem and present to the sales clerk. They will then enter their merchant PIN to activate your voucher.</p>
              </div>
            </div>
          </div>
			
          <!-- Controls -->
          <div>
              <button type="button" class="btn btn-black col-xs-6 btn-lg" data-dismiss="modal" style="background-color:#353535; padding: 25px 16px; border-radius: 0px;">SKIP</button>
                <div class="controls-ctn col-xs-6">
                  <a class="left" href="#carousel-tutorial" role="button" data-slide="prev">
                   <span class="">Previous</span>
                  </a>
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#carousel-tutorial" data-slide-to="0" class="active">1<span>/</span>4</li>
                    <li data-target="#carousel-tutorial" data-slide-to="1">2<span>/</span>4</li>
                    <li data-target="#carousel-tutorial" data-slide-to="2">3<span>/</span>4</li>
                    <li data-target="#carousel-tutorial" data-slide-to="3">4<span>/</span>4</li>
                    <li>/5</li>
                 </ol>
                 <a class="right" href="#carousel-tutorial" role="button" data-slide="next">
                    <span>Next</span>
                  </a>
                  <a href="#" class="tutorial-done-btn" data-dismiss="modal">
                    <span>Done</span>
                  </a>
              </div>
          </div>
        </div>
    </div>
  </div>
</div>
</div>