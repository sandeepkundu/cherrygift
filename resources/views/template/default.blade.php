<!DOCTYPE html>
<html lang="en">

    <head>
        <title>@yield('title')</title>
        <meta name="description" content="@yield('description')">
        @include('template.header_include')
    </head>
    <body>
        <?php        
        $currentRoute = Route::current();
        $params = $currentRoute->parameters();
        if(Request::server('HTTP_REFERER') == url() . '/') {
            $currentRoute->type;
            $currentRoute->id;
        } else {
            $currentRoute->type = '';
            $currentRoute->id = '';
        }
        ?> 
        @include('template.header')  
        @yield('homebanner')
        <!-- Page Search -->
        @if(empty($no_search_bar))
        @include('template.search_bar')
        @endif
        <!--End of search Page-->
        @yield('content')
        @include('template.footer')

        @include('template.tutorial')

        @include('template.footer_include')
        <script type="text/javascript" src="{{ URL::asset('/scripts/validation.min.js') }}"></script>  
        <script>
            $('#tutorialModal').on('hidden.bs.modal',function(){
                $('#carousel-tutorial').carousel(0)
            })
            $('#carousel-tutorial').on('slid.bs.carousel', checkitem);
            checkitem()
            function checkitem(){
                var $this = $("#carousel-tutorial");
                $this.find('.controls-ctn .tutorial-done-btn').hide();
                if($('#carousel-tutorial .carousel-inner .item:first').hasClass('active')) {
                    $this.find('.controls-ctn .left').hide();
                } else {
                    $this.find('.controls-ctn .left').show();
                }
                if($('.carousel-inner .item:last').hasClass('active')) {
                    $this.find('.controls-ctn .right').hide();
                    $this.find('.controls-ctn .tutorial-done-btn').show();
                } else {
                    $this.find('.controls-ctn .right').show();
                    $this.find('.controls-ctn .tutorial-done-btn').hide();
                }
            }
        </script>
        <script type="text/javascript">
            window.__lc = window.__lc || {};
            window.__lc.license = 7576301;

            (function() {
                var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
            })();
        </script>

    </body>
</html>


