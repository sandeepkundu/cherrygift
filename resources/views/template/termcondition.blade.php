<?php /* TERMS & CONDITION PAGE HTML */ ?>

<!-- Modal -->
<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
      </div>
      <div class="modal-body">
      	<div class="scrollbar-macosx">
        <div class="termsCont">
                    <p>This is an agreement between You, the Customer, (“user”, “Customer” or “You”) and cherrygift. The terms and conditions set out in this Agreement are the basis upon which cherrygift is prepared to supply the Voucher. If You register as a user/customer with cherrygift, then You are agreeing to accept and comply with the terms and conditions set out in this Agreement on and from the date of registration until your arrangements with cherrygift come to an end. </p>
                    <h4>background</h4>
                    <ul class="upperAlpha">
                        <li><span>cherrygift is a web-based business which allows customers to purchase gift vouchers from a number of different business/Suppliers/providers, with delivery of such voucher to be effected by text message to the nominated recipient’s mobile phone, which must be a smartphone.</span></li>
                    </ul>
                    <h4>THE PARTIES AGREE:</h4>
                    <h4>DEFINITIONS</h4>
                    <p>In this Agreement unless inconsistent with the context or subject matter:</p>
                    <ul class="number">
                        <li><span>“Australian Consumer Laws” means The Australian Consumer Law contained in Schedule 2 of Competition and Consumer Act 2010 as amended from time to time;</span></li>
                        <li><span>“cherrygift” means Fionnghal Pty Ltd ABN 52 146 304 181 trading as cherrygift; </li>
                        <li><span>“cherrygift Website” means this website and any other websites associated with cherrygift’s business;</span></li>
                        <li><span>“Services” means the services provided by cherrygift as set out under clause headed “Services”</span></li>
                        <li><span>“smartphone” means a device which combines a mobile telephone with a handheld computer, and which must, for the purposes of this Agreement, have Internet access capabilities (WIFI or data enabled at the time of redemption);</span></li>
                        <li><span>“Supplier” means the retailer, merchant, product provider or business which offers gift vouchers via the cherrygift Website;</span></li>
                        <li><span>“Voucher” means the gift voucher purchased via the cherrygift Website;</span></li>
                        <li><span>“Voucher Details” means the URL sent to the nominated mobile phone number to allow the recipient to access Voucher details.</span></li>
                    </ul>
                    <h4>registration</h4>
                    <ul class="number">
                        <li><span>If You wish to take advantage of the Services offered by cherrygift, You are required to apply to register as a user on the cherrygift Website. Any information provided to cherrygift shall be handled in accordance with the cherrygift privacy policy available on the cherrygift Website.</span></li>
                        <li><span>cherrygift reserves the right, in its absolute discretion, to refuse, decline or deny any application for registration on whatsoever grounds cherrygift deems appropriate and cherrygift is under no obligation to provide any explanation of any such refusal, declination or denial. cherrygift further reserves the right, in its absolute discretion, to terminate the registration of any user on whatsoever grounds cherrygift deems appropriate.</span> </li>
                        <li><span>In registering as a user, you are warranting to cherrygift that:-</span>
                    <ul class="alphaRound">
                        <li>The information you are providing is true and accurate;</li>
                        <li>The recipient of the Voucher and the nominated mobile phone number is attached to a ‘smartphone’ and the number is correct;</li>
                        <li>You are able to enter a legally binding contract</li>
                        <li>You accept the terms and conditions contained in this Agreement and as set out on the cherrygift Website from time to time;</li>
                        <li>All intellectual property rights including without limitation any copyright or trademark rights, remains the intellectual property of cherrygift and shall not be used in any manner without the consent of cherrygift.</li>
                    </ul>
                    </li>
                        <li><span>It shall be the sole responsibility of the Customer to keep the details of their registration up to date and notified to cherrygift and cherrygift takes no responsibility for loss occasioned as due to You failing to keep the details of your registration current</span></li>
                        <li><span>You may not use the content of the cherrygift Website in any manner that would threaten or infringe the intellectual property rights of cherrygift.</span></li>
                    </ul>
                    <h4>services</h4>
                    <ul class="number">
                        <li><span>The Services to be provided by cherrygift are:</span> 
                        <ul class="alphaRound">
                           <li>Allowing customers to purchase gift vouchers online for cherrygift’s various Suppliers; </li>
                           <li>Sending gift vouchers to nominated recipients via text message in accordance with voucher order requirements (as input by the customer);</li>
                           <li>Issue alert reminders to voucher holders in accordance with cherrygift policies.</li>
                            </ul>
                        </li>
                        
                    </ul>
                    <h4>VOUCHERS, payment and delivery</h4>
                    <ul class="number">
                        <li><span>You may use the cherrygift Website to purchase gift vouchers to be used at the business of a Supplier.</span></li>
                        <li><span>All prices quoted on the cherrygift Website are in AUD and are inclusive of GST.</li>
                        <li><span>The Voucher you purchase shall not be delivered until payment has been received in full. Payment methods are as set out on the cherrygift Website from time to time.</span></li>
                        <li><span>Upon purchasing the Voucher, the Customer will nominate a mobile telephone number for delivery of the Voucher Details. The User may also requested the date and time (in the future) at which the Voucher Details shall be sent to the recipient’s mobile phone. cherrygift will use all reasonable endeavours to send Voucher Details at the requested time. The recipient may be the Customer or another person. The Voucher Details will only be sent to one nominated mobile phone number and can only be sent to a smartphone.</span></li>
                        <li><span>The Customer is responsible for ensuring the accuracy of the mobile phone number for delivery of the Voucher Details. cherrygift accepts no responsibilities for incorrect or incomplete delivery of Voucher Details resulting from incorrect details being provided to cherrygift.</span></li>
                        <li><span>cherrygift reserves its right to decline any transaction in its absolute discretion.</span></li>
                        <li><span>All gift vouchers (including the Voucher) offered for purchase through the cherrygift Website are subject to the terms and conditions of the Supplier in providing the product or service the subject of the Voucher.</span></li>
                        <li><span>cherrygift shall send a reminder notice to the nominated mobile phone number six (6) months prior to the expiry of the Voucher, if the Voucher has not yet been redeemed.</span></li>
                    </ul>
                    <h4>refunds and re-delivery</h4>
                    <ul class="number">
                        <li><span>If You have purchased a Voucher and change your mind you are not entitled to a refund.</span></li>
                        <li><span>In the event Voucher Details are not delivered to the recipient (unless as a result of cherrygift’s error), then the Customer can request redelivery of the Voucher Details by notifying cherrygift. cherrygift will consider each request for redelivery on its merits. cherrygift is not required to redeliver Voucher Details unless due to cherrygift’s error, but can agree to do so in its absolute discretion. cherrygift is not responsible for any non-delivery of Voucher Details as a result of being blocked by the recipient’s provider or due to an error in inputting details (made by the Customer).</span></li>
                        <li><span>Any refund agreed by cherrygift shall be processed to the Customer only. A recipient of Voucher Details is not entitled to a refund.</span></li>
                        <li><span>In the event Voucher Details are inadvertently deleted, the Customer or the recipient should notify cherrygift and cherrygift will, subject to cherrygift being satisfied that it is appropriate to do so, re-send the Voucher Details to the nominated mobile phone number at the time of purchase. </span></li>
                        <li><span>In the event the nominated mobile phone number is disconnected or otherwise unavailable due to cancellation of a service or change of number, then the Customer can request that the Voucher Details be re-sent to an alternative mobile phone number. cherrygift will use its best endeavours to comply with such requests.</span></li>
                        <li><span>A redelivery charge may be applied in cherrygift’s absolute discretion.</span></li>
                    </ul>
                    <h4>voucher redemption process</h4>
                     <ul class="number">
                         <li><span>The Voucher purchase and redemption process is as set out below:-</span>
                     <ul class="alphaRound">
                         <li>The Customer shall purchase the Voucher via the cherrygift Website. The Customer may nominate him or herself or another party as the recipient (“recipient”);</li>
                         <li>Following the successful purchase of the Voucher, cherrygift shall send the Voucher Details to the recipient by text message in accordance with the purchase directions provided by the Customer;</li>
                          <li>The Voucher shall be linked, in cherrygift’s system, to the Supplier who is provided with a unique and secure Supplier ID;</li>
                           <li>The voucher holder (whether the customer, the nominated recipient or another party bearing a valid and unredeemed Voucher) shall redeem the Voucher by presenting to the Supplier the Voucher on the voucher holder’s smartphone. The recipient must have a smartphone with internet access at the time of redemption in order to redeem the Voucher;</li>
                            <li>Provided that the Voucher is not shown to be already redeemed, the Supplier will then accept presentation of the Voucher and cause it to be redeemed by inputting the Supplier’s unique ID;</li>
                             <li>Each Voucher is linked to a Supplier ID and once the Supplier ID has been entered, the Voucher will be noted as ‘redeemed’ in cherrygift’s system and can no longer be used.</li>
                             <li>The terms of the Voucher redemption are as follows:-
                             <ul class="lowerRoman">
                                 
                                 <li>The Voucher must be redeemed in one transaction; </li>
                                 <li>If the value of the Voucher is not utilised in one transaction, it is at the discretion of the Supplier whether the Supplier provides cash back or refund or additional credit;</li>
                                 <li>The purchase of any product/service over and above the face value of the Voucher shall be the responsibility of the voucher holder;</li>
                                 <li>Each Voucher is valid for a period of twelve months unless cherrygift deems, in its absolute discretion, that an extension of this period is warranted in the circumstances.
</li>
                             </ul>
                             </li>
                     </ul>
                     </li>
                    </ul>
                    <h4>guarantees, warranties and indemnities</h4>
                    <ul class="number">
                        <li><span>Where a Customer is a ‘consumer’ under the Australian Consumer Laws and where the Australian Consumer Laws apply to a transaction, cherrygift will comply with its obligations under those laws in respect of the statutory guarantees provided.</span></li>
                        <li><span>If the Australian Consumer Laws do not apply in respect of a transaction, and the Customer is not a consumer then to the greatest extent allowed at law, cherrygift limits its liability for breach of an implied warranty or condition to the value of the Voucher or payment of the costs of having the Voucher reissued.</span></li>
                        <li><span>cherrygift accepts no liability for any loss suffered by You or a recipient of the Voucher or the voucher holder at the time of redemption other than to the extent that cherrygift cannot limit its liability at law.</span></li>
                        <li><span>The product, service, experience or other which is the subject of the Voucher shall be subject to the terms and conditions of the relevant Supplier and cherrygift shall have no connection with or liability in relation to the product, service, experience or other provided by the Supplier.</span></li>
                        <li><span>In accessing the cherrygift Website and purchasing a Voucher, You agree to indemnify and hold cherrygift harmless from any claims, demands, actions, damages, costs and expenses (including legal fees) which arise from or in connection with your use of the cherrygift Website and/or the purchase or redemption of the Voucher (to the extent permitted at law).</span></li>
                    </ul>
                    <h4>website</h4>
                    <ul class="number">
                        <li><span>Where the cherrygift Website contains advertising or links to other websites, cherrygift takes no responsibility for and excludes any liability resulting from the content of those websites. A link or advertisement on the cherrygift website is in no way an endorsement of that business or provider.</span></li>
                        <li><span>Any trademarks, copyrights, graphics, images, text, layout, logos or similar which are used on the cherrygift Website (excluding the advertisements and links referred to above) are owned by or licensed to cherrygift and may not be used, downloaded, recreated, copied or in any way exploited without the express written consent of cherrygift.</span> </li>
                        <li><span>Where any ‘comments’ or ‘feedback’ are left on the cherrygift Website then such comments become the property of cherrygift and may be used by cherrygift, without compensation to the ‘commenter’ in promoting the cherrygift Website.</span></li>
                        <li><span>cherrygift may suspend the cherrygift Website in its absolute discretion and shall not be liable to any person in the event of the suspension of the cherrygift Website.</span> </li>
                        <li><span>cherrygift will take all due care to ensure that the cherrygift Website is free from virus, Trojan horse software and/or malware but accepts no responsibility or liability which arises from the use of the cherrygift Website and any advertisement or linked website.</span></li>
                    </ul>
                    <h4>No liability</h4>
                    <ul class="number">
                        <li><span>In addition to the limitations set out in this Agreement, cherrygift shall not be responsible for any loss or damage caused:-</span>
  <ul class="alphaRound">
      <li>If You fail to follow the Voucher Redemption Process in relation to a Voucher;</li>
      <li>By any fraudulent use of a Voucher;</li>
      <li>By the failure or faults with the cherrygift Website (and any associated software/platform) (although cherrygift shall take all reasonable steps to rectify such failure or fault as soon as possible). 
</li>
 
  </ul>
                        </li>
                    </ul>
                    
                    <h4>PRIVACY</h4>
                    <ul class="number">
                        <li><span>cherrygift shall at all times comply with its Privacy Policy, details of which are available on the cherrygift Website. cherrygift shall decline to provide any information or take any action if to do so would breach its Privacy Policy. </span></li>
                        </ul>
                    
                   <h4>GENERAL</h4>
                    <ul class="number">
                        <li><span>The parties agree that:-</span>
                        <ul class="alphaRound">
                            <li>This Agreement contains the entire understanding and agreement between the parties as to the subject matter of this Agreement;</li>
                            <li>All previous negotiations, understandings, representations, warranties, memoranda or commitments about the subject matter of this Agreement are merged in this Agreement and are of no further effect.</li>
                            <li>No oral explanation or information provided by a party to another affects the meaning or interpretation of this Agreement or constitutes any collateral agreement, warranty or understanding.</li>
                            
                        </ul>
                        </li>
                        <li><span>No waiver by a party of a provision of this Agreement is binding unless made in writing.</span></li>
                        <li><span>cherrygift may vary the terms of the Agreement by notice in writing to you, provided that you are not materially prejudiced by the variation. </span></li>
                        <li><span>If a provision of this Agreement is void or unenforceable it must be severed from this Agreement and the provisions that are not void or unenforceable are unaffected by the severance.</span></li>
                        <li><span>The rights and remedies of a party to this Agreement are in addition to the rights or remedies conferred on the party at law or in equity.</span></li>
                    </ul>
                    <h4>GOVERNING LAW</h4>
                     <ul class="number">
                         <li><span>This Agreement is governed by the laws of Queensland and the Commonwealth of Australia which are in force in Queensland.</span></li>
                         <li><span>The parties submit to the jurisdiction of the Courts of Queensland, relevant Federal Courts and Courts competent to hear appeals from them.</span></li>
                    </ul>
                </div>
        </div>
      </div>
      <div class="modal-footer">
       
        <button type="button" class="btn btn-success btn-block" id="termconditionaccepted">OK</button>
      </div>
    </div>
  </div>
</div>