<?php
$useMinified = true;
/*"<?php echo asset('css/common.css')?>"
* "{{{ asset('/css/style.css') }}}"
*  Some common CSS and head section of page */ ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="msapplication-tap-highlight" content="no" />
<meta name="apple-mobile-web-app-capable" content="yes">

<link rel="apple-touch-icon-precomposed" href="<?php echo  asset('/images/cherry.ico') ?>">
<link rel="shortcut icon" href="<?php echo  asset('/images/cherry.ico') ?>">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!--FONTS-->
<link href="<?php echo  asset('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800') ?>" rel='stylesheet' type='text/css'>
<?php
if ($useMinified) {
    ?>
    <link type='text/css' rel='stylesheet' href="/styles/cherry.min.css" >
    <script src="/scripts/cherry-gift.min.js"></script>

    <?php    
}   
else {     
    ?>
    <link type='text/css' rel='stylesheet' href="<?php echo asset('/styles/font-awesome.min.css') ?>" >
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/bootstrap.css') ?> ">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/jquery.mCustomScrollbar.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/jquery.scrollbar.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/bootstrap-datetimepicker.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/bootstrap-select.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/fonts.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/common-layout.css') ?> ">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/style.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/style-responsive.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/jquery-ui.min.css') ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/owl.carousel.css') ?>">

    <script src="<?php echo asset('/scripts/avatar.js') ?>"></script>
    <script src="<?php echo asset('/scripts/moment.min.js') ?>"></script>
    <script src="<?php echo asset('/scripts/bootstrap-datetimepicker.min.js') ?>"></script>
    <script src="<?php echo asset('/scripts/jquery-migrate-1.2.1.min.js') ?>"></script>

    <script src="<?php echo asset('scripts/jquery-ui.js') ?>"></script>

    <script src="<?php echo asset('scripts/jstz.min.js') ?>"></script>
    <link type="text/css" rel="stylesheet" href="<?php echo asset('/styles/bootstrap-select.min.css') ?>">    
    <?php
}
?>    

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-77931905-1', 'auto');
    ga('send', 'pageview');

</script>








