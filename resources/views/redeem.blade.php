<!DOCTYPE html>
<html lang="en">
    <head>
        <title>cherrygift - redeem</title>
        @include('template.header_include')
        @include('template.headerlogo')
        </head>
    <body class="mar-cont">
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>
     <div class="redeem-wrapper">
         <div class="container-fluid">
             <h2 class="page-heading hidden-xs">Redeem</h2></div>
    	<div class="container-fluid redeem-main">
    	    <div class="container redeem-code">
    	        <div class="col-sm-12 col-md-10 col-md-offset-1">
    	               <div class="row mr-b20 buy-voucher">
                    	<div class="col-xs-12 col-sm-7 mob-pad0">
                        	<div class="product-item inline-block mprd-top">
                                    @foreach($companybusinessimages as $voucharimage)
                                        @if($voucharimage->voucher_image==1)
                                    <img src="{{$s3_url}}{{$s3_env}}{{$voucharimage->image}}" style="width: 321px; height: 200px;" alt="" class="img-responsive img-rounded mob-smimg">
                                @endif
                                    @endforeach
                                    <div class="item-upper-bg">
                                    <div class="media"> 
                                        <div class="media-left"> 
                                            <img class="media-object small-circle" src="{{$s3_url}}{{$s3_env}}{{$companybusiness->logo}}" style="width: 35px; height: 35px;">
                                        </div> 
                                        <div class="media-body media-middle"> 
                                            <h4 class="media-heading">{{$companybusiness->store_location}}</h4> 
                                        </div>
                                    </div>
                                </div>
                        	</div>
                        	    <div class="inline-block">
                       		<h3 class="mr-0 price">${{number_format($voucherdetails->voucher_price,0)}}</h3>
                        </div>
                        </div>
                    
                        <div class="col-sm-5 redeem-v-code text-center hidden-xs">
                	<p class="text-label">VOUCHER CODE</p>
                    <p class="text-code">{{$voucherdetails->voucher_unique_id}}</p>
                    @if($voucherdetails->redeemed==1)
                    <p class="clsError">Gift voucher already redeemed!</p>
                    
                    @elseif($voucherdetails->voucher_status==0)
                    <p class="clsError">Cancelled!</p>
                    @elseif(strtotime(date('Y-m-d')) > strtotime(date('Y-m-d', strtotime(date("Y-m-d", strtotime($voucherdetails->sms_date)) . " +1 year"))))
                    <p class="clsError">Gift voucher expired!</p>
                    @else
                    <a href="javascript:void(0)" data-target="#redeemModal" data-toggle="modal"><span class="voucher-delete"><img width="62" height="62" alt="" src="{{asset('images/trash-b-icon.png')}}"></span></a>
                    <br/><br/>
                    <div>Please take your phone to redeem digital voucher</div>
                @endif
                        </div>
                    </div>
                    <div class="row  hidden-xs">
                        <div class="col-sm-12">
                           <div class="redeemText">
                            <h3>{{$companybusiness->store_location}}</h3>
                            <p>{{$companybusiness->short_desc}}</p>
                        </div>
                    </div>
                </div>
                
                <div class="redeem-list row">
               
               
                <div class="col-sm-12">
                    <h2 class="hidden-xs">Store address</h2>
                    <div class="right-item">
                        <div class="row redeem-info-list">
                            <div class="col-sm-4 column-first">
                                <div class="general-info">
                                <p class="cmpy-name text-dots">{{$companybusiness->store_location}}</p>
                                <p class="address">{{$companybusiness->address}}<br> {{$statename}}, {{$cityname}} {{$companybusiness->post_code}}</p>
                            </div>
                            </div>
                            <div class="col-sm-4 general-info">
                                <div class="column-common">
                                    <h5 class="heading">CONTACT US</h5>
                                    <ul class=" email-list contact-info contact-list">
                                        @if(!empty($companybusiness->email))<li title="{{$companybusiness->email}}"><span class="email"></span><a href="mailto:{{$companybusiness->email}}">Send an Email</a></li>@endif
                                        @if(!empty($companybusiness->phone))<li><span class="mobile"></span><b>@if(substr($companybusiness->phone, 0, 4)!="1300") {{$companybusiness->country_code}} @endif  {{$companybusiness->phone}}</b></li>@endif
                                        @if(!empty($companybusiness->website))<a href="http://{{$companybusiness->website}}" title="{{$companybusiness->website}}" target="_blank"><li><span class="web"></span>Visit Website</li></a>@endif
                            </ul>{{--*/ @$count = 0;  /*--}}
                        <ul class="info-list contact-info contact-list mr-t5">
                            @foreach($companybusinesslocation as $locations) 
                            @if($locations->id!=$companybusiness->id)
                            {{--*/ @$count++;  /*--}}
                            <li><span class="location" id="all_locations"></span>{{$locations->address}}, <br> {{nl2br(e($locations->state_name))}},<br> {{$locations->locality}}</li>
                            @endif
                            @endforeach
                        </ul>
                                     @if($count>=3)
                        <div class="loadMore">See All</div>
<div class="showLess" id="showLesslocation">See Less</div>
@endif
                    
                                </div>
                            </div>
                            <div class="col-sm-4 general-info">
                                <div class="column-common">
                                    <h5 class="heading">WE ARE OPEN</h5>
                                     <ul class="info-list time-info">
                             @for($j=1; $j<=7; $j++) 
                             <li><span class="day">{{ucfirst(strtolower(Config::get('constants.dayname.'.$j)))}} - </span><b>{{$companytiming[$j]}}</b></li>
                            
                @endfor
                        </ul>
                        <div class="loadMore">See All</div>
<div class="showLess" id="showLessday">See Less</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mr-t15">
                  <div class="col-sm-12 redeem-v-code text-center visible-xs">
                	<p class="text-label">VOUCHER CODE</p>
                    <p class="text-code">{{$voucherdetails->voucher_unique_id}}</p>
                    @if($voucherdetails->redeemed==1)
                    <p class="clsError">Gift voucher already redeemed!</p> 
                    @else
                    <a href="javascript:void(0)" data-target="#redeemModal" data-toggle="modal"><span class="voucher-delete"><img width="62" height="62" alt="" src="{{asset('images/trash-b-icon.png')}}"></span></a>
                    <br/><br/>
                    <div>Please take your phone to redeem digital voucher</div>
               @endif
                  </div>
            </div>
    	    </div>
    	</div>
        
        
    
        
        </div></div>
 	<div class="modal fade" id="redeemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        <h4 class="modal-title" id="myModalLabel" style="padding-bottom:5px ">Merchant PIN</h4>
        <div class="text-center">Retailer Use Only</div>
        <span class="danger"  style="color:red; text-align:center; display: block"></span>
      </div>


        <form class="form-horizontal" role="form" method="POST" action="validate/merchant_pin">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="voucher_id" value="{{ $voucherdetails->id }}">
                        <input type="text" class="form-control f_24 mob-f15" name="merchant_pin" id="merchant_pin" placeholder="Enter your 8 digit Merchant PIN" value="{{ old('merchant_pin') }}" maxlength="8">
                <button type="button" class="btn btn-success btn-block btn-lg" id="check_merchant_pin"><span>${{number_format($voucherdetails->voucher_price,0)}}</span>REDEEM</button>


		</form>

                             
    </div>
  </div>
</div>

<div class="modal fade approve-modal" id="redeemedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bdr-n">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
        
      </div>
      <div class="modal-body">
      	
        <h3 class="text-center mr-b50"><strong id="myHeader">VOUCHER APPROVED</strong></h3>
      </div>
     <div class="modal-footer bdr-n">
        <button id="emailconfirmed" class="btn btn-approve btn-block" type="button"><i class="ic-tick text-hide">Tick</i></button>
      </div>
    </div>
  </div>
</div>  
    
    
        @include('template.footer_include')

<script type="text/javascript" src="{{ URL::asset('/scripts/validation.min.js') }}"></script> 
</body>
</html>
