Hi {{ $enquiry->first_name }},
<br>
<br>
Thank you for your enquiry.  We will be in contact shortly.  A copy of your enquiry is included below.
<hr />
<table cellpadding="2" cellspacing="2">
    <tr>
        <td><strong>First name</strong></td>
        <td>{{ $enquiry->first_name }}</td>
    </tr>
    <tr>
        <td><strong>Last name</strong></td>
        <td>{{ $enquiry->last_name }}</td>
    </tr>
    <tr>
        <td><strong>Contact Number</strong></td>
        <td>{{ $enquiry->contact_number }}</td>
    </tr>
    <tr>
        <td><strong>Email</strong></td>
        <td>{{ $enquiry->email }}</td>
    </tr>
    <tr>
        <td valign="top"><strong>Message</strong></td>
        <td valign="top"><?= nl2br(htmlspecialchars($enquiry->message)) ?></td>
    </tr>
    
</table>
<hr />
<br>
Regards <br>
cherrygift<br><br>
<img src="{{ $message->embed(public_path('images/email-logo.png')) }}" alt="logo">

