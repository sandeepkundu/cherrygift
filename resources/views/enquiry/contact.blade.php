@extends('template.main')
@section('title','cherrygift contact us')

@section('content')

<?php
Form::macro('bsLabel',function($name,$label,$req=false) {
    return '<label for="'.$name.'" class="col-sm-4 control-label">'.htmlspecialchars($label).($req ? '<em>*</em>' : null).'</label>';
})
?>
<style>
body {
    background-color:#fff;
}
</style>
<div class="page-wrapper clearfix enquiry-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <header class="text-center">
                    <h2 class="page-heading">CONTACT US</h2>
                </header>
                <?php
                if (!count($errors)) { ?>
                    <section>
                        <h2>Have a question for us? Check out our <a href="{{url()}}/faq">Frequently Asked Questions</a> and if your answer isn’t there please use the form below to contact us...</h2>
                    </section>
                    <?php
                } 
                ?>
                <section class="mt-20 mb-20">
                    <div class="row">
                        <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                            <?php
                            if (count($errors)) { ?>
                                <div class="alert alert-danger mt-m20">
                                    <h2>Unable to submit your enquiry</h2>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <?php
                            }
                            echo Form::model($enquiry,['class' => 'form-horizontal']); 
                            ?>
                            <div class="form-group">
                                <?= Form::bsLabel('first_name','First Name',true); ?>
                                <div class="col-sm-8">
                                    <?=  Form::text('first_name', null,['class' => 'form-control']); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Form::bsLabel('last_name','Last Name',true); ?>
                                <div class="col-sm-8">
                                    <?=  Form::text('last_name', null,['class' => 'form-control']); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Form::bsLabel('contact_number','Contact Number'); ?>
                                <div class="col-sm-8">
                                    <?=  Form::text('contact_number', null,['class' => 'form-control']); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Form::bsLabel('email','Email',true); ?>
                                <div class="col-sm-8">
                                    <?=  Form::text('email', null,['class' => 'form-control']); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Form::bsLabel('message','Your Message',true); ?>
                                <div class="col-sm-8">
                                    <?=  Form::textarea('message', null,['class' => 'form-control']); ?>
                                </div>
                            </div>
                            <div class="form-group mr-b0">
                                <div class="col-sm-offset-4 col-sm-5 submit-ctn">
                                    <?= Form::submit('Send Enquiry',['class' => 'btn btn-success btn-block']) ?>
                                </div>
                            </div>                            
                            <?php
                            echo Form::close();                              
                            ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>
@stop





