@extends('template.main')
@section('title','cherrygift contact us')

@section('content')
<style>
body {
    background-color:#fff;
}
</style>
<div class="page-wrapper clearfix enquiry-thanks-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <header class="text-center">
                    <h2 class="page-heading">CONTACT US</h2>
                </header>
                <h2 class="text-center">Thank you for your enquiry.<br />We will be in contact shortly.</h2>
            </div>
        </div>
    </div>

</div>
@stop





