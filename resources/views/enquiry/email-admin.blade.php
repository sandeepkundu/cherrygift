Hi Cherry Gift,
<br>
<br>
The following enquiry was just received via the web site.
<hr />
<table cellpadding="2" cellspacing="2">
    <tr>
        <td><strong>Enquiry</strong></td>
        <td><?= sprintf('#%05d',$enquiry->id) ?></td>
    </tr>
    <tr>
        <td><strong>Submitted</strong></td>
        <td><?= $enquiry->created_at->modify('+10 hour')->format('d-M-Y g:ia') ?></td>
    </tr>
    <tr>
        <td><strong>First name</strong></td>
        <td>{{ $enquiry->first_name }}</td>
    </tr>
    <tr>
        <td><strong>Last name</strong></td>
        <td>{{ $enquiry->last_name }}</td>
    </tr>
    <tr>
        <td><strong>Contact Number</strong></td>
        <td>{{ $enquiry->contact_number }}</td>
    </tr>
    <tr>
        <td><strong>Email</strong></td>
        <td>{{ $enquiry->email }}</td>
    </tr>
    <tr>
        <td valign="top"><strong>Message</strong></td>
        <td valign="top"><?= nl2br(htmlspecialchars($enquiry->message)) ?></td>
    </tr>
    
</table>
<hr />
<br>
Regards <br>
cherrygift<br><br>
<img src="{{ $message->embed(public_path('images/email-logo.png')) }}" alt="logo">

