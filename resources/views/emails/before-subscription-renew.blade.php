Hi  {{$firstname}}

<br>
<br>
It’s almost time…. to renew your subscription!
<br/>
<br/>
This is just a courtesy reminder that your cherrygift subscription with auto-renew in 30 days.
<br/>
Please ensure that your account information is all up to date to avoid any disturbance to your listing.
<br/>
<br/>
If you have any concerns regarding your account, please contact one of our team on 1300 122 321 or email {{env('contact_email')}}
<br/>
<br/>
We look forward to continue working with you to help your business GROW one cherrygift at a time!
<br/>
<br/>
<br/>
Cheers,
<br/>
From the cherrygift team!<br/><br/>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">