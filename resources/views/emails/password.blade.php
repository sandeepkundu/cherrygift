Hi @if($user->role=="user") ? {{$user->first_name}} : {{'Admin'}}, @endif

<br>
<br>
We’ve received a request to change your cherrygift password.<br>
If you made this request, please click on the link below to reset your password using our secure server.<br>

Else, you can ignore this email and your password will remain as is.<br><br>
<a href="{{ url('password/reset/'.$token.'?mail='.$user->email) }}">Reset Password</a><br><br>

The above link will expire in 42 hours.
<br>
<br>
Regards <br>
cherrygift<br><br>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">