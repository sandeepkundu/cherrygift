Hi  {{$firstname}}
<br/>
<br/>
Your Subscription for {{$store_location}} is auto-renewed.
<br/>
Enjoy 12 months of subscription.
<br/>
<br/>
If you have any concerns regarding your account, please contact one of our team on 1300 122 321 or email {{env('contact_email')}}.
<br/>
We look forward to continue working with you to help your business GROW one cherrygift at a time!
<br/>
<br/>
<br/>
Cheers,
<br/>
From the cherrygift team!<br/><br/>
<img src="{{ $message->embed(public_path('images/image.jpg')) }}" alt="logo">