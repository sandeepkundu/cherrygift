@extends('template.default')
@section('title','cherrygift - order detail') 
<?php 
$s3_url = env('s3_url');
$s3_env = env('s3_env');
?>
@section('content') 
<div class="buy-voucher-wrapper">
    <div class="container">
        <div class="row">
            @if(Session::has('message'))
            <div class="alert alert-success">
                <h3 style='text-align: center'>{{ Session::get('message') }}</h3>
            </div>
            @endif

            @if(!empty($invalid_voucher)) 
            <div class="invalid-error">Invalid Voucher</div>
            @else
            <div class="col-sm-12 col-md-10 col-md-offset-1 invaild-message">
                <h2 class="page-title">Order Detail</h2>
                <div class="row mr-b50 buy-voucher">
                    <div class="col-sm-4 inline-block">
                        <div class="product-item ">
                            <h3 class="price visible-xs">${{$voucher_price}}</h3>
                            <img src="{{$s3_url}}{{$s3_env}}{{$image}}" alt="" class="img-responsive img-rounded" style="height: 200px;" width="300"height="200" >

                            <div class="item-upper-bg">
                                <div class="media"> 
                                    <div class="media-left"> 
                                        <img class="small-circle" src="{{$s3_url}}{{$s3_env}}{{$company_logo}}" style="width: 35px; height: 35px;"> 
                                    </div> 
                                    <div class="media-body media-middle"> 
                                        <h4 class="media-heading">{{$store_location}}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inline-block">
                        <h3 class="mr-0 price hidden-xs">${{$voucher_price}}</h3>
                    </div>
                </div>
                <div class="row">
                    <form action="{{url()}}/post_buy_voucher" method="post" id="buy_voucher_form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="scheduled" value="0">
                        <input type="hidden" id="sms_schedule" name="sms_schedule" value="0">
                        <input type="hidden" id="sms_timezone" name="sms_timezone" value="">
                        <input type="hidden" id="sms_timezone_offset" name="sms_timezone_offset" value="">
                        <input type="hidden" id="company_business_profile_id" name="company_business_profile_id" value="{{$company_business_profile_id}}">
                        <input type="hidden" id="voucher_price" name="voucher_price" value="{{$voucher_price}}">
                        <input type="hidden" id="cart_voucher_id" name="cart_voucher_id" @if(!empty($voucher_cart_id)) value="{{$voucher_cart_id}}" @endif >
                               <div class="col-sm-6">
                            <div id="datetimepicker">
                                <input type="hidden" id="datetimepicker1" name="sms_date" @if(!empty($sms_date)) value="{{$sms_date}}" @endif>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">

                                <div class="col-xs-12">
                                    <div class="form-group">

                                        <div class="row">
                                            <div class="col-xs-4">&nbsp;
                                                <input type="text" style="background-color: lightgrey" readonly="" value="+61" name="country_code" id="country_code" class="form-control">
                   <!--                                                    <select class="selectpicker" data-width="100%">
                                                                           <option>option 1</option>
                                                                           <option>option 2</option>
                                                                           <option>obtion 3</option>
                                                                       </select>-->
                                            </div>
                                            <div class="col-xs-8"><span style="padding-left: 1px">Recipient's mobile number here</span>
                                                <input type="text" @if(!empty($phone_number)) value="{{$phone_number}}" @endif  class="form-control" name="phone_number" id="mobile_number" placeholder="Mobile number" maxlength="15">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <textarea  name="sms_txt" id="sms_txt" maxlength="1600" class="form-control"
                                                   placeholder="Your message here. 
From 
Dave x 
(max 1600 characters)" rows="5"><?php
                                            if (!empty($sms_message)) {
                                                echo $sms_message;
                                            }
                                            ?></textarea>

                                    </div>
                                    <button id="send_now" name="send_now" value="send_new" class="btn btn-block btn-success mr-b20">SEND NOW</button>
                                    <div class="form-group mr-b30">
                                        <label>Send in the future? (24hr clock)   <br/> 
                                            <span style="display: none" id='ct'  ></span>
                                            <span id="client_timezone"></span>
                                        </label>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <select class="selectpicker"  name="sms_hour" id="sms_hour" data-width="100%" maxOptions="5" data-size="5">
                                                    <option>Hour</option>
                                                    <?php
                                                    for ($i = 00; $i < 24; $i++) {
                                                        $i = str_pad($i, 2, "0", STR_PAD_LEFT);
                                                        ?> 
                                                        <option @if(!empty($sms_hour) && $sms_hour==$i) selected="selected" @endif value="{{$i}}">{{$i}}</option>
<?php } ?>
                                                </select><div class="errors" id="errorshours"></div>

                                            </div>
                                            <div class="col-xs-6">
                                                <select class="selectpicker"  data-width="100%" name="sms_minutes" id="sms_minutes" maxOptions="5" data-size="5">
                                                    <option>Minutes</option>


<?php
for ($j = 00; $j < 60; $j++) {
    $j = str_pad($j, 2, "0", STR_PAD_LEFT);
    ?> 

                                                        <option @if(!empty($sms_minutes) && ($sms_minutes == $j)) selected="selected" @endif value="{{$j}}">{{$j}}</option>
<?php } ?>
                                                </select>
                                                <div class="errors" id="errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mr-b0">
                                    <div class="submit-ctn col-xs-12">
                                        <button id="confirm" name="confirm" value="confirm"  class="btn btn-danger btn-block" >CONFIRM</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif


        </div>
    </div>
</div> 
<script type="text/javascript">
    $(function () {
        var lat="1";
        var long="2";
        var dateToday = new Date();
        var selectDate = new Date();
        @if (!empty($sms_date))
                var selectDate = new Date('{{$sms_date}}');
        selectDate = (selectDate >= dateToday)?selectDate:dateToday;
                @endif
                
                //console.log(dateToday);
                var dateAfterYear = new Date(new Date().setYear(new Date().getFullYear() + 1));
        $('#datetimepicker').datetimepicker({
            inline: true,
            format: 'YYYY-MM-DD',
            minDate: dateToday,
            maxDate: dateAfterYear,
            defaultDate: selectDate
        });


    });

    function display_c() {
        var refresh = 1000; // Refresh rate in milli seconds
        mytime = setTimeout('display_ct()', refresh)
    }

    function display_ct() {
        var strcount
        var x = new Date();
        var x1 = x.toUTCString();// changing the display to UTC string
        //console.log(x.getTimezoneOffset()/60);
        document.getElementById('ct').innerHTML = x1.replace("GMT","UTC");
        tt = display_c();
    }
    //setInterval(display_ct(), 1000);
    display_c();

var tz = jstz.determine();
 var timezone = tz.name();
 console.log(timezone);
var tz_offset = new Date().getTimezoneOffset();
console.log(tz_offset);
tz_offset = -(tz_offset);
//var x = new Date();
//var x1 = x.toUTCString();
//console.log(x1);
            var hours = parseInt( tz_offset / 60 ) % 24;
            var minutes = parseInt( tz_offset  ) % 60;
            $("#sms_timezone").val(timezone);
            $("#sms_timezone_offset").val(tz_offset);
            $("#client_timezone").html("Your current timezone (GMT+"+hours+":"+minutes+") "+timezone);
//var tz_offset = new Date().getTimezoneOffset();
//  long = 2;
//  $.ajax({url: "{{url()}}/getUserTimezone/"+(-(tz_offset)), success: function(result){
            //var offset_hours = (result.rawOffset)/(60*60);
            
        //console.log(result);
//    }});
</script>
@stop