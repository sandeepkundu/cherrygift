<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Authentication Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */

    'addingvouchererror' => 'Error in adding voucher in cart.Please try again.',
    'showorderrerror' => 'Error in showing order.Please try again.',
    'sessionerrerror' => 'Session expired, please login again',
    'voucherupdated' => 'Voucher has been updated',
    'login' => 'Please login.',
    'passworderror' => 'Current password does not match our records.',
    'updatesucess' => 'Updated successfully',
    'profileDeleted' => 'Profile deleted',
    'uniqueEmail' => 'Email id is already registered with please Login',
    'requiredFierstName' => 'First name required.',
    'wrongCredentials' => 'Wrong credentials or profile deactivated, please try again',
    'loginContinue' => 'Please login to continue"',
    'passwordchanged' => 'Password have been changed',
    'profileExists' => 'This profile is already in our system',
    'subscriptionCanceled' => 'Your subscription has been cancelled.',
    'ordercomplete' => 'Order successful.',
    'ordercancel' => 'Order cancel',
    'linkExpired' => 'Link has been expired',
    'trylater' => 'There is some problem with paypal. please try again after some time.',
    'alreadyredeemed' => 'Gift voucher already redeemed!',
    'failed' => 'failed',
    'invalideMerchantPIN' => 'Invalid Merchant PIN',
    'voucherApproved' => 'VOUCHER APPROVED',
    'somethingwrong' =>'Something went wrong, please try again later',
    'somethingwrongintry' =>'Something went wrong in try exception, please try again later',
    'somethingwrongincatch' =>'Something went wrong in catch exception, please try again later',
    'somethingwrongindata' =>'Request data is empty, please try again later',
    'requestapproved' =>'Request Approved',
    'verificationlinkexpired' =>'Reset password link expired',
    'uniqueEmail' => 'Company Email already exists',
    'ValideEmail' => 'Company Email is not valid',
    'success' =>'cherrygift voucher updated successfully',
    'paymentAborted' =>'Payment Aborted',
    'voucherSent'=>'Vouchers sent successfully.',
    'failedPayment'=>'Your Payment has not been Successfull !! Please Try again'
];
