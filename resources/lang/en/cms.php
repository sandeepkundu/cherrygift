<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Authentication Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */
   'companydetails' =>'Company Details Updated Successfully', 
   'userdetails' =>'User Details Updated Successfully',
   'venderdetails' =>'Vendor Details Updated Successfully',
   'userdelete' =>'User Delete Successfully',
   'companydelete' =>'Supplier Delete Successfully',
   'companyactive' =>'Company Activated Successfully',
   'companydeactive' =>'Company Deactivated Successfully',
   'companyreject' =>'Company Rejected Successfully',
   'storedetails' =>'Store Details Updated Successfully', 
   'storeapprove' =>'Store Approved Successfully',
   'storeactive' =>'Store Activated Successfully',
   'storedeactive' =>'Store Deactivated Successfully',
   'storereject' =>'Store Rejected Successfully',
   'vouchercancel' =>'Voucher Cancelled Successfully And Send Sms On Your Phone Number',
   'redeemedcancel' =>'Voucher Redeemed Successfully',
   'subscriptionupdate' =>'Subscription Details Updated Successfully'
];
