<html>
    <body>
        <form name="frm_payment_method" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="business" value="fiona-facilitator@cherrygift.com" />
            <input type="hidden" name="notify_url" value="http://54.245.110.34/cherrygift/public/paypal_ipn/" />
            <input type="hidden" name="cancel_return" value="{{url()}}/payment/cancel/{{$companybusiness->id}}" />
            <input type="hidden" name="return" value="{{url()}}/payment/thanks/{{$companybusiness->id}}" />
            <input type="hidden" name="rm" value="2" />
            <input type="hidden" name="custom" value="{{$companybusiness->id}}" />
            <input type="hidden" name="lc" value="" />
            <input type="hidden" name="no_shipping" value="1" />
            <input type="hidden" name="no_note" value="1" />
            <input type="hidden" name="currency_code" value="AUD" />
            <input type="hidden" name="page_style" value="paypal" />
            <input type="hidden" name="charset" value="utf-8" />
            <input type="hidden" name="item_name" value="Supplier Subscription" />
            <input type="hidden" name="cmd" value="_xclick-subscriptions" />
            <input type="hidden" name="src" value="1" />
            <input type="hidden" name="srt" value="0" /> 
            <input type="hidden" name="a1" value="198" />
            <input type="hidden" name="p1" value="1" />
            <input type="hidden" name="t1" value="Y"/>
            <input type="hidden" name="a3" value="198" />
            <input type="hidden" name="p3" value="1" />
            <input type="hidden" name="t3" value="Y" />
            <!-- PayPal reattempts failed recurring payments. -->
            <input type="hidden" name="sra" value="1">
        </form>
    </body>
    <script>setTimeout("document.frm_payment_method.submit()", 0);</script>
</html>