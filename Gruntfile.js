module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: "\n"
            },
            publicDist: {
                src: [
                    'public/scripts/avatar.js',
                    'public/scripts/moment.min.js',
                    //                    'public/scripts/bootstrap-datetimepicker.min.js',
                    'public/scripts/jquery-migrate-1.2.1.min.js',
                    'public/scripts/jquery-ui.js',
                    'public/scripts/jstz.min.js',
                    '!public/scripts/cherry-gift*.js',
                ],
                dest: 'public/scripts/cherry-gift.js'
            },
            footerDist: {
                src: [
                    'public/scripts/bootstrap.min.js',
                    'public/scripts/bootstrap-datetimepicker.min.js',
                    'public/scripts/jquery.mCustomScrollbar.concat.min.js',
                    'public/scripts/jquery.scrollbar.min.js',
                    'public/scripts/bootstrap-select.min.js',
                    'public/scripts/script.js',
                    'public/scripts/load-image.all.min.js',
                    'public/scripts/jquery.mask.js',
                    '!public/scripts/cherry-gift*.js',
                ],
                dest: 'public/scripts/cherry-gift-footer.js'
            },
            publicCss: {
                src: [
                    'public/styles/font-awesome.min.css',
                    'public/styles/bootstrap.css',
                    'public/styles/jquery.mCustomScrollbar.css',
                    'public/styles/jquery.scrollbar.css',
                    'public/styles/bootstrap-datetimepicker.css',
                    'public/styles/bootstrap-select.min.css',
                    'public/styles/fonts.css',
                    'public/styles/common-layout.css',
                    'public/styles/style.css',
                    'public/styles/style-responsive.css',
                    'public/styles/jquery-ui.min.css',
                    'public/styles/owl.carousel.css',
                ],
                dest: 'public/styles/_cherry-gift.scss'
            },
        },
        uglify: {
            options: {
                banner: '/*! cherry-gift <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            publicDist: {
                files: [
                    {
                        src: ['<%= concat.publicDist.dest %>'],
                        dest: 'public/scripts/cherry-gift.min.js'
                    },
                    {
                        src: ['<%= concat.footerDist.dest %>'],
                        dest: 'public/scripts/cherry-gift-footer.min.js'
                    },
                    {
                        src: ['public/scripts/validation.js'],
                        dest: 'public/scripts/validation.min.js'
                    }
                    
                ],
                
            },
        },
        jshint: {
            files: [
                'Gruntfile.js',
                //'public/scripts/*.js',
            ],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            },
        },
        sass: {
            options: {
                outputStyle: 'compressed',
                sourceMap: true
            },
            publicDist: {
                files: [{
                    expand: true,
                    cwd: 'public/styles/',
                    src: ['*.sass'],
                    dest: 'public/styles/',
                    ext: '.min.css'
                }]
            },
        },
        watch: {
            publicScripts: {
                files: ['<%= concat.publicDist.src %>'],
                tasks: ['jshint', 'concat:publicDist', 'uglify:publicDist']
            },
            publicCss: {
                files: ['public/styles/*.css','public/styles/*.scss'],
                tasks: ['sass:publicDist']
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');

    grunt.registerTask('default', ['watch']);
};