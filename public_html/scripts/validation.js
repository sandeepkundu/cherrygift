$(document).ready(function () {
    var a = true;
    var pattern = /^([+]61)?[a-zA-Z0-9_.+ ]{8,20}$/;
    var pattern1 = /^[a-zA-Z0-9_.+ ]{8,20}$/;
    //var abnacn = /^(\d *?){11}$/; 
    var abnacn = /^(\d{9}|\d{11})$/;
    var accnumber = /^(\d *?){6,10}$/;
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var alphabets = /^[a-zA-Z\s]+$/;
    var re = /^www.[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    var numbersonly = /^[0-9]+$/;
    var valid_extensions = /(\.jpg|\.jpeg|\.gif|\.png)$/i;
    $('#business_login').addClass("active");
    $(document.body).on('click', '#sendresetpasslink', function () {
        if ($('input[name=useremail]').val().trim() == "")
        {
            $('.danger').text('Please fill in the email');
            $('#sendresetpasslink').prop('disabled', false);
            return false;
        }
        $('#sendresetpasslink').prop('disabled', true);
        $.ajax({
            url: APP_URL + '/password/email',
            type: "POST",
            //dataType: "json",
            data: {'email': $('input[name=useremail]').val(), '_token': $('input[name=_token]').val()},
            success: function (data) {
                $('.mr-b50 strong').text($('input[name=useremail]').val());
                $("#forgotApproveModal.modal-body").html('<h3>' + $('input[name=useremail]').val() + '</h3>');

                if (data == "passwords.sent")
                {
                    $('#forgotModal').modal('hide');
                    $('#forgotApproveModal').modal('show');
                } else {
                    $('.danger').text('This email is not registered with us');
                    $('#sendresetpasslink').prop('disabled', false);

                }

            },
            error: function (e) {
                $('.danger').text('Invalid Email');
                $('#sendresetpasslink').prop('disabled', false);
                return false;
            }
        });
    });

    $('#login').click(function ()
    {
        a = true;
        if ($('input[name=email]').val().trim() == "") {
            $('input[name=email]').css('border-color', 'red');
            $('#uerror').text('This field is required.');
            a = false;
        } else if (!regex.test($('input[name=email]').val())) {
            $('input[name=email]').css('border-color', 'red');
            $('#uerror').text('Invalid Email');
            a = false;
        } else
        {
            $('input[name=email]').css('border-color', '');
            $('#uerror').text('');
        }

        if ($('input[name=password]').val().trim() == "") {
            $('input[name=password]').css('border-color', 'red');
            $('#upass').text('This field is required.');
            a = false;
        } else

        if ($('input[name=password]').val().trim().length < 6) {
            $('input[name=password]').css('border-color', 'red');
            $('#upass').text('Password should be atleast 6 characters long.');
            a = false;
        } else
        {
            $('input[name=password]').css('border-color', '');
            $('#upass').text('');
        }
        if (a == false)
        {
            return false;
        }
    });
    $('#forgarepass').click(function ()
    {
        $('input[name=useremail]').val("");
        $('.danger').text('');
        $('#sendresetpasslink').prop('disabled', false);
        ;

    });

    $('#signup').click(function ()
    {
        a = true;
        if ($('input[name=first_name]').val().trim() == '') {
            $('input[name=first_name]').css('border-color', 'red');
            $('#fname_error').text('This field is required.');
            a = false;
        } else {
            $('input[name=first_name]').css('border-color', '');
            $('#fname_error').text('');
        }
        if ($('input[name=last_name]').val().trim() == '') {
            $('input[name=last_name]').css('border-color', 'red');
            $('#lname_error').text('This field is required.');
            a = false;
        } else {
            $('input[name=last_name]').css('border-color', '');
            $('#lname_error').text('');
        }

        if ($('input[name=mobile_number]').val().trim() == "") {
            $('input[name=mobile_number]').css('border-color', 'red');
            $('#mobile_error').text('This field is required.');
            a = false;
        } else 
            if ((!pattern.test($('input[name=mobile_number]').val())) || ($('input[name=mobile_number]').val().replace(/ +/g, '').replace(/[_+\s]/g, '').length < 8)) {
            $('input[name=mobile_number]').css('border-color', 'red');
            $('#mobile_error').text('This is not a valid mobile number.');
            a = false;
        } else {
            $('input[name=mobile_number]').css('border-color', '');
            $('#mobile_error').text('');
        }
        if ($('input[name=email]').val().trim() == "") {
            $('input[name=email]').css('border-color', 'red');
            $('#email_error').text('This field is required.');
            a = false;
        } else if (!regex.test($('input[name=email]').val())) {
            $('input[name=email]').css('border-color', 'red');
            $('#email_error').text('Enter a valid E-mail address.');
            a = false;
        } else
        {
            $('input[name=email]').css('border-color', '');
            $('#email_error').text('');
        }
        if (($('input[name=password]').val().trim().length == "")) {
            $('input[name=password]').css('border-color', 'red');
            $('#password_error').text('This field is required.');
            a = false;
        } else if (($('input[name=password]').val().trim().length <= 5)) {
            $('input[name=password]').css('border-color', 'red');
            $('#password_error').text('Password should not be less than 6 characters');
            a = false;
        } else
        {
            $('input[name=password]').css('border-color', '');
            $('#password_error').text('');
        }

        if (($('input[name=password_confirmation]').val().trim() == "")) {
            $('input[name=password_confirmation]').css('border-color', 'red');
            $('#confirmpassword_error').text('This field is required.');
            a = false;
        } else if (($('input[name=password_confirmation]').val().trim().length <= 5)) {
            $('input[name=password_confirmation]').css('border-color', 'red');
            $('#confirmpassword_error').text('Re-type password should not be less than 6 characters');
            a = false;
        } else
        {
            $('input[name=password_confirmation]').css('border-color', '');
            $('#confirmpassword_error').text('');
        }


        if ($('input[name=password]').val() != $('input[name=password_confirmation]').val() && ($('input[name=password]').val() != "") && ($('input[name=password_confirmation]').val() != "")) {
            $('input[name=password_confirmation]').css('border-color', 'red');
            $('#confirmpassword_error').text('Password and Re-type password does not match');
            a = false;
        } else {
            //$('input[name=password_confirmation').css('border-color', '');
            //$('#confirmpassword_error').text('');
        }
        if (!$('input[name=term_condition]').prop('checked')) {
            $('input[name=term_condition]').css('border-color', 'red');
            $('#termcondition_error').text('Please accept the Terms and Conditions.');
            a = false;
        } else {
            $('input[name=term_condition]').css('border-color', '');
            $('#termcondition_error').text('');
        }
        if (a == false)
            return false;
    });

    $('#user_email').blur(function ()
    {
        var user_email = this.value.trim();
        $.getJSON(APP_URL + "/getemail", {user_email: user_email}, function (data) {

            if (data == 1)
            {
                $('#email_error').text('Email already exists');
                $('#signup').prop('disabled', true);
            } else
            {
                $('#email_error').text('');
                $('#signup').prop('disabled', false);
            }
        });
    });

    $('#termconditionaccepted').click(function ()
    {
        $('#termsModal').modal('hide');
        $('input[name=term_condition]').prop('checked', true);
    });

    $("#fileupload").change(function () {
        //$("#dvPreview").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (regex.test($(this).val().toLowerCase())) {
            if ($.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                //$("#dvPreview").show();
                $("#dvPreview")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
            } else {
                if (typeof (FileReader) != "undefined") {
                    //$("#dvPreview").show();
                    //$("#dvPreview").append("<img />");
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#dvPreview").attr("src", e.target.result);
                        $("#upadate").prop("disabled", false);
                    }
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            }
        } else {
            //alert("Please upload a valid image file.");
        }
    });

    $('#upadate').click(function ()
    {
        a = true;
        if ($('input[name="first_name"]').val().trim() == '') {
            $('input[name=first_name]').css('border-color', 'red');
            $('#fname_error').text('This field is required.');
            a = false;
        } else {
            $('input[name=first_name]').css('border-color', '');
            $('#fname_error').text('');
        }
        if ($('input[name=last_name]').val().trim() == '') {
            $('input[name=last_name]').css('border-color', 'red');
            $('#lname_error').text('This field is required.');
            a = false;
        } else {
            $('input[name=last_name]').css('border-color', '');
            $('#lname_error').text('');
        }
        if ($('input[name=mobile_number]').val().trim() == "") {
            $('input[name=mobile_number]').css('border-color', 'red');
            $('#mobile_error').text('This field is required.');
            a = false;
        } else
        if ((!pattern.test($('input[name=mobile_number]').val())) || ($('input[name=mobile_number]').val().replace(/ +/g, '').replace(/[_+\s]/g, '').length < 8)) {
            $('input[name=mobile_number]').css('border-color', 'red');
            $('#mobile_error').text('This is not a valid mobile number.');
            a = false;
        } else {
            $('input[name=mobile_number]').css('border-color', '');
            $('#mobile_error').text('');
        }
        if ($('#facebook_id').val() === undefined)
        {
            if ($('input[name=new_password]').val() != "")
            {

                if (($('input[name=now_password]').val() == ($('input[name=new_password]').val().trim()))) {
                    $('input[name=new_password]').css('border-color', 'red');
                    $('#confirmpassword_error').text('Current and New password are same');
                    a = false;
                } else {
                    $('input[name=new_password]').css('border-color', '');
                    $('#confirmpassword_error').text('');
                }
            }

            if ($('input[name=now_password]').val() != "" && ($('input[name=now_password]').val().trim().length) < 6)
            {
                $('input[name=now_password]').css('border-color', 'red');
                $('#password_error').text('Password should be at least 6 digit');
                a = false;
            } else
            {
                $('input[name=now_password]').css('border-color', '');
                //$('input[name=new_password').css('border-color', '');
                $('#password_error').text('');
            }
            if ($('input[name=new_password]').val() != "" && ($('input[name=new_password]').val().trim().length) < 6)
            {
                $('input[name=new_password]').css('border-color', 'red');
                $('#confirmpassword_error').text('Password should be at least 6 digit');
                a = false;
            }
            if (($('input[name=now_password]').val().trim() != "") && ($('input[name=new_password]').val().trim() == ""))
            {
                $('input[name=new_password]').css('border-color', 'red');
                $('#confirmpassword_error').text('Enter new password');
                a = false;
            } else if (($('input[name=now_password]').val().trim() == "") && ($('input[name=new_password]').val().trim() != ""))
            {
                $('input[name=now_password]').css('border-color', 'red');
                $('#password_error').text('This field is required');
                a = false;
            }
        } else
        {
            if ($('input[name=new_password]').val() != "" && ($('input[name=new_password]').val().trim().length) < 6)
            {
                $('input[name=new_password]').css('border-color', 'red');
                $('#confirmpassword_error').text('Password should be at least 6 digit');
                a = false;
            } else {
                $('input[name=new_password]').css('border-color', '');
                $('#confirmpassword_error').text('');
            }
        }


        if (a == false)
        {
            return false;
        } else
        {
            return true;
        }
    });

    $('#emailconfirmed').click(function ()
    {
        $('#forgotApproveModal').modal('hide');
        $('#redeemedModal').modal('hide');
    });
    // Sprint 2  Start
    $('#state-list').on('change', function () {
        $("#loader").show();
        var selected = $(this).find("option:selected").val();
        //console.log(APP_URL);
        $.getJSON(APP_URL + "/getcity", {state_id: +selected}, function (data) {

            var city = $('#city-list');

            city.html('').selectpicker('refresh');

            var str = '';
            $.each(data, function (index, element) {
                str += "<option value='" + element.id + "' data-tokens='" + element.locality + "'>" + element.locality + "</option>";
            });
            city.html(str).selectpicker('refresh');
            $("#loader").hide();
            if ($('#search-toggle').is(":visible")) {
                $('#suburblink_id')[0].click();
            }
        });
//
    });



    $(document.body).on("click", ".search_filter_action", function () {

        if ($("#filter_tag_html").find(":contains(" + $(this).text() + ")").length) {

            return false;
        }

        var filter_type = $(this).attr("filter_type");
        var filter_id = $(this).attr("filter_id");
        var filter_name = $(this).attr("filter_name");

        create_filter_json(filter_type, filter_id, filter_name, true);
    });



    function create_filter_json(filter_type, filter_id, filter_name, search) {

        var prime_search_arr = {};
        prime_search_arr['search_txt'] = $("#search_txt").val();
        prime_search_arr['search_state'] = $("#state-list").val();
        prime_search_arr['search_city'] = $("#city-list").val();
        prime_search_arr['url_param_type'] = $("#url_param_type").val();
        prime_search_arr['url_param_id'] = $("#url_param_id").val();
        //console.log($("#state-list").val());
        if (filter_json == '') {
            var filter_arr = {};
            filter_arr['search'] = [];
            filter_arr['cat'] = [];
            filter_arr['state'] = [];
            filter_arr['city'] = [];
            filter_arr['rec'] = [];
            filter_arr['oca'] = [];
        } else {
            var filter_arr = JSON.parse(filter_json);
        }

        filter_arr['search'] = prime_search_arr;

        var indx_arr = {};

        indx_arr['name'] = filter_name;
        indx_arr['id'] = filter_id;
        indx_arr['type'] = filter_type;

        filter_arr[filter_type].push(indx_arr);

        if (filter_type == 'state')
        {
            var filter_state_arr = filter_arr['state'];
            var city_state_full_list = JSON.parse(city_state_json);
            var city_html_str = '';
            for (state_id in filter_state_arr)
            {
                var state_arr = filter_state_arr[state_id];
                var inner_state_arr = city_state_full_list[state_arr['id']];

                for (var city_id in inner_state_arr)
                {
                    var inner_city_arr = inner_state_arr[city_id];
                    city_html_str += '<li><a class="search_filter_action" filter_type="city" filter_id="' + inner_city_arr['id'] + '" filter_name="' + inner_city_arr['locality'] + '" href="javascript:void(0);">' + inner_city_arr['locality'] + '</a></li>';
                }
            }

            $('#filter_city_list').html('');
            $('#filter_city_list').html(city_html_str);

        }

        // console.log(filter_arr);
        filter_json = JSON.stringify(filter_arr);

        if (search)
            search_ajax();

        create_filter_html();

    }



    function create_filter_html()
    {
        var filter_arr = JSON.parse(filter_json);

        $("#clear_all_filter").hide();
        var filter_html_str = '';

        for (var type in filter_arr)
        {
            if (type != 'search') {
                var inner_filter_arr = filter_arr[type];
                for (var i in inner_filter_arr)
                {
                    filter_html_str += '<p class="tag" id=""><span class="tag-label">' + inner_filter_arr[i]['name'] + '</span><a href="javascript:void(0);" filter_type="' + inner_filter_arr[i]['type'] + '" filter_name="' + inner_filter_arr[i]['name'] + '" filter_id="' + inner_filter_arr[i]['id'] + '" class="tag-close filter-tag-remove">x</a></p>';
                }
            }
        }

        if (filter_html_str != '')
        {
            $("#clear_all_filter").show();
        }


        $("#filter_tag_html").html("");
        $("#filter_tag_html").append(filter_html_str);

    }

    function search_ajax()
    {
        $("#loader").show();
        var strjsonresponce = "<ui class='list-unstyled catalog-product-list'>";
        var filter_arr = JSON.parse(filter_json);
        console.log(filter_arr);
        $.ajax({// ajax call starts
            url: APP_URL + "/search_ajax", // JQuery loads serverside.php
            data: {"filter_json": filter_arr, _token: $('input[name=_token]').val()}, // Send value of the clicked button
            dataType: 'json', // Choosing a JSON datatype
            method: "POST"
        })
                .done(function (data) {
                    var ui = $('#display_data');
                    var div_data = "";
                    ui.empty("");
                    if ($(jQuery.parseJSON(JSON.stringify(data))).length == 0)
                    {

                        div_data = "<li class='col-sm-12 text-center'>Sorry, no vouchers found</li>";

                    }
                    $(jQuery.parseJSON(JSON.stringify(data))).each(function () {

                        div_data += "<li class=\"\col-sm-4\"\><div class=\"\product-item multi bck_white\"\>";
                        div_data += "<a href='" + APP_URL + "/voucher-details/" + this.id + "' class='dis-block auto-height' >";
                        if (this.image && this.image != '') {
                            div_data += "<img src=" + s3_url + s3_env + this.image + " class=\"\img-responsive image-proportion\"\ style=\"\width:300px;height:200px;\"\>";
                        } else {
                            div_data += "<img src='" + APP_URL + "/images/cherry gift.png'" + "class=\"\img-responsive image-proportion\"\ style=\"\width:300px;height:200px;\"\>";
                        }
                        div_data += "<div class=\"\item-upper-bg\"\><div class=\"\media\"\><div class=\"\media-left\"\>";
                        div_data += "<img class=\"\media-object small-circle\"\ src=" + s3_url + s3_env + this.logo + " style=\"\width: 35px; height: 35px;\"\>";
                        div_data += "</div><div class=\"\media-body media-middle\"\><h4 class=\"\media-heading\"\>" + this.store_location + "</h4></div></div></div>";
                        div_data += "<span class=\"\ full-background \"\><p>" + this.short_desc + "</p></span></a></div></li>";
                    })
                    $(div_data).html(ui);
                    $("#loader").hide();
                    $('#search-accordion').collapse('hide');
                })
                .fail(function () {
                    $("#loader").hide();

                })

    }


    if (typeof url_param_id !== "undefined" && url_param_id != "") {

        create_filter_json(url_param_type, url_param_id, url_param_name, false);
    }

    $(document.body).on("click", ".filter-tag-remove", function () {
        var filter_arr = JSON.parse(filter_json);
        //console.log(filter_arr);
        var filter_type = $(this).attr("filter_type");
        var filter_id = $(this).attr("filter_id");
        var filter_name = $(this).attr("filter_name");

        for (var i in filter_arr[filter_type])
        {
            var inner_filter_arr = filter_arr[filter_type][i];

            if (inner_filter_arr['type'] == filter_type && inner_filter_arr['id'] == filter_id && inner_filter_arr['name'] == filter_name)
            {
                filter_arr[filter_type].splice(i, 1);
            }

        }

        filter_json = JSON.stringify(filter_arr);
        create_filter_html();
        search_ajax();

    });

    $(document.body).on("click", ".tag-close", function () {
        $(this).closest('p').hide();

    });
//Reset password validation start
//$(document.body).on("click", "#sendpass", function () {
//
//    });
//Reset password validation end
    $("input").keyup(function () {
        $("#upadate,#contect-info-save,#save-bussiness-info").prop("disabled", false);
    });
    $("input,textarea").click(function () {
        $("#upadate,#contect-info-save,#save-bussiness-info").prop("disabled", false);
    });

    $('#admin_signup').click(function ()
    {
        a = true;
        if ($('#signupemail').val().trim() == "") {
            $('#signupemail').css('border-color', 'red');
            $('#signup_email_error').text('This field is required.');
            a = false;
        } else if (!regex.test($('#signupemail').val())) {
            $('#signupemail').css('border-color', 'red');
            $('#signup_email_error').text('Enter a valid E-mail address.');
            a = false;
        } else
        {
            $('#signup_email_error').text('');
            $('#signupemail').css('border-color', '');

        }
        if (($('#signup_password').val().trim().length == "")) {
            $('#signup_password').css('border-color', 'red');
            $('#signup_password_error').text('This field is required.');
            a = false;
        } else if (($('#signup_password').val().trim().length <= 5)) {
            $('#signup_password').css('border-color', 'red');
            $('#signup_password_error').text('Password should not be less than 6 characters');
            a = false;
        } else
        {
            $('#signup_password').css('border-color', '');
            $('#signup_password_error').text('');
        }
        if (!$('#term_condition').prop('checked')) {
            $('#term_condition').css('border-color', 'red');
            $('#termcondition_error').show();
            $('#termcondition_error').text('Please accept the Terms and Conditions.');
            a = false;
        } else {
            $('#term_condition').css('border-color', '');
            $('#termcondition_error').hide();
            $('#termcondition_error').text('');
        }
        if (a == false)
        {
            return false;
        }
    });

    $('#signupemail').blur(function () {

        var admin_email = $('#signupemail').val();
        $.getJSON(APP_URL + "/getadminemail", {user_email: admin_email}, function (data) {

            if (data == 1)
            {
                a = false;
                $('#signup_email_error').text('Email already exists');
                $('#signupemail').css('border-color', 'red');
                $('#admin_signup').prop('disabled', true);
            } else
            {
                $('#signup_email_error').text('');
                $('#signupemail').css('border-color', '');
                $('#admin_signup').prop('disabled', false);
            }
        });
    }
    );


    $('#admin_login').click(function ()
    {
        a = true;
        if ($('#email').val().trim() == "") {
            $('#email').css('border-color', 'red');
            $('#email_error').text('This field is required.');
            a = false;
        } else if (!regex.test($('#email').val())) {
            $('#email').css('border-color', 'red');
            $('#email_error').text('Enter a valid E-mail address.');
            a = false;
        } else
        {
            $('#signupemail').css('border-color', '');
            $('#email_error').text('');
        }
        if (($('#password').val().trim().length == "")) {
            $('#password').css('border-color', 'red');
            $('#password_error').text('This field is required.');
            a = false;
        } else if (($('#password').val().trim().length <= 5)) {
            $('#password').css('border-color', 'red');
            $('#password_error').text('Password should not be less than 6 characters');
            a = false;
        } else
        {
            $('#password').css('border-color', '');
            $('#password_error').text('');
        }

        if (a == false)
        {
            return false;
        }
    });
    $('#business_sign_up').click(function (e) {
        $('#signup_email_error').text('');
        $('#signup_password_error').text('');
        $('#termcondition_error').text('');
        $('#signup_password').css('border-color', '');
        $('#signupemail').css('border-color', '');
    }
    );
    if ($("#city").length) {
        $("#city").autocomplete({
            source: function (request, response) {
                if ($('#state1').val() == "")
                {
                    $('#suburbs_error').text('Please select a State first');
                    return false;
                } else
                {
                    $('#suburbs_error').text('');
                }
                $.getJSON(APP_URL + "/suggesionExample", {stateid: $('#state1').val(), term: request.term},
                        response).success(function (data) {
                    if (data.length == 0)
                    {
                        $('#suburbs_error').text('No record found');
                        $('#txtAllowSearch').val('');
                    } else {
                        $('#suburbs_error').text('');
                    }
                });


            },
            minLength: 2, select: function (event, ui) {
                $("#txtAllowSearch").val(ui.item.id); // display the selected text
                $("#loader").hide();
            }

        });
    }
    $('#contect-info-save').click(function (e) {
        var isValid = 1; var cnttxt=0;
        $('input:text').each(function () {
            if ($(this).val().trim() == '') {
                $(this).css('border', '1px solid red');
                isValid = 0;
                if (cnttxt == 0)
                {
                    //e.preventDefault();
                    this.focus();
                   
                    cnttxt = 1;
                    
                }
                //return false;
            } else {
                $(this).css('border', '');
            }
        });
        if ((!alphabets.test($('#fname').val().trim()) && ($('#fname').val().trim() != "")))
        {
            isValid = 0;
            $('#fname').css('border', '1px solid red');
            $('#fname_error').text('First Name have alphabets and space only');

        } else
        {
            $('#fname_error').text('');
        }
        if ((!alphabets.test($('#lname').val().trim()) && ($('#lname').val().trim() != "")))
        {
            isValid = 0;
            $('#lname').css('border', '1px solid red');
            $('#lname_error').text('Last Name have alphabets and space only');

        } else
        {
            $('#lname_error').text('');
        }
        if ((!regex.test($('#conemail').val().trim())) && ($('#conemail').val().trim() != ""))
        {
            isValid = 0;
            $('#conemail').css('border', '1px solid red');
            $('#conemail_error').text('Enter valid email');

        } else
        {
            $('#conemail_error').text('');
        }
        if (($('#connumber').val().trim() != ""))
        {
            if (((!pattern1.test($('#connumber').val()))) || ($('#connumber').val().replace(/ +/g, '').replace(/[_+\s]/g, '').length <= 7)) {
                isValid = 0;
                $('#connumber').css('border-color', 'red');
                $('#contact_error').text('This is not a valid contact number.');

            } else {
                $('#contact_error').text('');
            }
        }
        if ((!regex.test($('#comemail').val().trim())) && ($('#comemail').val().trim() != ""))
        {
            isValid = 0;
            $('#comemail').css('border', '1px solid red');
            $('#company_email_error').text('Enter valid email');

        } else
        {
            $('#comemail').css('border', '');
            $('#company_email_error').text('');
        }
        if ($('#comemail').val().trim() == "")
        {
            isValid = 0;
            $('#comemail').css('border', '1px solid red');


        } else
        {
            //$('#company_email_error').text('');
        }
        if ($('#abnacnnumber').val().trim() != "")
        {
            var abnstr = $('#abnacnnumber').val();
            abnstr = abnstr.replace(/ +/g, "");
            if ((!abnacn.test(abnstr))) {
                isValid = 0;
                $('#abnacnnumber').css('border-color', 'red');
                $('#abnacnnumber_error').text('This is not a valid ABC/ACN number.');

            } else {
                $('#abnacnnumber_error').text('');
            }
        }

        if ($('#state1').val() == "")
        {
            isValid = 0;
            $('.bootstrap-select').css('border', '1px solid red');
            //$('#state_error').text('Select state');
        } else
        {
            $('.bootstrap-select').css('border', '');
            $('#state_error').text('');
        }
        if ($('#txtAllowSearch').val() == "" && $('#city').val().trim() != "")
        {
            isValid = 0;
            $('#suburbs_error').text('Enter a valid suburbs');
        } else
        {
            $('#suburbs_error').text('');
        }
        if ($('#postcode').val().trim() != "")
        {

            var value = $('#postcode').val();
            var regexpatern = /^[0-9]{4}$/;
            if (!value.match(regexpatern))
            {
                isValid = 0;
                $('#postcode').css('border', '1px solid red');
                $('#postcode_error').text('Postcode is invalid');
            } else
            {
                $('#postcode_error').text('');
            }
        }

//        if ((!alphabets.test($('#accountname').val().trim()) && ($('#accountname').val().trim() != "")))
//        {
//            isValid = 0;
//            $('#accountname').css('border', '1px solid red');
//            $('#accountname_error').text('Account Name have alphabets and space only');
//
//        } else
//        {
//            $('#accountname_error').text('');
//        }

        if ($('#accountnumber').val().trim() != "")
        {
            var accountnumber = $('#accountnumber').val();
            var accpatern = /^[0-9]{6,10}$/;
            if (!accountnumber.match(accpatern))
            {
                isValid = 0;
                $('#accountnumber').css('border', '1px solid red');
                $('#accountnumber_error').text('Invalid Account Number');
            } else
            {
                $('#accountnumber_error').text('');
            }
        }

//        if ($('#accountnumber').val().trim() != "")
//        {
//            var accountnumber = $('#accountnumber').val();
//            var accpatern = /^[0-9 ]{10,25}$/;
//            if (!accountnumber.match(accpatern))
//            {
//                isValid = 0;
//                $('#accountnumber').css('border', '1px solid red');
//                $('#accountnumber_error').text('Invalid Account Numnber');
//            } else
//            {
//                $('#accountnumber_error').text('');
//            }
//        }
        if ($('#bsb').val().trim() != "")
        {
            var bsb = $('#bsb').val();
            var bsbpatern = /^[0-9 ]{6}$/;
            if (!bsb.match(bsbpatern))
            {
                isValid = 0;
                $('#bsb').css('border', '1px solid red');
                $('#bsb_error').text('Invalid BSB');
            } else
            {
                $('#bsb_error').text('');
            }
        }
        if ($('[name="category"]:checked').length === 0) {
            isValid = 0;
            $('#category_error').text("Select one Category");
        } else {
            $('#category_error').text("");
        }
        if (isValid == 0) {
            $('#cls_error').text('All field(s) with * are required.');
            return false;
        }
    });

    $('#save-bussiness-info,#save-bussiness-info-preview').click(function (e) {

        var isValid = 1;
        var cnttxt = 0;
        $('input[type="email"],textarea,.custom_number').each(function () {
            if ($(this).val().trim() == '') {
                isValid = 0;
                $(this).css({
                    "border": "1px solid red"
                });
                if (cnttxt == 0)
                {
                    this.focus();
                    cnttxt = 1;
                    e.preventDefault();
                }
            } else {

                $(this).css({
                    "border": "",
                    "background": ""

                });

            }

        });
        if ($("#img-logo-id").attr('src') == APP_URL + '/images/admin-images/demo-avatar.jpg')
        {
            isValid = 0;
            $('.upload-logo-ctn').css('border', '1px solid red');
            $("#business-logo").focus();
            e.preventDefault();

        } else
        {

            $('.upload-logo-ctn').css('border', '');
        }
        if ($('div.business-company-upload1:visible').length == 0) {
            isValid = 0;
            $('#file1').css('border', '1px solid red');
            if (cnttxt == 0) {
                $('#business-company-upload1').focus();
                cnttxt = 1;
                e.preventDefault();
            }
        } else
        {
            $('#file1').css('border', '');
        }

        if ($('#brief_description').val().length > 300) {
            isValid = 0;
            $('#brief_description').css('border', '1px solid red');
            $('#brief_description_error').text('Character length exceeded 300');
        } else {
            $('.brief_description').css('border', '');
            $('#brief_description_error').text('');
        }
        if ($('#long_desc').val().length > 2000) {
            isValid = 0;
            $('#long_desc').css('border', '1px solid red');
            $('#long_desc_error').text('Character length exceeded 2000');
        } else {
//      $('#long_desc').css('border', '');
//    $('#long_desc_error').text('');
        }
        if ($('#state1').val() == "")
        {
            isValid = 0;
            $('.bootstrap-select').css('border', '1px solid red');
            //$('#state_error').text('Select state');
        } else
        {
            $('.bootstrap-select').css('border', '');

        }
        if ($('#txtAllowSearch').val() == "" && $('#city').val().trim() != "")
        {
            isValid = 0;
            $('#suburbs_error').text('Enter a valid suburbs');
        } else
        {
            $('#suburbs_error').text('');
        }
        if (($('#email').val().trim() != ""))
        {
            if (!regex.test($('#email').val().trim()))
            {
                isValid = 0;
                $('#email').css('border', '1px solid red');
                $('#email_error').text('Enter valid email');

            } else
            {
                $('#email_error').text('');
            }
        }
        if ($('#website').val().trim() != "")
        {
            if (!re.test($("#website").val().toLowerCase())) {
                isValid = 0;
                $('#website_error').text('Invalid URL');
                $("#website").css('border', '1px solid red');

            } else {
                $('#website_error').text('');
            }
        }

        if (($('#phone_number').val().trim() != ""))
        {
            if (((!pattern1.test($('#phone_number').val()))) || ($('#phone_number').val().replace(/ +/g, '').replace(/[_+\s]/g, '').length < 8)) {
                isValid = 0;
                $('#phone_number').css('border-color', 'red');
                $('#contact_error').text('This is not a valid contact number.');

            } else {
                $('#contact_error').text('');
            }
        }
        if ($('#post_code').val().trim() != "")
        {

            var value = $('#post_code').val();
            var regexpatern = /^[0-9]{4}$/;
            if (!value.match(regexpatern))
            {
                isValid = 0;
                $('#post_code').css('border', '1px solid red');
                $('#postcode_error').text('Postcode is invalid');
            } else
            {
                $('#postcode_error').text('');
            }
        }
//        if($('#minimum_amount').val().trim() != "")
//        {
//            
//         if(!$('#minimum_amount').val().trim().match(numbersonly))
//        {
//                isValid = 0;
//                $('#minimum_amount').css('border', '1px solid red');
//                $('#minimum_amount_error').text('Pin code is invalid');
//            } else
//            {
//                $('#minimum_amount_error').text('');
//            }
//        }

        if (parseInt($('#minimum_amount').val()) > parseInt($('#maximum_amount').val()))
        {
            isValid = 0;
            $('#minimum_amount').css('border', '1px solid red');
            $('#minimum_amount_error').text('Minimum limit should be less than Maximum limit');
        } else
        {
            $('#minimum_amount_error').text('');
        }
//        if($('#incremental_amount').val().trim() != "")
//        {
//            
//        }
        if ($('[name="occation_name[]"]:checked').length == 0) {
            isValid = 0;
            $('#occation_error').text("Select atleast one Occation");
            if (cnttxt == 0) {
                var errorDiv = $('.ck-btn:visible').first();
                var scrollPos = errorDiv.offset().top;
                $(window).scrollTop(scrollPos);
                cnttxt = 1;
            }
        } else {
            $('#occation_error').text("");
        }
        if ($('[name="recpient_name[]"]:checked').length == 0) {
            isValid = 0;
            $('#receipient_error').text("Select atleast one Recipient");
            if (cnttxt == 0) {
                var errorDiv = $('.ck-btn:visible').first();
                var scrollPos = errorDiv.offset().top;
                $(window).scrollTop(scrollPos);
                cnttxt = 1;
            }
        } else {
            $('#receipient_error').text("");
        }

        if (isValid == 0) {

            $('#cls_error').text('All field(s) with * are required.');
            return false;
        } else
        {

        }
    });
    $("#forgotModal,#termsModal").on("shown.bs.modal", function () {
        backdrop: 'static';
    });
    //$('#forgotModal').modal({backdrop: 'static', keyboard: false}); 
    $('#forgat_password').click(function (e) {
        $(".danger").text('');
        $("#reset_pass_email").val('');
        $('#send_resetpass_link').prop('disabled', false);
    });
    $(document.body).on('click', '#send_resetpass_link', function () {
        if ($('input[name=useremail]').val().trim() == "")
        {
            $('.danger').text('Please fill in the email');
            $('#send_resetpass_link').prop('disabled', false);
            return false;
        }
        $('#send_resetpass_link').prop('disabled', true);
        $.ajax({
            url: 'suplier/sendresetmail',
            type: "POST",
            //dataType: "json",
            data: {'email': $('input[name=useremail]').val(), '_token': $('input[name=_token]').val()},
            success: function (email) {
                $('.mr-b50 strong').text($('input[name=useremail]').val());
                $("#forgotApproveModal.modal-body").html('<h3>' + $('input[name=useremail]').val() + '</h3>');

                if (email == "sucess")
                {
                    $('#send_resetpass_link').prop('disabled', false);
                    $('#forgotModal').modal('hide');
                    $('#forgotApproveModal').modal('show');
                } else {
                    $('.danger').text('This email is not registered with us');
                    $('#send_resetpass_link').prop('disabled', false);

                }

            },
            error: function (e) {
                $('.danger').text('Invalid Email');
                $('#send_resetpass_link').prop('disabled', false);
                return false;
            }
        });
    });
//    $('#send_resetpass').click(function (e)
//    {
//        var a = 1;
//        $('input:password').each(function () {
//            if ($(this).val().trim() == '') {
//                $(this).css('border', '1px solid red');
//                $('#cls_error').text('All field(s) with * are required.');
//                a = 0;
//                //return false;
//            } else {
//                $(this).css('border', '');
//                $('#cls_error').text('');
//            }
//        });
//        if ($('#password_confirmation').val().trim().length < 6 && $('#password_confirmation').val().trim().length != 0)
//        {
//            a = 0;
//            $('#password_confirm_error').text('Confirm password should be atleast 6 digit.');
//            $("#password_confirmation").css('border', '1px solid red');
//        } else
//        {
//            $('#password_confirm_error').text('');
//        }
//        if ($('#password').val().trim().length < 6 && $('#password').val().trim().length != 0)
//        {
//            a = 0;
//            $('#password_error').text('Password should be atleast 6 digit.');
//            $("#password").css('border', '1px solid red');
//        } else
//        {
//            $('#password_error').text('');
//        }
//        if ($('#password_confirmation').val().trim() != "" && $('#password').val().trim() != "")
//            if ($('#password').val().trim() != $('#password_confirmation').val().trim())
//            {
//                a = 0;
//                $('#password_confirm_error').text('New password and confirm password does not match');
//                $("#password_confirmation").css('border', '1px solid red');
//            }
//        if (a == 0) {
//
//            return false;
//        }
//    });
//    $('#connumber,#phone_number').keyup(function () {
//        var foo = $(this).val().split(" ").join(""); // remove hyphens
//        if (foo.length > 0) {
//            foo = foo.match(new RegExp('.{1,4}', 'g')).join(" ");
//        }
//        $(this).val(foo);
//    });
    $('#abnacnnumber').keyup(function () {
        var foo = $(this).val().split(" ").join(""); // remove hyphens
        var abntxt = foo;
        if (foo.length > 0) {
            foo = foo.match(new RegExp('.{1,3}', 'g')).join(" ");
            if (abntxt.length == 11) {
                foo = abntxt.substr(0, 2) + ' ' + abntxt.substr(2, 3) + ' ' + abntxt.substr(5, 3) + ' ' + abntxt.substr(8, 3);
            }
        }
        $(this).val(foo);
    });

    if ($("#register-form").length) {
        $("#register-form").validate({
            errorClass: "errors",
            // Specify the validation rules
            rules: {
                old_password: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                // Specify the validation error messages
                messages: {
                    old_password: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password_confirmation: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long",
                        equalTo: "Password and confirm password does not match"
                    },
                    errorPlacement: function () {
                        return false;
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }}}


        });

    }
//    $('#sucess-pay').on('click', function ()
//    {
//        location.href = APP_URL + '/admin/contact-info';
//    });
//    if ($(".owl-carousel").length) {
//
//        $('.owl-carousel').owlCarousel({
//            loop: true,
//            margin: 0,
//            nav: false,
//            dots: false,
//            autoplay: true,
//            responsiveClass: true,
//            responsive: {
//                0: {
//                    items: 1,
//                    nav: true
//                },
//                600: {
//                    items: 3,
//                    nav: false
//                },
//                1000: {
//                    items: 5,
//                    nav: true,
//                    loop: false
//                }
//            }
//        });
//    }
    $('#state1').on('change', function () {
        $('#city').val('');
    });
    $('#email,#business_login,#business_sign_up').on('click', function ()
    {
        $('.alert-danger').hide();
    });
    $('#sucess-pay,#companyprofile_submit').on('click', function ()
    {
        //location.href = APP_URL + '/admin/contact-info';
        $.ajax({
            url: APP_URL + '/suplier/company',
            type: "GET",
            //dataType: "json",
            success: function (data) {
                if (data == 0) {
                    location.href = APP_URL + '/admin/business-detail';
                } else
                {
                    location.href = APP_URL + '/admin/contact-info';
                }
            }
        });
    });
    $("#business-company-upload1,#business-company-upload2,#business-company-upload3,#business-company-upload4,#business-company-upload5,#business-company-upload6").change(function () {
        ext = $(this).val().toLowerCase().split('.')[1];
        var file_size = this.files[0].size;
        $("#temp_loader_" + element_name).show();
        var $ctn=$(this).closest('.upload-company-ctn');
        var $err=$ctn.find('.img-error');
        if (ext == 'png' || ext == 'jpg' || ext == 'jpeg')
        {
            $err.text('').hide();
        } else
        {

            isValid = 0;
            $err.text('Invalid image').show();
            $(this).val("");
            return false;
        }

        if (file_size > 3145728) {
            isValid = 0;
            $err.text('Image size more then 3MB not allowed. ').show();
            $(this).val("");
            return false;
        }
        var company_code = $("#store_pin").val();
        var myFormData = new FormData();
        var element_name = $(this).attr("name");
        myFormData.append(element_name, this.files[0]);
        myFormData.append('store_pin', company_code);
        var voucher_image = $(this).attr("number");
        myFormData.append('voucher_image', voucher_image);
        var img_ele_numbrer = $(this).attr("number");
        var loader_id = $(this).attr("loader-id");
        $("#hidden_" + element_name).val('');
        //if(voucher_image == )
        //admin/upload-business-images
        $.ajax({
            url: APP_URL + '/admin/upload-business-images',
            type: 'POST',
            processData: false, // important
            contentType: false, // important
            dataType: 'json',
            data: myFormData
        }).success(function (data) {

            if (data.status == 1) {

                $("#hidden_" + element_name).val(data.msg);
                $("#business-company-upload" + img_ele_numbrer).val('');
                $("#temp_loader_" + element_name).hide();

            } else {
                //console.log(data.msg);
                $("#business-company-upload" + img_ele_numbrer).val('');
                $err.html(data.msg).show();
                $("#temp_loader_" + element_name).hide();
                $ctn.find('.load-img,.close-img').hide();
                $ctn.find('.show_img').show();
                //return false
            }

        });

    });
    $("#business-logo").change(function () {
        ext = $(this).val().toLowerCase().split('.')[1];

        if (ext == 'png' || ext == 'jpg' || ext == 'jpeg')
        {
            $('#cls_error').text('');
        } else
        {
            //isValid = 0;

            $('#cls_error').text('Invalid image');

            $(this).val("");

            return false;
        }
    });
    $('#comp-img-1').click(function ()
    {
        $('#business-company-upload1').val("");
    });
    $('#comp-img-2').click(function ()
    {
        var voucher_img_id = 2;
        var company_id = $(this).attr("custom_val");
        var element_img_id = $(this).attr("img-tag");
        var temp_img_id = $("#hidden_" + element_img_id).val();
        $('#business-company-upload2').val("");
        deleteImage(company_id, voucher_img_id, temp_img_id);

    });
    $('#comp-img-3').click(function ()
    {
        var voucher_img_id = 3;
        var company_id = $(this).attr("custom_val");
        var element_img_id = $(this).attr("img-tag");
        var temp_img_id = $("#hidden_" + element_img_id).val();
        $('#business-company-upload3').val("");
        deleteImage(company_id, voucher_img_id, temp_img_id);
    });
    $('#comp-img-4').click(function ()
    {
        var voucher_img_id = 4;
        var company_id = $(this).attr("custom_val");
        $('#business-company-upload4').val("");
        var element_img_id = $(this).attr("img-tag");
        var temp_img_id = $("#hidden_" + element_img_id).val();
        deleteImage(company_id, voucher_img_id, temp_img_id);
    });
    $('#comp-img-5').click(function ()
    {
        var voucher_img_id = 5;
        var company_id = $(this).attr("custom_val");
        var element_img_id = $(this).attr("img-tag");
        var temp_img_id = $("#hidden_" + element_img_id).val();
        $('#business-company-upload5').val("");
        deleteImage(company_id, voucher_img_id, temp_img_id);
    });
    $('#comp-img-6').click(function ()
    {
        var voucher_img_id = 6;
        var company_id = $(this).attr("custom_val");
        var element_img_id = $(this).attr("img-tag");
        var temp_img_id = $("#hidden_" + element_img_id).val();
        $('#business-company-upload6').val("");
        deleteImage(company_id, voucher_img_id, temp_img_id);
    });

    if ($("#resetpassword_suplier").length) {
        $("#resetpassword_suplier").validate({
            errorClass: "errors",
            // Specify the validation rules
            rules: {
                password: {
                    required: true,
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                },
                // Specify the validation error messages
                messages: {
                    password: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password_confirmation: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long",
                        equalTo: "Password and confirm password does not match"
                    },
                    errorPlacement: function () {
                        return false;
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }}}
        });

    }
    // vouchar details

    $('.count-plus').click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name

        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        // If is not undefined
        if (!isNaN(currentVal) && currentVal < $('#max_value').val() && Number(currentVal + 10) < Number($('#max_value').val())) {
            // Increment

            $('input[name=' + fieldName + ']').val(currentVal + 10);
        } else {
            // Otherwise put max value
            $('input[name=' + fieldName + ']').val($('#max_value').val());
        }
    });

    // This button will decrement the value till 0
    if ($(".info-list li").length && $(".info-list li").length <= 3) {
        $("loadMore").empty();
    }
    $(".count-minus").click(function (e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > $('#min_value').val() && Number(currentVal - 10) > Number($('#min_value').val())) {
            // Decrement one
            $('input[name=' + fieldName + ']').val(currentVal - 10);
        } else {
            // Otherwise put a 0 there
            $('input[name=' + fieldName + ']').val($('#min_value').val());
        }
    });

    //Show more function script

    size_li = $(".info-list li").size();
    x = 2;
//    $('.info-list li:lt('+x+')').show();

    $('.info-list').each(function () {
        $(this).find('li:lt(' + x + ')').show();
    });

    $('.loadMore').click(function () {
        //x= (x+5 <= size_li) ? x+5 : size_li;
        x = size_li;
        $(this).parent().find('.info-list li:lt(' + x + ')').show();
        $(this).parent().find('.showLess').show();
        //if(x == size_li){
        $(this).hide();
        //}
    });
    $('#showLesslocation').click(function () {
        //x=(x-5<3) ? 3 : x-5;
        x = 2;
        $(this).parent().find('.info-list li').not(':lt(' + x + ')').hide();
        $(this).parent().find('.loadMore').show();
        $(this).show();
        //if(x == 3){
        $(this).hide();
        //}
    });

    $('#showLessday').click(function () {
        //x=(x-5<3) ? 3 : x-5;
        x = 2;
        $(this).parent().find('.info-list li').not(':lt(' + x + ')').hide();
        $(this).parent().find('.loadMore').show();
        $(this).show();
        //if(x == 3){
        $(this).hide();
        //}
    });


    //------------- buy voucher form----------//

    jQuery.validator.addMethod("valid_phone", function (value, element) {

        var phone_number = value.replace(/ +/g, '');
        var phone_number_length = phone_number.length;

        if (!pattern.test(value) || isNaN(phone_number) || phone_number_length < 8) {
            return false;
        }
        return true;
    }, jQuery.validator.format("Invalid phone number."));

    var buy_voucher_form_button_type = false;
    var last_hour_value;
    jQuery.validator.addMethod("valid_schedule_time", function (value, element) {
        if (buy_voucher_form_button_type)
        {
            console.log(value);
            if (value == "Hour" || value == "Minutes")
                return false;
            else
                return true;
//            if (element.name == "sms_hour")
//            {
//                last_hour_value = value;
//            }
//            var date = new Date();
//            var current_date = new Date();
//            current_date = current_date.setHours(0, 0, 0, 0);
//            //console.log(current_date);
//            var datepickerdate = $("#datetimepicker1").val();
//
//            var date_picker_obj = new Date(datepickerdate);
//            date_picker_obj = date_picker_obj.setHours(0, 0, 0, 0);
//            if (value == "" || isNaN(value))
//                return false;
//            else
//            {
//                if (element.name == "sms_hour" && value < date.getHours() && current_date == date_picker_obj)
//                    return false;
//                else if (element.name == "sms_minutes" && value < date.getMinutes() && last_hour_value == date.getHours() && current_date == date_picker_obj)
//                    return false;
//                else
//                    return true;
//            }
        } else
            return true;

    }, jQuery.validator.format("Invalid schedule time."));

    $("#buy_voucher_form").validate({
        errorClass: "errors",
        ignore: [],
        // Specify the validation rules
        rules: {
            phone_number: {
                required: true,
                valid_phone: true,
            },
            sms_txt: {
                required: true,
                maxlength: 160,
            },
            sms_hour: {
                valid_schedule_time: true
            },
            sms_minutes: {
                valid_schedule_time: true
            },
            // Specify the validation error messages
            messages: {
                phone_number: {
                    required: "Phone number is required.",
                    valid_phone: "Invalid Phone number."
                },
                sms_txt: {
                    required: "Sms text is required",
                    maxlength: "Maximium 120 characters are allowed."
                },
                sms_hour: {
                    required: "Schedule time required.",
                },
                sms_minutes: {
                    required: "Schedule time required.",
                },
                errorPlacement: function (error, element) {
                    element.parents('.col-xs-6').find('.bootstrap-select').after(error);
                },
                submitHandler: function (form) {
                    form.submit();
                }}}


    });
    $('#sms_hour').change(function () {
        var selected_date = $('#datetimepicker1').val();
        var curdate = moment().format('YYYY-MM-DD');
        if (($('#sms_hour').val() == "Hour") && (Date.parse(curdate) == Date.parse(selected_date)))
        {
            $('#sms_minutes').val("Minuts");
            $('#sms_minutes').selectpicker('refresh');
            $('#confirm').prop('disabled', true);
            $('#send_now').prop('disabled', false);
        } else
        {
            $('#confirm').prop('disabled', false);
            $('#send_now').prop('disabled', true);
        }
        var dt = new Date();
        var hour = dt.getUTCHours();
        if (Date.parse(curdate) == Date.parse(selected_date) && ($('#sms_hour').val() < hour))
        {
            $('#confirm').prop('disabled', true);
            $('#send_now').prop('disabled', true);
            $('#sms_minutes').prop('disabled', true);
            $('#errorshours').text('Invalid schedule time.');
        } else
        {
            $('#errorshours').text('');
            $('#sms_minutes').prop('disabled', false);
        }
    });
    $('#sms_minutes').change(function () {
        var dt = new Date();
        var hour = dt.getUTCHours();
        var minut = dt.getUTCMinutes();
        var selected_date = $('#datetimepicker1').val();
        var curdate = moment().format('YYYY-MM-DD');
        if (Date.parse(curdate) == Date.parse(selected_date) && (parseInt($('#sms_hour').val()) <= parseInt(hour)) && (parseInt($('#sms_minutes').val()) <= parseInt(minut)))
        {
            $('#confirm').prop('disabled', true);
            $('#send_now').prop('disabled', true);
            $('#errors').text('Invalid schedule time.');
        } else
        {
            $('#confirm').prop('disabled', false);
            $('#send_now').prop('disabled', true);
            $('#errors').text('');
        }
    });
    $("#confirm").click(function () {
        buy_voucher_form_button_type = true;
        $("#sms_schedule").val('1');
    });

    $("#send_now").click(function () {
        buy_voucher_form_button_type = false;
        $("#sms_schedule").val('0');
    });

    $('#add_to_cart').on('click', function () {


        if (Number($("#quantity").val()) != 0) {
            if (Number($("#quantity").val()) < Number($("#min_value").val()))
            {
                $('#error_max_amount').text('Invalid amount');
                return false;
            }
            if (Number($("#quantity").val()) > Number($("#max_value").val()))
            {
                $('#error_max_amount').text('Invalid amount');
                return false;

            } else
            {
                $('#error_max_amount').text();
                var edit_voucher = $("#edit_cart_voucher").val();
                if (edit_voucher == 0)
                    location.href = APP_URL + '/buy_voucher/' + $("#voucher_id").val() + '/' + $("#quantity").val();
                else
                    location.href = APP_URL + '/buy_voucher/' + $("#voucher_id").val() + '/' + $("#quantity").val() + '/' + $("#cart_voucher_id").val();

            }
        } else
        {
            $('#error_max_amount').text('Invalid amount');
            return false;
        }
        return false;
    });

    $("#quantity").on("keypress keyup blur", function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="tooltip"]').css('cursor', 'pointer');


    if ($('.sum_price').length)
    {
        var sum = 0;
        var item_number = 0;
        $('.sum_price').each(function () {
            sum += Number($(this).val());
            item_number++;
        });
        $('.voucher-price').text('$' + sum);
        var fee = parseFloat($('#fee').text());
        var total = Number(sum + (item_number * fee));
        var substr = total.toString().split('.');
        if (substr[1] == "00")
        {
            $('#total_price').text('$' + substr[0]);
        } else
        {
            $('#total_price').text('$' + total);
        }
        $('#paypal_amt').val(total);
    }
    if ($('.price').length)
    {
        var sum = 0;
        var item_number = 0;
        $('.price').each(function () {
            sum += Number($(this).text().replace(/\D/g, ''));
            item_number++;
        });
        $('.voucher-price').text('$' + sum);
        var fee = parseFloat($('#fee').text()).toFixed(2);
        var total = parseFloat(sum + (item_number * fee)).toFixed(2);
        var substr = total.toString().split('.');
        if (substr[1] == "00")
        {
            $('#total_price').text('$' + substr[0]);
        } else
        {
            $('#total_price').text('$' + total);
        }
        $('#paypal_amt').val(total);
    }
    $(document.body).on('click', '#cart_id_del', function () {
        var num = this.value;
        var cartdeleteconfirm = confirm('Are you sure, you want to remove the voucher from cart ?');
        if (cartdeleteconfirm)
        {
            $.ajax({
                url: APP_URL + '/delete_voucher/' + this.value,
                type: "GET",
                //dataType: "json",
                success: function (data) {
                    if (data == "sucess") {
                        $('#voucher-list' + num).remove();
                        var total_item = Number($('#badge_item').text());
                        $('.badge').text(Number(total_item - 1));
                        $('#quantity_val').val(Number(total_item - 1));
                        var sum = 0;
                        var item_number = 0;
                        if ($('.price').length != 0) {
                            $('.price').each(function () {
                                sum += Number($(this).text().replace(/\D/g, ''));
                                item_number++;
                            });
                            $('.voucher-price').text('$' + sum);
                            var fee = parseFloat($('#fee').text());
                            var texamount = fee * item_number;
                            $('.fee').text('$' + texamount);
                            var total = Number(sum + (item_number * fee));

                            var substr = total.toString().split('.');
                            if (substr[1] == "00")
                            {
                                $('#total_price').text('$' + substr[0]);
                            } else
                            {
                                $('#total_price').text('$' + total);
                            }
                            $('#paypal_amt').val(total);

                        } else
                        {

                            //$('#no_item_card').text('There is no item in cart');
                            $('.clearfix').remove();
                            $('.payPal-ctn').remove();
                            $('.badge').remove();
                            $('#payment').html("");
                            $('#payment').html("There is no item in cart");
                        }
                        //location.href = APP_URL + '/post_buy_voucher'; 
                    } else
                    {
                        //location.href = APP_URL + 'buy_voucher/{voucher_id}/{price}';  
                    }
                }
            });
        }
    });
//    $(document.body).on('click', '#paynow', function () {
//        var dateString = Number($('#curdate_time').val());
//        var dateValue = Number($('#curdate_date').val());
//        var givendate;
//        var isValid = 1;
//        $('.icon-calendar').each(function () {
//            givendate = Number($(this).attr("custom_date_value"));
//            if (dateString > givendate && (dateValue > givendate)) {
//                isValid = 0
//                $(this).siblings('span:first').css('border', '1px solid red');
//                $(this).siblings('span:first').text('Past date/time voucher can not be paid, please edit the voucher date/time');
//
//                isValid = 0;
//            }
//
//        });
//
//        if (isValid == 0) {
//            //scrollTop: ($('.error_cls').first().offset().top);
//            return false;
//        }
//        
//        
//    });

    $('#back-to-search').on('click', function () {
        location.href = APP_URL + '/search';
        return false;
    });

    $('body').on('click', function () {
        $('.alert-success').hide();
        $('.alert-danger').hide();
        //$('.fa-question-circle').hide();

    });
    $(document.body).on('click', '#check_merchant_pin', function () {
        if ($("#merchant_pin").val().trim().length == 8)
        {
            $('#check_merchant_pin').prop('disabled', true);
            $(".danger").html();

            $.ajax({
                url: APP_URL + '/validate/merchant_pin',
                type: "post",
                data: {'merchant_pin': $('input[name=merchant_pin]').val(), 'voucher_id': $('input[name=voucher_id]').val(), '_token': $('input[name=_token]').val()},
                success: function (data) {
                    if (data == "failed")
                    {
                        $(".danger").html("Gift voucher already redeemed");
                        $('#check_merchant_pin').prop('disabled', false);

                    } else if (data == "Invalid Merchant PIN")
                    {

                        $(".danger").html("Invalid Merchant PIN");
                        $('#check_merchant_pin').prop('disabled', false);
                    } else if (data == "VOUCHER APPROVED")
                    {
                        $('#redeemModal').modal('hide');
                        $('#redeemedModal').modal('show');

                    }
                }
            });
        } else
        {

            $(".danger").html("Please enter a valid Merchant PIN");
        }
    });
    $("#merchant_pin").on("keypress keyup blur", function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $(".danger").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
    $('#redeemModal').on('shown.bs.modal', function (e) {
        $(".danger").html('');
        $("#merchant_pin").val('');
        $('#check_merchant_pin').prop('disabled', false);

    });
    $('#redeemModal').on('hidden.bs.modal', function (e) {
        $(".danger").html('');
        $("#merchant_pin").val('');
        $('#check_merchant_pin').prop('disabled', false);
    });
    $(document.body).on('click', '#search_contact', function () {
        //$('#resend-vocher-id').remove();
        //$('.row').remove();
//$(this).val('');
    });
    if ($("#voucher_search").length) {
        $("#voucher_search").validate({
            errorClass: "errors",
            // Specify the validation rules
            rules: {
                search_contact: {
                    required: true,
                    minlength: 8
                },
                messages: {
                    search_contact: {
                        required: "This is required",
                        minlength: "mobile number must be at least 8 digits long"
                    },
                    errorPlacement: function () {
                        return false;
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }}}

        });

    }
    $(document.body).on('click', '#resend_voucher', function () {
        $.ajax({
            url: APP_URL + '/send_all',
            type: "get",
            data: {'search_contact': $('input[name=search_contact]').val()},
            success: function (data) {
                if (data.length > 0) {
                    $("#error_cls").text("");
                    //$(data).appendTo( $("#error_cls"));
                    var error_str = "";
                    $.each(data, function (index) {
                        error_str += data[index] + "<br/>";
                    });
                    $("#error_cls").append(error_str);
                } else {
                    $("#error_cls").text("");
                    $("#error_cls").text("message sent");
                }
            }
        });
        return false;
    });

    $(document).on('dp.change', '#datetimepicker', function () {
        var selected_date = $('#datetimepicker1').val();
        var curdate = moment().format('YYYY-MM-DD');
        if (Date.parse(curdate) == Date.parse(selected_date)) {
            $('#send_now').prop('disabled', false);
            $('#confirm').prop('disabled', true);
        } else
        {
            $('#send_now').prop('disabled', true);
            $('#confirm').prop('disabled', false);
        }
    });
    function deleteImage(company_id, voucher_img_id, temp_img_id)
    {
        $.ajax({
            url: APP_URL + '/deleteimagesbyid',
            type: "get",
            data: {company_id: +company_id, voucher_img_id: +voucher_img_id, temp_img_id: +temp_img_id},
            success: function (data) {
                if (data.length > 0) {

                } else {
                }
            }
        });
    }


    $('#comemail').blur(function ()
    {
        {
            var user_email = this.value.trim();
            if (user_email == "")
            {
                $('#company_email_error').text('Company email should not be empty');
                $('#comemail').css('border', '1px solid red');
                $('#contect-info-save').prop('disabled', true);
                return false;
            }
            var company_id = $('#company_id').val();
            $.getJSON(APP_URL + "/getesuppliermail", {user_email: user_email, company_id: company_id}, function (data) {

                if (data == 1)
                {
                    $('#company_email_error').text('Company email already exists');
                    $('#comemail').css('border', '1px solid red');
                    $('#contect-info-save').prop('disabled', true);
                } else
                {
                    $('#company_email_error').text('');
                    $('#comemail').css('border', '');
                    $('#contect-info-save').prop('disabled', false);
                }
            });
        }
    });
    $('#store_location').blur(function ()
    {
        {
            var store_name = this.value.trim();
            var company_id = $('#store_id').val();
            if (company_id === undefined)
            {
                company_id = 0;
            }
            $.getJSON(APP_URL + "/getstorename", {store_name: store_name, company_id: company_id}, function (data) {

                if (data == 1)
                {
                    $('#store_location_error').text('Store with same name is already registered with us, Please try some other name');
                    $('#store_location').css('border', '1px solid red');
                    $('#contect-info-save').prop('disabled', true);
                } else
                {
//                    $('#store_location_error').text('');
//                    $('#store_location').css('border', '');
                    $('#contect-info-save').prop('disabled', false);
                }
            });
        }
    });
    $('#tradname').blur(function ()
    {
        {
            var trade_name = this.value.trim();
            var company_id = $('#company_id').val();
            if (company_id === undefined)
            {
                company_id = 0;
            }
            if (trade_name != "")
            {
                $.getJSON(APP_URL + "/gettradename", {trade_name: trade_name, company_id: company_id}, function (data) {

                    if (data == 1)
                    {
                        $('#trading_name_error').text('Company by same name is already registered with us, please enter another name');
                        $('#tradname').css('border', '1px solid red');
                        $('#contect-info-save').prop('disabled', true);
                    } else
                    {
                        $('#trading_name_error').text('');
                        $('#tradname').css('border', '');
                        $('#contect-info-save').prop('disabled', false);
                    }
                });
            }
        }
    });

    $("#paypal_pay_now").click(function () {
        var dateString = Number($('#curdate_time').val());
        var givendate;
        var cur_date = Number($('#curdate_date').val());
        var sms_schedule;
        var isValid = 1;
        $('.icon-calendar').each(function () {
            givendate = Number($(this).attr("custom_date_value"));
            sms_schedule = Number($(this).attr("sms_schedule"));
            if (dateString > givendate && sms_schedule == 1) {
                isValid = 0;
                $(this).siblings('span:first').css('border', '0px solid red');
                $(this).siblings('span:first').text('Past date/time voucher can not be paid, please edit the voucher date/time');

                isValid = 0;
            }

            if (cur_date > givendate && sms_schedule == 0) {
                isValid = 0;
                $(this).siblings('span:first').css('border', '0px solid red');
                $(this).siblings('span:first').text('Past date/time voucher can not be paid, please edit the voucher date/time');

                isValid = 0;
            }
        });

        if (isValid == 0) {
            var errorDiv = $('.error_cls:visible').first();
            var scrollPos = errorDiv.offset().top;
            $(window).scrollTop(scrollPos);
        } else
        {
            var cart_id = $("#cart_id").val();
            $.ajax({
                url: APP_URL + '/prePaypal/' + cart_id,
                type: "get",
                data: {},
                success: function (data) {
                    console.log(data);
                    $("#custom").val(data);
                    document.frm_payment_method.submit();
                }
            });
        }
    });
    $('#brief_description').on('blur keyup paste change', function (e) {
        var _this = this, maxlength = 300;
        setTimeout(function () {
            if (_this.value.length > maxlength) {
                _this.value = _this.value.substr(0, maxlength);
            }
        }, 1);
    });
    $('#long_desc').on('blur keyup paste change', function (e) {
        var _this = this, maxlength = 2000;
        setTimeout(function () {
            if (_this.value.length >= maxlength) {
                _this.value = _this.value.substr(0, maxlength);
            }
        }, 1);
    });

    //$('#connumber').mask('AAA AAA AAA AAA AAA AAA');
    $('#connumber').keyup(function(){ 
        if($('#connumber').val().length >= 2) {
  var cty = $(this).val().charAt(0);
  var cty_1 = $(this).val().substring(0,2);
  var cty_2 = $(this).val().substring(0,5);
  if(cty=='4'){ 
    // us masking 
    $('#connumber').unmask().mask('AAA AAA AAA AAA AAA AAA'); 
  }
  else if(cty_1=='04')
  {
   $('#connumber').unmask().mask('AAA AAA AAA AAA AAA AAA');   
  }
  else if(cty_2=='1 300'  || cty_2=='1300')
  {
   $('#connumber').unmask().mask('0000 000 000');   
  }
  else { 
    // just numbers for international 
    $('#connumber').unmask().mask("A AAAA AAAA AAAA AAAA AAAA"); 
  }} 
});
//$('#mobile_number').keyup(function(){ 
//        if($('#mobile_number').val().length >= 2) {
//  var cty = $(this).val().charAt(0);
//  var cty_1 = $(this).val().substring(0,2);
//  var cty_2 = $(this).val().substring(0,5);
//  if(cty=='4'){ 
//    // us masking 
//    $('#mobile_number').unmask().mask('AAA AAA AAA AAA AAA AAA'); 
//  }
//  else if(cty_1=='04')
//  {
//   $('#mobile_number').unmask().mask('AAA AAA AAA AAA AAA AAA');   
//  }
//  else if(cty_2=='1 300' || cty_2=='1300')
//  {
//   $('#mobile_number').unmask().mask('0000 000 000');   
//  }
//  else { 
//    // just numbers for international 
//    $('#mobile_number').unmask().mask("A AAAA AAAA AAAA AAAA AAAA"); 
//  }} 
//});
    
    //$('#phone_number').mask('AAAA AAAA AAAA AAAA');
$('#phone_number').keyup(function(){ 
    if($('#phone_number').val().length >= 2) {
  var cty = $(this).val().charAt(0);
  var cty_1 = $(this).val().substring(0,2);
  var cty_2 = $(this).val().substring(0,5);
  if(cty=='4'){ 
    // us masking 
    $('#phone_number').unmask().mask('AAA AAA AAA AAA AAA AAA'); 
  }
  else if(cty_1=='04')
  {
   $('#phone_number').unmask().mask('AAA AAA AAA AAA AAA AAA');   
  }
  else if(cty_2=='1 300' || cty_2=='1300')
  {
   $('#phone_number').unmask().mask('0000 000 000');   
  }
        else { 
    // just numbers for international 
    $('#phone_number').unmask().mask("A AAAA AAAA AAAA AAAA AAAA AAAA"); 
  } }
});//if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
//    $('#city-list').selectpicker('mobile');
//}

    if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {

        $('#redeemModal').on('show.bs.modal', function () {

            // Position modal absolute and bump it down to the scrollPosition
            $(this)
                    .css({
                        position: 'absolute',
                        marginTop: $(window).scrollTop() + 'px',
                        bottom: 'auto'
                    });

            // Position backdrop absolute and make it span the entire page
            //
            // Also dirty, but we need to tap into the backdrop after Boostrap 
            // positions it but before transitions finish.
            //
            setTimeout(function () {
                $('.modal-backdrop').css({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: Math.max(
                            document.body.scrollHeight, document.documentElement.scrollHeight,
                            document.body.offsetHeight, document.documentElement.offsetHeight,
                            document.body.clientHeight, document.documentElement.clientHeight
                            ) + 'px'
                });
            }, 0);
        });
    }



    $(".store_ajax_img_upload").change(function () {

    });
    $(".toll-cross-btn").click(function () {
        $(".tool-tip-new").hide();
    }
    );
});