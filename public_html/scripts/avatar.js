jQuery.event.props.push('dataTransfer');

// IIFE to prevent globals
(function() {
  var s;
  function avatarObject(element){
	var ele = element
    this.settings = {
      fileInput: element
    },

    this.init = function() {
      s = this.settings;
      this.bindUIActions();
	  return ele
    },

    this.bindUIActions = function() {
	var list = this;
      var timer;
        
        $(document).delegate(s.fileInput, 'change', function(event) {
            event.preventDefault();
            list.handleDrop(event.target.files);
        });
    },

    this. showDroppableArea = function() {
      s.bod.addClass("droppable");
    },

    this.hideDroppableArea = function() {
      s.bod.removeClass("droppable");
    },

    this.handleDrop = function(files) {
	var list = this;
      // Multiple files can be dropped. Lets only deal with the "first" one.
      var file = files[0];
      if (file.type.match('image.*')) {

        list.resizeImage(file, function(data) {
          list.placeImage(file,data,ele);
        });

      } else {

        //alert("That file wasn't an image.");

      }

    },

    this.resizeImage = function(file, callback) {
      var list = this;
      var fileTracker = new FileReader;
      fileTracker.onload = function(event) {
        var data = event.target.result;
       var options = {
                    canvas: true
                };
		
		loadImage.parseMetaData(file, function(data) {
                var size = parseFloat(file.size / 1024).toFixed(2);
                if ((file.type.match('image/png') || file.type.match('image/jpeg') || file.type.match('image/jpg')) && size <= 10240) {
                    // Get the correct orientation setting from the EXIF Data
                    if (data.exif) {
                        options.orientation = data.exif.get('Orientation');
                    }
                    // Load the image from disk and inject it into the DOM with the correct orientation
                    loadImage(
                        file,
                        function(canvas) {
                            var imgDataURL = canvas.toDataURL("image/jpeg"); 
                             list.placeImage(file,imgDataURL,ele);
                        },
                        options
                    );
                } 
            });
      }
      fileTracker.readAsDataURL(file);

      fileTracker.onabort = function() {
       // alert("The upload was aborted.");
      }
      fileTracker.onerror = function() {
        //alert("An error occured while reading the file.");
      }

    },
	this.placeImage = function(file,data,ele) {
        $(ele).parent().parent().find("img").attr("src", data);
        
    }
}
var packageInfo = new avatarObject(".signup-upload");
  	packageInfo.placeImage = function(file,data,ele) {
		var imgCtn = '<img src="'+data+'" class="img-circle"  alt="Image for Profile" width="97" height="97" >';
		var divid = $(ele).parent().parent().attr("id");
		$('#'+divid).find("img").remove();
        $('#'+divid).prepend(imgCtn);
	}
	packageInfo.init();
})();