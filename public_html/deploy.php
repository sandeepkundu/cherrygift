<?php
date_default_timezone_set('Australia/Brisbane');

require('../Deploy.php');

use app\Deploy;

$ip         = $_SERVER['REMOTE_ADDR'];
$allowedIps = ['120.146.38.202', '110.142.69.162'];
$ranges     = [
    '131.103.20.160/27',
    '165.254.145.0/26',
    '104.192.143.0/24',
];

if (in_array($ip, $allowedIps) || Deploy::ipInRanges($ip, $ranges)) {
    $deploy = new Deploy(
        "/home/cherryg15",
        "/home/cherryg15/cherry-gift.git",
        ['log' => '/home/cherryg15/deployments.log']
    );
    $deploy->execute();
    exec("cp /home/cherryg15/.env.precedence /home/cherryg15/.env");
    exec("php artisan view:clear");
    header("HTTP/1.1 200 OK");
    exit();
}

header("HTTP/1.1 403 OK");
exit();
