$('#cms-loginbtn').click(function ()
{
    $('#cms-admin-login').submit();
});

function view_customer(id)
{
    $('.errors').each(function () {
             $(this).html('');
      });
    $('.form-control').each(function () {
             $(this).css({
                    "border": "",
                    "background": ""
                });
      });
    var dataString = {id: id};
    $.ajax({
        type: "POST",
        url: SITE_URL + "/super-admin/view-customer",
        data: dataString,
        dataType: "html",
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
        success: function (data)
        {

            obj = JSON.parse(data);
            $("#companyid").val(obj.id);
            $("#pin").val(obj.pin);
            $("#fname").val(obj.first_name);
            $("#lname").val(obj.last_name);
            $("#conemail").val(obj.contact_email);
            $("#connumber").val(obj.phone);
            $("#comemail").val(obj.email);
            $("#tradname").val(obj.company_name);
            $("#abnacnnumber").val(obj.acn_abn_number);
            $("#comaddress").val(obj.company_address);
            $("#state1").val(obj.state_id);
            $("#city").val(obj.suburbsname);
            $("#txtAllowSearch").val(obj.suburs_id);
            $("#postcode").val(obj.postal_code);
            $("#accountname").val(obj.account_name);
            $("#bsb").val(obj.bsb);
            $("#accountnumber").val(obj.account_number);
            $("#accmail").val(obj.accounts_email);
            $("#category").val(obj.category_id);
            /*if (obj.is_subscribed == '1') {
                $('#rejectBtn').hide();
                $('#approveBtn').hide();
                $('#deactivateBtn').show();
            } else {
                $('#rejectBtn').show();
                $('#approveBtn').show();
                $('#deactivateBtn').hide();
            }*/
        }

    });

    $('#view_customer_model').modal({
        backdrop: 'static',
        keyboard: true
    });

}
function view_user(id){


   
    var dataString = {id: id};
    $.ajax({
        type: "POST",
        url: SITE_URL + "/super-admin/view-user",
        data: dataString,
        dataType: "html",
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
        success: function (data)
        {
            //console.log(data);
            obj = JSON.parse(data);
            
           
            $("#fname").val(obj.first_name);
            $("#lname").val(obj.last_name);
            $("#email").val(obj.email);
            $("#connumber").val(obj.mobile_number);
            //$("#password").val(obj.password);
            $("#userid").val(obj.id);
           
            
            /*if (obj.is_subscribed == '1') {
                $('#rejectBtn').hide();
                $('#approveBtn').hide();
                $('#deactivateBtn').show();
            } else {
                $('#rejectBtn').show();
                $('#approveBtn').show();
                $('#deactivateBtn').hide();
            }*/
        }

    });





    $('#view_user_model').modal({
        backdrop: 'static',
        keyboard: true
    });

}


function view_vendor(id){

$('.errors').each(function () {
             $(this).html('');
      });
    $('.form-control').each(function () {
             $(this).css({
                    "border": "",
                    "background": ""
                });
      });
    var dataString = {id: id};
    $.ajax({
        type: "POST",
        url: SITE_URL + "/super-admin/view-vendor",
        data: dataString,
        dataType: "html",
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
        success: function (data)
        {
            //console.log(data);
            obj = JSON.parse(data);
            
           $("#vendorid").val(obj.id);
            $("#pin").val(obj.pin);
            $("#fname").val(obj.first_name);
            $("#lname").val(obj.last_name);
            $("#conemail").val(obj.contact_email);
            $("#connumber").val(obj.phone);
            $("#comemail").val(obj.email);
            $("#tradname").val(obj.company_name);
            $("#abnacnnumber").val(obj.acn_abn_number);
            $("#comaddress").val(obj.company_address);
            $("#state1").val(obj.state_id);
            $("#city").val(obj.suburbsname);
            $("#txtAllowSearch").val(obj.suburs_id);
            $("#postcode").val(obj.postal_code);
            $("#accountname").val(obj.account_name);
            $("#bsb").val(obj.bsb);
            $("#accountnumber").val(obj.account_number);
            $("#category").val(obj.category_id);
            
           
            
            /*if (obj.is_subscribed == '1') {
                $('#rejectBtn').hide();
                $('#approveBtn').hide();
                $('#deactivateBtn').show();
            } else {
                $('#rejectBtn').show();
                $('#approveBtn').show();
                $('#deactivateBtn').hide();
            }*/
        }

    });





    $('#view_vendor_model').modal({
        backdrop: 'static',
        keyboard: true
    });

}




function delete_company(id)
{ 
    //alert('Are you sure you want to delete ?');
    var conf;
   
     conf = confirm("Are you sure you want to delete ?");
     //alert(conf);
    if (conf!="") {
    var dataString = {id: id, conf};
    $.ajax({
        type: "POST",
        url: SITE_URL + "/super-admin/delete-company",
        data: dataString,
        dataType: "json",
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
        success: function (data)
        {

            console.log(data);
            //console.log(data);
            // obj = JSON.parse(data);
            // if (obj.status == 'success')
            // {
                 location.reload();
            // }
        }
    });
}
}

function delete_user(id)
{ 
    //alert('Are you sure you want to delete ?');
    var conf;
   
     conf = confirm("Are you sure you want to delete ?");
     alert(conf);
    if (conf==true) {
    var dataString = {id: id, conf};
    $.ajax({
        type: "POST",
        url: SITE_URL + "/super-admin/delete-user",
        data: dataString,
        dataType: "json",
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
        success: function (data)
        {

            console.log(data);
            //console.log(data);
            // obj = JSON.parse(data);
            // if (obj.status == 'success')
            // {
                 location.reload();
            // }
        }
    });
}
}

function delete_vendor(id)
{ 
    //alert('Are you sure you want to delete ?');
    var conf;
   
     conf = confirm("Are you sure you want to delete ?");
     //alert(conf);
    if (conf!="") {
    var dataString = {id: id, conf};
    $.ajax({
        type: "POST",
        url: SITE_URL + "/super-admin/delete-vendor",
        data: dataString,
        dataType: "html",
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
        success: function (data)
        {
            console.log(data);
            // obj = JSON.parse(data);
            // if (obj.status == 'success')
            // {
                 location.reload();
            // }
        }
    });
}
}





function activate_customer(id, value)
{ var conf;
    if(value==1)
    {
     conf = confirm("Are you sure you want to activate ?");
 }
 if(value==2)
    {
     conf = confirm("Are you sure you want to deactivate ?");
 }
    if (conf!="") {
    var dataString = {id: id, value: value};
    $.ajax({
        type: "POST",
        url: SITE_URL + "/super-admin/customer-action",
        data: dataString,
        dataType: "html",
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
        success: function (data)
        {
            obj = JSON.parse(data);
            if (obj.status == 'success')
            {
                location.reload();
            }
        }
    });
}}

function companyReject() {
    var cid = $("#companyid").val();
    $("#rejcompanyid").val(cid);
    $('#view_reject_model').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function activate_store(id, value)
{
    var con;
    if(value==1)
    {
     con = confirm("Are you sure you want to activate ?");
 }
 if(value==2)
    {
     con = confirm("Are you sure you want to deactivate ?");
 }
 if(value==4)
    {
     con = confirm("Are you sure you want to approve ?");
 }
    if (con == true) {
        $("#loader").show(); 
        var dataString = {id: id, value: value};
        $.ajax({
            type: "POST",
            url: SITE_URL + "/super-admin/store-action",
            data: dataString,
            dataType: "html",
            headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')},
            success: function (data)
            {
                obj = JSON.parse(data);
                if (obj.status == 'success')
                {
                    if($('#company_id').length==0){
                        location.reload();
                       
                    }else{
                      var compid=  $('#company_id').val();
                      location.href= SITE_URL + '/super-admin/store/'+compid;
                    }
                }
            }
        });
    }
}
function storeReject() {
    var cid = $("#companyid").val();
    $("#rejcompanyid").val(cid);
    $('#view_reject_model').modal({
        backdrop: 'static',
        keyboard: true
    });
}

$(document).ready(function () {
    var pattern = /^[a-zA-Z0-9_.+ ]{8,20}$/;
    //var abnacn = /^(\d *?){11}$/; 
    var abnacn = /^(\d{9}|\d{11})$/;
    var accnumber = /^(\d *?){6,10}$/;
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var alphabets = /^[a-zA-Z\s]+$/;
    var re = /^www.[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    var numbersonly = /^[0-9]+$/;
    var valid_extensions = /(\.jpg|\.jpeg|\.gif|\.png)$/i;
    var conf;
    $("input").keyup(function () {
        $("#upadate,#contect-info-save,#save-bussiness-info").prop("disabled", false);
    });
    $("input,textarea").click(function () {
        $("#upadate,#contect-info-save,#save-bussiness-info").prop("disabled", false);
    });

    $('#save-bussiness-info,#editUpdatebtn').click(function (e) {

        var isValid = 1;
        $('input[type="email"],.custom_number').each(function () {
            if ($(this).val().trim() == '') {
                $('#cls_error').text('All field(s) with * are required.');
                isValid = 0;
                $(this).css({
                    "border": "1px solid red"
                });

            } else {

                $(this).css({
                    "border": "",
                    "background": ""

                });

            }

        });

        if ($("#img-logo-id").attr('src') == APP_URL + '/images/admin-images/demo-avatar.jpg')
        {
            isValid = 0;
            $('.upload-logo-ctn').css('border', '1px solid red');
        } else
        {

            $('.upload-logo-ctn').css('border', '');
        }
        
        if ($('#state1').val() == "")
        {
            isValid = 0;
            $('.bootstrap-select').css('border', '1px solid red');
            //$('#state_error').text('Select state');
        } else
        {
            $('.bootstrap-select').css('border', '');

        }
        if ($('#txtAllowSearch').val() == "" && $('#city').val().trim() != "")
        {
            isValid = 0;
            $('#suburbs_error').text('Enter a valid suburbs');
        } else
        {
            $('#suburbs_error').text('');
        }
        if (($('#email').val().trim() != ""))
        {
            if (!regex.test($('#email').val().trim()))
            {
                isValid = 0;
                $('#email').css('border', '1px solid red');
                $('#email_error').text('Enter valid email');

            } else
            {
                $('#email_error').text('');
            }
        }
        if ($('#website').val().trim() != "")
        {
            if (!re.test($("#website").val().toLowerCase())) {
                isValid = 0;
                $('#website_error').text('Invalid URL');
                $("#website").css('border', '1px solid red');

            } else {
                $('#website_error').text('');
            }
        }

        if (($('#phone_number').val().trim() != ""))
        {
            if (((!pattern.test($('#phone_number').val()))) || ($('#phone_number').val().replace(/ +/g, '').replace(/[_+\s]/g, '').length < 8)) {
                isValid = 0;
                $('#phone_number').css('border-color', 'red');
                $('#contact_error').text('This is not a valid contact number.');

            } else {
                $('#contact_error').text('');
            }
        }
        if ($('#post_code').val().trim() != "")
        {

            var value = $('#post_code').val();
            var regexpatern = /^[0-9]{4}$/;
            if (!value.match(regexpatern))
            {
                isValid = 0;
                $('#post_code').css('border', '1px solid red');
                $('#postcode_error').text('Postcode is invalid');
            } else
            {
                $('#postcode_error').text('');
            }
        }
        if ($('div.business-company-upload1:visible').length == 0) {
                isValid = 0;
                $('#file1').css('border', '1px solid red');
            } else
            {
                $('#file1').css('border', '');
            }
//        if($('#minimum_amount').val().trim() != "")
//        {
//            
//         if(!$('#minimum_amount').val().trim().match(numbersonly))
//        {
//                isValid = 0;
//                $('#minimum_amount').css('border', '1px solid red');
//                $('#minimum_amount_error').text('Pin code is invalid');
//            } else
//            {
//                $('#minimum_amount_error').text('');
//            }
//        }

        if (parseInt($('#minimum_amount').val()) > parseInt($('#maximum_amount').val()))
        {
            isValid = 0;
            $('#minimum_amount').css('border', '1px solid red');
            $('#minimum_amount_error').text('Minimum limit should be less than Maximum limit');
        } else
        {
            $('#minimum_amount_error').text('');
        }
//        if($('#incremental_amount').val().trim() != "")
//        {
//            
//        }
        if ($('[name="occation_name[]"]:checked').length == 0) {
            isValid = 0;
            $('#occation_error').text("Select atleast one Occation");
        } else {
            $('#occation_error').text("");
        }
        if ($('[name="recpient_name[]"]:checked').length == 0) {
            isValid = 0;
            $('#receipient_error').text("Select atleast one Recipient");
        } else {
            $('#receipient_error').text("");
        }

        if (isValid == 0) {
           return false;
        } else
        {

        }
    });
    if ($("#city").length) {
        $("#city").autocomplete({
            source: function (request, response) {
                if ($('#state1').val() == "")
                {
                    $('#suburbs_error').text('Please select a State first');
                    return false;
                } else
                {
                    $('#suburbs_error').text('');
                }
                $.getJSON(APP_URL + "/suggesionExample", {stateid: $('#state1').val(), term: request.term},
                response).success(function (data) {
                    if (data.length == 0)
                    {
                        $('#suburbs_error').text('No record found');
                        $('#txtAllowSearch').val('');
                    } else {
                        $('#suburbs_error').text('');
                    }
                });


            },
            minLength: 2, select: function (event, ui) {
                $("#txtAllowSearch").val(ui.item.id); // display the selected text
            }

        });
    }
    $('#state1').on('change', function () {
        $('#city').val('');
    });
    $('#savebtn').click(function () {
        var isValid = 1;
        $('input:text').each(function () {
            if ($(this).val().trim() == '') {
                $(this).css('border', '1px solid red');
                isValid = 0;
                //return false;
            } else {
                $(this).css('border', '');
            }
        });


        if ((!alphabets.test($('#fname').val().trim()) && ($('#fname').val().trim() != "")))
        {
            isValid = 0;
            $('#fname').css('border', '1px solid red');
            $('#fname_error').text('First Name have alphabets and space only');

        } else
        {
            $('#fname_error').text('');
        }
        if ((!alphabets.test($('#lname').val().trim()) && ($('#lname').val().trim() != "")))
        {
            isValid = 0;
            $('#lname').css('border', '1px solid red');
            $('#lname_error').text('Last Name have alphabets and space only');

        } else
        {
            $('#lname_error').text('');
        }
        if ((!regex.test($('#conemail').val().trim())) && ($('#conemail').val().trim() != ""))
        {
            isValid = 0;
            $('#conemail').css('border', '1px solid red');
            $('#conemail_error').text('Enter valid email');

        } else
        {
            $('#conemail_error').text('');
        }
        if (($('#connumber').val().trim() != ""))
        {
            if (((!pattern.test($('#connumber').val()))) || ($('#connumber').val().replace(/ +/g, '').replace(/[_+\s]/g, '').length < 8)) {
                isValid = 0;
                $('#connumber').css('border-color', 'red');
                $('#contact_error').text('This is not a valid contact number.');

            } else {
                $('#contact_error').text('');
            }
        }
        if ($('#abnacnnumber').val().trim() != "")
        {
            var abnstr = $('#abnacnnumber').val();
            abnstr = abnstr.replace(/ +/g, "");
            if ((!abnacn.test(abnstr))) {
                isValid = 0;
                $('#abnacnnumber').css('border-color', 'red');
                $('#abnacnnumber_error').text('This is not a valid ABC/ACN number.');

            } else {
                $('#abnacnnumber_error').text('');
            }
        }

        if ($('#state1').val() == "")
        {
            isValid = 0;
            $('.bootstrap-select').css('border', '1px solid red');
            //$('#state_error').text('Select state');
        } else
        {
            $('.bootstrap-select').css('border', '');
            $('#state_error').text('');
        }
        if ($('#txtAllowSearch').val() == "" && $('#city').val().trim() != "")
        {
            isValid = 0;
            $('#suburbs_error').text('Enter a valid suburbs');
        } else
        {
            $('#suburbs_error').text('');
        }
        if ($('#postcode').val().trim() != "")
        {

            var value = $('#postcode').val();
            var regexpatern = /^[0-9]{4}$/;
            if (!value.match(regexpatern))
            {
                isValid = 0;
                $('#postcode').css('border', '1px solid red');
                $('#postcode_error').text('Postcode is invalid');
            } else
            {
                $('#postcode_error').text('');
            }
        }
        if ($('#accountnumber').val().trim() != "")
        {
            var accountnumber = $('#accountnumber').val();
            var accpatern = /^[0-9]{6,10}$/;
            if (!accountnumber.match(accpatern))
            {
                isValid = 0;
                $('#accountnumber').css('border', '1px solid red');
                $('#accountnumber_error').text('Invalid Account Number');
            } else
            {
                $('#accountnumber_error').text('');
            }
        }
        if ($('#bsb').val().trim() != "")
        {
            var bsb = $('#bsb').val();
            var bsbpatern = /^[0-9 ]{6}$/;
            if (!bsb.match(bsbpatern))
            {
                isValid = 0;
                $('#bsb').css('border', '1px solid red');
                $('#bsb_error').text('Invalid BSB');
            } else
            {
                $('#bsb_error').text('');
            }
        }
        if ($('#category').val() == "") {

            isValid = 0;
            $('#category_error').text("Select one Category");
        } else {
            $('#category_error').text("");
        }
        if (isValid == 0) {
            $('#cls_error').text('All field(s) with * are required.');
            return false;
        }
    });
    $('#send-reject').on('click', function () {

        if ($("#reason").val() == "") {
            $('.error_message').text('This field is required.');
            return false;
        }
        else
        {
            $('.error_message').text('');
        }
    });
    $('body').on('click', function () {
        $('.alert-success').hide();
        $('.alert-danger').hide();
        //$('.fa-question-circle').hide();

    });
    $('#abnacnnumber').keyup(function () {
        var foo = $(this).val().split(" ").join(""); // remove hyphens
        var abntxt = foo;
        if (foo.length > 0) {
            foo = foo.match(new RegExp('.{1,3}', 'g')).join(" ");
            if (abntxt.length == 11) {
                foo = abntxt.substr(0, 2) + ' ' + abntxt.substr(2, 3) + ' ' + abntxt.substr(5, 3) + ' ' + abntxt.substr(8, 3);
            }
        }
        $(this).val(foo);
    });
    $(document.body).on('click', '#sendresetpasslink', function () {
        if ($('input[name=useremail]').val().trim() == "")
        {
            $('.danger').text('Please fill in the email');
            $('#sendresetpasslink').prop('disabled', false);
            return false;
        }
        $('#sendresetpasslink').prop('disabled', true);
        $.ajax({
            url: APP_URL + '/password/email',
            type: "POST",
            //dataType: "json",
            data: {'email': $('input[name=useremail]').val(), '_token': $('input[name=_token]').val()},
            success: function (data) {
                $('.mr-b50 strong').text($('input[name=useremail]').val());
                $("#forgotApproveModal.modal-body").html('<h3>' + $('input[name=useremail]').val() + '</h3>');

                if (data == "passwords.sent")
                {
                    $('#forgotModal').modal('hide');
                    $('#forgotApproveModal').modal('show');
                } else {
                    $('.danger').text('This email is not registered with us');
                    $('#sendresetpasslink').prop('disabled', false);

                }

            },
            error: function (e) {
                $('.danger').text('Invalid Email');
                $('#sendresetpasslink').prop('disabled', false);
                return false;
            }
        });
    });
    $('#forgat_password').click(function (e) {
        $(".danger").text('');
        $("#reset_pass_email").val('');
        $('#sendresetpasslink').prop('disabled', false);
        $('#useremail').val('');
    });
    $('#emailconfirmed').click(function ()
    {
        $('#forgotApproveModal').modal('hide');
        $('#redeemedModal').modal('hide');
    });
    if ($("#cms-admin-login").length) {
        $("#cms-admin-login").validate({
            errorClass: "errors",
            // Specify the validation rules
            rules: {
                email: {
                    required: true,
                    email: true

                },
                password: {
                    required: true,
                    minlength: 6
                },
                // Specify the validation error messages
                messages: {
                    email: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long",
                        equalTo: "Password and confirm password does not match"
                    },
                    errorPlacement: function () {
                        return false;
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }}}


        });

    }
   
if ($("#reset_password_frm").length)
{
     
 $("#reset_password_frm").validate({
            errorClass: "errors",
            // Specify the validation rules
            rules: {
                password: {
                    required: true,
                    minlength: 6

                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: '#password'

                },
                // Specify the validation error messages
                messages: {
                    password: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password_confirmation: {
                        required: "This is required",
                        minlength: "Your password must be at least 6 characters long",
                        equalTo: "Password and confirm password does not match"
                    },
                    errorPlacement: function () {
                        return false;
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }}}


        });   
}
  $("#frm_subscription").validate({
            errorClass: "errors",
            // Specify the validation rules
            rules: {
                
                sub_price: {
                    required: true,
                    number: true
                },
                // Specify the validation error messages
                messages: {
                    sub_price: {
                        required: "This is required",
                        minlength: "Your subscription price must be at least 1 digit long"
                    },
                    errorPlacement: function () {
                        return false;
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }}}


        }); 
        $('#comp-img-1').click(function ()
    {
        $('#business-company-upload1').val("");
    });
    $('#comp-img-2').click(function ()
    {
        var voucher_img_id = 2;
        var company_id = $(this).attr("custom_val"); 
        $('#business-company-upload2').val("");
        deleteImage(company_id,voucher_img_id);
        
    });
    $('#comp-img-3').click(function ()
    {
        var voucher_img_id = 3;
        var company_id = $(this).attr("custom_val");
        $('#business-company-upload3').val("");
        deleteImage(company_id,voucher_img_id);
    });
    $('#comp-img-4').click(function ()
    {
        var voucher_img_id = 4;
        var company_id = $(this).attr("custom_val");
        $('#business-company-upload4').val("");
        deleteImage(company_id,voucher_img_id);
    });
    $('#comp-img-5').click(function ()
    {
        var voucher_img_id = 5;
        var company_id = $(this).attr("custom_val");
        $('#business-company-upload5').val("");
        deleteImage(company_id,voucher_img_id);
    });
    $('#comp-img-6').click(function ()
    {
        var voucher_img_id = 6;
        var company_id = $(this).attr("custom_val");
        $('#business-company-upload6').val("");
        deleteImage(company_id,voucher_img_id);
    });
        function deleteImage(company_id,voucher_img_id)
       {
        $.ajax({
            url: APP_URL + '/deleteimagesbyid',
            type: "get",
            data: {company_id: +company_id,voucher_img_id:+voucher_img_id},
            success: function (data) {
                if (data.length > 0) {
                    
                } else {
                    }
            }
        });   
       }
   $("#redeemed_submit").click(function(){
        window.location=APP_URL+"/super-admin/redeemed_history/"+$("#month").val()+"/"+$("#year").val();
    });
    $("#Download_Redeemed_List").click(function(){
        window.location=APP_URL+"/super-admin/getRedeemedDataInCSV/"+$("#month").val()+"/"+$("#year").val();
    });
    $("#redeemed_history_submit").click(function(){
        window.location=APP_URL+"/super-admin/voucher_id/"+$("#company_id").val()+"/"+$("#month").val()+"/"+$("#year").val();
    });
   $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            //if( log ) //alert(log);
        }
        
    });
});
$('#comemail').blur(function ()
    {
        {
            var user_email = this.value.trim();
            if(user_email=="")
            {
            $('#company_email_error').text('Company email should not be empty');
                    $('#comemail').css('border', '1px solid red');
                    $('#savebtn').prop('disabled', true); 
                    return false;
            }
            var company_id = $('#companyid').val();
            $.getJSON(APP_URL + "/getesuppliermail", {user_email: user_email,company_id: company_id}, function (data) {

                if (data == 1)
                {
                    $('#company_email_error').css("display","block");
                    $('#company_email_error').text('Company email already exists');
                    $('#comemail').css('border', '1px solid red');
                    $('#savebtn').prop('disabled', true);
                    $('#company_email_error').delay(5000).fadeOut('slow');
                } else
                {
                    $('#company_email_error').text('');
                    $('#comemail').css('border', '');
                    $('#savebtn').prop('disabled', false);
                }
            });
        }
    });
    $('#tradname').blur(function ()
    {
        {
            var trade_name = this.value.trim();
            var company_id = $('#companyid').val();
            if(company_id===undefined)
            {
             company_id =0;   
            }
            if(trade_name!="")
            {
            $.getJSON(APP_URL + "/gettradename", {trade_name: trade_name,company_id:company_id}, function (data) {

                if (data == 1)
                {
                    $('#trading_name_error').css("display","block");
                    $('#trading_name_error').text('Company by same name is already registered with us, please enter another name');
                    $('#tradname').css('border', '1px solid red');
                    $('#contect-info-save').prop('disabled', true);
                    $('#trading_name_error').delay(5000).fadeOut('slow');
                } else
                {
                    $('#trading_name_error').text('');
                    $('#tradname').css('border', '');
                    $('#contect-info-save').prop('disabled', false);
                }
            });
        }}
    });
    $('#store_location').blur(function ()
    {
        {
            var store_name = this.value.trim();
            var company_id = $('#store_id').val();
            if(company_id===undefined)
            {
             company_id =0;   
            }
            $.getJSON(APP_URL + "/getstorename", {store_name: store_name, company_id:company_id}, function (data) {

                if (data == 1)
                {
                    $('#store_location_error').css("display","block");
                    $('#store_location_error').text('You already have a store by this name, Please try some other name');
                    $('#store_location').css('border', '1px solid red');
                    $('#contect-info-save').prop('disabled', true);
                    $('#store_location_error').delay(5000).fadeOut('slow');
                } else
                {
                    $('#store_location_error').text('');
                    $('#store_location').css('border', '');
                    $('#contect-info-save').prop('disabled', false);
                }
            });
        }
    });
}); 

function customerAction(value){
    var vendorid=$("#companyid").val();
    activate_customer(vendorid,value);
}

function cancel_voucher(id,value)
{
 var conf = confirm("Are you sure you want to cancel the voucher?");
 if(conf) {
     //$("#loader").show(); 
    var dataString = {id: id,value:value};
     $.ajax({
            type: "POST",
            url: SITE_URL+"/super-admin/voucher-action",
            data: dataString,
            dataType: "html",
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
            success: function (data)
            {
                obj = JSON.parse(data);
                if (obj.status == 'success')
                {
                    location.reload();
                }else if(obj.status == 'error'){
                        location.reload();
                }
            }
       });
    
}

}


function redeemed_voucher(id,value)
{
     

 
    // $("#loader").show(); 
    var dataString = {id: id,value: value};
     $.ajax({
            type: "POST",
            url: SITE_URL+"/super-admin/redeemed-action",
            data: dataString,
            dataType: "html",
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
            success: function (data)
            {
                 obj = JSON.parse(data);
            
                 $("#merchant_pin").val('');
                 $("#voucherid").val(obj.voucher_unique_id);  
                    //console.log(data['merchant_pin']);
                   // location.reload();
               
            }
       });
    

 $('#redeemed_voucher_model').modal({
                       // $('#merchant_pin').attr('value');
                        backdrop: 'static',
                        keyboard: true
                    });
 

}

 

         var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $("#merchant_pin").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                $("#errMsges").html( ret ? "" :"Only numeric allow" );
                return ret;
            });
            $("#merchant_pin").bind("paste", function (e) {
                return false;
            });
            $("#merchant_pin").bind("drop", function (e) {
                return false;
            });
        });


$('#submit-redeemvoucher').click(function ()
{
    if($("#merchant_pin").val()=="")
    {
        $('#errMsges').html('Please enter marchant pin');
        return false;

    }
    var marchant_val=$("#merchant_pin").val().length
    
    if(marchant_val<8)
    {
        $('#errMsges').html('Marchant pin min 8 digits ');
        return false;

    }
    var merchant_pin=$("#merchant_pin").val();
   
    var voucher_id=$("#voucherid").val();
   //alert(marchant_pin);

    var dataString = {merchant_pin: merchant_pin,voucher_id: voucher_id};
     $.ajax({
            type: "POST",
            url: SITE_URL+"/validate/merchant_pin",
            data: dataString,
            dataType: "html",
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
            success: function (data)
            {
                 
                // console.log(data);
                // obj = JSON.parse(data);
                if (data == 'VOUCHER APPROVED')
                {
                   $('#redeemed_voucher_model').modal('hide');
                 $('#approved_voucher_model').modal('show'); 
                }
                else{
                    
                    $('#errMsges').html(data);
                    
                }
                 //$("#merchant_pin").val(obj.merchant_pin);
                   
                    //console.log(data['merchant_pin']);
                    //location.reload();
               
            }
       });
    

    //$('#cms-admin-login').submit();
});

$('#redeemconfirmed').click(function ()
    {
        $('#forgotApproveModal').modal('hide');
        $('#approved_voucher_model').modal('hide');
        location.reload();
    });

