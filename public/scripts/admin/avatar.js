jQuery.event.props.push('dataTransfer');

// IIFE to prevent globals
(function() {
  var s;
  function avatarObject(element){
	var ele = element
    this.settings = {
      fileInput: element
    },

    this.init = function() {
      s = this.settings;
      this.bindUIActions();
	  return ele
    },

    this.bindUIActions = function() {
	var list = this;
      var timer;
        
        $(document).delegate(s.fileInput, 'change', function(event) {
            event.preventDefault();
            list.handleDrop(event.target.files);
        });
    },

    this. showDroppableArea = function() {
      s.bod.addClass("droppable");
    },

    this.hideDroppableArea = function() {
      s.bod.removeClass("droppable");
    },

    this.handleDrop = function(files) {
	var list = this;
      // Multiple files can be dropped. Lets only deal with the "first" one.
      var file = files[0];
      if (file.type.match('image.*')) {

        list.resizeImage(file, function(data) {
          list.placeImage(file,data,ele);
        });

      } else {

        //alert("That file wasn't an image.");

      }

    },

    this.resizeImage = function(file, callback) {
      var list = this;
      var fileTracker = new FileReader;
      fileTracker.onload = function(event) {
        var data = event.target.result;
       var options = {
                    canvas: true
                };
		
		loadImage.parseMetaData(file, function(data) {
                var size = parseFloat(file.size / 1024).toFixed(2);
                if ((file.type.match('image/png') || file.type.match('image/jpeg') || file.type.match('image/jpg')) && size <= 10240) {
                    // Get the correct orientation setting from the EXIF Data
                    if (data.exif) {
                        options.orientation = data.exif.get('Orientation');
                    }
                    // Load the image from disk and inject it into the DOM with the correct orientation
                    loadImage(
                        file,
                        function(canvas) {
                            var imgDataURL = canvas.toDataURL("image/jpeg"); 
                             list.placeImage(file,imgDataURL,ele);
                        },
                        options
                    );
                } 
            });
      }
      fileTracker.readAsDataURL(file);

      fileTracker.onabort = function() {
       // alert("The upload was aborted.");
      }
      fileTracker.onerror = function() {
        //alert("An error occured while reading the file.");
      }

    },
	this.placeImage = function(file,data,ele) {
		var newValue = ele.replace('#', '.');
                var loader_id = $(ele).attr("loader-id");
		var imgCtn = '<div class="company-img"><div id="'+loader_id+'" class="img-loader-ctn"><i class="fa fa-spinner img-spinner fa-2x fa-spain"></i></div><img src="'+data+'" class="img-rounded" alt=""></div>';
       	var divid = $(ele).parents(".upload-company-ctn").find(newValue);
		divid.empty();
        divid.prepend(imgCtn);
    }
}
var packageInfo = new avatarObject(".business-logo-upload");
  	packageInfo.placeImage = function(file,data,ele) {
		var imgCtn = '<img src="'+data+'" class="img-rounded"  width="90" height="90" >';
		var divid = $(ele).parent().parent();
		divid.find("img").remove();
        divid.prepend(imgCtn);
	}
	packageInfo.init();
	
var product1 = new avatarObject("#business-company-upload1");
var product2 = new avatarObject("#business-company-upload2");
var product3 = new avatarObject("#business-company-upload3");
var product4 = new avatarObject("#business-company-upload4");
var product5 = new avatarObject("#business-company-upload5");
var product6 = new avatarObject("#business-company-upload6");
	product1.init();
	product2.init();
	product3.init();
	product4.init();
	product5.init();
	product6.init(); 
	
})();